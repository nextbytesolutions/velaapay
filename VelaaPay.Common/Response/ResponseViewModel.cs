using System.Collections.Generic;

namespace VelaaPay.Common.Response
{
    public class ResponseViewModel
    {
        public Dictionary<string,object> Response { get; set; }
        public int Status { get; set; }

        public ResponseViewModel()
        {
            Status = 200;
        }
        
        public ResponseViewModel CreateOk(object data)
        {
            Status = 200;
            Response = new Dictionary<string, object>()
            {
                { "data",data }
            };
            return this;
        }
        public ResponseViewModel CreateOk(object data, int count)
        {
            Status = 200;
            Response = new Dictionary<string, object>()
            {
                { "data",data },
                { "count",count },
            };
            return this;
        }
        public ResponseViewModel CreateOkCustom(List<KeyValuePair<string,object>> values)
        {
            Status = 200;
            Response = new Dictionary<string, object>();
            foreach (var data in values)
            {
                Response.Add(data.Key,data.Value);
            }
            return this;
        }
        public ResponseViewModel CreateCustom(int status, params object[] values)
        {
            Status = status;
            Response = new Dictionary<string, object>();
            foreach (var data in values)
            {
                Response.Add(nameof(data),data);
            }
            return this;
        }
    }
}