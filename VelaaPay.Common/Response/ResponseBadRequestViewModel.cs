using System.Collections.Generic;

namespace VelaaPay.Common.Response
{
    public class ResponseBadRequestViewModel
    {
        public string Message { get; set; }
        public List<string> Errors { get; set; }
    }

    public class UnAuthorizedBadRequestViewModel
    {
        public string Message { get; set; }
        public bool IsExpired { get; set; }
    }
}