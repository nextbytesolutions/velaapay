using System;
using Newtonsoft.Json;
using VelaaPay.Common.Constants;

namespace VelaaPay.Common.Util
{
    public class SettingValueConverter
    {
        public static object GetValue(SettingDataType dataType, string value)
        {
            switch (dataType)
            {
                case SettingDataType.Integer:
                    return int.Parse(value);
                case SettingDataType.Float:
                    return float.Parse(value);
                case SettingDataType.Double:
                    return double.Parse(value);
                case SettingDataType.String:
                    return (value);
                case SettingDataType.Json:
                    return JsonConvert.DeserializeObject(value);
                default:
                    throw new ArgumentOutOfRangeException(nameof(dataType), dataType, null);
            }
        }
    }
}