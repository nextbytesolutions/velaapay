namespace VelaaPay.Common.Enums
{
    public enum CompanyType
    {
        BillPayment = 1,
        AirTopUp,
    }
}