namespace VelaaPay.Common.Enums
{
    public enum CashInRequestType
    {
        Bank = 1,
        Orange = 0,
        Wari = 2,
        Stripe = 3,
    }
}