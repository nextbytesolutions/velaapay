namespace VelaaPay.Common.Enums
{
    public enum LoadType
    {
        /// <summary>
        /// Send Money
        /// </summary>
        SendMoney = 1,
        /// <summary>
        /// By Debit Card
        /// </summary>
        DebitCard = 0,
        /// <summary>
        /// Requested Money
        /// </summary>
        Sender = 2,
        /// <summary>
        /// Vendor Payment
        /// </summary>
        Vendor = 3,
        /// <summary>
        /// Bill Payment
        /// </summary>
        Bill = 4,
        /// <summary>
        /// Withdraw Request
        /// </summary>
        Withdraw = 5,
        /// <summary>
        /// Bill Rejected By Admin  
        /// </summary>
        BillRejected = 6,
        /// <summary>
        /// Withdraw Rejected By Admin  
        /// </summary>
        WithdrawRequestRejected = 7,
        /// <summary>
        /// Air Time Top Up Rejected By Admin  
        /// </summary>
        AirTimeTopUpRequestRejected = 8,
        /// <summary>
        /// Cash In Requests (Merchant Top Up)
        /// </summary>
        CashInRequest = 9,
        /// <summary>
        /// Cash In By Customer
        /// </summary>
        CashIn = 10,
        /// <summary>
        /// Cash out done by Vendor or Cashier for the Customer
        /// </summary>
        CashOut = 11,
        /// <summary>
        /// Air Time Top Up  
        /// </summary>
        AirTimeTopUpRequest = 12,
        /// <summary>
        /// Transfer Money in Employee  
        /// </summary>
        TransferMoney = 13,
        /// <summary>
        /// Scan To Pay via QrCode
        /// </summary>
        ScanToPay = 14,
        /// <summary>
        /// Purchase Item
        /// </summary>
        PurchaseItem = 15
    }
}