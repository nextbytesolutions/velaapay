namespace VelaaPay.Common.Enums
{
    public enum RequestStatus
    {
        None = 0,
        Accepted = 1,
        Rejected = 2
    }
}