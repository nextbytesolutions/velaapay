namespace VelaaPay.Common.Constants
{
    public class RoleNames
    {
        public const string Admin = "Administrator";
        public const string Vendor = "Vendor";
        public const string Supervisor = "Supervisor";
        public const string Cashier = "Cashier";
        public const string Customer = "Customer";
    }
}