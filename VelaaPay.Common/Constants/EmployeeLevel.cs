namespace VelaaPay.Common.Constants
{
    public class EmployeeLevel
    {
        public const int Vendor = 1;
        public const int Supervisor = 2;
        public const int Cashier = 3;
    }
}