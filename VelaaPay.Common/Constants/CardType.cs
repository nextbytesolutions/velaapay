namespace VelaaPay.Common.Constants
{
    public enum CardType
    {
        Visa = 1,
        MasterCard,
        AmericanExpress,
        Jcb,
        Discover,
        Diners
    }
}