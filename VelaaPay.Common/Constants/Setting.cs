namespace VelaaPay.Common.Constants
{
    public class SettingConstant
    {
        public const string TodayLimit = "TodayLimit";
    }

    public enum SettingDataType
    {
        Integer,
        Float,
        Double,
        String,
        Json
    }
}