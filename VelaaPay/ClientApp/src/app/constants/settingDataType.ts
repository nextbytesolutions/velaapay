export enum SettingDataType {
  Integer = 'Integer',
  Float = 'Float',
  Double = 'Double',
  String = 'String',
  Json = 'Json'
}
