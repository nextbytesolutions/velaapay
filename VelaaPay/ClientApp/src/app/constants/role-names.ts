export class RoleNames {
  public static readonly Administrator = 'Administrator';
  public static readonly Vendor = 'Vendor';
  public static readonly Supervisor = 'Supervisor';
  public static readonly Cashier = 'Cashier';
}
