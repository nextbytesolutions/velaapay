export class Token {
  private _token: string;
  private _expiry: number;


  get token(): string {
    return this._token;
  }

  set token(value: string) {
    this._token = value;
  }

  get expiry(): number {
    return this._expiry;
  }

  set expiry(value: number) {
    this._expiry = value;
  }
}
