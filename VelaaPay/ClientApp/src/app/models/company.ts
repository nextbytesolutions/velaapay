export class Company {
  public id: number;
  public name: string;
  public type: number;
  public imageUrl: string;
  public companyId: number;
}
