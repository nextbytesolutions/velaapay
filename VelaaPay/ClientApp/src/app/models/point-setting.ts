export class PointSetting {
  id: number;
  updatedDate: string;
  loadType: number;
  point: number;
}
