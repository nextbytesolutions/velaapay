import {Transaction} from './transaction';
import {Request} from './request';

export class UserDetail {
  name: string;
  email: string;
  phone: string;
  wallet: string;
  cnic: string;
  image: string;
  transactions: Transaction[];
  requests: Request[];
  date: string;
  totalSent: number;
  totalReceived: number;
  isEnabled: boolean;
  isVerified: boolean;
}

