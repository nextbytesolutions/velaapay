export class Employee {
  public employeeName: string;
  public employeeLevel: string;
  public phoneNumber: string;
  public image: string;
  public password: string;
  public startTime: string;
  public endTime: string;
  public role: string;
  public supervisorId: number;
  public employeeId: number;
  public shopId: number;
  public shopName: string;
  public confirmPassword: string;
  public userId: string;
}
