export class Request {
  senderName: string;
  receiverName: string;
  amount: number;
  date: string;
  status: number;
}
