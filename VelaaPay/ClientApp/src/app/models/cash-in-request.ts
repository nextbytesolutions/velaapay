export class CashInRequest {
  public requestId: number;
  public image: string;
  public name: string;
  public type: number;
  public status: number;
  public amount: number;
  public transactionCode: string;
  public createdDate: string;
}
