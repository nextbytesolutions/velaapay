export class Transaction {
  public transactionId: string;
  public transactionCode: string;
  public amount: number;
  public senderName: number;
  public receiverName: string;
  public loadType: number;
  public date: string;
  public isReverted: boolean;
}
