export class CashInTrack {
  public transactionId: string;
  public amount: number;
  public createdDate: string;
  public employee: string;
  public customer: string;
  public customerId: string;
  public otp: string;
  public pin: string;
  public shop: string;
}
