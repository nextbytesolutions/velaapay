import {Transaction} from './transaction';

export class MerchantDetail {
  fullName: string;
  userName: string;
  email: string;
  phone: string;
  address: string;
  wallet: string;
  transactions: Transaction[];
  date: string;
  isApproved: boolean;
  isEnabled: boolean;
  image: string;
  role: string;
}
