export class Merchant {
  fullName: string;
  name: string;
  dateOfBirth: string;
  email: string;
  phone: string;
  businessName: string;
  businessType: string;
  phoneNumber: string;
  password: string;
  confirmPassword: string;
  userName: string;
  rccm: string;
  rccmImage: string;
  address: string;
  id: string;
  isEnabled: boolean;
  isApproved: boolean;
  userId: string;
  role: string;
  isVerified: boolean;
  ntnNumber: string;
  identificationNumber: string;
  identificationImage: string;
  image: string;
  public totalBalance: number;
  constructor() {
    this.confirmPassword = this.fullName = this.email = this.phone = this.password = this.userName = '';
  }

}
