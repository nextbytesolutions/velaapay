export class DateViewModel {
  private _date: string;
  private _value: number;


  get date(): string {
    return this._date;
  }

  set date(value: string) {
    this._date = value;
  }

  get value(): number {
    return this._value;
  }

  set value(value: number) {
    this._value = value;
  }
}
