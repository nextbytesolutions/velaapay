import {Sale} from "./sale";
import {CashInTrack} from "./cash-in-track";
import {CashOutTrack} from "./cash-out-track";

export class ShopDetail {
  totalSales: number;
  totalCashIn: number;
  totalCashOut: number;
  sales: Sale[];
  cashInTracks: CashInTrack[];
  cashOuts: CashOutTrack[];
}
