// ====================================================


// ====================================================

import { PermissionValues } from './permission.model';
import {ResponseErrorModel} from './response-error-model';
import {User} from './user';


export interface LoginResponse {
    phone: string;
    token: string;
    expiry: number;
    user: User;
    info: User;
    error: ResponseErrorModel;
    isTwoFactorEnabled: boolean;
}


export interface IdToken {
    sub: string;
    name: string;
    studyCenterId: string;
    fullname: string;
    jobtitle: string;
    email: string;
    phone: string;
    role: string | string[];
    permission: PermissionValues | PermissionValues[];
    configuration: string;
    refresh_expire_in: number;
}
