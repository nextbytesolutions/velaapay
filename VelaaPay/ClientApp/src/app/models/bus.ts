export class Bus {
  public id: number;
  public busNo: string;
  public createdDate: string;
  public to: string;
  public from: string;
  public password: string;
  public ticketPrice: number;
  public isEnabled: boolean;
  public confirmPassword: string;
}
