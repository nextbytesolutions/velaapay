export class MobileTopUp {
  public id: number;
  public customer: string;
  public company: string;
  public amount: number;
  public mobileNo: string;
  public status: number;

}
