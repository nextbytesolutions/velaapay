export class Setting {
  id: string;
  dataType: string;
  value: string;
  key: string;
}
