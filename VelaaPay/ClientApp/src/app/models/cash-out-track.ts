export class CashOutTrack {
  public status: number;
  public transactionId: string;
  public amount: number;
  public createdDate: string;
  public employee: string;
  public customer: string;
  public shop: string;
  customerId: string;
  otp: string;
  pin: string;
}
