export class Shop {
  public shopId: number;
  public name: string;
  public address: string;
  public phoneNumber: string;
  public startTime: string;
  public endTime: string;
}
