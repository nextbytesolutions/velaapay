export class Bill {
  public billId: number;
  public company: string;
  public billNo: string;
  public customer: string;
  public status: number;
  public referenceNo: string;
  public date: string;
  public amount: number;
  public response: string;
  public remarks: string;

}
