export class AirTimeTopUp {
  public requestId: number;
  public name: string;
  public company: string;
  public amount: number;
  public mobileNo: string;
  public status: number;
  public remarks: string;
  public response: string;
}
