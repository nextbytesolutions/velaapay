export class PurchaseOrder {
  public appName: string;
  public fullName: string;
  public productName: string;
  public createdDate: string;
  public amount: number;
  public id: string;
  public quantity: number;
  public salePrice: number;
  public shippingCharges: number;
  public totalPrice: string;
  public orderId: string;
}
