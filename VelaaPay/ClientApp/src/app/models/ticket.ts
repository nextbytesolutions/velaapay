export class Ticket {
  public id: number;
  public fullName: string;
  public amount: number;
  public busNo: string;
  public createdDate: string;
}
