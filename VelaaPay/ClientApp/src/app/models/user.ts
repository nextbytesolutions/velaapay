export class User {
  roles: any;
  email: string;
  fullName: string;
  image: string;
  userId: string;
  isVerified: boolean;
  employeeId: number;
  id: string;
  cnic: string;
  cnicImage: string;
  pin: string;
  confirmPin: string;
  isEnabled: boolean;
  shopId: number;
  shopName: string;
  phoneNumber: string;
}

