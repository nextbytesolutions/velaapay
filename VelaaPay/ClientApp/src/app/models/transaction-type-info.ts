export class TransactionTypeInfo {
  private _type: number;
  private _amount: number;
  private _count: number;


  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
  }

  get type(): number {
    return this._type;
  }

  set type(value: number) {
    this._type = value;
  }

  get amount(): number {
    return this._amount;
  }

  set amount(value: number) {
    this._amount = value;
  }
}

