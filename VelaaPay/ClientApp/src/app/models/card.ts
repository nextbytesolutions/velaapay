export class Card {
  public id: number;
  public customer: string;
  public name: string;
  public cardType: number;
  public createdDate: string;
  public expiryDate: string;
  public lastDigits: string;
}
