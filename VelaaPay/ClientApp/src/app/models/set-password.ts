export class SetPassword {
  password: string;
  confirmPassword: string;
  pin: string;
  userId: string;
  token: string;
}
