export class ResponseErrorModel {
  message: string;
  errors: string[];
}
