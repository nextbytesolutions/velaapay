export class Sale {
  public id: number;
  public amount: number;
  public image: string;
  public cashier: string;
  public name: string;
  public date: string;
  public shop: string;
  public shopId: number;
  public employeeId: number;
}
