export class WithdrawRequest {
  public requestId: number;
  public accountNo: string;
  public amount: number;
  public status: number;
  public name: string;
  public type: string;
  public bank: string;
}
