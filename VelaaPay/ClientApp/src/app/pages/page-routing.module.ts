import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'login',
      loadChildren: './login/login.module#LoginModule',
    },
    {
      path: 'logout',
      loadChildren: './logout/logout.module#LogoutModule',
    },
    {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule',
    },
    {
      path: 'merchant-registration',
      loadChildren: './merchant-registration/merchant-registration.module#MerchantRegistrationModule',
    },
    {
      path: 'set-password',
      loadChildren: './set-password/set-password.module#SetPasswordModule',
    },
    {
      path: '',
      redirectTo: 'login',
    },
    {
      path: '**',
      redirectTo: 'login',
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageRoutingModule { }
