import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutComponent } from './components/logout/logout.component';
import {LogoutRoutingModule} from './logout-routing.module';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {MaterialModule} from '../../shared/material.module';

@NgModule({
  declarations: [LogoutComponent],
  imports: [
    CommonModule,
    LogoutRoutingModule,
    MaterialModule
  ]
})
export class LogoutModule { }
