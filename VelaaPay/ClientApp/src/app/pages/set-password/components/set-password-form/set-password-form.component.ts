import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SetPasswordService} from '../../services/set-password.service';
import {AlertService, MessageSeverity} from '../../../../services/alert.service';
import {SetPassword} from '../../../../models/set-password';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-set-password-form',
  templateUrl: './set-password-form.component.html',
  styleUrls: ['./set-password-form.component.scss']
})
export class SetPasswordFormComponent implements OnInit {

  isHidden = true;
  passwordFormGroup: FormGroup;
  passwordControl: FormControl;
  confirmPasswordControl: FormControl;
  pinControl: FormControl;

  setPassword: SetPassword;
  token: string;
  userId: string;
  constructor(private passwordService: SetPasswordService,
              private route: ActivatedRoute,
              private router: Router,
              private alertService: AlertService) { }


  ngOnInit() {
    this.route.queryParams.subscribe(p => {
      this.token = p['token'];
      this.userId = p['userId'];
    });
    this.pinControl = new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]);
    this.passwordFormGroup = new FormGroup({
      pin: this.pinControl,
    });
  }

  get isSaving () {
    return this.alertService.isLoadingInProgress;
  }
  setPasswordAndPin() {
    if (!this.token && !this.userId) {
      this.alertService.showMessage('Info', 'Invalid Token or UserId', MessageSeverity.error);
      return;
    }
    if (this.passwordFormGroup.valid) {
      this.setPassword = new SetPassword();
      this.setPassword.pin = this.pinControl.value;
      this.setPassword.userId = this.userId;
      this.setPassword.token = this.token;
      this.alertService.startLoadingMessage('Saving...');
      this.passwordService.setPassword(this.setPassword).subscribe(p => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'Password updated successfully', MessageSeverity.success);
        setTimeout(() => {
          this.router.navigateByUrl('login');
        }, 2000);
        });
    } else {
      this.alertService.showMessage('Info', 'Check Form errors', MessageSeverity.error);
    }
  }

  showErrorMessage() {
    this.alertService.showMessage('Info', 'Check Form errors', MessageSeverity.error);
  }
}
