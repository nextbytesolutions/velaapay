import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetPasswordFormComponent } from './components/set-password-form/set-password-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PipeModule} from '../../pipes/pipe.module';
import {SetPasswordRoutingModule} from './set-password-routing.module';
import {MaterialModule} from '../../shared/material.module';

@NgModule({
  declarations: [SetPasswordFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SetPasswordRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class SetPasswordModule { }
