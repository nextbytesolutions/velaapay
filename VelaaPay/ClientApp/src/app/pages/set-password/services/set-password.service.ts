import { Injectable } from '@angular/core';
import {SetPassword} from "../../../models/set-password";
import {ResponseModel} from "../../../models/response.model";
import {SetPasswordEndpointService} from "./set-password-endpoint.service";

@Injectable({
  providedIn: 'root'
})
export class SetPasswordService {
  constructor(private passwordService: SetPasswordEndpointService) {

  }

  setPassword(model: SetPassword) {
    return this.passwordService.setPasswordEndpoint(model)
      .map((response) => <ResponseModel>response);
  }


}
