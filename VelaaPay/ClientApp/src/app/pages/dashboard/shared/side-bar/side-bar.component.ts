import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../../services/auth.service';
import {RoleNames} from '../../../../constants/role-names';


class RouteInfo {
  public path: string;
  public title: string;
  public icon: string;
  public class: string;
  public isFontAwesome: boolean;
}

export const VERIFYROUTES: RouteInfo[] = [
  {path: '/dashboard/settings', title: 'Settings', icon: 'settings', class: '', isFontAwesome: false},
  {path: '/logout', title: 'Logout', icon: 'close', class: '', isFontAwesome: false},
];
export const ADMINROUTES: RouteInfo[] = [
  // {path: '/dashboard/admin-dashboard', title: 'Dashboard', icon: 'home', class: '', isFontAwesome: false},
  {path: '/dashboard/users', title: 'Customers', icon: 'person', class: '', isFontAwesome: false},
  {path: '/dashboard/merchants', title: 'Merchants', icon: 'person', class: '', isFontAwesome: false},
  {path: '/dashboard/transactions', title: 'Transactions', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/companies', title: 'Companies', icon: 'account_balance', class: '', isFontAwesome: false},
  {path: '/dashboard/banks', title: 'Banks', icon: 'account_balance', class: '', isFontAwesome: false},
  {path: '/dashboard/requests', title: 'User Requests', icon: 'assignment_ind', class: '', isFontAwesome: false},
  {path: '/dashboard/cards', title: 'Cards', icon: 'fa fa-credit-card', class: '', isFontAwesome: true},
  {path: '/dashboard/air-requests', title: 'AirTimeTopup Requests', icon: 'fa fa-plane', class: '', isFontAwesome: true},
  {
    path: '/dashboard/merchant-top-up-requests',
    title: 'TopUp Requests',
    icon: 'fa fa-plane',
    class: '',
    isFontAwesome: true
  },
  // {path: '/dashboard/mobile-requests', title: 'Mobile Requests', icon: 'fa fa-plane', class: '', isFontAwesome: true},
  {path: '/dashboard/bill-requests', title: 'Bill Requests', icon: 'fa fa-file-invoice', class: '', isFontAwesome: true},
  {
    path: '/dashboard/withdraw-requests',
    title: 'Withdraw Requests',
    icon: 'assignment_returned',
    class: '',
    isFontAwesome: false
  },
  {path: '/dashboard/settings', title: 'Settings', icon: 'settings', class: '', isFontAwesome: false},
  {path: '/logout', title: 'Logout', icon: 'close', class: '', isFontAwesome: false},
];

export const VENDORROUTES: RouteInfo[] = [
  {path: '/dashboard/shops', title: 'Shops', icon: 'store', class: '', isFontAwesome: false},
  {path: '/dashboard/employees', title: 'Employees', icon: 'person', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-in-requests', title: 'Cash In Requests', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/transactions', title: 'Transactions', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-in-tracks', title: 'Cash Ins', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-out-tracks', title: 'Cash Outs', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/settings', title: 'Settings', icon: 'settings', class: '', isFontAwesome: false},
  {path: '/logout', title: 'Logout', icon: 'close', class: '', isFontAwesome: false},
];

export const CASHIERROUTES: RouteInfo[] = [
  {path: '/dashboard/transactions', title: 'Transaction', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-in-tracks', title: 'Cash Ins', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-out-tracks', title: 'Cash Outs', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/settings', title: 'Settings', icon: 'settings', class: '', isFontAwesome: false},
  {path: '/logout', title: 'Logout', icon: 'close', class: '', isFontAwesome: false},
];
export const SUPERVISOR: RouteInfo[] = [
  {path: '/dashboard/employees', title: 'Employees', icon: 'person', class: '', isFontAwesome: false},
  {path: '/dashboard/transactions', title: 'Transaction', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-in-tracks', title: 'Cash Ins', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/cash-out-tracks', title: 'Cash Outs', icon: 'attach_money', class: '', isFontAwesome: false},
  {path: '/dashboard/settings', title: 'Settings', icon: 'settings', class: '', isFontAwesome: false},
  {path: '/logout', title: 'Logout', icon: 'close', class: '', isFontAwesome: false},
];

declare const $: any;

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {



  public static Routes: RouteInfo[];
  currentRoutes: RouteInfo[];
  constructor(private authService: AuthService) { }

  ngOnInit() {
    if (this.authService.isRole(RoleNames.Administrator)) {
      this.currentRoutes = ADMINROUTES.filter(p => true);
    } else if (!(+this.authService.currentUser.isVerified)) {
      this.currentRoutes = VERIFYROUTES.filter(p => true);
    } else if (this.authService.isRole(RoleNames.Vendor)) {
      this.currentRoutes = VENDORROUTES.filter(p => true);
    } else if (this.authService.isRole(RoleNames.Supervisor)) {
      this.currentRoutes = SUPERVISOR.filter(p => true);
    } else if (this.authService.isRole(RoleNames.Cashier)) {
      this.currentRoutes = CASHIERROUTES.filter(p => true);
    }
    SideBarComponent.Routes = this.currentRoutes.filter(p => true);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
  getProfileImage(): string {
    return this.authService.currentUser.image;
  }
  getProfileName(): string {
    return this.authService.currentUser.fullName;
  }
  getProfileRole(): string[] {
    return this.authService.currentUser.roles;
  }

}
