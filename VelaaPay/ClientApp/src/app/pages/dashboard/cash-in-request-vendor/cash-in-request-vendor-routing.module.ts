import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CashInRequestVendorManagementComponent} from './components/cash-in-request-vendor-management/cash-in-request-vendor-management.component';

const routes: Routes = [
  {
    path: '',
    component: CashInRequestVendorManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashInRequestVendorRoutingModule { }
