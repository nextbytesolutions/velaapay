import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashInRequestVendorManagementComponent } from './components/cash-in-request-vendor-management/cash-in-request-vendor-management.component';
import { CashInRequestVendorInfoComponent } from './components/cash-in-request-vendor-info/cash-in-request-vendor-info.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CashInRequestVendorRoutingModule} from './cash-in-request-vendor-routing.module';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  declarations: [CashInRequestVendorManagementComponent, CashInRequestVendorInfoComponent],
  entryComponents: [CashInRequestVendorInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CashInRequestVendorRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CashInRequestVendorModule { }
