import { Injectable } from '@angular/core';
import {CashInRequestVendorEndpointService} from './cash-in-request-vendor-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {CashInRequest} from '../../../../models/cash-in-request';

@Injectable({
  providedIn: 'root'
})
export class CashInRequestVendorService {


  constructor(private cashInRequestService: CashInRequestVendorEndpointService) {

  }

  getCashInRequest(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.cashInRequestService.getCashInRequestsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }
  createCashInRequest(cashInRequest: CashInRequest) {
    return this.cashInRequestService.createCashInRequestsEndpoint(cashInRequest)
      .map((response) => <ResponseModel>response);
  }
}
