import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {CashInRequest} from '../../../../models/cash-in-request';

@Injectable({
  providedIn: 'root'
})
export class CashInRequestVendorEndpointService extends EndpointFactoryService {

  private readonly _createCashInRequestUrl: string = '/api/v1/cashInRequest';

  get CashInRequestsUrl() { return this.configurations.baseUrl + this._createCashInRequestUrl; }
  getCashInRequestsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '')
      .set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CashInRequestsUrl, { headers: this.getRequestHeaders().headers, params: params });
  }
  createCashInRequestsEndpoint(cashInRequest: CashInRequest): Observable<any> {
    const request = {
      type: cashInRequest.type,
      transactionCode: cashInRequest.transactionCode,
      image: cashInRequest.image,
      amount: cashInRequest.amount,
    };
    return this.http.post(this.CashInRequestsUrl, request, {headers: this.getRequestHeaders().headers});
  }


}
