import {CashInRequestVendorService} from '../../services/cash-in-request-vendor.service';
import {CashInRequestVendorInfoComponent} from '../cash-in-request-vendor-info/cash-in-request-vendor-info.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {CashInRequest} from '../../../../../models/cash-in-request';
import {AlertService} from '../../../../../services/alert.service';
import {ResponseModel} from '../../../../../models/response.model';

@Component({
  selector: 'app-cash-in-request-vendor-management',
  templateUrl: './cash-in-request-vendor-management.component.html',
  styleUrls: ['./cash-in-request-vendor-management.component.scss']
})
export class CashInRequestVendorManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Request[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['amount', 'type', 'transactionCode', 'status', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private requestService: CashInRequestVendorService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getCashInRequest(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Request[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }


  addCashInRequest() {
    const modal = this.dialog.open(CashInRequestVendorInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.newRequest();
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.loadData();
    };

  }

  viewCashInRequest(row: CashInRequest) {
    const modal = this.dialog.open(CashInRequestVendorInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.editRequest(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }
}
