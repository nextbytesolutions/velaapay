import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CashInRequestVendorService} from '../../services/cash-in-request-vendor.service';
import {CashInRequest} from '../../../../../models/cash-in-request';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {ResponseModel} from '../../../../../models/response.model';
import {CashInRequestType} from '../../../../../models/enums';

@Component({
  selector: 'app-cash-in-request-vendor-info',
  templateUrl: './cash-in-request-vendor-info.component.html',
  styleUrls: ['./cash-in-request-vendor-info.component.scss']
})
export class CashInRequestVendorInfoComponent implements OnInit {

  image: any = null;
  requestType = '';
  requestTypes = [];
  isNewRequest = false;
  get isSaving () {
    return this.alertService.isLoadingInProgress;
  }
  requestEdit: CashInRequest = new CashInRequest();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  requestAmountControl: FormControl;
  requestCodeControl: FormControl;
  requestImageControl: FormControl;
  requestTypeControl: FormControl;
  requestFormGroup: FormGroup;

  constructor(private alertService: AlertService,
              private requestService: CashInRequestVendorService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }


  initForm() {
    this.requestTypes = this.requestTypeEnumList();
    this.requestAmountControl = new FormControl('', [Validators.required, Validators.min(1)]);
    this.requestTypeControl = new FormControl('', [Validators.required]);
    this.requestCodeControl = new FormControl('', [Validators.required]);
    this.requestImageControl = new FormControl(null, [Validators.required]);
    this.requestFormGroup = new FormGroup({
      image: new FormControl(''),
      temp: this.requestImageControl,
      type: this.requestTypeControl,
      code: this.requestCodeControl,
      amount: this.requestAmountControl,
    });

  }

  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.requestEdit.transactionCode = this.requestEdit.transactionCode.trim();
    if (this.isNewRequest) {
      this.requestService.createCashInRequest(this.requestEdit).subscribe(result => this.saveSuccessHelper(result));
    }
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.resetForm();
    if (this.isNewRequest) {
      this.alertService.showMessage('Success', `Request was created successfully`, MessageSeverity.success);
    }
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }


  cancel() {
    this.requestEdit = new CashInRequest();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.requestEdit = new CashInRequest();
    this.resetForm();
  }


  resetForm() {
    this.requestFormGroup.reset();
  }


  newRequest() {
    this.isNewRequest = true;
    this.requestEdit = new CashInRequest();
    this.initForm();
    return this.requestEdit;
  }

  requestTypeEnumList(): any {
    const keys = Object.keys(CashInRequestType);
    const values = Object.values(CashInRequestType);
    const keyValue = [];
    for (let i = 0; i < keys.length; i++) {
      if (isNaN(Number(keys[i] + ''))) {
        keyValue.push({
          value: values[i],
          text: values[i]
        });
      }
    }
    return keyValue;
  }

  editRequest(request: CashInRequest) {
    this.initForm();
    if (request) {
      this.isNewRequest = false;
      this.requestEdit = new CashInRequest();
      Object.assign(this.requestEdit, request);
      this.requestEdit.requestId = request.requestId;
      this.requestCodeControl.setValue(this.requestEdit.transactionCode);
      this.requestAmountControl.setValue(this.requestEdit.amount);
      this.requestImageControl.setValue(this.requestEdit.image);
      this.requestTypeControl.setValue(this.requestEdit.type);
      return this.requestEdit;
    } else {
      this.isNewRequest = true;
      return new CashInRequest();
    }
  }

  formSubmit() {
    if (this.requestFormGroup.valid) {
      this.requestEdit.image = this.requestImageControl.value;
      this.requestEdit.transactionCode = this.requestCodeControl.value;
      this.requestEdit.type = this.requestTypeControl.value;
      this.requestEdit.image = this.requestImageControl.value;
      this.requestEdit.amount = this.requestAmountControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }


  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.image = e.currentTarget.result;
        this.requestImageControl.setValue(this.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
}
