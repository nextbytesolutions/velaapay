import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {BillManagementComponent} from "./components/bill-management/bill-management.component";

const routes: Routes = [
  {
    path: '',
    component: BillManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillRequestRoutingModule { }
