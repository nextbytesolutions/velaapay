import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillManagementComponent } from './components/bill-management/bill-management.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BillRequestRoutingModule} from './bill-request-routing.module';
import { BillInfoComponent } from './components/bill-info/bill-info.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {MaterialModule} from '../../../shared/material.module';
import {PipeModule} from '../../../pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    BillRequestRoutingModule,
    MaterialModule,
    PipeModule
  ],
  declarations: [BillManagementComponent, BillInfoComponent],
  entryComponents: [BillInfoComponent]
})
export class BillRequestModule { }
