import { Injectable } from '@angular/core';
import {BillRequestEndpointService} from './bill-request-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {Bill} from '../../../../models/bill';

@Injectable({
  providedIn: 'root'
})
export class BillRequestService {

  constructor(private billEndpoint: BillRequestEndpointService) {

  }

  getBills(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.billEndpoint.getBillsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  acceptBill(bill: Bill) {
    return this.billEndpoint.getAcceptBillsEndpoint(bill)
      .map((response) => <ResponseModel>response);
  }

  rejectBill(bill: Bill) {
    return this.billEndpoint.getRejectBillsEndpoint(bill)
      .map((response) => <ResponseModel>response);
  }
}
