import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Bill} from '../../../../models/bill';

@Injectable({
  providedIn: 'root'
})
export class BillRequestEndpointService extends EndpointFactoryService {

  private readonly _createBillUrl: string = '/api/v1/bill';

  get BillsUrl() {
    return this.configurations.baseUrl + this._createBillUrl;
  }

  AcceptBillsUrl(id: number) {
    return this.configurations.baseUrl + this._createBillUrl + '/accept' + '/' + id ;
  }

  RejectBillsUrl(id: number) {
    return this.configurations.baseUrl + this._createBillUrl  + '/reject' + '/' + id;
  }

  getBillsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.BillsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getAcceptBillsEndpoint(bill: Bill): Observable<any> {
    return this.http.put(this.AcceptBillsUrl(bill.billId), null, {headers: this.getRequestHeaders().headers});
  }
  getRejectBillsEndpoint(bill: Bill): Observable<any> {
    return this.http.put(this.RejectBillsUrl(bill.billId), null, {headers: this.getRequestHeaders().headers});
  }
}
