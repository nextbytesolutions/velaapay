import { Component, OnInit } from '@angular/core';
import {Bill} from '../../../../../models/bill';

@Component({
  selector: 'app-bill-info',
  templateUrl: './bill-info.component.html',
  styleUrls: ['./bill-info.component.scss']
})
export class BillInfoComponent implements OnInit {

  public changesCancelledCallback: () => void;
  bill: Bill;
  constructor() { }

  ngOnInit() {
  }

  setBill(bill: Bill) {
    this.bill = bill;
  }

  getResponseObj() {
    if (this.bill) {
      return JSON.parse(this.bill.response);
    }
    return null;
  }
}
