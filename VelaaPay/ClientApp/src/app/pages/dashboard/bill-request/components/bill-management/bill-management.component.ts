import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {BillRequestService} from '../../services/bill-request.service';
import {ResponseModel} from '../../../../../models/response.model';
import {Bill} from '../../../../../models/bill';
import {RequestStatus} from '../../../../../models/enums';
import {BillInfoComponent} from '../bill-info/bill-info.component';

@Component({
  selector: 'app-bill-management',
  templateUrl: './bill-management.component.html',
  styleUrls: ['./bill-management.component.scss']
})
export class BillManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Request[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['customer', 'amount', 'company', 'billNo', 'referenceNo',  'status', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private requestService: BillRequestService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getBills(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Request[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }
  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  acceptBill(row: Bill) {
    this.alertService.showDialog('Are you sure you want to accept the request of \"' + row.customer + '\"?', DialogType.confirm,
      () => this.acceptBillHelper(row));
  }


  acceptBillHelper(row: Bill) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Accepting...');
    this.loadingIndicator = true;

    this.requestService.acceptBill(row)
      .subscribe(results => {
            this.alertService.stopLoadingMessage();
            this.alertService.showMessage('Success', 'AirTime Request Accepted', MessageSeverity.success);
            this.loadingIndicator = false;
            this.page = 0;
            this.search = '';
            this.loadData();
        });
  }

  rejectBill(row: Bill) {
    this.alertService.showDialog('Are you sure you want to reject the request of \"' + row.customer + '\"?', DialogType.confirm,
      () => this.rejectBillHelper(row));
  }


  rejectBillHelper(row: Bill) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Rejecting...');
    this.loadingIndicator = true;

    this.requestService.rejectBill(row)
      .subscribe(results => {
            this.alertService.stopLoadingMessage();
            this.loadingIndicator = false;
            this.alertService.showMessage('Success', 'AirTime Request Rejected', MessageSeverity.success);

            this.page = 0;
            this.search = '';
            this.loadData();
        });
  }

  get NoneStatus() {
    return RequestStatus.None;
  }

  showBillResponse(element: Bill) {
    const modal = this.dialog.open(BillInfoComponent, {
      width: '600px',
    });
    modal.componentInstance.setBill(element);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }
}
