import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {AlertService} from '../../../../../services/alert.service';
import {CashInTrackService} from '../../services/cash-in-track.service';
import {CashInTrack} from '../../../../../models/cash-in-track';
import {ResponseModel} from '../../../../../models/response.model';
import {CashInRequestVendorInfoComponent} from '../../../cash-in-request-vendor/components/cash-in-request-vendor-info/cash-in-request-vendor-info.component';
import {CashInTrackInfoComponent} from '../cash-in-track-info/cash-in-track-info.component';

@Component({
  selector: 'app-cash-in-track-management',
  templateUrl: './cash-in-track-management.component.html',
  styleUrls: ['./cash-in-track-management.component.scss']
})
export class CashInTrackManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: CashInTrack[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['employee', 'customer', 'amount', 'shop', 'transactionId', 'createdDate'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private cashInTrackService: CashInTrackService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.cashInTrackService.getCashInTrack(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as CashInTrack[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  addCashIn() {
    const modal = this.dialog.open(CashInTrackInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.initForm();
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.loadData();
    };

  }
}
