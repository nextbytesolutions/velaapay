import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {CashInTrackService} from '../../services/cash-in-track.service';
import {UserService} from '../../../users/services/user.service';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';
import {CashInTrack} from '../../../../../models/cash-in-track';

@Component({
  selector: 'app-cash-in-track-info',
  templateUrl: './cash-in-track-info.component.html',
  styleUrls: ['./cash-in-track-info.component.scss']
})
export class CashInTrackInfoComponent implements OnInit {

  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  step = 1;
  customerId: string;
  cashInFormGroup: FormGroup;
  cashInAmountControl: FormControl;
  cashInPhoneControl: FormControl;
  cashInCustomerControl: FormControl;
  cashInPinControl: FormControl;
  cashInOtpControl: FormControl;
  constructor(private alertService: AlertService,
              private userService: UserService,
              private cashInTrackService: CashInTrackService) {

  }

  ngOnInit() {
  }

  initForm() {
    this.cashInAmountControl = new FormControl('0', [Validators.required]);
    this.cashInPinControl = new FormControl('', [Validators.required]);
    this.cashInCustomerControl = new FormControl('', [Validators.required]);
    this.cashInOtpControl = new FormControl('', [Validators.required]);
    this.cashInPhoneControl = new FormControl('', [Validators.required, CustomValidator.Phone()]);
    this.cashInFormGroup = new FormGroup({
      amount: this.cashInAmountControl,
      phone: this.cashInPhoneControl,
      customer: this.cashInCustomerControl,
      pin: this.cashInPinControl,
      otp: this.cashInOtpControl,
    });
  }

  findUser() {
    if (this.cashInPhoneControl.value && this.cashInPhoneControl.valid) {
      this.alertService.startLoadingMessage('Searching...');
      this.userService.getUserByPhone(this.cashInPhoneControl.value).subscribe(result => {
        this.alertService.stopLoadingMessage();
        if (result != null && result.data != null) {
          if (result.data.id) {
            this.customerId = result.data.id;
            this.cashInCustomerControl.setValue(result.data.name);
            this.step = 2;
          }
        } else {
          this.alertService.showMessage('Not Found','Customer Not Found for Phone ' + this.cashInPhoneControl.value,MessageSeverity.warn);
        }

      });
    }
  }

  sendOtp() {
    if (this.cashInCustomerControl.valid) {
      this.alertService.startLoadingMessage('Sending Otp...');
      this.userService.sendOtp(this.customerId).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.step = 3;
      });
    }
  }

  formSubmit() {
    if (this.cashInFormGroup.valid) {
      const cashIn = new CashInTrack();
      cashIn.amount = this.cashInAmountControl.value;
      cashIn.customerId = this.customerId;
      cashIn.otp = this.cashInOtpControl.value;
      cashIn.pin = this.cashInPinControl.value;
      this.alertService.startLoadingMessage('Saving...');
      this.cashInTrackService.createCashIn(cashIn).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'CashIn Created', MessageSeverity.success);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      });
    }
  }
  back() {
    this.step = 1;
  }
  cancel() {
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
}
