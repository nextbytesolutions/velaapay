import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashInTrackManagementComponent } from './components/cash-in-track-management/cash-in-track-management.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {CashInTrackRoutingModule} from './cash-in-track-routing.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../shared/material.module';
import { CashInTrackInfoComponent } from './components/cash-in-track-info/cash-in-track-info.component';

@NgModule({
  declarations: [CashInTrackManagementComponent, CashInTrackInfoComponent],
  entryComponents: [CashInTrackInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CashInTrackRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CashInTrackModule { }
