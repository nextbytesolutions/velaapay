import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CashInTrackManagementComponent} from "./components/cash-in-track-management/cash-in-track-management.component";

const routes: Routes = [
  {
    path: '',
    component:CashInTrackManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CashInTrackRoutingModule { }
