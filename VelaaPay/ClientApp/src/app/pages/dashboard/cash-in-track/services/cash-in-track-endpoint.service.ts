import {Injectable} from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {CashInTrack} from '../../../../models/cash-in-track';

@Injectable({
  providedIn: 'root'
})
export class CashInTrackEndpointService extends EndpointFactoryService {


  private readonly _getCashInTrackUrl: string = '/api/v1/cashInTrack';
  private readonly _createCashInUrl: string = '/api/v1/cashIn';

  get CashInTracksUrl() {
    return this.configurations.baseUrl + this._getCashInTrackUrl;
  }

  get CashInUrl() {
    return this.configurations.baseUrl + this._createCashInUrl;
  }

  getCashInTracksEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams({
      encoder: this.encoderService
    }).set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CashInTracksUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getCreateCashInEndpoint(cashIn: CashInTrack) {
    return this.http.post(this.CashInUrl, cashIn, {headers: this.getRequestHeaders().headers});
  }
}
