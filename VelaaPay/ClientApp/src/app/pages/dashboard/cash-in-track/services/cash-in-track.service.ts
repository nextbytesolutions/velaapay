import {Injectable} from '@angular/core';
import {CashInTrackEndpointService} from './cash-in-track-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {CashInTrack} from '../../../../models/cash-in-track';

@Injectable({
  providedIn: 'root'
})
export class CashInTrackService {

  constructor(private cashInTrackService: CashInTrackEndpointService) {

  }

  getCashInTrack(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.cashInTrackService.getCashInTracksEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  createCashIn(cashIn: CashInTrack) {
    return this.cashInTrackService.getCreateCashInEndpoint(cashIn)
      .map((response) => <ResponseModel>response);

  }
}
