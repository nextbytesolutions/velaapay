import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {CashInRequestService} from '../../services/cash-in-request.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {CashInRequest} from '../../../../../models/cash-in-request';
import {ResponseModel} from '../../../../../models/response.model';
import {CashInRequestInfoComponent} from '../cash-in-request-info/cash-in-request-info.component';
import {RequestStatus} from '../../../../../models/enums';

@Component({
  selector: 'app-cash-in-request-management',
  templateUrl: './cash-in-request-management.component.html',
  styleUrls: ['./cash-in-request-management.component.scss']
})
export class CashInRequestManagementComponent implements OnInit {


  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Request[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['name', 'amount', 'type', 'transactionCode',  'status', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private requestService: CashInRequestService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getCashInRequests(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Request[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  acceptCashInRequest(row: CashInRequest) {
    this.alertService.showDialog('Are you sure you want to accept the request of \"' + row.name + '\"?', DialogType.confirm,
      () => this.acceptCashInRequestHelper(row));
  }


  acceptCashInRequestHelper(row: CashInRequest) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Accepting...');
    this.loadingIndicator = true;

    this.requestService.acceptCashInRequests(row)
      .subscribe(results => {
          this.alertService.stopLoadingMessage();
          this.alertService.showMessage('Success', 'MobileTime Request Accepted', MessageSeverity.success);
          this.loadingIndicator = false;
          row.status = +RequestStatus.Accepted;
        });
  }

  rejectCashInRequest(row: CashInRequest) {
    this.alertService.showDialog('Are you sure you want to reject the request of \"' + row.name + '\"?', DialogType.confirm,
      () => this.rejectCashInRequestHelper(row));
  }


  rejectCashInRequestHelper(row: CashInRequest) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Rejecting...');
    this.loadingIndicator = true;

    this.requestService.rejectCashInRequests(row)
      .subscribe(results => {
          this.alertService.stopLoadingMessage();
          this.loadingIndicator = false;
          this.alertService.showMessage('Success', 'MobileTime Request Rejected', MessageSeverity.success);
          row.status = +RequestStatus.Rejected;
        });
  }

  get NoneStatus() {
    return RequestStatus.None;
  }

  viewCashInRequest(row: CashInRequest) {
    const modal = this.dialog.open(CashInRequestInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.setRequest(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }
}
