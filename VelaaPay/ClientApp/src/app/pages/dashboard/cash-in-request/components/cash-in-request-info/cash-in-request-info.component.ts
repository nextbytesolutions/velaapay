import { Component, OnInit } from '@angular/core';
import {CashInRequest} from '../../../../../models/cash-in-request';

@Component({
  selector: 'app-cash-in-request-info',
  templateUrl: './cash-in-request-info.component.html',
  styleUrls: ['./cash-in-request-info.component.scss']
})
export class CashInRequestInfoComponent implements OnInit {

  cashInRequest: CashInRequest;
  public changesCancelledCallback: () => void;

  constructor() { }

  ngOnInit() {
  }


  setRequest(request: CashInRequest) {
    this.cashInRequest = request;
  }

  cancel() {
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
    this.cashInRequest = null;
  }
}
