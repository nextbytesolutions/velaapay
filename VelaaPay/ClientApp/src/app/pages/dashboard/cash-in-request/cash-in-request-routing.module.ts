import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CashInRequestManagementComponent} from './components/cash-in-request-management/cash-in-request-management.component';

const routes: Routes = [
  {
    path: '',
    component: CashInRequestManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashInRequestRoutingModule { }
