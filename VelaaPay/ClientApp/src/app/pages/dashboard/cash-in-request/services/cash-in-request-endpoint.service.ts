import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {CashInRequest} from '../../../../models/cash-in-request';

@Injectable({
  providedIn: 'root'
})
export class CashInRequestEndpointService  extends EndpointFactoryService {

  private readonly _createCashInRequestUrl: string = '/api/v1/cashInRequest';

  get CashInRequestsUrl() {
    return this.configurations.baseUrl + this._createCashInRequestUrl;
  }

  AcceptCashInRequestsUrl(id: number) {
    return this.configurations.baseUrl + this._createCashInRequestUrl + '/' + 'accept/' + id;
  }

  RejectCashInRequestsUrl(id: number) {
    return this.configurations.baseUrl + this._createCashInRequestUrl + '/' + 'reject/' + id;
  }

  getCashInRequestsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CashInRequestsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getAcceptCashInRequestsEndpoint(cashInRequest: CashInRequest): Observable<any> {
    return this.http.put(this.AcceptCashInRequestsUrl(cashInRequest.requestId), null, {headers: this.getRequestHeaders().headers});
  }
  getRejectCashInRequestsEndpoint(cashInRequest: CashInRequest): Observable<any> {
    return this.http.put(this.RejectCashInRequestsUrl(cashInRequest.requestId), null, {headers: this.getRequestHeaders().headers});
  }
}
