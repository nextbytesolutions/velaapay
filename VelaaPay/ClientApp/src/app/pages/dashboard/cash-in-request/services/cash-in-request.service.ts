import {Injectable} from '@angular/core';
import {CashInRequestEndpointService} from './cash-in-request-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {CashInRequest} from '../../../../models/cash-in-request';

@Injectable({
  providedIn: 'root'
})
export class CashInRequestService {

  constructor(private cashInRequestEndpoint: CashInRequestEndpointService) {

  }

  getCashInRequests(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.cashInRequestEndpoint.getCashInRequestsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  acceptCashInRequests(cashInRequest: CashInRequest) {
    return this.cashInRequestEndpoint.getAcceptCashInRequestsEndpoint(cashInRequest)
      .map((response) => <ResponseModel>response);
  }

  rejectCashInRequests(cashInRequest: CashInRequest) {
    return this.cashInRequestEndpoint.getRejectCashInRequestsEndpoint(cashInRequest)
      .map((response) => <ResponseModel>response);
  }
}
