import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashInRequestManagementComponent } from './components/cash-in-request-management/cash-in-request-management.component';
import { CashInRequestInfoComponent } from './components/cash-in-request-info/cash-in-request-info.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {CashInRequestRoutingModule} from './cash-in-request-routing.module';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  entryComponents: [CashInRequestInfoComponent],
  declarations: [CashInRequestManagementComponent, CashInRequestInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CashInRequestRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CashInRequestModule { }
