import {Component, OnInit, ViewChild} from '@angular/core';
import {Bank} from '../../../../../models/bank';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {BankService} from '../../services/bank.service';
import {ResponseModel} from '../../../../../models/response.model';
import {BankInfoComponent} from '../bank-info/bank-info.component';

@Component({
  selector: 'app-bank-management',
  templateUrl: './bank-management.component.html',
  styleUrls: ['./bank-management.component.scss']
})
export class BankManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: Bank[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['name', 'swiftCode', 'phoneNumber', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private bankService: BankService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.bankService.getBanks(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Bank[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;
  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }


  addBank() {
    const modal = this.dialog.open(BankInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.editBank(null);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.page = 0;
      this.loadData();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  editBank(row: Bank) {
    const modal = this.dialog.open(BankInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.editBank(row);
    modal.componentInstance.changesSavedCallback = (bank: Bank) => {
      modal.close();
      Object.assign(row, bank);
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  deleteBank(row: Bank) {
    this.alertService.showDialog('Are you sure you want to delete \"' + row.name + '\"?',
      DialogType.confirm, () => this.deleteBankHelper(row));
  }

  deleteBankHelper(row: Bank) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Deleting...');
    this.loadingIndicator = true;

    this.bankService.deleteBank(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.page = 0;
        this.search = '';
        this.loadData();
      });
  }

}
