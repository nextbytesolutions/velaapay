import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Bank} from '../../../../../models/bank';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {BankService} from '../../services/bank.service';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';
import {ResponseModel} from '../../../../../models/response.model';

@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss']
})
export class BankInfoComponent implements OnInit {

  private isNewBank = false;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  bankEdit: Bank = new Bank();
  public changesSavedCallback: (bank: Bank) => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  bankNameControl: FormControl;
  bankSwiftCodeControl: FormControl;
  bankAddressControl: FormControl;
  bankPhoneNumberControl: FormControl;
  bankFormGroup: FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private alertService: AlertService, private bankService: BankService, private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
    // this.studyService.getStudies().subscribe(result => { this.allStudies = result });
  }

  initControls() {
    this.bankNameControl = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.bankSwiftCodeControl = new FormControl('', [Validators.required, CustomValidator.swiftCodeValidator(8, 11)]);
    this.bankPhoneNumberControl = new FormControl('', [Validators.required, Validators.maxLength(this.mask.length)]);
    this.bankAddressControl = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.bankFormGroup = new FormGroup({
      name: this.bankNameControl,
      swiftCode: this.bankSwiftCodeControl,
      address: this.bankAddressControl,
      phoneNumber: this.bankPhoneNumberControl,
    });
  }
  showErrorAlert(caption: string, message: string) {
    this.alertService.showMessage(caption, message, MessageSeverity.error);
  }

  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.bankEdit.name = this.bankEdit.name.trim();
    if (this.isNewBank) {
      this.bankService.createBank(this.bankEdit).subscribe(result => this.saveSuccessHelper(result));
    } else {
      this.bankService.updateBank(this.bankEdit).subscribe(result => this.saveSuccessHelper(result));
    }
  }


  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleBankCountChanged(this.bank, this.bankEdit);

    this.alertService.stopLoadingMessage();


    this.resetForm();
    if (this.isNewBank) {
      this.alertService.showMessage('Success', `Bank \"${this.bankEdit.name}\" was created successfully`,
        MessageSeverity.success);
    } else {
      this.alertService.showMessage('Success', `Changes to bank \"${this.bankEdit.name}\"  was saved successfully`,
        MessageSeverity.success);
    }
    if (this.changesSavedCallback) {
      this.changesSavedCallback(this.bankEdit);
    }
  }



  cancel() {
    this.bankEdit = new Bank();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.bankEdit = new Bank();
    this.resetForm();
  }
  resetForm() {
    this.bankFormGroup.reset();
  }

  editBank(bank: Bank) {
    this.initControls();
    if (bank) {
      this.isNewBank = false;

      this.bankEdit = new Bank();
      Object.assign(this.bankEdit, bank);
      this.bankEdit.id = bank.id;
      this.bankNameControl.setValue(this.bankEdit.name);
      this.bankSwiftCodeControl.setValue(this.bankEdit.swiftCode);
      this.bankAddressControl.setValue(this.bankEdit.address);
      this.bankPhoneNumberControl.setValue(this.bankEdit.phoneNumber);
      return this.bankEdit;
    } else {
      this.isNewBank = true;
      return new Bank();
    }
  }

  formSubmit() {
    if (this.bankFormGroup.valid) {
      this.bankEdit.name = this.bankNameControl.value;
      this.bankEdit.phoneNumber = this.bankPhoneNumberControl.value;
      this.bankEdit.swiftCode = this.bankSwiftCodeControl.value;
      this.bankEdit.address = this.bankAddressControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }
}
