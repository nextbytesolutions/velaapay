import {Injectable} from '@angular/core';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {Bank} from '../../../../models/bank';

@Injectable({
  providedIn: 'root'
})
export class BankEndpointService extends EndpointFactoryService {

  private readonly _createBankUrl: string = '/api/v1/bank';

  get BanksUrl() { return this.configurations.baseUrl + this._createBankUrl; }
  get createBankUrl() { return this.configurations.baseUrl + this._createBankUrl; }
  get updateBankUrl() { return this.configurations.baseUrl + this._createBankUrl; }
  get deleteBankUrl() { return this.configurations.baseUrl + this._createBankUrl; }

  getBanksEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.BanksUrl, { headers: this.getRequestHeaders().headers, params: params });
  }


  getCreateBanksEndpoint(bank: Bank): Observable<any> {
    const requestBody = {
      Address: bank.address,
      PhoneNumber: bank.phoneNumber,
      swiftCode: bank.swiftCode,
      Name: bank.name
    };
    return this.http.post(this.createBankUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }

  getUpdateBanksEndpoint(bank: Bank): Observable<any> {
    const requestBody = {
      bankId: bank.id,
      Address: bank.address,
      PhoneNumber: bank.phoneNumber,
      swiftCode: bank.swiftCode,
      Name: bank.name
    };
    return this.http.put(this.updateBankUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }



  getDeleteBanksEndpoint(bank: Bank): Observable<any> {
    return this.http.delete(this.deleteBankUrl + '/' + bank.id, { headers: this.getRequestHeaders().headers });
  }
}
