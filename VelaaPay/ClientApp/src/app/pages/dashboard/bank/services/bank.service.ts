import {Injectable} from '@angular/core';
import {BankEndpointService} from './bank-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {Bank} from '../../../../models/bank';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor(private bankEndpoint: BankEndpointService) {

  }

  getBanks(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.bankEndpoint.getBanksEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  deleteBank(bank: Bank) {
    return this.bankEndpoint.getDeleteBanksEndpoint(bank)
      .map((response) => <ResponseModel>response);
  }
  createBank(bank: Bank) {
    return this.bankEndpoint.getCreateBanksEndpoint(bank)
      .map((response) => <ResponseModel>response);
  }

  updateBank(bank: Bank) {
    return this.bankEndpoint.getUpdateBanksEndpoint(bank)
      .map((response) => <ResponseModel>response);
  }

}
