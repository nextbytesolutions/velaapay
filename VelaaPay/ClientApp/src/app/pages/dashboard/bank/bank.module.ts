import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {BankRoutingModule} from './bank-routing.module';
import {MaterialModule} from '../../../shared/material.module';
import {BankManagementComponent} from './components/bank-management/bank-management.component';
import {BankInfoComponent} from './components/bank-info/bank-info.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    BankRoutingModule,
    MaterialModule,
    PipeModule,
    TextMaskModule
  ],
  declarations: [BankManagementComponent, BankInfoComponent],
  entryComponents: [BankInfoComponent]
})
export class BankModule { }
