import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BankManagementComponent} from './components/bank-management/bank-management.component';

const routes: Routes = [
  {
    path: '',
    component: BankManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankRoutingModule { }
