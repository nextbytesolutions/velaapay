import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {CashOutTrack} from '../../../../../models/cash-out-track';
import {AlertService} from '../../../../../services/alert.service';
import {CashOutTrackService} from '../../services/cash-out-track.service';
import {ResponseModel} from '../../../../../models/response.model';
import {CashInTrackInfoComponent} from '../../../cash-in-track/components/cash-in-track-info/cash-in-track-info.component';
import {CashOutTrackInfoComponent} from '../cash-out-track-info/cash-out-track-info.component';

@Component({
  selector: 'app-cash-out-track-management',
  templateUrl: './cash-out-track-management.component.html',
  styleUrls: ['./cash-out-track-management.component.scss']
})
export class CashOutTrackManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: CashOutTrack[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['employee', 'customer', 'amount', 'shop', 'transactionId', 'createdDate'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private cashOutTrackService: CashOutTrackService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.cashOutTrackService.getCashOutTrack(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as CashOutTrack[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  addCashOut() {
    const modal = this.dialog.open(CashOutTrackInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.initForm();
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.loadData();
    };
  }
}
