import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {UserService} from '../../../users/services/user.service';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';
import {CashOutTrackService} from '../../services/cash-out-track.service';
import {CashOutTrack} from '../../../../../models/cash-out-track';

@Component({
  selector: 'app-cash-out-track-info',
  templateUrl: './cash-out-track-info.component.html',
  styleUrls: ['./cash-out-track-info.component.scss']
})
export class CashOutTrackInfoComponent implements OnInit {

  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  step = 1;
  customerId: string;
  cashOutFormGroup: FormGroup;
  cashOutAmountControl: FormControl;
  cashOutPhoneControl: FormControl;
  cashOutCustomerControl: FormControl;
  cashOutPinControl: FormControl;
  cashOutOtpControl: FormControl;
  constructor(private alertService: AlertService,
              private userService: UserService,
              private cashOutTrackService: CashOutTrackService) {

  }

  ngOnInit() {
  }

  initForm() {
    this.cashOutAmountControl = new FormControl('0', [Validators.required]);
    this.cashOutPinControl = new FormControl('', [Validators.required]);
    this.cashOutCustomerControl = new FormControl('', [Validators.required]);
    this.cashOutOtpControl = new FormControl('', [Validators.required]);
    this.cashOutPhoneControl = new FormControl('', [Validators.required, CustomValidator.Phone()]);
    this.cashOutFormGroup = new FormGroup({
      amount: this.cashOutAmountControl,
      phone: this.cashOutPhoneControl,
      customer: this.cashOutCustomerControl,
      pin: this.cashOutPinControl,
      otp: this.cashOutOtpControl,
    });
  }

  findUser() {
    if (this.cashOutPhoneControl.value && this.cashOutPhoneControl.valid) {
      this.alertService.startLoadingMessage('Searching...');
      this.userService.getUserByPhone(this.cashOutPhoneControl.value).subscribe(result => {
        this.alertService.stopLoadingMessage();
        if (result.data.id) {
          this.customerId = result.data.id;
          this.cashOutCustomerControl.setValue(result.data.name);
          this.step = 2;
        }
      });
    }
  }

  sendOtp() {
    if (this.cashOutCustomerControl.valid) {
      this.alertService.startLoadingMessage('Sending Otp...');
      this.userService.sendOtp(this.customerId).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.step = 3;
      });
    }
  }

  formSubmit() {
    if (this.cashOutFormGroup.valid) {
      const cashOut = new CashOutTrack();
      cashOut.amount = this.cashOutAmountControl.value;
      cashOut.customerId = this.customerId;
      cashOut.otp = this.cashOutOtpControl.value;
      cashOut.pin = this.cashOutPinControl.value;
      this.alertService.startLoadingMessage('Saving...');
      this.cashOutTrackService.createCashOut(cashOut).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', 'CashOut Created', MessageSeverity.success);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      });
    }
  }
  back() {
    this.step = 1;
  }
  cancel() {
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }

}
