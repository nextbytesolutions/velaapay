import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashOutTrackManagementComponent } from './components/cash-out-track-management/cash-out-track-management.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {MaterialModule} from '../../../shared/material.module';
import {CashOutTrackRoutingModule} from './cash-out-track-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {PipeModule} from '../../../pipes/pipe.module';
import { CashOutTrackInfoComponent } from './components/cash-out-track-info/cash-out-track-info.component';

@NgModule({
  declarations: [CashOutTrackManagementComponent, CashOutTrackInfoComponent],
  entryComponents: [CashOutTrackInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CashOutTrackRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CashOutTrackModule { }
