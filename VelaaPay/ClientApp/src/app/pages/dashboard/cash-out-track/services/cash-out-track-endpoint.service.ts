import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {CashOutTrack} from '../../../../models/cash-out-track';

@Injectable({
  providedIn: 'root'
})
export class CashOutTrackEndpointService extends EndpointFactoryService {

  private readonly _getCashOutTrackUrl: string = '/api/v1/cashOutTrack';
  private readonly _createCashOutUrl: string = '/api/v1/cashOut';

  get CashOutTracksUrl() { return this.configurations.baseUrl + this._getCashOutTrackUrl; }
  get CashOutUrl() { return this.configurations.baseUrl + this._createCashOutUrl; }
  getCashOutTracksEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CashOutTracksUrl, { headers: this.getRequestHeaders().headers, params: params });
  }

  getCreateCashOutEndpoint(cashOut: CashOutTrack) {
    return this.http.post(this.CashOutUrl, cashOut, {headers: this.getRequestHeaders().headers});
  }
}
