import {Injectable} from '@angular/core';
import {CashOutTrackEndpointService} from './cash-out-track-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {CashOutTrack} from '../../../../models/cash-out-track';

@Injectable({
  providedIn: 'root'
})
export class CashOutTrackService {


  constructor(private cashOutTrackService: CashOutTrackEndpointService) {

  }

  getCashOutTrack(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.cashOutTrackService.getCashOutTracksEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  createCashOut(cashOut: CashOutTrack) {
    return this.cashOutTrackService.getCreateCashOutEndpoint(cashOut)
      .map((response) => <ResponseModel>response);
  }
}
