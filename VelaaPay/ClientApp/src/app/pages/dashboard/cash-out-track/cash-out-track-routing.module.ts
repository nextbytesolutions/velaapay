import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CashOutTrackManagementComponent} from './components/cash-out-track-management/cash-out-track-management.component';

const routes: Routes = [
  {
    path: '',
    component: CashOutTrackManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CashOutTrackRoutingModule { }
