import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {TransactionService} from '../../services/transaction.service';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {LocalStoreManager} from '../../../../../services/local-store-manager.service';
import {RoleNames} from '../../../../../constants/role-names';
import {Transaction} from '../../../../../models/transaction';
import {SearchBoxComponent} from '../../../../../shared/search-box/components/search-box.component';
import {AuthService} from '../../../../../services/auth.service';
import {ResponseModel} from '../../../../../models/response.model';
import {LoadType} from '../../../../../models/enums';
import {Dbkey} from '../../../../../services/db-key';

@Component({
  selector: 'app-transaction-management',
  templateUrl: './transaction-management.component.html',
  styleUrls: ['./transaction-management.component.scss']
})
export class TransactionManagementComponent implements OnInit, AfterViewInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Transaction[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchBox') searchBox: SearchBoxComponent;
  dataSource: any;
  keyValue: any;
  displayedColumns: string[] = ['id', 'amount', 'senderName', 'receiverName', 'loadType', 'createdDate'];
  roles = [];
  isAdmin = false;
  constructor(private alertService: AlertService,
              private authService: AuthService,
              private localStorageManager: LocalStoreManager,
              private transactionService: TransactionService) {
  }


  ngOnInit() {
    this.dataSource  = new MatTableDataSource();
    this.keyValue = this.loadTypeEnumList();
    console.log(this.keyValue);
    this.page = 0;
    this.roles = this.authService.roles;
    if (this.roles.some(p => p === RoleNames.Administrator)) {
      this.isAdmin = true;
    }

  }
  ngAfterViewInit(): void {
    if (this.localStorageManager.exists(Dbkey.SAVED_DATA)) {
      const searchValue = this.localStorageManager.getData(Dbkey.SAVED_DATA);
      this.searchBox.setValue(searchValue);
      this.search = searchValue;
      this.localStorageManager.deleteData(Dbkey.SAVED_DATA);
    } else {
      this.loadData();
    }
  }




  loadData() {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.transactionService.getTransactions(this.loadType, this.page + 1, 10, this.search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Transaction[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.page = pageInfo.pageIndex;
    this.loadData();
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    this.page = 0;
    if (this.direction) {
      this.loadData();
    } else {
      this.sort = '';
      this.loadData();
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData();
  }

  loadTypeEnumList(): any {
    const keys = Object.keys(LoadType);
    const values = Object.values(LoadType);
    const keyValue = [];
    for (let i = 0; i < keys.length; i++) {
      if (isNaN(Number(keys[i] + ''))) {
        keyValue.push({
          key: values[i],
          value: values[i]
        });
      }
    }
    return keyValue;
  }

  changeType(type) {
    this.loadType = type.value;
    this.loadData();
  }
}
