import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionEndpointService extends EndpointFactoryService {
  private readonly _createTransactionUrl: string = '/api/v1/transaction';

  get TransactionsUrl() {
    return this.configurations.baseUrl + this._createTransactionUrl;
  }

  RevertTransactionsUrl(id: string) {
    return this.configurations.baseUrl + this._createTransactionUrl + '/' + id + '/revert';
  }

  getTransactionsEndpoint(type = 0, page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams({
      encoder: this.encoderService
    }).set('type', type + '').set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.TransactionsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  revertTransactionEndpoint(transactionId: string) {
    return this.http.post(this.RevertTransactionsUrl(transactionId), null, {headers: this.getRequestHeaders().headers});
  }
}
