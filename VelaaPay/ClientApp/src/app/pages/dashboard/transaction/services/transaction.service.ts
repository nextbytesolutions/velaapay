import {Injectable} from '@angular/core';
import {TransactionEndpointService} from './transaction-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private transactionEndpoint: TransactionEndpointService) {

  }

  getTransactions(type = 0, page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.transactionEndpoint.getTransactionsEndpoint(type, page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }



  revertTransaction(transactionId: string) {
    return this.transactionEndpoint.revertTransactionEndpoint(transactionId)
      .map((response) => <ResponseModel>response);

  }
}
