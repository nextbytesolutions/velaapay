import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {TransactionManagementComponent} from './components/transaction-management/transaction-management.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {TransactionRoutingModule} from './transaction-routing.module';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    TransactionRoutingModule,
    PipeModule,
    MaterialModule
  ],
  declarations: [TransactionManagementComponent]
})
export class TransactionModule { }
