import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {AuthGuard} from '../../gaurds/auth-gaurd.guard';
import {RoleNames} from '../../constants/role-names';

const routes: Routes = [{
  path: '',
  component: DashboardComponent,
  children: [
    {
      path: 'admin-dashboard',
      loadChildren: './admin-dashboard/admin-dashboard.module#AdminDashboardModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'users',
      loadChildren: './users/users.module#UsersModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'merchants',
      loadChildren: './merchant/merchant.module#MerchantModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'air-requests',
      loadChildren: './air-top-request/air-top-request.module#AirTopRequestModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'bill-requests',
      loadChildren: './bill-request/bill-request.module#BillRequestModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'cash-in-tracks',
      loadChildren: './cash-in-track/cash-in-track.module#CashInTrackModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Vendor, RoleNames.Supervisor, RoleNames.Cashier]
      }
    },
    {
      path: 'cash-out-tracks',
      loadChildren: './cash-out-track/cash-out-track.module#CashOutTrackModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Vendor, RoleNames.Supervisor, RoleNames.Cashier]
      }
    },
    {
      path: 'transactions',
      loadChildren: './transaction/transaction.module#TransactionModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator, RoleNames.Vendor, RoleNames.Supervisor, RoleNames.Cashier]
      }
    },
    {
      path: 'shops',
      loadChildren: './shop/shop.module#ShopModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator, RoleNames.Vendor]
      }
    },
    {
      path: 'employees',
      loadChildren: './employee/employee.module#EmployeeModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator, RoleNames.Supervisor, RoleNames.Vendor]
      }
    },
    {
      path: 'withdraw-requests',
      loadChildren: './withdraw-request/withdraw-request.module#WithdrawRequestModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'merchant-top-up-requests',
      loadChildren: './cash-in-request/cash-in-request.module#CashInRequestModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator],
      }
    },
    {
      path: 'cards',
      loadChildren: './card/card.module#CardModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'cash-in-requests',
      loadChildren: './cash-in-request-vendor/cash-in-request-vendor.module#CashInRequestVendorModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Vendor],
        isVerified: true,
      }
    },
    {
      path: 'user-details/:userId',
      loadChildren: './user-detail/user-detail.module#UserDetailModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'banks',
      loadChildren: './bank/bank.module#BankModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'companies',
      loadChildren: './company/company.module#CompanyModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: 'settings',
      loadChildren: './setting/setting.module#SettingModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator, RoleNames.Vendor]
      }
    },
    {
      path: 'requests',
      loadChildren: './request/request.module#RequestModule',
      canActivateChild: [AuthGuard],
      data: {
        role: [RoleNames.Administrator]
      }
    },
    {
      path: '',
      redirectTo: 'admin-dashboard',
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
