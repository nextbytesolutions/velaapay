import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Merchant} from '../../../../models/merchant';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MerchantEndpointService extends EndpointFactoryService {

  private readonly _getMerchantUrl: string = '/api/v1/merchant';
  private readonly _getMerchantDetailUrl: string = '/api/v1/merchant/detail';
  private readonly _getUpdateMerchantUrl: string = '/api/v1/merchant/update';
  private readonly _getUpdateMerchantByUserUrl: string = '/api/v1/merchant/update/business-info';
  private readonly _getBusinessInfoMerchantUrl: string = '/api/v1/merchant/business-info';
  private readonly _getUpdateMerchantPasswordUrl: string = '/api/v1/merchant/update/password';
  private readonly _getUpdateMerchantEnableStatusUrl: string = '/api/v1/merchant/enable';
  private readonly _getUpdateMerchantDisableStatusUrl: string = '/api/v1/merchant/disable';
  private readonly _getApproveMerchantStatusUrl: string = '/api/v1/merchant/approve';

  get MerchantsUrl() {
    return this.configurations.baseUrl + this._getMerchantUrl;
  }
  get MerchantDetailUrl() {
    return this.configurations.baseUrl + this._getMerchantDetailUrl;
  }
  get MerchantUpdateUrl() {
    return this.configurations.baseUrl + this._getUpdateMerchantUrl;
  }
  get BusinessInfoMerchantUrl() {
    return this.configurations.baseUrl + this._getBusinessInfoMerchantUrl;
  }
  get MerchantUpdateUrlByUser() {
    return this.configurations.baseUrl + this._getUpdateMerchantByUserUrl;
  }
  get MerchantUpdatePasswordUrl() {
    return this.configurations.baseUrl + this._getUpdateMerchantPasswordUrl;
  }
  get UpdateMerchantEnableStatusUrl() {
    return this.configurations.baseUrl + this._getUpdateMerchantEnableStatusUrl;
  }
  get UpdateMerchantDisableStatusUrl() {
    return this.configurations.baseUrl + this._getUpdateMerchantDisableStatusUrl;
  }
  get ApproveMerchantStatusUrl() {
    return this.configurations.baseUrl + this._getApproveMerchantStatusUrl;
  }
  getMerchantsEndpoint(role: string, verificationStatus: string, page = 1, pageSize = 10,
                       search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('status', verificationStatus + '')
      .set('role', role + '').set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.MerchantsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getBlockMerchantsEndpoint(merchant: Merchant) {
    return this.http.put(this.UpdateMerchantDisableStatusUrl + '/' + merchant.id, null, {headers: this.getRequestHeaders().headers});
  }
  getBusinessInfoMerchantEndpoint(merchant?: Merchant) {
    if (merchant) {
      return this.http.get(this.BusinessInfoMerchantUrl + '/' + merchant.id, {headers: this.getRequestHeaders().headers});
    }
    return this.http.get(this.BusinessInfoMerchantUrl + '/undefined', {headers: this.getRequestHeaders().headers});
  }
  getUnBlockMerchantsEndpoint(merchant: Merchant) {
    return this.http.put(this.UpdateMerchantEnableStatusUrl + '/' + merchant.id, null, {headers: this.getRequestHeaders().headers});
  }

  getApproveMerchantsEndpoint(merchant: Merchant) {
    return this.http.put(this.MerchantsUrl + '/' + merchant.id + '/approve/' + merchant.role,
      null, {headers: this.getRequestHeaders().headers});
  }
  getMerchantDetailEndpoint(merchantId: string) {
    return this.http.get(this.MerchantDetailUrl + '/' + merchantId, {headers: this.getRequestHeaders().headers});
  }

  getUpdateMerchantEndpoint(merchant: Merchant) {
    const requestBody = {
      rccm: merchant.rccm,
      rccmImage: merchant.rccmImage,
      identification: merchant.identificationNumber,
      identificationImage: merchant.identificationImage,
      ntnNumber: merchant.ntnNumber,
    };
    return this.http.put(this.MerchantUpdateUrl + '/' + merchant.id, requestBody, {headers: this.getRequestHeaders().headers});
  }

  getUpdateMerchantPasswordEndpoint(merchant: Merchant) {
    const requestBody = {
      pin: merchant.password,
      confirmPin: merchant.confirmPassword,
    };
    return this.http.put(this.MerchantUpdatePasswordUrl + '/' + merchant.id, requestBody, {headers: this.getRequestHeaders().headers});

  }

  getUpdateMerchantByUserEndpoint(merchant: Merchant) {
    const requestBody = {
      rccm: merchant.rccm,
      rccmImage: merchant.rccmImage,
      identification: merchant.identificationNumber,
      identificationImage: merchant.identificationImage,
      ntnNumber: merchant.ntnNumber,
    };
    return this.http.put(this.MerchantUpdateUrlByUser, requestBody, {headers: this.getRequestHeaders().headers});

  }

  getVerifyMerchantsEndpoint(merchant: Merchant) {
    return this.http.put(this.MerchantsUrl + '/verify/' + merchant.id, null,
      {headers: this.getRequestHeaders().headers});
  }
}
