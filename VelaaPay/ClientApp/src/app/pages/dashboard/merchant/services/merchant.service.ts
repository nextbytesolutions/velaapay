import {Injectable} from '@angular/core';
import {Merchant} from '../../../../models/merchant';
import {ResponseModel} from '../../../../models/response.model';
import {MerchantEndpointService} from './merchant-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  constructor(private merchantEndpointService: MerchantEndpointService) {

  }

  getMerchants(role: string, verificationStatus: string, page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.merchantEndpointService.getMerchantsEndpoint(role, verificationStatus, page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  getMerchantDetail(merchantId: string) {
    return this.merchantEndpointService.getMerchantDetailEndpoint(merchantId)
      .map((response) => <ResponseModel>response);
  }

  blockMerchant(merchant: Merchant) {
    return this.merchantEndpointService.getBlockMerchantsEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }
  getBusinessInfoMerchant(merchant?: Merchant) {
    return this.merchantEndpointService.getBusinessInfoMerchantEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }
  unBlockMerchant(merchant: Merchant) {
    return this.merchantEndpointService.getUnBlockMerchantsEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }

  approveMerchant(merchant: Merchant) {
    return this.merchantEndpointService.getApproveMerchantsEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }


  updateMerchant(merchant: Merchant) {
    return this.merchantEndpointService.getUpdateMerchantEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }
  updateMerchantByUser(merchant: Merchant) {
    return this.merchantEndpointService.getUpdateMerchantByUserEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }

  updateMerchantPassword(merchant: Merchant) {
    return this.merchantEndpointService.getUpdateMerchantPasswordEndpoint(merchant)
      .map((response) => <ResponseModel>response);

  }

  verifyMerchant(merchant: Merchant) {
    return this.merchantEndpointService.getVerifyMerchantsEndpoint(merchant)
      .map((response) => <ResponseModel>response);

  }
}
