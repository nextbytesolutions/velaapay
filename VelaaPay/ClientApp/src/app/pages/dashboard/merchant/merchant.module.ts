import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantManagementComponent } from './components/merchant-management/merchant-management.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MerchantRoutingModule} from './merchant-routing.module';
import { MerchantDetailComponent } from './components/merchant-detail/merchant-detail.component';
import { MerchantVerifyInfoComponent } from './components/merchant-verify-info/merchant-verify-info.component';
import { MerchantEditInfoComponent } from './components/merchant-edit-info/merchant-edit-info.component';
import { MerchantPasswordComponent } from './components/merchant-password/merchant-password.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {MaterialModule} from '../../../shared/material.module';
import {QRCodeModule} from "angularx-qrcode";

@NgModule({
  entryComponents: [MerchantVerifyInfoComponent, MerchantEditInfoComponent, MerchantPasswordComponent],
  declarations: [MerchantManagementComponent, MerchantDetailComponent, MerchantVerifyInfoComponent,
    MerchantEditInfoComponent, MerchantPasswordComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    MerchantRoutingModule,
    MaterialModule,
    PipeModule,
    QRCodeModule
  ]
})
export class MerchantModule { }
