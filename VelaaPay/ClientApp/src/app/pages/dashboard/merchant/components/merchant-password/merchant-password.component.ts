import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Merchant} from '../../../../../models/merchant';
import {ResponseModel} from '../../../../../models/response.model';
import {MerchantService} from '../../services/merchant.service';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';

@Component({
  selector: 'app-merchant-password',
  templateUrl: './merchant-password.component.html',
  styleUrls: ['./merchant-password.component.scss']
})
export class MerchantPasswordComponent implements OnInit {


  get isSaving() {
   return this.alertService.isLoadingInProgress;
  }
  merchantEdit: Merchant = new Merchant();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  merchantPasswordControl: FormControl;
  merchantConfirmPasswordControl: FormControl;
  merchantFormGroup: FormGroup;

  constructor(private alertService: AlertService,
              private merchantService: MerchantService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }


  initForm() {
    this.merchantPasswordControl = new FormControl('', [Validators.required]);
    this.merchantConfirmPasswordControl = new FormControl('', [Validators.required,
      CustomValidator.PasswordMatch(this.merchantPasswordControl)]);
    this.merchantFormGroup = new FormGroup({
      password: this.merchantPasswordControl,
      confirmPassword: this.merchantConfirmPasswordControl,
    });

  }

  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.merchantService.updateMerchantPassword(this.merchantEdit).subscribe(result => this.saveSuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.resetForm();
    this.alertService.showMessage('Success', `Changes to merchant \"${this.merchantEdit.fullName}\"  was saved successfully`
      , MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }




  cancel() {
    this.merchantEdit = new Merchant();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.merchantEdit = new Merchant();
    this.resetForm();
  }


  resetForm() {
    this.merchantFormGroup.reset();
  }



  editMerchant(merchant: Merchant) {
    this.initForm();
    if (merchant) {
      this.merchantEdit = new Merchant();
      Object.assign(this.merchantEdit, merchant);
      this.merchantEdit.userId = merchant.userId;
      return this.merchantEdit;
    } else {
      return new Merchant();
    }
  }

  formSubmit() {
    if (this.merchantFormGroup.valid) {
      this.merchantEdit.password = this.merchantPasswordControl.value;
      this.merchantEdit.confirmPassword = this.merchantConfirmPasswordControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }


}
