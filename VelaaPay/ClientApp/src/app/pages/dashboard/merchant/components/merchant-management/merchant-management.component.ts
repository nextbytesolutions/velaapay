import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {MerchantVerifyInfoComponent} from '../merchant-verify-info/merchant-verify-info.component';
import {MerchantPasswordComponent} from '../merchant-password/merchant-password.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {RoleNames} from '../../../../../constants/role-names';
import {Merchant} from '../../../../../models/merchant';
import {ResponseModel} from '../../../../../models/response.model';
import {MerchantService} from '../../services/merchant.service';
import {MerchantEditInfoComponent} from '../merchant-edit-info/merchant-edit-info.component';

@Component({
  selector: 'app-merchant-management',
  templateUrl: './merchant-management.component.html',
  styleUrls: ['./merchant-management.component.scss']
})
export class MerchantManagementComponent implements OnInit {

  verificationStatus = 'All';
  role = 'All';
  roles = [RoleNames.Vendor];
  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: Merchant[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;

  @ViewChild('editorModal')
  editorModal: any;


  displayedColumns: string[] = ['fullName', 'balance', 'phoneNumber', 'isEnabled', 'isVerified', 'createdDate', 'action'];

  constructor(private alertService: AlertService,
              private merchantService: MerchantService,
              private dialog: MatDialog,
              private router: Router,
              private activedRoute: ActivatedRoute) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.activedRoute.params
      .subscribe(() => {
        this.page = 0;
        this.loadData();
      });


  }


  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.page = page - 1;
    this.merchantService.getMerchants(this.role, this.verificationStatus, page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    const data = result.data as Merchant[];
    data.forEach((merchant, index) => {
      (<any>merchant).index = index + 1;
    });
    this.rows = data;
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

    // this.allRoles = roles;
  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
    // this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.merchantText));
  }

  blockMerchant(row: Merchant) {
    this.alertService.showDialog('Are you sure you want to block \"' + row.fullName + '\"?', DialogType.confirm,
      () => this.blockMerchantHelper(row), () => this.alertService.closeDialog());
  }

  unBlockMerchant(row: Merchant) {
    this.alertService.showDialog('Are you sure you want to unblock \"' + row.fullName + '\"?', DialogType.confirm,
      () => this.unBlockMerchantHelper(row), () => this.alertService.closeDialog());
  }

  blockMerchantHelper(row: Merchant) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Saving...');
    this.loadingIndicator = true;

    this.merchantService.blockMerchant(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        row.isEnabled = false;
      });
  }

  unBlockMerchantHelper(row: Merchant) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Saving...');
    this.loadingIndicator = true;
    this.merchantService.unBlockMerchant(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        row.isEnabled = true;
      });
  }


  viewMerchant(row: Merchant) {
    this.router.navigateByUrl(`/dashboard/merchants/detail/${row.id}`);
  }

  approveMerchant(row: Merchant) {
    this.alertService.showDialog('Are you sure you want to approve \"' + row.fullName + '\"?', DialogType.confirm,
      () => {
        this.approveMerchantHelper(row);
      }, () => this.alertService.closeDialog());
  }

  approveMerchantHelper(row: Merchant) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Saving...');
    this.loadingIndicator = true;
    this.merchantService.approveMerchant(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        row.isApproved = true;
        row.isEnabled = true;
        row.isVerified = true;
        this.alertService.showMessage('Success', 'Merchant Approved and Enabled Successfully', MessageSeverity.success);
      });
  }

  onRoleChange(event: any) {
    this.role = event;
    this.page = 0;
    this.search = '';
    this.loadData();
  }

  onVerificationStatusChange(event: any) {
    this.verificationStatus = event;
    this.page = 0;
    this.search = '';
    this.loadData();
  }

  verifyMerchant(row: Merchant) {
    if (!row.ntnNumber) {
      this.merchantService.getBusinessInfoMerchant(row).subscribe(result => {
        row.ntnNumber = result.data.ntnNumber;
        row.rccm = result.data.rccm;
        row.rccmImage = result.data.rccmImage;
        row.identificationNumber = result.data.identification;
        row.identificationImage = result.data.identificationImage;
        this.openVerifyDialog(row);
      });
    } else {
      this.openVerifyDialog(row);
    }
  }
  openVerifyDialog(row: Merchant) {
    const modal = this.dialog.open(MerchantVerifyInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });

    modal.componentInstance.setMerchant(row);

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      row.isVerified = true;
      modal.close();
    };
  }

  editMerchant(row: Merchant) {
    const modal = this.dialog.open(MerchantEditInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });

    modal.componentInstance.editMerchant(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = (merchant: Merchant) => {
      Object.assign(row, merchant);
      modal.close();
    };
  }

  changePassword(row: Merchant) {
    const modal = this.dialog.open(MerchantPasswordComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });

    modal.componentInstance.editMerchant(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
    };
  }

}
