import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MerchantService} from '../../services/merchant.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Merchant} from '../../../../../models/merchant';

@Component({
  selector: 'app-merchant-verify-info',
  templateUrl: './merchant-verify-info.component.html',
  styleUrls: ['./merchant-verify-info.component.scss']
})
export class MerchantVerifyInfoComponent implements OnInit {

  private isNewMerchant = false;
  isSaving = false;
  merchant: Merchant = new Merchant();
  public changesCancelledCallback: () => void;
  public changesSavedCallback: () => void;

  cnicNameControl: FormControl;
  ntnNameControl: FormControl;
  cnicImageControl: FormControl;
  cnicFormGroup: FormGroup;
  rccmNumberControl: FormControl;
  rccmNumberImageControl: FormControl;

  constructor(private alertService: AlertService, private merchantService: MerchantService) {
    this.cnicNameControl = new FormControl('', [Validators.required]);
    this.ntnNameControl = new FormControl('', [Validators.required]);
    this.rccmNumberControl = new FormControl('', [Validators.required]);
    this.cnicImageControl = new FormControl(null, [Validators.required]);
    this.rccmNumberImageControl = new FormControl(null, [Validators.required]);
    this.cnicFormGroup = new FormGroup({
      name: this.cnicNameControl,
      rccm: this.rccmNumberControl,
      rccmImage: this.rccmNumberImageControl,
      ntnName: this.ntnNameControl,
      image: new FormControl(''),
      temp: this.cnicImageControl,
    });
  }

  ngOnInit() {

  }


  cancel() {
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  resetForm() {
    this.cnicFormGroup.reset();
  }

  verifyMerchant() {
    if (this.cnicFormGroup.valid) {
      this.merchantService.verifyMerchant(this.merchant)
        .subscribe(results => {
          this.alertService.stopLoadingMessage();
          this.alertService.showMessage('Success', 'Merchant Verified and Enabled Successfully', MessageSeverity.success);
          if (this.changesSavedCallback) {
            this.changesSavedCallback();
          }
        });
    }
  }

  setMerchant(merchant: Merchant) {
    if (merchant) {
      Object.assign(this.merchant, merchant);
      this.rccmNumberControl.setValue(merchant.rccm);
      this.cnicNameControl.setValue(merchant.identificationNumber);
      this.ntnNameControl.setValue(merchant.ntnNumber);
      if (merchant.identificationImage) {
        this.cnicImageControl.setValue(merchant.identificationImage);
      }
      if (merchant.rccmImage) {
        this.rccmNumberImageControl.setValue(merchant.rccmImage);
      }
    }
  }
}
