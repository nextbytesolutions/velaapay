import {Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {MerchantDetail} from '../../../../../models/merchant-detail';
import {ActivatedRoute, Router} from '@angular/router';
import {MerchantService} from '../../services/merchant.service';
import {LocalStoreManager} from '../../../../../services/local-store-manager.service';
import {Location} from '@angular/common';
import {Dbkey} from '../../../../../services/db-key';

@Component({
  selector: 'app-merchant-detail',
  templateUrl: './merchant-detail.component.html',
  styleUrls: ['./merchant-detail.component.scss']
})
export class MerchantDetailComponent implements OnInit {


  merchantId: string;
  merchantDetail: MerchantDetail;
  displayedTransactionColumns: string[] = ['transactionCode', 'amount', 'senderName', 'receiverName', 'loadType', 'date'];

  constructor(private activatedRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router,
              private location: Location,
              private localStorageManager: LocalStoreManager,
              private merchantDetailService: MerchantService
  ) { }

  getDetails() {
    if (this.merchantId) {
      this.alertService.startLoadingMessage();
      this.merchantDetailService.getMerchantDetail(this.merchantId).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.merchantDetail = result.data;
      });
    } else {
      this.router.navigate(['merchants']);
    }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(p => {
      this.merchantId = p['merchantId'];
      this.getDetails();
    });
  }

  goBack() {
    this.location.back();
  }

  viewMoreTransactions() {
    this.localStorageManager.saveSessionData(this.merchantId, Dbkey.SAVED_DATA);
    this.router.navigateByUrl('/dashboard/transactions');
  }
}
