import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Merchant} from '../../../../../models/merchant';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {MerchantService} from '../../services/merchant.service';
import {ResponseModel} from '../../../../../models/response.model';

@Component({
  selector: 'app-merchant-edit-info',
  templateUrl: './merchant-edit-info.component.html',
  styleUrls: ['./merchant-edit-info.component.scss']
})
export class MerchantEditInfoComponent implements OnInit {

  public changesSavedCallback: (merchant: Merchant) => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  merchant: Merchant = new Merchant();
  merchantNameControl: FormControl;
  merchantNtnControl: FormControl;
  merchantRccmControl: FormControl;
  merchantRccmImageControl: FormControl;
  merchantCnicControl: FormControl;
  merchantCnicImageControl: FormControl;
  merchantGroup: FormGroup;
  isSaving = false;

  constructor(private alertService: AlertService, private merchantService: MerchantService, private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }

  initForm() {
    this.merchantNameControl = new FormControl('', [Validators.required]);
    this.merchantNtnControl = new FormControl('',
      [Validators.required, Validators.minLength(9), Validators.maxLength(9)]);
    this.merchantRccmControl = new FormControl('', [Validators.required]);
    this.merchantRccmImageControl = new FormControl('', [Validators.required]);
    this.merchantCnicControl = new FormControl('',
      [Validators.required, Validators.pattern('^(?=[a-zA-Z0-9]*$)(?:.{13}|.{17})$')]);
    this.merchantCnicImageControl = new FormControl('', [Validators.required]);
    this.merchantGroup = new FormGroup({
      name: this.merchantNameControl,
      cnic: this.merchantCnicControl,
      ntn: this.merchantNtnControl,
      rccm: this.merchantRccmControl,
      rccmImage: this.merchantRccmImageControl,
      cnicImage: this.merchantCnicImageControl,
    });
  }

  editMerchant(merchant: Merchant) {
    this.initForm();
    if (merchant) {
      Object.assign(this.merchant, merchant);
      this.merchantNameControl.setValue(this.merchant.fullName);
      this.merchantNtnControl.setValue(this.merchant.ntnNumber);
      this.merchantRccmControl.setValue(this.merchant.rccm);
      this.merchantRccmImageControl.setValue(this.merchant.rccmImage);
      this.merchantCnicControl.setValue(this.merchant.identificationNumber);
      this.merchantCnicImageControl.setValue(this.merchant.identificationImage);
    }
  }

  formSubmit() {
    if (this.merchantGroup.valid) {
      this.merchant.fullName = this.merchantNameControl.value;
      this.merchant.ntnNumber = this.merchantNtnControl.value;
      this.merchant.rccm = this.merchantRccmControl.value;
      this.merchant.rccmImage = this.merchantRccmImageControl.value;
      this.merchant.identificationImage = this.merchantCnicImageControl.value;
      this.merchant.identificationNumber = this.merchantCnicControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }

  private save() {
    this.alertService.resetStickyMessage();
    this.isSaving = true;
    this.alertService.startLoadingMessage('Saving changes...');
    this.merchantService.updateMerchant(this.merchant).subscribe(result => this.saveSuccessHelper(result));
  }

  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleBankCountChanged(this.bank, this.bankEdit);

    this.isSaving = false;
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', `Merchant \"${this.merchant.fullName}\" was updated successfully`, MessageSeverity.success);
    this.merchant.rccmImage = result.data.rccmImage;
    this.merchant.identificationImage = result.data.identificationImage;
    if (this.changesSavedCallback) {
      this.changesSavedCallback(this.merchant);
    }
  }

  onFileAddedIdentification(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.identificationImage = e.currentTarget.result;
        this.merchantCnicImageControl.setValue(this.merchant.identificationImage);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  onFileAddedRccm(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.rccmImage = e.currentTarget.result;
        this.merchantRccmImageControl.setValue(this.merchant.rccmImage);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }

  }

  cancel() {
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
}
