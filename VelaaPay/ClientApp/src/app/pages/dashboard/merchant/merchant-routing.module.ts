import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MerchantManagementComponent} from './components/merchant-management/merchant-management.component';
import {MerchantDetailComponent} from './components/merchant-detail/merchant-detail.component';

const routes: Routes = [
  {
    path: '',
    component: MerchantManagementComponent
  },
  {
    path: 'detail/:merchantId',
    component: MerchantDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantRoutingModule { }
