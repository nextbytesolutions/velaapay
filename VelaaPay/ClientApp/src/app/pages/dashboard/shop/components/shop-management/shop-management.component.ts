import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {Shop} from '../../../../../models/shop';
import {ShopInfoComponent} from '../shop-info/shop-info.component';
import {ShopService} from '../../services/shop.service';
import {ResponseModel} from '../../../../../models/response.model';
import {AuthService} from "../../../../../services/auth.service";

@Component({
  selector: 'app-shop-management',
  templateUrl: './shop-management.component.html',
  styleUrls: ['./shop-management.component.scss']
})
export class ShopManagementComponent implements OnInit {

  link = '';
  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: Shop[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['name', 'phoneNumber', 'address', 'startTime', 'endTime', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private router: Router,
              private authService: AuthService,
              private shopService: ShopService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  getPhone(): string{
    return this.authService.currentUser.phoneNumber;
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.shopService.getShops(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Shop[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }


  addShop() {
    const modal = this.dialog.open(ShopInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.editShop(null);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.page = 0;
      this.loadData();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  editShop(row: Shop) {
    const modal = this.dialog.open(ShopInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.editShop(row);
    modal.componentInstance.changesSavedCallback = (shop: Shop) => {
      modal.close();
      Object.assign(row, shop);
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  showDetails(row: Shop) {
    this.router.navigateByUrl('dashboard/shops/details/' + row.shopId);
  }

  deleteShop(row: Shop) {
    this.alertService.showDialog('Are you sure you want to delete \"' + row.name + '\"?', DialogType.confirm,
      () => this.deleteShopHelper(row));
  }


  deleteShopHelper(row: Shop) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Deleting...');
    this.loadingIndicator = true;

    this.shopService.deleteShop(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.page = 0;
        this.search = '';
        this.loadData();
      });
  }
  private getElementByXpath(path) {
    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
  }
  downloadQrCode() {
    ////*[@id="qrCode"]/img
    const img = this.getElementByXpath("//*[@id=\"qrCode\"]/img") as HTMLImageElement;
    const y = document.getElementById('hiddenQrCode') as HTMLAnchorElement;
    y.href = img.src;
    y.click();
  }
}
