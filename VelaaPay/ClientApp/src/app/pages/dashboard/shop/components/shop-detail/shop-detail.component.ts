import { Component, OnInit } from '@angular/core';
import {ShopService} from '../../services/shop.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {ShopDetail} from '../../../../../models/shop-detail';

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit {

  saleDisplayedColumns: string[] = ['cashier', 'amount', 'name', 'shop', 'date'];
  cashInDisplayedColumns: string[] = ['employee', 'customer', 'amount', 'shop', 'createdDate'];
  cashOutDisplayedColumns: string[] = ['employee', 'customer', 'amount', 'shop', 'status', 'createdDate'];

  isLoading = false;
  shopId: number;
  shopDetail: ShopDetail;
  constructor(private shopService: ShopService, private alertService: AlertService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.shopId = p['shopId'];
      this.isLoading = true;
      this.alertService.startLoadingMessage();
      this.shopService.getShopDetails(this.shopId).subscribe(result => {
        this.isLoading = false;
        this.alertService.stopLoadingMessage();
        this.shopDetail = result.data as ShopDetail;
      });
    });
  }

}
