import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ShopService} from '../../services/shop.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Shop} from '../../../../../models/shop';
import {ResponseModel} from '../../../../../models/response.model';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';

@Component({
  selector: 'app-shop-info',
  templateUrl: './shop-info.component.html',
  styleUrls: ['./shop-info.component.scss']
})
export class ShopInfoComponent implements OnInit {

  private isNewShop = false;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  shopEdit: Shop = new Shop();
  public changesSavedCallback: (shop: Shop) => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  shopNameControl: FormControl;
  shopStartTimeControl: FormControl;
  shopEndTimeControl: FormControl;
  shopAddressControl: FormControl;
  shopPhoneNumberControl: FormControl;
  shopFormGroup: FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public timeMask = [/[0-1]/, /\d/, ':', /[0-5]/, /\d/, ' ', /[AP]/, 'M'];

  constructor(private alertService: AlertService, private shopService: ShopService, private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
    // this.studyService.getStudies().subscribe(result => { this.allStudies = result });

  }

  initControls() {
    this.shopNameControl = new FormControl('',
      [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.shopStartTimeControl = new FormControl('',
      [Validators.required, Validators.maxLength(this.timeMask.length), CustomValidator.NoUnderScore()]);
    this.shopEndTimeControl = new FormControl('',
      [Validators.required, Validators.maxLength(this.timeMask.length), CustomValidator.NoUnderScore()]);
    this.shopPhoneNumberControl = new FormControl('',
      [Validators.required, Validators.maxLength(this.mask.length), CustomValidator.NoUnderScore()]);
    this.shopAddressControl = new FormControl('',
      [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.shopFormGroup = new FormGroup({
      name: this.shopNameControl,
      address: this.shopAddressControl,
      phoneNumber: this.shopPhoneNumberControl,
      startTime: this.shopStartTimeControl,
      endTime: this.shopEndTimeControl,
    });
  }

  save() {
    if (this.shopEdit.startTime.includes('_') || this.shopEdit.endTime.includes('_')) {
      this.alertService.showMessage('Error', 'Incorrect startTime and endTime format', MessageSeverity.error);
      return;
    }
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.shopEdit.name = this.shopEdit.name.trim();

    if (this.isNewShop) {
      this.shopService.createShop(this.shopEdit).subscribe(result => this.saveSuccessHelper(result));
    } else {
      this.shopService.updateShop(this.shopEdit).subscribe(result => this.saveSuccessHelper(result));
    }
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();


    this.resetForm();
    if (this.isNewShop) {
      this.alertService.showMessage('Success', `Shop \"${this.shopEdit.name}\" was created successfully`, MessageSeverity.success);
    } else {
      this.alertService.showMessage('Success', `Changes to shop \"${this.shopEdit.name}\"  was saved successfully`,
        MessageSeverity.success);
    }
    if (this.changesSavedCallback) {
      this.changesSavedCallback(this.shopEdit);
    }
  }




  cancel() {
    this.shopEdit = new Shop();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.shopEdit = new Shop();
    this.resetForm();
  }


  resetForm() {
    this.shopFormGroup.reset();
  }

  editShop(shop: Shop) {
    this.initControls();
    if (shop) {
      this.isNewShop = false;

      this.shopEdit = new Shop();
      Object.assign(this.shopEdit, shop);
      this.shopEdit.shopId = shop.shopId;
      this.shopNameControl.setValue(this.shopEdit.name);
      this.shopStartTimeControl.setValue(this.shopEdit.startTime);
      this.shopEndTimeControl.setValue(this.shopEdit.endTime);
      this.shopAddressControl.setValue(this.shopEdit.address);
      this.shopPhoneNumberControl.setValue(this.shopEdit.phoneNumber);
      return this.shopEdit;
    } else {
      this.isNewShop = true;
      return new Shop();
    }
  }

  formSubmit() {
    if (this.shopFormGroup.valid) {
      this.shopEdit.name = this.shopNameControl.value;
      this.shopEdit.phoneNumber = this.shopPhoneNumberControl.value;
      this.shopEdit.startTime = this.shopStartTimeControl.value;
      this.shopEdit.endTime = this.shopEndTimeControl.value;
      this.shopEdit.address = this.shopAddressControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }
}
