import {Injectable} from '@angular/core';
import {Shop} from '../../../../models/shop';
import {ResponseModel} from '../../../../models/response.model';
import {ShopEndpointService} from './shop-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(private shopEndpoint: ShopEndpointService) {

  }

  getShops(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.shopEndpoint.getShopsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  deleteShop(shop: Shop) {
    return this.shopEndpoint.getDeleteShopsEndpoint(shop)
      .map((response) => <ResponseModel>response);
  }
  createShop(shop: Shop) {
    return this.shopEndpoint.getCreateShopsEndpoint(shop)
      .map((response) => <ResponseModel>response);
  }

  updateShop(shop: Shop) {
    return this.shopEndpoint.getUpdateShopsEndpoint(shop)
      .map((response) => <ResponseModel>response);
  }

  getComboBoxShops() {
    return this.shopEndpoint.getComboBoxShopsEndpoint()
      .map((response) => <ResponseModel>response);

  }

  getShopDetails(shopId: number) {
    return this.shopEndpoint.getShopDetailsEndpoint(shopId)
      .map((response) => <ResponseModel>response);

  }
}
