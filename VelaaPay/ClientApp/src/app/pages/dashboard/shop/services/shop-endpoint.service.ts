import {Injectable} from '@angular/core';
import {Shop} from '../../../../models/shop';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class ShopEndpointService extends EndpointFactoryService {

  private readonly _createShopUrl: string = '/api/v1/shop';

  get ShopsUrl() { return this.configurations.baseUrl + this._createShopUrl; }
  get createShopUrl() { return this.configurations.baseUrl + this._createShopUrl; }
  get updateShopUrl() { return this.configurations.baseUrl + this._createShopUrl; }
  get deleteShopUrl() { return this.configurations.baseUrl + this._createShopUrl; }

  getShopsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.ShopsUrl, { headers: this.getRequestHeaders().headers, params: params });
  }


  getCreateShopsEndpoint(shop: Shop): Observable<any> {
    const requestBody = {
      address: shop.address,
      phoneNumber: shop.phoneNumber,
      startTime: shop.startTime,
      endTime: shop.endTime,
      name: shop.name
    };
    return this.http.post(this.createShopUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }

  getUpdateShopsEndpoint(shop: Shop): Observable<any> {
    const requestBody = {
      shopId: shop.shopId,
      address: shop.address,
      phoneNumber: shop.phoneNumber,
      startTime: shop.startTime,
      endTime: shop.endTime,
      name: shop.name
    };
    return this.http.put(this.updateShopUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }



  getDeleteShopsEndpoint(shop: Shop): Observable<any> {
    return this.http.delete(this.deleteShopUrl + '/' + shop.shopId, { headers: this.getRequestHeaders().headers });
  }

  getComboBoxShopsEndpoint() {
    return this.http.get(this.ShopsUrl + '/comboBox', { headers: this.getRequestHeaders().headers });
  }

  getShopDetailsEndpoint(shopId: number) {
    return this.http.get(this.ShopsUrl + '/detail/' + shopId, { headers: this.getRequestHeaders().headers });
  }
}
