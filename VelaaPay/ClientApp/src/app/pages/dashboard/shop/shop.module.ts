import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {ShopManagementComponent} from './components/shop-management/shop-management.component';
import {ShopInfoComponent} from './components/shop-info/shop-info.component';
import {PipeModule} from '../../../pipes/pipe.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {ShopDetailComponent} from './components/shop-detail/shop-detail.component';
import {ShopRoutingModule} from './shop-routing.module';
import {MaterialModule} from '../../../shared/material.module';
import {TextMaskModule} from 'angular2-text-mask';
import {QRCodeModule} from "angularx-qrcode";

@NgModule({
  declarations: [ShopManagementComponent, ShopInfoComponent, ShopDetailComponent],
  entryComponents: [ShopInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    ShopRoutingModule,
    MaterialModule,
    PipeModule,
    TextMaskModule,
    QRCodeModule
  ]
})
export class ShopModule { }
