import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShopManagementComponent} from './components/shop-management/shop-management.component';
import {ShopDetailComponent} from './components/shop-detail/shop-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ShopManagementComponent
  },
  {
    path: 'details/:shopId',
    component: ShopDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
