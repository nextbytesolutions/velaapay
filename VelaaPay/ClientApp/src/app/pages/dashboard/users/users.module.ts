import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManagementComponent } from './components/user-management/user-management.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UsersRoutingModule} from './users-routing.module';
import { UserVerifyComponent } from './components/user-verify/user-verify.component';
import { SendMessageComponent } from './components/send-message/send-message.component';
import { UserEditInfoComponent } from './components/user-edit-info/user-edit-info.component';
import { UserPasswordComponent } from './components/user-password/user-password.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    UsersRoutingModule,
    MaterialModule,
  ],
  entryComponents: [UserVerifyComponent, SendMessageComponent, UserEditInfoComponent, UserPasswordComponent],
  declarations: [UserManagementComponent, UserVerifyComponent, SendMessageComponent, UserEditInfoComponent, UserPasswordComponent]
})
export class UsersModule { }
