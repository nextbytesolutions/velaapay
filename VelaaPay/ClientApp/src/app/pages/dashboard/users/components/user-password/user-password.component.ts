import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {User} from '../../../../../models/user';
import {ResponseModel} from '../../../../../models/response.model';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';

@Component({
  selector: 'app-user-password',
  templateUrl: './user-password.component.html',
  styleUrls: ['./user-password.component.scss']
})
export class UserPasswordComponent implements OnInit {


  get isSaving (){
    return this.alertService.isLoadingInProgress;
  }
  userEdit: User = new User();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  userPinControl: FormControl;
  userConfirmPinControl: FormControl;
  userFormGroup: FormGroup;

  constructor(private alertService: AlertService,
              private userService: UserService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }


  initForm() {
    this.userPinControl = new FormControl('', [Validators.required, Validators.maxLength(4),
      Validators.minLength(4)]);
    this.userConfirmPinControl = new FormControl('', [Validators.required,
      CustomValidator.PasswordMatch(this.userPinControl)]);
    this.userFormGroup = new FormGroup({
      pin: this.userPinControl,
      confirmPin: this.userConfirmPinControl,
    });

  }


  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.userService.updateUserPin(this.userEdit).subscribe(result => this.saveSuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.resetForm();
    this.alertService.showMessage('Success', `Changes to user \"${this.userEdit.fullName}\"  was saved successfully`,
      MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }


  cancel() {
    this.userEdit = new User();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.userEdit = new User();
    this.resetForm();
  }


  resetForm() {
    this.userFormGroup.reset();
  }



  editUser(user: User) {
    this.initForm();
    if (user) {
      this.userEdit = new User();
      Object.assign(this.userEdit, user);
      this.userEdit.userId = user.userId;
      return this.userEdit;
    } else {
      return new User();
    }
  }

  formSubmit() {
    if (this.userFormGroup.valid) {
      this.userEdit.pin = this.userPinControl.value;
      this.userEdit.confirmPin = this.userConfirmPinControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }


}
