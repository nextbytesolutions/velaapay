import {Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {User} from '../../../../../models/user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {ResponseModel} from '../../../../../models/response.model';

@Component({
  selector: 'app-user-verify',
  templateUrl: './user-verify.component.html',
  styleUrls: ['./user-verify.component.scss']
})
export class UserVerifyComponent implements OnInit {


  private isNewUser = false;
  get isSaving(){
    return this.alertService.isLoadingInProgress;
  }
  cnicEdit: User = new User();
  public formResetToggle = true;
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  cnicNameControl: FormControl;
  cnicImageControl: FormControl;
  cnicFormGroup: FormGroup;

  constructor(private alertService: AlertService, private cnicService: UserService) {
    this.cnicNameControl = new FormControl('', [Validators.required]);
    this.cnicImageControl = new FormControl(null, [Validators.required]);
    this.cnicFormGroup = new FormGroup({
      name: this.cnicNameControl,
      image: new FormControl(''),
      temp: this.cnicImageControl,
    });
  }

  ngOnInit() {

  }
  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.cnicService.verifyUser(this.cnicEdit).subscribe(result => this.saveSuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleUserCountChanged(this.cnic, this.cnicEdit);
    this.alertService.stopLoadingMessage();


    this.resetForm();
    this.alertService.showMessage('Success', `User \"${this.cnicEdit.fullName}\" was verified successfully`, MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }

  cancel() {
    this.cnicEdit = new User();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }




  resetForm() {
    this.cnicFormGroup.reset();
  }

  editUser(cnic: User) {
    if (cnic) {
      this.cnicEdit = new User();
      Object.assign(this.cnicEdit, cnic);
      this.cnicEdit.id = cnic.id;
      this.cnicNameControl.setValue(cnic.cnic);
      if (cnic.cnicImage) {
        this.cnicImageControl.setValue(cnic.cnicImage);
      }
      return this.cnicEdit;
    } else {
      this.isNewUser = true;
      return new User();
    }
  }

  formSubmit() {
    if (this.cnicFormGroup.valid) {
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }
}
