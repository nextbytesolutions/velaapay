import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {User} from '../../../../../models/user';
import {UserService} from '../../services/user.service';
import {UserPasswordComponent} from '../user-password/user-password.component';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {SendMessageComponent} from '../send-message/send-message.component';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {UserEditInfoComponent} from '../user-edit-info/user-edit-info.component';
import {UserVerifyComponent} from '../user-verify/user-verify.component';
import {ResponseModel} from '../../../../../models/response.model';
import {AuthService} from '../../../../../services/auth.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit, AfterViewInit {

  apiCall: any = null;
  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: User[] = [];
  suggestions: string[] = [];
  isLoadingSuggestion = false;
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;

  @ViewChild('editorModal')
  editorModal: any;


  userEditor: MatDialogRef<UserVerifyComponent>;
  messageEditor: MatDialogRef<SendMessageComponent>;
  displayedColumns: string[] = ['fullName', 'balance', 'phoneNumber', 'isEnabled', 'isVerified', 'createdDate', 'action'];

  constructor(private alertService: AlertService,
              private userService: UserService,
              private dialog: MatDialog,
              private router: Router,
              private authService: AuthService,
              private activedRoute: ActivatedRoute) {
  }


  currentId: string;

  currentUserId() {
    return this.currentId;
  }

  ngOnInit() {

    this.currentId = this.authService.currentUser.userId;
    this.dataSource = new MatTableDataSource();
    this.activedRoute.params
      .subscribe(() => {
        this.page = 0;
        this.loadData();
      });


  }


  ngAfterViewInit() {


  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.page = page - 1;
    this.userService.getUsers(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as User[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;
  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
    // this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.userText));
  }

  blockUser(row: User) {
    this.alertService.showDialog('Are you sure you want to block \"' + row.fullName + '\"?', DialogType.confirm,
      () => this.blockUserHelper(row));
  }

  unBlockUser(row: User) {
    this.alertService.showDialog('Are you sure you want to unblock \"' + row.fullName + '\"?', DialogType.confirm,
      () => this.unBlockUserHelper(row));
  }

  blockUserHelper(row: User) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Saving...');
    this.loadingIndicator = true;

    this.userService.blockUser(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', row.fullName + ' is Blocked', MessageSeverity.success);
        this.loadingIndicator = false;
        row.isEnabled = false;
      });
  }

  unBlockUserHelper(row: User) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Saving...');
    this.loadingIndicator = true;
    this.userService.unBlockUser(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Success', row.fullName + ' is Unblocked', MessageSeverity.success);
        this.loadingIndicator = false;
        row.isEnabled = true;
      });
  }

  verifyUser(row: User) {
    this.userEditor = this.dialog.open(UserVerifyComponent, {
      width: '600px',
    });

    this.userEditor.componentInstance.editUser(row);
    this.userEditor.componentInstance.changesSavedCallback = () => {
      this.userEditor.close();
      row.isEnabled = true;
      row.isVerified = true;
    };
    this.userEditor.componentInstance.changesCancelledCallback = () => {
      this.userEditor.close();
    };
  }

  sendMessage(row: User) {
    this.messageEditor = this.dialog.open(SendMessageComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });

    this.messageEditor.componentInstance.setUser(row);
    this.messageEditor.componentInstance.changesSavedCallback = () => {
      this.messageEditor.close();
    };
    this.messageEditor.componentInstance.changesCancelledCallback = () => {
      this.messageEditor.close();
    };
  }

  back() {
    // this._location.back();
  }


  viewUser(row: User) {
    this.router.navigateByUrl(`dashboard/user-details/${row.id}`);
  }

  onSearchValueChange($event: string) {
    // this.loadSuggestions($event);
  }

  loadSuggestions(search: string) {
    if (search) {
      if (this.apiCall != null) {
        return;
      }
      this.apiCall = this.userService.getUserSuggestions(search).subscribe(result => {
        this.suggestions = result.data as string[];
        this.apiCall.unsubscribe();
        this.apiCall = null;
      });
    } else {
      this.suggestions = [];
    }
  }

  editUser(row: User) {
    const modal = this.dialog.open(UserEditInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });

    modal.componentInstance.editUser(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = (user: User) => {
      Object.assign(row, user);
      modal.close();
    };
  }

  changePin(row: User) {
    const modal = this.dialog.open(UserPasswordComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.editUser(row);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
    };
  }
}
