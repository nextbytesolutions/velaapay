import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {User} from '../../../../../models/user';
import {ResponseModel} from '../../../../../models/response.model';

@Component({
  selector: 'app-user-edit-info',
  templateUrl: './user-edit-info.component.html',
  styleUrls: ['./user-edit-info.component.scss']
})
export class UserEditInfoComponent implements OnInit {


  public changesSavedCallback: (customer: User) => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  customer: User = new User();
  customerNameControl: FormControl;
  customerCnicControl: FormControl;
  customerCnicImageControl: FormControl;
  customerGroup: FormGroup;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  constructor(private alertService: AlertService, private customerService: UserService, private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }

  initForm() {
    this.customerNameControl = new FormControl('', [Validators.required]);
    this.customerCnicControl = new FormControl('', [Validators.required, Validators.pattern('^(?=[a-zA-Z0-9]*$)(?:.{13}|.{17})$')]);
    this.customerCnicImageControl = new FormControl('', [Validators.required]);
    this.customerGroup = new FormGroup({
      name: this.customerNameControl,
      cnic: this.customerCnicControl,
      cnicImage: this.customerCnicImageControl,
    });
  }

  editUser(customer: User) {
    this.initForm();
    if (customer) {
      Object.assign(this.customer, customer);
      this.customerNameControl.setValue(this.customer.fullName);
      this.customerCnicControl.setValue(this.customer.cnic);
      this.customerCnicImageControl.setValue(this.customer.cnicImage);
    }
  }

  formSubmit() {
    if (this.customerGroup.valid) {
      this.customer.fullName = this.customerNameControl.value;
      this.customer.cnicImage = this.customerCnicImageControl.value;
      this.customer.cnic = this.customerCnicControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }

  private save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.customerService.updateUser(this.customer).subscribe(result => this.saveSuccessHelper(result));
  }

  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleBankCountChanged(this.bank, this.bankEdit);
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', `User \"${this.customer.fullName}\" was updated successfully`, MessageSeverity.success);
    this.customer.cnicImage = result.data.cnicImage;
    if (this.changesSavedCallback) {
      this.changesSavedCallback(this.customer);
    }
  }

  onFileAddedIdentification(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.customer.cnicImage = e.currentTarget.result;
        this.customerCnicImageControl.setValue(this.customer.cnicImage);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
  cancel() {
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
}
