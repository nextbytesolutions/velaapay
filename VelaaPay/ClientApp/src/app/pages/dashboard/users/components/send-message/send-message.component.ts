import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {User} from '../../../../../models/user';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss']
})
export class SendMessageComponent implements OnInit {
  messageControl: FormControl;
  messageGroup: FormGroup;
  user: User = null;
  get isSaving () {
    return this.alertService.isLoadingInProgress;
  }
  public changesSavedCallback: () => void;
  public changesCancelledCallback: () => void;

  constructor(private alertService: AlertService,
            private userService: UserService) {
    this.messageControl = new FormControl(null, [Validators.required, Validators.maxLength(100)]);
    this.messageGroup = new FormGroup({
      message: this.messageControl,
    });
  }



  setUser(user: User) {
    this.user = user;
  }

  ngOnInit() {
  }

  formSubmit() {
    if (this.messageGroup.valid) {
      this.sendMessage();
    }
  }

  sendMessage() {
    this.alertService.startLoadingMessage('Sending...');
    if (this.user != null) {
      this.userService.SendMessage(this.user, this.messageControl.value).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Message Sent', '', MessageSeverity.success);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      });
    } else {
      this.userService.SendMessageAll(this.messageControl.value).subscribe(result => {
        this.alertService.stopLoadingMessage();
        this.alertService.showMessage('Message Sent', '', MessageSeverity.success);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      });
    }

  }
  cancel() {
    this.user = null;
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
  resetForm() {
    this.messageGroup.reset();
  }


}
