import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {User} from '../../../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserEndpointService extends EndpointFactoryService {

  private readonly _sendOtpUrl: string = '/api/v1/otp';
  private readonly _createUserUrl: string = '/api/v1/customer';
  private readonly _updateUserUrl: string = '/api/v1/customer/update';
  private readonly _updatePinUserUrl: string = '/api/v1/customer/update/pin';
  private readonly _getUserSuggestionUrl: string = '/api/v1/customer/dropDown';
  private readonly _verifyUserUrl: string = '/api/v1/customer/verify';
  private readonly _blockUserUrl: string = '/api/v1/customer/disable';
  private readonly _unBlockUserUrl: string = '/api/v1/customer/enable';
  private readonly _sendMessageUrl: string = '/api/v1/customer/message';
  private readonly _sendMessageAllUrl: string = '/api/v1/customer/messages/all';

  get SendOtpUrl() {
    return this.configurations.baseUrl + this._sendOtpUrl;
  }
  get UsersUrl() {
    return this.configurations.baseUrl + this._createUserUrl;
  }
  get UpdateUserUrl() {
    return this.configurations.baseUrl + this._updateUserUrl;
  }
  get UpdatePinUserUrl() {
    return this.configurations.baseUrl + this._updatePinUserUrl;
  }

  get UsersSuggestionUrl() {
    return this.configurations.baseUrl + this._getUserSuggestionUrl;
  }


  get verifyUserUrl() {
    return this.configurations.baseUrl + this._verifyUserUrl;
  }
  get blockUserUrl() {
    return this.configurations.baseUrl + this._blockUserUrl;
  }
  get unBlockUserUrl() {
    return this.configurations.baseUrl + this._unBlockUserUrl;
  }
  get sendMessageUrl() {
    return this.configurations.baseUrl + this._sendMessageUrl;
  }
  get sendMessageAllUrl() {
    return this.configurations.baseUrl + this._sendMessageAllUrl;
  }

  getUsersEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams({
      encoder: this.encoderService
    }).set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.UsersUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getVerifyUsersEndpoint(user: User): Observable<any> {
    return this.http.delete(this.verifyUserUrl + '/' + user.id, {headers: this.getRequestHeaders().headers});
  }

  getBlockUsersEndpoint(user: User) {
    return this.http.put(this.blockUserUrl + '/' + user.id, null, {headers: this.getRequestHeaders().headers});
  }
  getUnBlockUsersEndpoint(user: User) {
    return this.http.put(this.unBlockUserUrl + '/' + user.id, null, {headers: this.getRequestHeaders().headers});
  }

  getSendMessageUserEndpoint(user: User, message: string) {
    return this.http.post(this.sendMessageUrl + '/' + user.id, { message }, {headers: this.getRequestHeaders().headers});
  }
  getSendMessageAllEndpoint(message: string) {
    return this.http.post(this.sendMessageAllUrl , { message }, {headers: this.getRequestHeaders().headers});
  }

  getUserSuggestionsEndpoint(search: string) {
    const params = new HttpParams({
      encoder: this.encoderService
    }).set('search', search + '');
    return this.http.get(this.UsersSuggestionUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getUpdateUserEndpoint(user: User) {
    const body = {
      cnic: user.cnic,
      cnicImage: user.cnicImage,
      fullName: user.fullName,
    };
    return this.http.put(this.UpdateUserUrl + '/' + user.id, body, {headers: this.getRequestHeaders().headers});
  }

  getUpdateUserPinEndpoint(user: User) {
    const body = {
      pin: user.pin,
      confirmPin: user.confirmPin,
    };
    return this.http.put(this.UpdatePinUserUrl + '/' + user.id, body, {headers: this.getRequestHeaders().headers});
  }

  getUserByPhoneEndpoint(phone: string) {
    const params = new HttpParams({
      encoder: this.encoderService
    }).set('phoneNumber', phone + '');
    return this.http.get(this.UsersUrl + '/phone', {headers: this.getRequestHeaders().headers, params: params});
  }

  getSendOtpEndpoint(id: string) {
    return this.http.post(this.SendOtpUrl, { userId: id }, {headers: this.getRequestHeaders().headers});
  }
}
