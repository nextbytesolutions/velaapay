import {Injectable} from '@angular/core';
import {User} from '../../../../models/user';
import {UserEndpointService} from './user-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private userEndpoint: UserEndpointService) {

  }

  getUsers(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.userEndpoint.getUsersEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  getUserByPhone(phone: string) {
    return this.userEndpoint.getUserByPhoneEndpoint(phone)
      .map((response) => <ResponseModel>response);
  }

  verifyUser(user: User) {
    return this.userEndpoint.getVerifyUsersEndpoint(user)
      .map((response) => <ResponseModel>response);
  }
  blockUser(user: User) {
    return this.userEndpoint.getBlockUsersEndpoint(user)
      .map((response) => <ResponseModel>response);
  }
  unBlockUser(user: User) {
    return this.userEndpoint.getUnBlockUsersEndpoint(user)
      .map((response) => <ResponseModel>response);
  }

  SendMessage(user: User, message: string) {
    return this.userEndpoint.getSendMessageUserEndpoint(user, message)
      .map((response) => <ResponseModel>response);
  }
  SendMessageAll(message: string) {
    return this.userEndpoint.getSendMessageAllEndpoint(message)
      .map((response) => <ResponseModel>response);
  }

  getUserSuggestions(search: string) {
    return this.userEndpoint.getUserSuggestionsEndpoint(search)
      .map((response) => <ResponseModel>response);

  }

  updateUser(customer: User) {
    return this.userEndpoint.getUpdateUserEndpoint(customer)
      .map((response) => <ResponseModel>response);


  }

  updateUserPin(userEdit: User) {
    return this.userEndpoint.getUpdateUserPinEndpoint(userEdit)
      .map((response) => <ResponseModel>response);


  }

  sendOtp(id: string) {
    return this.userEndpoint.getSendOtpEndpoint(id)
      .map((response) => <ResponseModel>response);
  }
}
