import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {WithdrawRequestService} from '../../services/withdraw-request.service';
import {ResponseModel} from '../../../../../models/response.model';
import {WithdrawRequest} from '../../../../../models/withdraw';
import {RequestStatus} from '../../../../../models/enums';

@Component({
  selector: 'app-withdraw-request-management',
  templateUrl: './withdraw-request-management.component.html',
  styleUrls: ['./withdraw-request-management.component.scss']
})
export class WithdrawRequestManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: WithdrawRequest[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['customer', 'bank', 'accountNo', 'amount', 'status', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private requestService: WithdrawRequestService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getWithdrawRequests(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as WithdrawRequest[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  acceptWithdrawRequest(request: WithdrawRequest) {
    this.alertService.showDialog('Do you want to accept this Withdraw Request?', DialogType.confirm, () => {
      this.acceptWithdrawRequestHelper(request);
      this.alertService.closeDialog();
    }, () => {
      this.alertService.closeDialog();
    });
  }

  rejectWithdrawRequest(request: WithdrawRequest) {
    this.alertService.showDialog('Do you want to reject this Withdraw Request?', DialogType.confirm, () => {
      this.rejectWithdrawRequestHelper(request);
      this.alertService.closeDialog();
    }, () => {
      this.alertService.closeDialog();
    });
  }

  acceptWithdrawRequestHelper(request: WithdrawRequest) {
    this.requestService.acceptWithdrawRequests(request.requestId).subscribe(result => {
      this.alertService.stopLoadingMessage();
      this.alertService.showMessage('Success', 'Withdraw request has been Accepted', MessageSeverity.success);
      request.status = +RequestStatus.Accepted;
    });
  }

  rejectWithdrawRequestHelper(request: WithdrawRequest) {
    this.requestService.rejectWithdrawRequests(request.requestId).subscribe(result => {
      this.alertService.stopLoadingMessage();
      this.alertService.showMessage('Success', 'Withdraw request has been Rejected', MessageSeverity.success);
      request.status = +RequestStatus.Rejected;
    });
  }

  getStatus() {
    return RequestStatus.None;
  }
}
