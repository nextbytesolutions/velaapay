import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WithdrawRequestManagementComponent} from './components/withdraw-request-management/withdraw-request-management.component';

const routes: Routes = [
  {
    path: '',
    component: WithdrawRequestManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class WithdrawRequestRoutingModule { }
