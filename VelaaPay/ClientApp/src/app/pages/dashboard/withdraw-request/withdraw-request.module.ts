import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {WithdrawRequestRoutingModule} from './withdraw-request-routing.module';
import {WithdrawRequestManagementComponent} from './components/withdraw-request-management/withdraw-request-management.component';
import {PipeModule} from '../../../pipes/pipe.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  declarations: [WithdrawRequestManagementComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    WithdrawRequestRoutingModule,
    PipeModule,
    MaterialModule
  ]
})
export class WithdrawRequestModule { }
