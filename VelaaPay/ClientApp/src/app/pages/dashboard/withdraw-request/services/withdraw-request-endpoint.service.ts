import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WithdrawRequestEndpointService extends EndpointFactoryService {

  private readonly _createWithdrawRequestUrl: string = '/api/v1/withdraw';

  get WithdrawRequestsUrl() {
    return this.configurations.baseUrl + this._createWithdrawRequestUrl;
  }

  getWithdrawRequestsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.WithdrawRequestsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getAcceptWithdrawRequestsEndpoint(withdrawId: number): Observable<any> {
    return this.http.put(this.WithdrawRequestsUrl + '/accept' + '/' + withdrawId,
      null, {headers: this.getRequestHeaders().headers});
  }

  getRejectWithdrawRequestsEndpoint(withdrawId: number): Observable<any> {
    return this.http.put(this.WithdrawRequestsUrl + '/reject' + '/' + withdrawId,
      null, {headers: this.getRequestHeaders().headers});
  }

}

