import {Injectable} from '@angular/core';
import {ResponseModel} from '../../../../models/response.model';
import {WithdrawRequestEndpointService} from './withdraw-request-endpoint.service';

@Injectable({
  providedIn: 'root'
})
export class WithdrawRequestService {

  constructor(private requestEndpoint: WithdrawRequestEndpointService) {

  }

  getWithdrawRequests(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.requestEndpoint.getWithdrawRequestsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }
  acceptWithdrawRequests(withdrawId: number) {
    return this.requestEndpoint.getAcceptWithdrawRequestsEndpoint(withdrawId)
      .map((response) => <ResponseModel>response);
  }
  rejectWithdrawRequests(withdrawId: number) {
    return this.requestEndpoint.getRejectWithdrawRequestsEndpoint(withdrawId)
      .map((response) => <ResponseModel>response);
  }
}
