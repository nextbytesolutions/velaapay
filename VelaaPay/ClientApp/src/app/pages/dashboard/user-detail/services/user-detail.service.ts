import { Injectable } from '@angular/core';
import {UserDetailEndpointService} from './user-detail-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {

  constructor(private userDetailEndpoint: UserDetailEndpointService) { }

  getUserDetails(userId: string) {
    return this.userDetailEndpoint.getUserDetailEndpoint(userId)
      .map((response) => <ResponseModel>response);
  }
}
