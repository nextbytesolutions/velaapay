import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class UserDetailEndpointService extends EndpointFactoryService {
  private readonly _userDetails: string = '/api/v1/customer/details/';

  get UserDetailUser() {
    return this.configurations.baseUrl + this._userDetails;
  }


  getUserDetailEndpoint(userId: string): Observable<any> {
    return this.http.get(this.UserDetailUser + userId, {headers: this.getRequestHeaders().headers});
  }


}
