import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import {UserDetailRoutingModule} from './user-detail-routing.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {MaterialModule} from '../../../shared/material.module';
import {QRCodeModule} from "angularx-qrcode";

@NgModule({
  declarations: [UserDetailComponent],
  imports: [
    CommonModule,
    UserDetailRoutingModule,
    MaterialModule,
    PipeModule,
    QRCodeModule
  ]
})
export class UserDetailModule { }
