import {Component, OnInit} from '@angular/core';
import {UserDetail} from '../../../../../models/user-detail';
import {AlertService} from '../../../../../services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStoreManager} from '../../../../../services/local-store-manager.service';
import {UserDetailService} from '../../services/user-detail.service';
import {Dbkey} from '../../../../../services/db-key';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  userId: string;
  userDetail: UserDetail;
  displayedTransactionColumns: string[] = ['transactionCode', 'amount', 'senderName', 'receiverName', 'loadType', 'date'];
  displayedRequestColumns: string[] = ['senderName', 'receiverName', 'amount',  'status', 'date'];

  constructor(private activatedRoute: ActivatedRoute,
      private alertService: AlertService,
      private router: Router,
      private localStorage: LocalStoreManager,
      private location: Location,
      private userDetailService: UserDetailService
  ) {
  }

  getDetails() {
    if (this.userId) {
      this.alertService.startLoadingMessage();
      this.userDetailService.getUserDetails(this.userId).subscribe(result => {
        this.alertService.stopLoadingMessage();
         this.userDetail = result.data;
      });
    } else {
      this.router.navigate(['users']);
    }
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(p => {
      this.userId = p['userId'];
      // console.log(p);
      this.getDetails();
    });
  }

  goBack() {
    this.location.back();
  }

  viewMoreTransactions() {
    this.localStorage.saveSessionData(this.userId, Dbkey.SAVED_DATA);
    this.router.navigateByUrl('/dashboard/transactions');
  }

  viewMoreRequests() {
    this.localStorage.saveSessionData(this.userId, Dbkey.SAVED_DATA);
    this.router.navigateByUrl('/dashboard/requests');

  }
}

