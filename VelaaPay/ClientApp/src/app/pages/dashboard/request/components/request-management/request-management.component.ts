import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {RequestService} from '../../services/request.service';
import {LocalStoreManager} from '../../../../../services/local-store-manager.service';
import {Dbkey} from '../../../../../services/db-key';
import {ResponseModel} from '../../../../../models/response.model';
import {AlertService} from '../../../../../services/alert.service';

@Component({
  selector: 'app-request-management',
  templateUrl: './request-management.component.html',
  styleUrls: ['./request-management.component.scss']
})
export class RequestManagementComponent implements OnInit, AfterViewInit {


  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Request[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['sender', 'receiver', 'amount',  'status', 'createdDate'];


  constructor(private alertService: AlertService,
              private localStorageManager: LocalStoreManager,
              private requestService: RequestService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
  }
  ngAfterViewInit(): void {
    if (this.localStorageManager.exists(Dbkey.SAVED_DATA)) {
      const searchValue = this.localStorageManager.getData(Dbkey.SAVED_DATA);
      this.search = searchValue;
      this.localStorageManager.deleteData(Dbkey.SAVED_DATA);
    } else {
      this.loadData();
    }
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getRequests(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Request[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }




}
