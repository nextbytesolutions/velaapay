import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class RequestEndpointService extends EndpointFactoryService {
  private readonly _createRequestUrl: string = '/api/v1/request';

  get RequestsUrl() {
    return this.configurations.baseUrl + this._createRequestUrl;
  }

  getRequestsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.RequestsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

}
