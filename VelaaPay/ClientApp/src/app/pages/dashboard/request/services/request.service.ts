import {Injectable} from '@angular/core';
import {RequestEndpointService} from './request-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private requestEndpoint: RequestEndpointService) {

  }

  getRequests(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.requestEndpoint.getRequestsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }
}
