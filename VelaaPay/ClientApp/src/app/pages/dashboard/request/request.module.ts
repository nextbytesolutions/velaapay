import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestManagementComponent } from './components/request-management/request-management.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RequestRoutingModule} from './request-routing.module';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {MaterialModule} from '../../../shared/material.module';

@NgModule({
  declarations: [RequestManagementComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    RequestRoutingModule,
    PipeModule,
    MaterialModule
  ]
})
export class RequestModule { }
