import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from "@angular/material";
import {AlertService} from "../../../../../services/alert.service";
import {ResponseModel} from "../../../../../models/response.model";
import {CardService} from "../../services/card.service";
import {Card} from "../../../../../models/card";

@Component({
  selector: 'app-card-management',
  templateUrl: './card-management.component.html',
  styleUrls: ['./card-management.component.scss']
})
export class CardManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Card[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['customer', 'name', 'cardType',  'expiryDate', 'createdDate', 'lastDigits'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private requestService: CardService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getCards(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Card[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

}
