import { Injectable } from '@angular/core';
import {ResponseModel} from "../../../../models/response.model";
import {CardEndpointService} from "./card-endpoint.service";

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private cardEndpoint: CardEndpointService) {

  }

  getCards(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.cardEndpoint.getCardsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

}
