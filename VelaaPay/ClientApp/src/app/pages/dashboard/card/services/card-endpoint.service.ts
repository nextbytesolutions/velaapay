import { Injectable } from '@angular/core';
import {EndpointFactoryService} from "../../../../services/endpoint-factory.service";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CardEndpointService  extends EndpointFactoryService {

  private readonly _createCardUrl: string = '/api/v1/cardHolder';

  get CardsUrl() {
    return this.configurations.baseUrl + this._createCardUrl;
  }

  getCardsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CardsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

}
