import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardManagementComponent } from './components/card-management/card-management.component';
import {SearchBoxModule} from "../../../shared/search-box/search-box.module";
import {ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../../../shared/material.module";
import {PipeModule} from "../../../pipes/pipe.module";
import {CardRoutingModule} from "./card-routing.module";

@NgModule({
  declarations: [CardManagementComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CardRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CardModule { }
