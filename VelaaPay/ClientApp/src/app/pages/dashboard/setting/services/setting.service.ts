import { Injectable } from '@angular/core';
import {SettingEndpointService} from './setting-endpoint.service';
import {Observable} from "rxjs";
import {ResponseModel} from "../../../../models/response.model";

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(private settingEndpoint: SettingEndpointService) { }

  updateSetting(key: string, value: string, dataType: string) : Observable<ResponseModel> {
    return this.settingEndpoint.getUpdateSettingsEndpoint(key, value,dataType)
      .map((response) => <ResponseModel>response);
  }

  getSetting(key: string) : Observable<ResponseModel> {
    return this.settingEndpoint.getSettingsEndpoint(key)
      .map((response) => <ResponseModel>response);
  }
}
