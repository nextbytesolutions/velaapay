import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {EndpointFactoryService} from "../../../../services/endpoint-factory.service";
import {HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SettingEndpointService extends EndpointFactoryService {

  private readonly _createSettingUrl: string = '/api/v1/setting';

  get SettingsUrl() {
    return this.configurations.baseUrl + this._createSettingUrl;
  }

  getUpdateSettingsEndpoint(key: string, value: string, dataType: string): Observable<any> {
    const requestBody = {
      Key: key,
      Value: value,
      DataType: dataType
    };
    return this.http.post(this.SettingsUrl, requestBody, {headers: this.getRequestHeaders().headers});
  }

  getSettingsEndpoint(key: string): Observable<any> {
    const httpParam = new HttpParams().set('Key', key);
    return this.http.get(this.SettingsUrl, {headers: this.getRequestHeaders().headers, params: httpParam});
  }
}
