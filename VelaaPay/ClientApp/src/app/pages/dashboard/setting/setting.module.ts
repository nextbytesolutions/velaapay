import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingManagementComponent } from './component/setting-management/setting-management.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { UserInfoComponent } from './component/user-info/user-info.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SettingRoutingModule} from './setting-routing.module';
import { BusinessInfoComponent } from './component/business-info/business-info.component';
import {MaterialModule} from '../../../shared/material.module';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import { TodayLimitComponent } from './component/today-limit/today-limit.component';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    FormsModule,
    SettingRoutingModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [SettingManagementComponent, ChangePasswordComponent, UserInfoComponent, BusinessInfoComponent, TodayLimitComponent],
})
export class SettingModule { }
