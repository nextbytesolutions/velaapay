import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Merchant} from '../../../../../models/merchant';
import {AuthService} from '../../../../../services/auth.service';
import {ResponseModel} from '../../../../../models/response.model';
import {MerchantService} from '../../../merchant/services/merchant.service';

@Component({
  selector: 'app-business-info',
  templateUrl: './business-info.component.html',
  styleUrls: ['./business-info.component.scss']
})
export class BusinessInfoComponent implements OnInit {

  merchant: Merchant = new Merchant();
  merchantNameControl: FormControl;
  merchantNtnControl: FormControl;
  merchantRccmControl: FormControl;
  merchantRccmImageControl: FormControl;
  merchantCnicControl: FormControl;
  merchantCnicImageControl: FormControl;
  merchantGroup: FormGroup;
  get isSaving () {
   return this.alertService.isLoadingInProgress;
  }

  constructor(private alertService: AlertService,
              private authService: AuthService,
              private merchantService: MerchantService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.initForm();
    this.merchantNameControl.setValue(this.authService.currentUser.fullName);
    this.merchantService.getBusinessInfoMerchant().subscribe(result => {
      this.merchantCnicControl.setValue(result.data.identification);
      this.merchantCnicImageControl.setValue(result.data.identificationImage);
      this.merchantNtnControl.setValue(result.data.ntnNumber);
      this.merchantRccmControl.setValue(result.data.rccm);
      this.merchantRccmImageControl.setValue(result.data.rccmImage);
    });
  }

  initForm() {
    this.merchantNameControl = new FormControl('', [Validators.required]);
    this.merchantNtnControl = new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]);
    this.merchantRccmControl = new FormControl('', [Validators.required]);
    this.merchantRccmImageControl = new FormControl('', [Validators.required]);
    this.merchantCnicControl = new FormControl('', [Validators.required, Validators.pattern('^(?=[a-zA-Z0-9]*$)(?:.{13}|.{17})$')]);
    this.merchantCnicImageControl = new FormControl('', [Validators.required]);
    this.merchantGroup = new FormGroup({
      cnic: this.merchantCnicControl,
      ntn: this.merchantNtnControl,
      rccm: this.merchantRccmControl,
      rccmImage: this.merchantRccmImageControl,
      cnicImage: this.merchantCnicImageControl,
    });
  }

  formSubmit() {
    if (this.merchantGroup.valid) {
      this.merchant.fullName = this.merchantNameControl.value;
      this.merchant.ntnNumber = this.merchantNtnControl.value;
      this.merchant.rccm = this.merchantRccmControl.value;
      this.merchant.rccmImage = this.merchantRccmImageControl.value;
      this.merchant.identificationImage = this.merchantCnicImageControl.value;
      this.merchant.identificationNumber = this.merchantCnicControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }

  private save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.merchantService.updateMerchantByUser(this.merchant).subscribe(result => this.saveSuccessHelper(result));
  }

  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleBankCountChanged(this.bank, this.bankEdit);
    this.authService.currentUser.fullName = this.merchant.fullName;
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', `Merchant \"${this.merchant.fullName}\" was updated successfully`, MessageSeverity.success);
    this.merchant.rccmImage = result.data.rccmImage;
    this.merchant.identificationImage = result.data.identificationImage;
  }

  onFileAddedIdentification(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.identificationImage = e.currentTarget.result;
        this.merchantCnicImageControl.setValue(this.merchant.identificationImage);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  onFileAddedRccm(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.rccmImage = e.currentTarget.result;
        this.merchantRccmImageControl.setValue(this.merchant.rccmImage);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
}
