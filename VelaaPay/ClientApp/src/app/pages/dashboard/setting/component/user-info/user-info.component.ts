import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../../services/auth.service';
import {User} from '../../../../../models/user';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

   user: User = new User();
  constructor(private accountService: AuthService) { }

  ngOnInit() {
    this.user = this.accountService.currentUser;
  }

}
