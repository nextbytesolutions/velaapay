import {Component, OnInit, ViewChild} from '@angular/core';
import {ChangePassword} from '../../../../../models/change-password';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {ConfigurationService} from '../../../../../services/configuration.service';
import {LoginService} from '../../../../login/services/login.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {




  get isSaving () {
    return this.alertService.isLoadingInProgress;
  }
  isPasswordRested = false;
  userEdit: ChangePassword = new ChangePassword();
  passwordFormGroup: FormGroup;
  currentPasswordControl: FormControl;
  newPasswordControl: FormControl;
  confirmPasswordControl: FormControl;
  constructor(private alertService: AlertService,
              public configurations: ConfigurationService,
              private loginService: LoginService) {

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.currentPasswordControl = new FormControl('', [Validators.required]);
    this.newPasswordControl = new FormControl('', [Validators.required]);
    this.confirmPasswordControl = new FormControl('', [Validators.required, CustomValidator.PasswordMatch(this.newPasswordControl)]);
    this.passwordFormGroup = new FormGroup({
      new: this.newPasswordControl,
      current: this.currentPasswordControl,
      confirm: this.confirmPasswordControl,
    });
  }
  save() {

    if (this.passwordFormGroup.valid) {
      this.alertService.startLoadingMessage('Saving changes...');
      this.userEdit.newPassword = this.newPasswordControl.value;
      this.userEdit.currentPassword = this.currentPasswordControl.value;
      this.userEdit.confirmPassword = this.confirmPasswordControl.value;
      this.loginService.changePassword(this.userEdit)
        .subscribe(result => {
          this.isPasswordRested = true;
          this.alertService.stopLoadingMessage();
          this.alertService.showMessage('Success', `Your password has been updated successfully`, MessageSeverity.success);
        });
    }
  }
}
