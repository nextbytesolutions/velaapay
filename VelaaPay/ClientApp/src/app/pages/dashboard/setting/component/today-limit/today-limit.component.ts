import { Component, OnInit } from '@angular/core';
import {ResponseModel} from "../../../../../models/response.model";
import {AlertService, MessageSeverity} from "../../../../../services/alert.service";
import {FormControl, Validators} from "@angular/forms";
import {SettingService} from "../../services/setting.service";
import {SettingDataType} from "../../../../../constants/settingDataType";
import {SettingConstant} from "../../../../../constants/settingConstant";
import {Setting} from "../../../../../models/Setting";

@Component({
  selector: 'app-today-limit',
  templateUrl: './today-limit.component.html',
  styleUrls: ['./today-limit.component.scss']
})
export class TodayLimitComponent implements OnInit {

  isSaving = false;
  key = SettingConstant.TodayLimit;
  limitControl: FormControl;
  constructor(private alertService: AlertService, private settingService: SettingService) { }

  ngOnInit() {
    this.limitControl = new FormControl('', [Validators.required, Validators.min(1)]);
    this.settingService.getSetting(this.key).subscribe(result => {
      this.alertService.stopLoadingMessage();
      let setting = result.data as Setting;
      this.limitControl.setValue(setting.value);
    });
  }

  save() {
    this.settingService.updateSetting(this.key, this.limitControl.value + '',SettingDataType.Double.toString()).subscribe(result => this.saveSuccessHelper(result));
  }

  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRolePaperCountChanged(this.paper, this.paperEdit);

    this.isSaving = false;
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Updated Charges', '', MessageSeverity.success);

  }
}
