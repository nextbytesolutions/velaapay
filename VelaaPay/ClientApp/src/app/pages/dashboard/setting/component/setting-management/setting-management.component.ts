import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../../../services/auth.service';
import {RoleNames} from '../../../../../constants/role-names';

@Component({
  selector: 'app-setting-management',
  templateUrl: './setting-management.component.html',
  styleUrls: ['./setting-management.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class SettingManagementComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  isVerified(): boolean {
    return this.authService.isVerified;
  }
  isVendor() {
    return this.authService.isRole(RoleNames.Vendor);
  }

   openCity(evt, cityName): void {
    // Declare all variables
     let i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }

    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(' active', '');
    }

    document.getElementById(cityName).style.display = 'block';
    evt.currentTarget.className += ' active';
  }
}
