import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Company} from '../../../../../models/company';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ResponseModel} from '../../../../../models/response.model';
import {CompanyType} from '../../../../../models/enums';
import {CompanyService} from '../../services/company.service';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})
export class CompanyInfoComponent implements OnInit {


  image: any = null;
  companyTypes = [{ text: 'Bill Payment', value: +CompanyType.BillPayment }, { text: 'Air Top Up', value: +CompanyType.AirTopUp }];
  private isNewCompany = false;
  get isSaving() {
   return this.alertService.isLoadingInProgress;
  }

  companyEdit: Company = new Company();
  public changesSavedCallback: (company: Company) => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  companyNameControl: FormControl;
  companyImageControl: FormControl;
  companyTypeControl: FormControl;
  companyFormGroup: FormGroup;

  constructor(private alertService: AlertService, private companyService: CompanyService, private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
    // this.studyService.getStudies().subscribe(result => { this.allStudies = result });
  }


  initForm() {
    this.companyNameControl = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.companyTypeControl = new FormControl('', [Validators.required]);
    this.companyImageControl = new FormControl(null, [Validators.required]);
    this.companyFormGroup = new FormGroup({
      name: this.companyNameControl,
      image: new FormControl(''),
      temp: this.companyImageControl,
      type: this.companyTypeControl,
    });

  }


  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.companyEdit.name = this.companyEdit.name.trim();
    if (this.isNewCompany) {
      this.companyService.createCompany(this.companyEdit).subscribe(result => this.saveSuccessHelper(result));
    } else {
      this.companyService.updateCompany(this.companyEdit).subscribe(result => this.saveSuccessHelper(result));
    }
  }


  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleCompanyCountChanged(this.company, this.companyEdit);
    this.alertService.stopLoadingMessage();


    this.resetForm();
    if (this.isNewCompany) {
      this.alertService.showMessage('Success', `Company \"${this.companyEdit.name}\" was created successfully`, MessageSeverity.success);
    } else {
      this.alertService.showMessage('Success', `Changes to company \"${this.companyEdit.name}\"  was saved successfully`
        , MessageSeverity.success);
    }
    if (this.changesSavedCallback) {
      this.changesSavedCallback(this.companyEdit);
    }
  }


  cancel() {
    this.companyEdit = new Company();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.companyEdit = new Company();
    this.resetForm();
  }








  resetForm() {
    this.companyFormGroup.reset();
  }

  editCompany(company: Company) {
    this.initForm();
    if (company) {
      this.isNewCompany = false;

      this.companyEdit = new Company();
      Object.assign(this.companyEdit, company);
      this.companyEdit.id = company.id;
      this.companyNameControl.setValue(this.companyEdit.name);
      this.companyImageControl.setValue(this.companyEdit.imageUrl);
      this.companyTypeControl.setValue(this.companyEdit.type);
      return this.companyEdit;
    } else {
      this.isNewCompany = true;
      return new Company();
    }
  }

  formSubmit() {
    if (this.companyFormGroup.valid) {
      this.companyEdit.imageUrl = this.companyImageControl.value;
      this.companyEdit.name = this.companyNameControl.value;
      this.companyEdit.type = this.companyTypeControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }

  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.image = e.currentTarget.result;
        this.companyImageControl.setValue(this.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

}
