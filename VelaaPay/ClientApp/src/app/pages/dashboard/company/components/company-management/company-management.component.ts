import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {CompanyType} from '../../../../../models/enums';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {Company} from '../../../../../models/company';
import {CompanyInfoComponent} from '../company-info/company-info.component';
import {ResponseModel} from '../../../../../models/response.model';
import {CompanyService} from '../../services/company.service';

@Component({
  selector: 'app-company-management',
  templateUrl: './company-management.component.html',
  styleUrls: ['./company-management.component.scss']
})
export class CompanyManagementComponent implements OnInit {

  type = 0;
  companyTypes = [{text: 'Bill Payment', value: +CompanyType.BillPayment}, {text: 'Air Top Up', value: +CompanyType.AirTopUp}];
  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  rows: Company[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['image', 'name', 'type', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private companyService: CompanyService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }

  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.companyService.getCompany(this.type, page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Company[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }


  addCompany() {
    const modal = this.dialog.open(CompanyInfoComponent, {
      width: '600px',
    });
    modal.componentInstance.editCompany(null);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.page = 0;
      this.loadData();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  editCompany(row: Company) {
    const modal = this.dialog.open(CompanyInfoComponent, {
      width: '600px',
    });
    modal.componentInstance.editCompany(row);
    modal.componentInstance.changesSavedCallback = (company: Company) => {
      modal.close();
      Object.assign(row, company);
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  deleteCompany(row: Company) {
    this.alertService.showDialog('Are you sure you want to delete \"' + row.name + '\"?', DialogType.confirm,
      () => this.deleteCompanyHelper(row));
  }


  deleteCompanyHelper(row: Company) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Deleting...');
    this.loadingIndicator = true;

    this.companyService.deleteCompany(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.page = 0;
        this.search = '';
        this.loadData();
      });
  }

  onCompanyTypeChange($event: any) {
    this.type = $event;
    this.page = 0;
    this.search = '';
    this.loadData();
  }
}
