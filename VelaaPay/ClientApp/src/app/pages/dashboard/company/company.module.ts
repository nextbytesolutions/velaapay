import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyManagementComponent } from './components/company-management/company-management.component';
import { CompanyInfoComponent } from './components/company-info/company-info.component';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../shared/material.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {CompanyRoutingModule} from './company-routing.module';

@NgModule({
  declarations: [CompanyManagementComponent, CompanyInfoComponent],
  entryComponents: [CompanyInfoComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    CompanyRoutingModule,
    MaterialModule,
    PipeModule
  ]
})
export class CompanyModule { }
