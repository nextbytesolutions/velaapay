import {Injectable} from '@angular/core';
import {Company} from '../../../../models/company';
import {CompanyEndpointService} from './company-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private billCompanyEndpoint: CompanyEndpointService) {

  }

  getCompany(type: number, page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.billCompanyEndpoint.getCompanysEndpoint(type, page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  deleteCompany(billCompany: Company) {
    return this.billCompanyEndpoint.getDeleteCompanysEndpoint(billCompany)
      .map((response) => <ResponseModel>response);
  }
  createCompany(billCompany: Company) {
    return this.billCompanyEndpoint.getCreateCompanysEndpoint(billCompany)
      .map((response) => <ResponseModel>response);
  }

  updateCompany(billCompany: Company) {
    return this.billCompanyEndpoint.getUpdateCompanysEndpoint(billCompany)
      .map((response) => <ResponseModel>response);
  }
}
