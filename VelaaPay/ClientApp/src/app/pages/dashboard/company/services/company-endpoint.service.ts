import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Company} from '../../../../models/company';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyEndpointService extends EndpointFactoryService {


  private readonly _createCompanyUrl: string = '/api/v1/company';

  get CompanysUrl() {
    return this.configurations.baseUrl + this._createCompanyUrl;
  }

  get createCompanyUrl() {
    return this.configurations.baseUrl + this._createCompanyUrl;
  }

  get updateCompanyUrl() {
    return this.configurations.baseUrl + this._createCompanyUrl;
  }

  get deleteCompanyUrl() {
    return this.configurations.baseUrl + this._createCompanyUrl;
  }

  getCompanysEndpoint(type: number, page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('type', type + '').set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.CompanysUrl, {headers: this.getRequestHeaders().headers, params: params});
  }


  getCreateCompanysEndpoint(company: Company): Observable<any> {
    const requestBody = {
      name: company.name,
      imageUrl: company.imageUrl,
      type: company.type,
    };
    return this.http.post(this.createCompanyUrl, requestBody, {headers: this.getRequestHeaders().headers});
  }

  getUpdateCompanysEndpoint(company: Company): Observable<any> {
    const requestBody = {
      companyId: company.companyId,
      name: company.name,
      imageUrl: company.imageUrl,
      type: company.type,
    };
    return this.http.put(this.updateCompanyUrl, requestBody, {headers: this.getRequestHeaders().headers});
  }


  getDeleteCompanysEndpoint(company: Company): Observable<any> {
    console.log(company);
    return this.http.delete(this.deleteCompanyUrl + '/' + company.companyId, {headers: this.getRequestHeaders().headers});
  }
}
