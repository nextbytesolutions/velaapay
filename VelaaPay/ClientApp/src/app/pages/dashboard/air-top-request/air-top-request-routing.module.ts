import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AirTopRequestManagementComponent} from "./components/air-top-request-management/air-top-request-management.component";

const routes: Routes = [
  {
    path: '',
    component: AirTopRequestManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AirTopRequestRoutingModule { }
