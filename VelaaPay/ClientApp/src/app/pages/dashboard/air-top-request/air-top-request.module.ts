import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AirTopRequestManagementComponent } from './components/air-top-request-management/air-top-request-management.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AirTopRequestRoutingModule} from './air-top-request-routing.module';
import { AirTopRequestInfoComponent } from './components/air-top-request-info/air-top-request-info.component';
import {MaterialModule} from '../../../shared/material.module';
import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {PipeModule} from '../../../pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    AirTopRequestRoutingModule,
    MaterialModule,
    PipeModule
  ],
  declarations: [AirTopRequestManagementComponent, AirTopRequestInfoComponent],
  entryComponents: [AirTopRequestInfoComponent]
})
export class AirTopRequestModule { }
