import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {AirTimeService} from '../../services/air-time.service';
import {AlertService, DialogType, MessageSeverity} from '../../../../../services/alert.service';
import {ResponseModel} from '../../../../../models/response.model';
import {AirTimeTopUp} from '../../../../../models/air-time-top-up';
import {RequestStatus} from '../../../../../models/enums';
import {AirTopRequestInfoComponent} from '../air-top-request-info/air-top-request-info.component';

@Component({
  selector: 'app-air-top-request-management',
  templateUrl: './air-top-request-management.component.html',
  styleUrls: ['./air-top-request-management.component.scss']
})
export class AirTopRequestManagementComponent implements OnInit {


  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  totalPage = 0;
  loadType = -1;
  rows: Request[] = [];
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['name', 'amount', 'company', 'mobileNo',  'status', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private requestService: AirTimeService) {
  }


  ngOnInit() {

    this.dataSource  = new MatTableDataSource();
    this.page = 0;
    this.loadData();
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.requestService.getAirTimeTopUps(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Request[];
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }
  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }



  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }

  acceptAirTop(row: AirTimeTopUp) {
    this.alertService.showDialog('Are you sure you want to accept the request of \"' + row.name + '\"?', DialogType.confirm,
      () => this.acceptAirTopHelper(row));
  }


  acceptAirTopHelper(row: AirTimeTopUp) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Accepting...');
    this.loadingIndicator = true;

    this.requestService.acceptAirTimeTopUps(row)
      .subscribe(results => {
            this.alertService.stopLoadingMessage();
            this.alertService.showMessage('Success', 'AirTime Request Accepted', MessageSeverity.success);
            this.loadingIndicator = false;
            this.page = 0;
            this.search = '';
            this.loadData();
        });
  }

  rejectAirTop(row: AirTimeTopUp) {
    this.alertService.showDialog('Are you sure you want to reject the request of \"' + row.name + '\"?', DialogType.confirm,
      () => this.rejectAirTopHelper(row));
  }


  rejectAirTopHelper(row: AirTimeTopUp) {

    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Rejecting...');
    this.loadingIndicator = true;

    this.requestService.rejectAirTimeTopUps(row)
      .subscribe(results => {
            this.alertService.stopLoadingMessage();
            this.loadingIndicator = false;
            this.alertService.showMessage('Success', 'AirTime Request Rejected', MessageSeverity.success);
          row.status = +RequestStatus.Rejected;
        });
  }

  get NoneStatus() {
    return RequestStatus.None;
  }

  showAirResponse(element: AirTimeTopUp) {
    const modal = this.dialog.open(AirTopRequestInfoComponent, {
      width: '600px',
    });
    modal.componentInstance.setAirTimeTopUp(element);
    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }


}
