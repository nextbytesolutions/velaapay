import {Component, OnInit} from '@angular/core';
import {AirTimeTopUp} from '../../../../../models/air-time-top-up';

@Component({
  selector: 'app-air-top-request-info',
  templateUrl: './air-top-request-info.component.html',
  styleUrls: ['./air-top-request-info.component.scss']
})
export class AirTopRequestInfoComponent implements OnInit {

  public changesCancelledCallback: () => void;
  airTimeTopUp: AirTimeTopUp;
  constructor() { }

  ngOnInit() {
  }

  setAirTimeTopUp(airTimeTopUp: AirTimeTopUp) {
    this.airTimeTopUp = airTimeTopUp;
  }

  getResponseObj() {
    if (this.airTimeTopUp) {
      return JSON.parse(this.airTimeTopUp.response);
    }
    return null;
  }
}
