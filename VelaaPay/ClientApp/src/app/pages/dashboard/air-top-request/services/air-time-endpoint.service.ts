import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {AirTimeTopUp} from '../../../../models/air-time-top-up';
import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';

@Injectable({
  providedIn: 'root'
})
export class AirTimeEndpointService extends EndpointFactoryService {

  private readonly _createAirTimeTopUpUrl: string = '/api/v1/airTimeTopUp';

  get AirTimeTopUpsUrl() {
    return this.configurations.baseUrl + this._createAirTimeTopUpUrl;
  }

  AcceptAirTimeTopUpsUrl(id: number) {
    return this.configurations.baseUrl + this._createAirTimeTopUpUrl + '/accept' + '/' + id ;
  }

  RejectAirTimeTopUpsUrl(id: number) {
    return this.configurations.baseUrl + this._createAirTimeTopUpUrl + '/reject' + '/' + id ;
  }

  getAirTimeTopUpsEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.AirTimeTopUpsUrl, {headers: this.getRequestHeaders().headers, params: params});
  }

  getAcceptAirTimeTopUpsEndpoint(airTimeTopUp: AirTimeTopUp): Observable<any> {
    return this.http.put(this.AcceptAirTimeTopUpsUrl(airTimeTopUp.requestId), null, {headers: this.getRequestHeaders().headers});
  }
  getRejectAirTimeTopUpsEndpoint(airTimeTopUp: AirTimeTopUp): Observable<any> {
    return this.http.put(this.RejectAirTimeTopUpsUrl(airTimeTopUp.requestId), null, {headers: this.getRequestHeaders().headers});
  }
}
