import {Injectable} from '@angular/core';
import {AirTimeEndpointService} from './air-time-endpoint.service';
import {ResponseModel} from '../../../../models/response.model';
import {AirTimeTopUp} from '../../../../models/air-time-top-up';

@Injectable({
  providedIn: 'root'
})
export class AirTimeService {

  constructor(private airTimeTopUpEndpoint: AirTimeEndpointService) {

  }

  getAirTimeTopUps(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.airTimeTopUpEndpoint.getAirTimeTopUpsEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  acceptAirTimeTopUps(airTimeTopUp: AirTimeTopUp) {
    return this.airTimeTopUpEndpoint.getAcceptAirTimeTopUpsEndpoint(airTimeTopUp)
      .map((response) => <ResponseModel>response);
  }

  rejectAirTimeTopUps(airTimeTopUp: AirTimeTopUp) {
    return this.airTimeTopUpEndpoint.getRejectAirTimeTopUpsEndpoint(airTimeTopUp)
      .map((response) => <ResponseModel>response);
  }
}

