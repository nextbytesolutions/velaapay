import {SearchBoxModule} from '../../../shared/search-box/search-box.module';
import {EmployeeRoutingModule} from './employee-routing.module';
import {PipeModule} from '../../../pipes/pipe.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {EmployeeInfoComponent} from './components/employee-info/employee-info.component';
import {EmployeePasswordComponent} from './components/employee-password/employee-password.component';
import {ReactiveFormsModule} from '@angular/forms';
import {EmployeeSendComponent} from './components/employee-send/employee-send.component';
import {MaterialModule} from '../../../shared/material.module';
import {EmployeeManagementComponent} from './components/employee-management/employee-management.component';
import {TextMaskModule} from 'angular2-text-mask';

@NgModule({
  declarations: [EmployeeManagementComponent, EmployeeInfoComponent, EmployeePasswordComponent, EmployeeSendComponent],
  entryComponents: [EmployeeInfoComponent, EmployeePasswordComponent, EmployeeSendComponent],
  imports: [
    CommonModule,
    SearchBoxModule,
    ReactiveFormsModule,
    EmployeeRoutingModule,
    MaterialModule,
    PipeModule,
    TextMaskModule
  ]
})
export class EmployeeModule { }
