import {EndpointFactoryService} from '../../../../services/endpoint-factory.service';
import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Employee} from '../../../../models/employee';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeEndpointService extends EndpointFactoryService {

  private readonly _createEmployeeUrl: string = '/api/v1/employee';
  private readonly _getSupervisorUrl: string = '/api/v1/employee/supervisor/combobox/';
  private readonly _transferMoneyUrl: string = '/api/v1/payment/transfer/';

  get EmployeesUrl() { return this.configurations.baseUrl + this._createEmployeeUrl; }
  get TransferMoneyUrl() { return this.configurations.baseUrl + this._transferMoneyUrl; }
  get SupervisorUrl() { return this.configurations.baseUrl + this._getSupervisorUrl; }
  get createEmployeeUrl() { return this.configurations.baseUrl + this._createEmployeeUrl; }
  get updateEmployeeUrl() { return this.configurations.baseUrl + this._createEmployeeUrl; }
  get deleteEmployeeUrl() { return this.configurations.baseUrl + this._createEmployeeUrl; }

  getEmployeesEndpoint(page = 1, pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('page', page + '').set('pageSize', pageSize + '')
      .set('search', search + '').set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.EmployeesUrl, { headers: this.getRequestHeaders().headers, params: params });
  }
  getEmployeesBySupervisorEndpoint(employeeId, shopId, page = 1,
                                   pageSize = 10, search = '', orderBy = '', direction = ''): Observable<any> {
    const params = new HttpParams().set('isSupervisor', '1').set('supervisorId', employeeId + '')
      .set('shopId', shopId + '').set('page', page + '').set('pageSize', pageSize + '').set('search', search + '')
      .set('isDescending', direction  === 'desc' ? 'true' : 'false').set('orderBy', orderBy + '');
    return this.http.get(this.EmployeesUrl, { headers: this.getRequestHeaders().headers, params: params });
  }

  getCreateEmployeesEndpoint(employee: Employee): Observable<any> {
    const requestBody = {
      name: employee.employeeName,
      phoneNumber: employee.phoneNumber,
      role: employee.role,
      image: employee.image,
      password: employee.password,
      startTime: employee.startTime,
      endTime: employee.endTime,
      shopId: employee.shopId,
      supervisorId: employee.supervisorId,
    };
    return this.http.post(this.createEmployeeUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }

  getUpdateEmployeesEndpoint(employee: Employee): Observable<any> {
    const requestBody = {
      phoneNumber: employee.phoneNumber,
      employeeId: employee.employeeId,
      name: employee.employeeName,
      image: employee.image,
      startTime: employee.startTime,
      endTime: employee.endTime,
      shopId: employee.shopId,
    };
    return this.http.put(this.updateEmployeeUrl, requestBody, { headers: this.getRequestHeaders().headers });
  }



  getDeleteEmployeesEndpoint(employee: Employee): Observable<any> {
    return this.http.delete(this.deleteEmployeeUrl + '/' + employee.employeeId, { headers: this.getRequestHeaders().headers });
  }

  getSupervisorsEndpoint(shopId: number) {
    return this.http.get(this.SupervisorUrl + shopId, { headers: this.getRequestHeaders().headers });
  }

  getUpdateEmployeesPasswordEndpoint(employee: Employee) {
    const requestBody = {
      employeeId: employee.employeeId,
      password: employee.password,
      confirmPassword: employee.confirmPassword,
    };
    return this.http.put(this.updateEmployeeUrl + '/password', requestBody, { headers: this.getRequestHeaders().headers });
  }

  transferMoneyToEmployeeEndpoint(userId: string, amount: number) {
    const requestBody = {
      receiverUserId: userId,
      amount
    };
    return this.http.post(this.TransferMoneyUrl, requestBody, { headers: this.getRequestHeaders().headers });

  }
}
