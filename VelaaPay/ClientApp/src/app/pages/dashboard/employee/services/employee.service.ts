import {EmployeeEndpointService} from './employee-endpoint.service';
import {Injectable} from '@angular/core';
import {Employee} from '../../../../models/employee';
import {ResponseModel} from '../../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private employeeEndpoint: EmployeeEndpointService) {

  }

  getEmployees(page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.employeeEndpoint.getEmployeesEndpoint(page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  getEmployeesBySupervisor(employeeId, shopId, page = 1, pageSize = 10, search = '', orderBy: string, direction: string) {
    return this.employeeEndpoint.getEmployeesBySupervisorEndpoint(employeeId, shopId, page, pageSize, search, orderBy, direction)
      .map((response) => <ResponseModel>response);
  }

  getSupervisor(shopId: number) {
    return this.employeeEndpoint.getSupervisorsEndpoint(shopId)
      .map((response) => <ResponseModel>response);

  }
  deleteEmployee(employee: Employee) {
    return this.employeeEndpoint.getDeleteEmployeesEndpoint(employee)
      .map((response) => <ResponseModel>response);
  }
  createEmployee(employee: Employee) {
    return this.employeeEndpoint.getCreateEmployeesEndpoint(employee)
      .map((response) => <ResponseModel>response);
  }

  updateEmployee(employee: Employee) {
    return this.employeeEndpoint.getUpdateEmployeesEndpoint(employee)
      .map((response) => <ResponseModel>response);
  }

  updateEmployeePassword(employee: Employee) {
    return this.employeeEndpoint.getUpdateEmployeesPasswordEndpoint(employee)
      .map((response) => <ResponseModel>response);
  }
  transferMoneyToEmployee(userId: string, amount: number) {
    return this.employeeEndpoint.transferMoneyToEmployeeEndpoint(userId, amount)
      .map((response) => <ResponseModel>response);
  }
}
