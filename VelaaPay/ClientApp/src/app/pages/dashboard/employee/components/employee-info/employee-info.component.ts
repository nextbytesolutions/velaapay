import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeeService} from '../../services/employee.service';
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Shop} from '../../../../../models/shop';
import {Employee} from '../../../../../models/employee';
import {AuthService} from '../../../../../services/auth.service';
import {ResponseModel} from '../../../../../models/response.model';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';
import {RoleNames} from '../../../../../constants/role-names';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss']
})
export class EmployeeInfoComponent implements OnInit {


  public timeMask = [/[0-1]/, /\d/, ':', /[0-5]/, /\d/, ' ', /[AP]/, 'M'];
  shops: Shop[];
  employeeType = '';
  employeeTypes = [{text: 'Cashier', value: 'Cashier'}, {text: 'Supervisor', value: 'Supervisor'}];
  isNewEmployee = false;
  isSupervisor = false;
  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  employeeEdit: Employee = new Employee();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  employeeNameControl: FormControl;
  employeePhoneNumberControl: FormControl;
  employeeImageControl: FormControl;
  employeeTypeControl: FormControl;
  employeePasswordControl: FormControl;
  employeeConfirmPasswordControl: FormControl;
  employeeShopControl: FormControl;
  employeeStartTimeControl: FormControl;
  employeeEndTimeControl: FormControl;
  employeeFormGroup: FormGroup;
  supervisors: Employee[] = [];
  image: any = null;

  constructor(private alertService: AlertService,
              private authService: AuthService,
              private employeeService: EmployeeService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
    // this.studyService.getStudies().subscribe(result => { this.allStudies = result });
  }


  initForm() {
    if (this.authService.isRole(RoleNames.Supervisor)) {
      this.isSupervisor = true;
      this.employeeTypes = [this.employeeTypes[0]];
      const e = new Employee();
      e.employeeId = this.authService.currentUser.employeeId;
      e.employeeName = this.authService.currentUser.fullName;
      this.supervisors = [e];
    }
    this.employeeNameControl = new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]);
    this.employeeTypeControl = new FormControl('', [Validators.required]);
    this.employeeShopControl = new FormControl('', [Validators.required, Validators.min(1)]);
    this.employeePhoneNumberControl = new FormControl('', [Validators.required]);
    this.employeePasswordControl = new FormControl('', [Validators.required]);
    this.employeeConfirmPasswordControl = new FormControl('', [Validators.required,
      CustomValidator.PasswordMatch(this.employeePasswordControl)]);
    this.employeeStartTimeControl = new FormControl('', [Validators.maxLength(this.timeMask.length),  CustomValidator.NoUnderScore()]);
    this.employeeEndTimeControl = new FormControl('', [Validators.maxLength(this.timeMask.length),  CustomValidator.NoUnderScore()]);
    this.employeeImageControl = new FormControl(null, [Validators.required]);
    this.employeeFormGroup = new FormGroup({
      name: this.employeeNameControl,
      image: new FormControl(''),
      temp: this.employeeImageControl,
      type: this.employeeTypeControl,
      startTime: this.employeeStartTimeControl,
      endTime: this.employeeEndTimeControl,
      phoneNumber: this.employeePhoneNumberControl,
      shop: this.employeeShopControl,
      password: this.employeePasswordControl,
      confirmPassword: this.employeeConfirmPasswordControl,
    });

  }

  loadSupervisor(shopId: number) {
    if (shopId > 0 && !this.isSupervisor) {
      this.employeeService.getSupervisor(shopId)
        .subscribe(results => this.onSupervisorLoadSuccessful(results));
    }
  }


  onSupervisorLoadSuccessful(result: ResponseModel) {
    this.supervisors = result.data as Employee[];
  }

  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.employeeEdit.employeeName = this.employeeEdit.employeeName.trim();
    if (this.isNewEmployee) {
      this.employeeService.createEmployee(this.employeeEdit).subscribe(result => this.saveSuccessHelper(result));
    } else {
      this.employeeService.updateEmployee(this.employeeEdit).subscribe(result => this.saveSuccessHelper(result));
    }
  }


  private saveSuccessHelper(result: ResponseModel) {
    // this.testIsRoleEmployeeCountChanged(this.employee, this.employeeEdit);

    this.alertService.stopLoadingMessage();


    this.resetForm();
    if (this.isNewEmployee) {
      this.alertService.showMessage('Success', `Employee \"${this.employeeEdit.employeeName}\" was created successfully`,
        MessageSeverity.success);
    } else {
      this.alertService.showMessage('Success', `Changes to employee \"${this.employeeEdit.employeeName}\"  was saved successfully`,
        MessageSeverity.success);
    }
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }


  cancel() {
    this.employeeEdit = new Employee();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.employeeEdit = new Employee();
    this.resetForm();
  }


  resetForm() {
    this.employeeFormGroup.reset();
  }


  newEmployee() {
    this.isNewEmployee = true;
    this.employeeEdit = new Employee();
    return this.employeeEdit;
  }

  editEmployee(employee: Employee) {
    this.initForm();
    if (employee) {
      this.isNewEmployee = false;
      this.employeeFormGroup.removeControl('password');
      this.employeeFormGroup.removeControl('confirmPassword');
      this.employeeEdit = new Employee();
      Object.assign(this.employeeEdit, employee);
      this.employeeEdit.employeeId = employee.employeeId;
      this.employeeNameControl.setValue(this.employeeEdit.employeeName);
      this.employeeImageControl.setValue(this.employeeEdit.image);
      this.employeeTypeControl.setValue(this.employeeEdit.role);
      this.employeeShopControl.setValue(this.employeeEdit.shopId);
      this.employeeStartTimeControl.setValue(this.employeeEdit.startTime);
      this.employeeEndTimeControl.setValue(this.employeeEdit.endTime);
      this.employeePhoneNumberControl.setValue(this.employeeEdit.phoneNumber);
      return this.employeeEdit;
    } else {
      this.isNewEmployee = true;
      return new Employee();
    }
  }

  formSubmit() {
    if (this.employeeFormGroup.valid) {
      this.employeeEdit.image = this.employeeImageControl.value;
      this.employeeEdit.employeeName = this.employeeNameControl.value;
      this.employeeEdit.role = this.employeeTypeControl.value;
      this.employeeEdit.phoneNumber = this.employeePhoneNumberControl.value;
      this.employeeEdit.startTime = this.employeeStartTimeControl.value;
      this.employeeEdit.endTime = this.employeeEndTimeControl.value;
      this.employeeEdit.password = this.employeePasswordControl.value;
      this.employeeEdit.shopId = this.employeeShopControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }


  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.image = e.currentTarget.result;
        this.employeeImageControl.setValue(this.image);
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  setShops(shops: Shop[]) {
    this.shops = shops;
  }
}
