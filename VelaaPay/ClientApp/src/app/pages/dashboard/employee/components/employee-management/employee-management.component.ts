import {AlertService, DialogType} from '../../../../../services/alert.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {EmployeeInfoComponent} from '../employee-info/employee-info.component';
import {EmployeeSendComponent} from '../employee-send/employee-send.component';
import {EmployeeService} from '../../services/employee.service';
import {Shop} from '../../../../../models/shop';
import {Employee} from '../../../../../models/employee';
import {AuthService} from '../../../../../services/auth.service';
import {ShopService} from '../../../shop/services/shop.service';
import {ResponseModel} from '../../../../../models/response.model';
import {EmployeePasswordComponent} from '../employee-password/employee-password.component';
import {EmployeeType} from '../../../../../models/enums';
import {RoleNames} from '../../../../../constants/role-names';

@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.component.html',
  styleUrls: ['./employee-management.component.scss']
})
export class EmployeeManagementComponent implements OnInit {

  sort = '';
  direction = 'desc';
  search = '';
  page = 0;
  shops: Shop[] = [];
  totalPage = 0;
  rows: Employee[] = [];
  shopId = 0;
  loadingIndicator: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: any;
  displayedColumns: string[] = ['employeeName', 'employeeLevel', 'phoneNumber',
    'shopName', 'startTime', 'endTime', 'createdDate', 'action'];


  constructor(private alertService: AlertService,
              private dialog: MatDialog,
              private authService: AuthService,
              private shopService: ShopService,
              private employeeService: EmployeeService) {
  }


  ngOnInit() {

    this.dataSource = new MatTableDataSource();
    this.page = 0;
    this.loadShops();
  }
  loadShops() {
    this.shopService.getComboBoxShops().subscribe(result => {
      this.shops = result.data as Shop[];
      this.loadData();
    });
  }
  loadData(page = 1, pageSize = 10, search = '') {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;
    this.employeeService.getEmployees(page, pageSize, search, this.sort, this.direction)
      .subscribe(results => this.onDataLoadSuccessful(results));
  }

  onDataLoadSuccessful(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.rows = result.data as Employee[];
    this.rows.forEach(p => {
      if (+p.employeeLevel == +EmployeeType.Cashier) {
        p.role = RoleNames.Cashier;
      } else {
        p.role = RoleNames.Supervisor;
      }
    });
    this.totalPage = result.count;
    this.dataSource.data = this.rows;

  }

  changePage(pageInfo: PageEvent) {
    this.loadData(pageInfo.pageIndex + 1, 10, this.search);
  }

  onSort(sort: Sort) {
    this.sort = sort.active;
    this.direction = sort.direction;
    if (this.direction) {
      this.loadData(1, 10, this.search);
    } else {
      this.sort = '';
      this.loadData(1, 10, this.search);
    }
  }


  onSearchChanged(value: string) {
    this.search = value;
    this.page = 0;
    this.loadData(this.page + 1, 10, this.search);
  }


  addEmployee() {
    const modal = this.dialog.open(EmployeeInfoComponent, {
      width: '600px',
      role: 'dialog',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.setShops(this.shops);
    modal.componentInstance.editEmployee(null);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.page = 0;
      this.loadData();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  editEmployee(row: Employee) {
    const modal = this.dialog.open(EmployeeInfoComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.setShops(this.shops);
    modal.componentInstance.editEmployee(row);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
      this.page = 0;
      this.loadData();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  deleteEmployee(row: Employee) {
    this.alertService.showDialog('Are you sure you want to delete \"' + row.employeeName + '\"?', DialogType.confirm,
      () => this.deleteEmployeeHelper(row));
  }


  deleteEmployeeHelper(row: Employee) {
    this.alertService.closeDialog();
    this.alertService.startLoadingMessage('Deleting...');
    this.loadingIndicator = true;

    this.employeeService.deleteEmployee(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.page = 0;
        this.search = '';
        this.loadData();
      });
  }

  changeEmployeePassword(element: Employee) {
    const modal = this.dialog.open(EmployeePasswordComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.editEmployee(element);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }

  sendMoney(element: Employee) {
    const modal = this.dialog.open(EmployeeSendComponent, {
      width: '600px',
      panelClass: 'custom-dialog',
    });
    modal.componentInstance.editEmployee(element);
    modal.componentInstance.changesSavedCallback = () => {
      modal.close();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };

  }
}
