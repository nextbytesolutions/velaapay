import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {Employee} from '../../../../../models/employee';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ResponseModel} from '../../../../../models/response.model';
import {EmployeeService} from '../../services/employee.service';

@Component({
  selector: 'app-employee-send',
  templateUrl: './employee-send.component.html',
  styleUrls: ['./employee-send.component.scss']
})
export class EmployeeSendComponent implements OnInit {

  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  employeeEdit: Employee = new Employee();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  employeeAmountControl: FormControl;
  employeeFormGroup: FormGroup;
  constructor(private alertService: AlertService,
              private employeeService: EmployeeService,
              private cd: ChangeDetectorRef) { }

  ngOnInit() {
  }

  initForm() {
    this.employeeAmountControl = new FormControl('', [Validators.required, Validators.min(1)]);
    this.employeeFormGroup = new FormGroup({
      amount: this.employeeAmountControl,
    });

  }

  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Sending Money...');
    this.employeeService.transferMoneyToEmployee(this.employeeEdit.userId, this.employeeAmountControl.value)
      .subscribe(result => this.saveSuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.resetForm();
    this.alertService.showMessage('Success', `Money sent to ` + this.employeeEdit.employeeName, MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }

  cancel() {
    this.employeeEdit = new Employee();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.employeeEdit = new Employee();
    this.resetForm();
  }


  resetForm() {
    this.employeeFormGroup.reset();
  }



  editEmployee(employee: Employee) {
    this.initForm();
    if (employee) {
      this.employeeEdit = new Employee();
      Object.assign(this.employeeEdit, employee);
      this.employeeEdit.userId = employee.userId;
      return this.employeeEdit;
    } else {
      return new Employee();
    }
  }

  formSubmit() {
    if (this.employeeFormGroup.valid) {
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }

}
