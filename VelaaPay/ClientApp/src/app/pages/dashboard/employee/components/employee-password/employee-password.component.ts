import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployeeService} from '../../services/employee.service';
import {Employee} from '../../../../../models/employee';
import {AlertService, MessageSeverity} from '../../../../../services/alert.service';
import {ResponseModel} from '../../../../../models/response.model';
import {CustomValidator} from '../../../../../customValidator/swift-code-validator';

@Component({
  selector: 'app-employee-password',
  templateUrl: './employee-password.component.html',
  styleUrls: ['./employee-password.component.scss']
})
export class EmployeePasswordComponent implements OnInit {

  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  employeeEdit: Employee = new Employee();
  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  employeePasswordControl: FormControl;
  employeeConfirmPasswordControl: FormControl;
  employeeFormGroup: FormGroup;

  constructor(private alertService: AlertService,
              private employeeService: EmployeeService,
              private cd: ChangeDetectorRef) {

  }

  ngOnInit() {
  }


  initForm() {
    this.employeePasswordControl = new FormControl('', [Validators.required,
      Validators.pattern('(?=^.{8,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z!@#$%^&*()]*$')]);
    this.employeeConfirmPasswordControl = new FormControl('', [Validators.required,
      CustomValidator.PasswordMatch(this.employeePasswordControl),
      Validators.pattern('(?=^.{8,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z!@#$%^&*()]*$')]);
    this.employeeFormGroup = new FormGroup({
      password: this.employeePasswordControl,
      confirmPassword: this.employeeConfirmPasswordControl,
    });

  }



  save() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Saving changes...');
    this.employeeService.updateEmployeePassword(this.employeeEdit).subscribe(result => this.saveSuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.resetForm();
    this.alertService.showMessage('Success', `Changes to employee \"${this.employeeEdit.employeeName}\"  was saved successfully`,
      MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }


  cancel() {
    this.employeeEdit = new Employee();
    this.resetForm();
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.employeeEdit = new Employee();
    this.resetForm();
  }


  resetForm() {
    this.employeeFormGroup.reset();
  }



  editEmployee(employee: Employee) {
    this.initForm();
    if (employee) {
      this.employeeEdit = new Employee();
      Object.assign(this.employeeEdit, employee);
      this.employeeEdit.employeeId = employee.employeeId;
      return this.employeeEdit;
    } else {
      return new Employee();
    }
  }

  formSubmit() {
    if (this.employeeFormGroup.valid) {
      this.employeeEdit.password = this.employeePasswordControl.value;
      this.employeeEdit.confirmPassword = this.employeeConfirmPasswordControl.value;
      this.save();
    } else {
      this.alertService.showMessage('Error', 'There are Errors in Form.\r\n Please do fix them', MessageSeverity.error);
    }

  }


}
