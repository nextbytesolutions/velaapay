import { Injectable } from '@angular/core';
import {LoginEndpointService} from './login-endpoint.service';
import {LoginResponse} from '../../../response/login-response';
import {Observable} from 'rxjs';
import {ChangePassword} from '../../../models/change-password';
import {ResponseModel} from '../../../models/response.model';
import {UserLogin} from '../../../models/user-login.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(protected loginEndpoint: LoginEndpointService) {

  }

  login(userLogin: UserLogin): Observable<LoginResponse> {
    return this.loginEndpoint.getLoginEndpoint<LoginResponse>(userLogin);
  }
  refreshToken(token: string): Observable<LoginResponse> {
    return this.loginEndpoint.getRefreshLoginEndpoint<LoginResponse>(token);
  }

  changePassword(changePassword: ChangePassword) {
    return this.loginEndpoint.getChangePasswordEndpoint(changePassword).map((response) => <ResponseModel>response);
  }
}
