import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from '../pages.component';
import {LoginFormComponent} from './components/login-form/login-form.component';

const routes: Routes = [{
  path: '',
  component: LoginFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule { }
