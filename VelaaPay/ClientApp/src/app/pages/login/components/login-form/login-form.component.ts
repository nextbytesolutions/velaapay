import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertService, MessageSeverity} from '../../../../services/alert.service';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';
import {LoginService} from '../../services/login.service';
import {UserLogin} from '../../../../models/user-login.model';
import {RoleNames} from '../../../../constants/role-names';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {

  loginFormGroup: FormGroup;
  userNameControl: FormControl;
  passwordControl: FormControl;
  roleControl: FormControl;
  roles = [RoleNames.Administrator, RoleNames.Vendor, RoleNames.Supervisor, RoleNames.Cashier];
  constructor(private authService: AuthService,
              public alertService: AlertService,
              private loginService: LoginService,
              private router: Router) {
  }

  ngOnInit() {
    this.initForm();
    this.authService.reevaluateLoginStatus();
    if (this.authService.isLoggedIn) {
      this.authService.redirectLoggedInUser();
    }
  }


  initForm() {
    this.userNameControl = new FormControl('', [Validators.required]);
    this.passwordControl = new FormControl('', [Validators.required]);
    this.roleControl = new FormControl('', [Validators.required]);
    this.loginFormGroup = new FormGroup({
      userName: this.userNameControl,
      password: this.passwordControl,
      role: this.roleControl,
    });
  }

  goToRegisterPage() {
    this.router.navigateByUrl('/merchant-registration');
  }

  formSubmit() {
    if (this.loginFormGroup.valid) {
      this.alertService.startLoadingMessage('Authenticating...');
      const userLogin = new UserLogin();
      userLogin.email = this.userNameControl.value;
      userLogin.password = this.passwordControl.value;
      userLogin.role = this.roleControl.value;
      this.loginService.login(userLogin).subscribe(result => {
        this.alertService.stopLoadingMessage();
        const user = this.authService.processLogin(result);
        this.alertService.showMessage('Success', 'Welcome ' + user.fullName, MessageSeverity.success);
        this.authService.redirectLoggedInUser();

      });
    }
  }
}
