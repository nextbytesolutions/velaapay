import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MerchantRegistrationComponent} from './components/merchant-registration/merchant-registration.component';

const routes: Routes = [
  {
    path: '',
    component: MerchantRegistrationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantRegistrationRoutingModule { }
