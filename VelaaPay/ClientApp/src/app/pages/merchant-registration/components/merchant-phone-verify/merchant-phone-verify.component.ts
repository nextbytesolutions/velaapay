import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ResponseModel} from '../../../../models/response.model';
import {AlertService, MessageSeverity} from '../../../../services/alert.service';
import {MerchantRegistrationService} from '../../services/merchant-registration.service';

@Component({
  selector: 'app-merchant-phone-verify',
  templateUrl: './merchant-phone-verify.component.html',
  styleUrls: ['./merchant-phone-verify.component.scss']
})
export class MerchantPhoneVerifyComponent implements OnInit {

  constructor(private alertService: AlertService, private merchantService: MerchantRegistrationService) { }

  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;


  step = 1;
  get isSaving (){
    return this.alertService.isLoadingInProgress;
  };
  phone: string;
  phoneFormGroup: FormGroup;
  phoneControl: FormControl;
  codeControl: FormControl;
  ngOnInit() {

  }


  initForm() {
    this.phoneControl = new FormControl(this.phone, [Validators.required]);
    this.codeControl = new FormControl('', [Validators.required]);
    this.phoneFormGroup = new FormGroup({
      phone: this.phoneControl,
      code: this.codeControl,
    });
  }

  setPhone(phone: string) {
    this.phone = phone;
    this.initForm();
  }


  sendOtp() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Sending OTP...');
    this.merchantService.sendPhoneOtp(this.phoneControl.value)
      .subscribe(result => this.saveSuccessHelper(result));
  }

  verifyOtp() {
    this.alertService.resetStickyMessage();
    this.alertService.startLoadingMessage('Verifying OTP...');
    this.merchantService.verifyPhoneOtp(this.phoneControl.value, this.codeControl.value)
      .subscribe(result => this.verifySuccessHelper(result));
  }


  private saveSuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', `Otp sent successfully`, MessageSeverity.success);
    this.step = 2;
  }
  private verifySuccessHelper(result: ResponseModel) {
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', `Otp sent successfully`, MessageSeverity.success);
    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }

  }



  cancel() {
    this.alertService.resetStickyMessage();
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }

  backStep() {
    if (this.step == 2) {
      this.step--;
    }
  }
}
