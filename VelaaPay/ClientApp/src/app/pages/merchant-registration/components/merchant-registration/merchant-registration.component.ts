import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Merchant} from '../../../../models/merchant';
import {MerchantRegistrationService} from '../../services/merchant-registration.service';
import {AlertService, MessageSeverity} from '../../../../services/alert.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {MerchantPhoneVerifyComponent} from '../merchant-phone-verify/merchant-phone-verify.component';

@Component({
  selector: 'app-merchant-registration',
  templateUrl: './merchant-registration.component.html',
  styleUrls: ['./merchant-registration.component.scss']
})
export class MerchantRegistrationComponent implements OnInit {

  get isSaving() {
    return this.alertService.isLoadingInProgress;
  }
  isError = false;
  errorMessage = '';
  isRegistered = false;
  isPhoneVerified = false;
  merchant: Merchant;
  constructor(private merchantService: MerchantRegistrationService,
              private router: Router,
              private dialog: MatDialog,
              private cd: ChangeDetectorRef,
              private alertService: AlertService) { }

  ngOnInit() {
    this.merchant = new Merchant();
    this.merchant.role = 'SuperMerchant';
    this.merchant.businessType = 'Test';
  }

  registerMerchant() {
    this.alertService.startLoadingMessage('Registering...');
    this.merchantService.createMerchant(this.merchant).subscribe(result => {
      const token = result.data.token;
      const id = result.data.userId;
      this.onSuccess(token, id);
    });
  }





  verifyPhone() {
    const modal = this.dialog.open(MerchantPhoneVerifyComponent, {
      width: '600px',
      panelClass: 'custom-dialog'
    });
    modal.componentInstance.setPhone(this.merchant.phoneNumber);
    modal.componentInstance.changesSavedCallback = () => {
      this.isPhoneVerified = true;
      modal.close();
      this.registerMerchant();
    };

    modal.componentInstance.changesCancelledCallback = () => {
      modal.close();
    };
  }
  onSuccess(token: string, id: string) {
    this.alertService.stopLoadingMessage();
    this.alertService.showMessage('Success', 'Merchant is registered successfully', MessageSeverity.success);
    this.isRegistered = true;
    setTimeout(() => {
      this.router.navigateByUrl('/set-password?token=' + token + '&userId=' + id);
    }, 1000);
  }

  goBack() {
    this.router.navigateByUrl('/login');
  }

  onFileAdded(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.image = e.currentTarget.result;
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }

  onFileAddedIdentification(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.identificationImage = e.currentTarget.result;
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
  onFileAddedRccm(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = (e: any) => {
        this.merchant.rccmImage = e.currentTarget.result;
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
}
