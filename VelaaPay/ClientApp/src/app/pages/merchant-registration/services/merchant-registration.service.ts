import { Injectable } from '@angular/core';
import {MerchantRegistrationEndpointService} from './merchant-registration-endpoint.service';
import {Merchant} from '../../../models/merchant';
import {ResponseModel} from '../../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class MerchantRegistrationService {

  constructor(private merchantService: MerchantRegistrationEndpointService) { }

  createMerchant(merchant: Merchant) {
    return this.merchantService.getCreateMerchantsEndpoint(merchant)
      .map((response) => <ResponseModel>response);
  }

  sendPhoneOtp(phone: string) {
    return this.merchantService.getSendPhoneOtpEndpoint(phone)
      .map((response) => <ResponseModel>response);
  }

  verifyPhoneOtp(phone: string, code: string) {
    return this.merchantService.getVerifyPhoneOtpEndpoint(phone, code)
      .map((response) => <ResponseModel>response);
  }
}
