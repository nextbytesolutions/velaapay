import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { EndpointFactoryService} from '../../../services/endpoint-factory.service';
import {Merchant} from '../../../models/merchant';
import {RoleNames} from '../../../constants/role-names';

@Injectable({
  providedIn: 'root'
})
export class MerchantRegistrationEndpointService extends EndpointFactoryService {

  private readonly _createMerchantUrl: string = '/api/v1/merchant';
  private readonly _sendOtpPhone: string = '/api/v1/merchant/sendOtp';
  private readonly _verifyOtpPhone: string = '/api/v1/merchant/verifyOtp';

  get MerchantsUrl() {
    return this.configurations.baseUrl + this._createMerchantUrl;
  }
  get SendPhoneUrl() {
    return this.configurations.baseUrl + this._sendOtpPhone;
  }
  get VerifyPhoneUrl() {
    return this.configurations.baseUrl + this._verifyOtpPhone;
  }

  getCreateMerchantsEndpoint(merchant: Merchant): Observable<any> {
    return this.http.post(this.MerchantsUrl, merchant, {headers: this.getRequestHeaders().headers});
  }

  getSendPhoneOtpEndpoint(phoneNumber: string) {
    return this.http.put(this.SendPhoneUrl, {
      phoneNumber,
      role: RoleNames.Vendor
    }, {headers: this.getRequestHeaders().headers});
  }
  getVerifyPhoneOtpEndpoint(phoneNumber: string, code: string) {
    return this.http.put(this.VerifyPhoneUrl, {
      phoneNumber,
      code,
      role: RoleNames.Vendor
    }, {headers: this.getRequestHeaders().headers});
  }
}
