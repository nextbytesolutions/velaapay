import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantRegistrationComponent } from './components/merchant-registration/merchant-registration.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MerchantRegistrationRoutingModule} from './merchant-registration-routing.module';
import { MerchantPhoneVerifyComponent } from './components/merchant-phone-verify/merchant-phone-verify.component';
import {MaterialModule} from '../../shared/material.module';

@NgModule({
  declarations: [MerchantRegistrationComponent, MerchantPhoneVerifyComponent],
  entryComponents: [MerchantPhoneVerifyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MerchantRegistrationRoutingModule
  ]
})
export class MerchantRegistrationModule { }
