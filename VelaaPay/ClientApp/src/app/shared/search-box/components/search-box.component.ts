// ====================================================


// ====================================================

import {Component, ViewChild, ElementRef, Input, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent {

  @Input()
  showSuggestions = false;
  @Input()
  isLoading = false;

  @Input()
  suggestions: string[] = [];

  @Input()
  placeholder: string = "Search...";

  @Input()
  set searchValue(value: string){
    this.searchInput.nativeElement.value = value;
  };

  @Output()
  searchChange = new EventEmitter<string>();

  @Output()
  valueChange = new EventEmitter<string>();


  @ViewChild("searchInput")
  searchInput: ElementRef;


  constructor(private ngCheck: ChangeDetectorRef) {

  }
  onSearchChange() {
    setTimeout(() => this.searchChange.emit(this.searchInput.nativeElement.value));
  }

  onValueChange() {
    setTimeout(() => this.valueChange.emit(this.searchInput.nativeElement.value),250);
  }


  clear() {
    this.searchInput.nativeElement.value = '';
    this.onSearchChange();
  }

  setValue(value: string) {
    this.searchInput.nativeElement.value = value;
    this.ngCheck.detectChanges();
    this.ngCheck.markForCheck();
    this.onSearchChange();
  }

  onEmptyValueChange(value: string) {

  }
}
