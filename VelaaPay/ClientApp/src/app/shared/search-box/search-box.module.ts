import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBoxComponent} from './components/search-box.component';
import { FormsModule } from '@angular/forms';
import {MaterialModule} from '../material.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MaterialModule
    ],
    declarations: [SearchBoxComponent],
    exports: [SearchBoxComponent]
})


export class SearchBoxModule { }
