import { NgModule } from '@angular/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatOptionModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatCardModule, MatToolbarModule,
    MatSidenavModule, MatSnackBarModule, MatIconModule, MatListModule,
    MatFormFieldModule, MatInputModule, MatDialogModule, MatTableModule, MatSortModule,
    MatPaginatorModule, MatOptionModule, MatSelectModule, MatTooltipModule, MatSlideToggleModule, MatSliderModule,
    MatAutocompleteModule, MatDatepickerModule, MatNativeDateModule],
  exports: [MatButtonModule, MatCheckboxModule, MatCardModule, MatToolbarModule,
    MatSidenavModule, MatSnackBarModule, MatIconModule, MatListModule,
    MatFormFieldModule, MatInputModule, MatDialogModule, MatTableModule, MatSortModule,
    MatPaginatorModule, MatOptionModule, MatSelectModule, MatTooltipModule, MatSlideToggleModule, MatSliderModule,
    MatAutocompleteModule, MatDatepickerModule, MatNativeDateModule],
})
export class MaterialModule { }
