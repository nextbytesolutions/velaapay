import { Pipe, PipeTransform } from '@angular/core';
import {CompanyType, EmployeeType} from '../models/enums';
import {Employee} from '../models/employee';

@Pipe({
  name: 'employeeType'
})
export class EmployeeTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case EmployeeType.Vendor:
        return 'Merchant';
      case EmployeeType.Supervisor:
        return 'Supervisor';
      case EmployeeType.Cashier:
        return 'Cashier';
    }
  }
}
