import { NgModule } from '@angular/core';
import { LoadTypePipe } from './load-type.pipe';
import { RequestTypePipe } from './request-type.pipe';
import { WithdrawRequestTypePipe } from './withdraw-request-type.pipe';
import { CompanyTypePipe } from './company-type.pipe';
import { CashInRequestTypePipe } from './cash-in-request-type.pipe';
import { SafeResourcePipe } from './safe-resource.pipe';
import { EmployeeTypePipe} from './employee-type.pipe';
import {CardTypePipe} from "./card-type.pipe";

@NgModule({
  imports: [
  ],
  declarations: [LoadTypePipe, RequestTypePipe, WithdrawRequestTypePipe, CompanyTypePipe,
    CashInRequestTypePipe, SafeResourcePipe, EmployeeTypePipe, CardTypePipe],
  exports: [LoadTypePipe, RequestTypePipe, WithdrawRequestTypePipe, CompanyTypePipe,
    CashInRequestTypePipe, SafeResourcePipe, EmployeeTypePipe, CardTypePipe]
})
export class PipeModule { }
