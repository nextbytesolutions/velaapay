import { Pipe, PipeTransform } from '@angular/core';
import {CashInRequestType} from '../models/enums';

@Pipe({
  name: 'cashInRequestType'
})
export class CashInRequestTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case CashInRequestType.Bank:
        return 'Bank';
      case CashInRequestType.Orange:
        return 'Orange';
      case CashInRequestType.Wari:
        return 'Wari';
      case CashInRequestType.Stripe:
        return 'Stripe';
    }
  }

}
