import { Pipe, PipeTransform } from '@angular/core';
import {CompanyType, LoadType} from '../models/enums';

@Pipe({
  name: 'companyType'
})
export class CompanyTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case CompanyType.BillPayment:
        return 'Bill Payment';
      case CompanyType.AirTopUp:
        return 'Air Top Up';

    }
  }

}
