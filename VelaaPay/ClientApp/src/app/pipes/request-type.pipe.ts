import { Pipe, PipeTransform } from '@angular/core';
import { RequestStatus} from '../models/enums';

@Pipe({
  name: 'requestType'
})
export class RequestTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case RequestStatus.None:
        return '<label class="badge badge-default">None</label>';
      case RequestStatus.Rejected:
        return '<label class="badge badge-danger">Rejected</label>';
      case RequestStatus.Accepted:
        return '<label class="badge badge-success">Accepted</label>';
    }
  }

}
