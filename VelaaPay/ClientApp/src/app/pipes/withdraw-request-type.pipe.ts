import { Pipe, PipeTransform } from '@angular/core';
import {WithdrawRequestType} from '../models/enums';

@Pipe({
  name: 'withdrawRequestType'
})
export class WithdrawRequestTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case WithdrawRequestType.MPesa:
        return 'MPesa';
      case WithdrawRequestType.Bank:
        return 'Bank';
    }
  }

}
