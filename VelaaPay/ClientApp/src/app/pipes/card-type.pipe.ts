import { Pipe, PipeTransform } from '@angular/core';
import {CardType} from "../models/enums";

@Pipe({
  name: 'cardType'
})
export class CardTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case CardType.AmericanExpress:
        return 'American Express';
      case CardType.Diners:
        return 'Diners';
      case CardType.Discover:
        return 'Discover';
      case CardType.Jcb:
        return 'JCB';
      case CardType.MasterCard:
        return 'Master Card';
      case CardType.Visa:
        return 'Visa';
    }
  }

}
