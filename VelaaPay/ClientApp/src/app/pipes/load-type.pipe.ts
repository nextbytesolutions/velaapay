import {Pipe, PipeTransform} from '@angular/core';
import {LoadType} from '../models/enums';

@Pipe({
  name: 'loadType'
})
export class LoadTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case LoadType.SendMoney:
        return 'Send Money';
      case LoadType.AirTimeTopUpRequest:
        return 'AirTimeTopup Requests';
      case LoadType.DebitCard:
        return 'Debit Card';
      case LoadType.Sender:
        return 'Request Money';
      case LoadType.Vendor:
        return 'Vendor Payment';
      case LoadType.Withdraw:
        return 'Withdraw Mooney';
      case LoadType.Bill:
        return 'Bill Payments';
      case LoadType.BillRejected:
        return 'Bill Rejected';
      case LoadType.WithdrawRequestRejected:
        return 'Withdraw Request Rejected';
      case LoadType.AirTimeTopUpRequestRejected:
        return 'AirTime Topup Request Rejected';
      case LoadType.CashIn:
        return 'Cash In By Employee';
      case LoadType.CashInRequest:
        return 'Cash In Request By Vendor';
      case LoadType.CashOut:
        return 'Cash Out By Employee';
      case LoadType.TransferMoney:
        return 'Transfer Money to Employee';
      case LoadType.ScanToPay:
        return 'Scan To Pay';
      case LoadType.PurchaseItem:
        return 'Purchase Item';

    }
  }
}


