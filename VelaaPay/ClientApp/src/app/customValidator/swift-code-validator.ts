import {AbstractControl, ValidatorFn} from '@angular/forms';

export class CustomValidator {
  static swiftCodeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return { 'swiftCode': true };
      }
      if ((control.value.length != min && control.value.length != max)) {
        return { 'swiftCode': true };
      }
      return null;
    };
  }
  static PasswordMatch(passwordControl: AbstractControl): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return { 'password': true };
      }
      if ((control.value !== passwordControl.value)) {
        return { 'password': true };
      }
      return null;
    };
  }

  static NoUnderScore(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return { 'underscore': true };
      }
      if ((control.value.toString().includes('_'))) {
        return { 'underscore': true };
      }
      return null;
    };
  }

  static Phone(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {

      if (control.value === undefined || control.value == null) {
        return { 'phone': true };
      }
      const  value = control.value.toString();
      if (!(value.startsWith('+') && value.length >= 11 && value.length <= 15)) {
        return { 'phone': true };
      }
      return null;
    };
  }
}
