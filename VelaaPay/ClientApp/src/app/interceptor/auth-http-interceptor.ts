import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {AuthService} from '../services/auth.service';
import {AlertService, MessageSeverity} from '../services/alert.service';
import {Router} from '@angular/router';
import {ResponseErrorModel} from '../models/response-error-model';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/catch';
import {ErrorHandlerService} from '../services/error-handler.service';
import {LoginService} from '../pages/login/services/login.service';
import {from, Observable, Subject, throwError} from 'rxjs';
import { mergeMap, switchMap, catchError } from 'rxjs/operators';
@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {
  constructor(
    private alertService: AlertService,
    private router: Router,
    private loginService: LoginService,
    private errorHandler: ErrorHandlerService,
    private authService: AuthService) {
  }

  private isRefreshing = false;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!window.navigator.onLine) {
      return throwError(
        // in place of clone:
        new HttpErrorResponse({
          error: this.CustomNotOnlineResponse(),
        }),
      );
    }
    if (this.authService.isLoggedIn) {
      if (this.authService.isSessionExpired && !this.isRefreshing) {
        this.alertService.startLoadingMessage('Refreshing Session...');
        this.isRefreshing = true;
        return this.loginService.refreshToken(this.authService.refreshToken).pipe(
          mergeMap(data => {
            this.isRefreshing = false;
            this.authService.processRefreshToken(data);
            return next.handle(req)
              .map(event => {
                if (event instanceof HttpResponse) {
                  if (event.body && event.body.message) {
                    event = event.clone({body: this.resolveError(event.body)});
                  }
                }
                return event;
              })
              .catch((error: HttpErrorResponse) => {
                this.errorHandler.handleError(error);
                return throwError(error);
              });
          }),
          catchError((err) => {
              this.isRefreshing = false;
              this.authService.logout();
              this.errorHandler.handleError(err);
              return from([]);
          }));
      }
    }
    return next.handle(req)
      .map(event => {
        if (event instanceof HttpResponse) {
          if (event.body && event.body.message) {
            event = event.clone({body: this.resolveError(event.body)});
          }
        }
        return event;
      })
      .catch((error: HttpErrorResponse) => {
        this.errorHandler.handleError(error);
        return throwError(error);
      });
  }

  private resolveError(body: any) {
    return {
      response: null,
      error: this.CustomMessageResponse(body.message)
    };
  }


  CustomMessageResponse(msg: string): ResponseErrorModel {
    const data: ResponseErrorModel = new ResponseErrorModel();
    data.errors = [];
    data.message = msg;
    return data;
  }

  CustomNotOnlineResponse(): ResponseErrorModel {
    const data: ResponseErrorModel = new ResponseErrorModel();
    data.message = 'Token Expired';
    return data;
  }

}
