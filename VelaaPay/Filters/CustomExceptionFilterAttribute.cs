using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;

namespace VelaaPay.Presentation.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger _logger;

        public CustomExceptionFilterAttribute(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("Exception");
        }
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ValidationException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new ResponseBadRequestViewModel
                {
                    Errors = ((ValidationException) context.Exception).Failures
                        .SelectMany(p => p.Value.Select(x => p.Key + " " + x).ToList()).ToList(),
                    Message = "Invalid Object"
                });
                return;
            }
            
            if (context.Exception is TokenExpiredException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                context.Result = new JsonResult(new UnAuthorizedBadRequestViewModel
                {
                    Message = context.Exception.Message,
                    IsExpired = true
                });
                return;
            }
            if (context.Exception is TokenNotFoundException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                context.Result = new JsonResult(new UnAuthorizedBadRequestViewModel
                {
                    Message = context.Exception.Message,
                    IsExpired = false
                });
                return;
            }
            
            var code = HttpStatusCode.InternalServerError;

            if (context.Exception is BadRequestException || context.Exception is TransactionExceededException ||context.Exception is DeleteFailureException ||context.Exception is AlreadyExistsException || context.Exception is CannotDeleteException)
            {
                code = HttpStatusCode.BadRequest;
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int) code;
                context.Result = new JsonResult(new CustomResponseExceptionViewModel()
                {
                    Message = context.Exception.Message
                });
                return;
            }


            if (context.Exception is NotFoundException)
            {
                code = HttpStatusCode.NotFound;
            }
            else if (context.Exception is ThirdPartyException)
            {
                code = HttpStatusCode.InternalServerError;
            }
            
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = (int) code;
            if (context.Exception.InnerException == null)
            {
                context.Result = new JsonResult(new ResponseExceptionViewModel
                {
                    Exception = context.Exception.Message,
                    Message = "Internal Server Error has occured"
                });
            }
            else
            {
                context.Result = new JsonResult(new ResponseExceptionViewModel
                {
                    Exception = context.Exception.InnerException.Message,
                    Message = "Internal Server Error has occured"
                });
            }
            WriteLogs(context.HttpContext.Request.Path,context.Exception);
        }

        private void WriteLogs(string path, Exception exception)
        {
            if (exception.InnerException != null)
            {
                _logger.LogError("Error: {Name} {@Request} " + Environment.NewLine + " {@Stack}", path, exception.Message + " -- " + exception.InnerException.Message, exception.StackTrace);
            }
            else
            {
                _logger.LogError("Error: {Name} {@Request} " + Environment.NewLine + " {@Stack}", path, exception.Message, exception.StackTrace);
            }
        }
    }
}