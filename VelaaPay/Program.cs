using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.Initializer;

namespace VelaaPay.Presentation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            IWebHost host = BuildWebHost(args);

            using (IServiceScope scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                try
                {
                    scope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    DatabaseInitializer.Initialize(context);

                }
                catch (Exception ex)
                {
                    ILogger<Program> logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogCritical("Error Occured When Initializing Database", ex, "DB Name");
                }
            }
            try
            {
                host.Run();
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseNLog()
                .Build();
    }
}
