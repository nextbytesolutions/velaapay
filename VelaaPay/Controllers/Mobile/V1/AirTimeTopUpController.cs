using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.AirTimeTopUp.Commands.AcceptAirTimeTopUp;
using VelaaPay.Application.AirTimeTopUp.Commands.CreateAirTimeTopUp;
using VelaaPay.Application.AirTimeTopUp.Commands.RejectAirTimeTopUp;
using VelaaPay.Application.AirTimeTopUp.Queries.GetAirTimeTopUp;
using VelaaPay.Application.AirTimeTopUp.Queries.GetAirTimeTopUpByUser;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class AirTimeTopUpController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetAirTimeTopUps([FromQuery] GetAirTimeTopUpRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("byUser")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetAirTimeTopUpByUser([FromQuery] GetAirTimeTopUpByUserRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> CreateAirTimeTopUp(CreateAirTimeTopUpRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("accept/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> AcceptAirTimeTopUp([FromRoute]AcceptAirTimeTopUpRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("reject/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> RejectAirTimeTopUp([FromRoute]RejectAirTimeTopUpRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}