using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Shops.Commands.CreateShop;
using VelaaPay.Application.Shops.Commands.DeleteShop;
using VelaaPay.Application.Shops.Commands.UpdateShop;
using VelaaPay.Application.Shops.Queries.GetShops;
using VelaaPay.Application.Shops.Queries.GetShopsByUser;
using VelaaPay.Application.Shops.Queries.GetShopsComboBox;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class ShopController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin + "," + RoleNames.Vendor)]
        public async Task<IActionResult> GetShops([FromQuery] GetShopsRequestModel model)
        {
            if (User.IsInRole(RoleNames.Admin))
            {
                var response = await Mediator.Send(model);
                return StatusCode(response.Status, response.Response);
            }
            else
            {
                var byUserModel = new GetShopsByUserRequestModel();
                model.Copy(byUserModel);
                byUserModel.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
                var response = await Mediator.Send(byUserModel);
                return StatusCode(response.Status, response.Response);
            }
        }
        [HttpGet("comboBox")]
        [Authorize(Roles = RoleNames.Admin + "," + RoleNames.Vendor + "," + RoleNames.Supervisor)]
        public async Task<IActionResult> GetShopsComboBox([FromQuery] GetShopsComboBoxRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("byUser")]
        [Authorize(Roles = RoleNames.Vendor)]
        public async Task<IActionResult> GetShopByUser([FromQuery] GetShopsByUserRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Vendor)]
        public async Task<IActionResult> CreateShop([FromBody] CreateShopRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut]
        [Authorize(Roles = RoleNames.Vendor)]
        public async Task<IActionResult> UpdateShop([FromBody] UpdateShopRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpDelete("{shopId}")]
        [Authorize(Roles = RoleNames.Vendor)]
        public async Task<IActionResult> DeleteShop([FromRoute] DeleteShopRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}