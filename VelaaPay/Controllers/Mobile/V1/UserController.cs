using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Users.Commands.ChangePassword;
using VelaaPay.Application.Users.Commands.ForgetPassword;
using VelaaPay.Application.Users.Commands.ResetPassword;
using VelaaPay.Application.Users.Commands.UpdateFcmId;
using VelaaPay.Application.Users.Commands.UpdateIdentification;
using VelaaPay.Application.Users.Commands.UpdateImage;
using VelaaPay.Application.Users.Commands.UpdateProfile;
using VelaaPay.Application.Users.Commands.UpdateProfileAddress;
using VelaaPay.Application.Users.Queries.GetBalance;
using VelaaPay.Application.Users.Queries.GetIdentification;
using VelaaPay.Application.Users.Queries.GetProfileAddress;
using VelaaPay.Application.Users.Queries.GetUserByPhoneAndRole;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class UserController : BaseController
    {
        [HttpPost("forgetPassword")]
        public async Task<IActionResult> ForgetPassword(ForgetPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost("verifyOtpPin")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("identification")]
        public async Task<IActionResult> UpdateIdentification(UpdateIdentificationRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [Authorize]
        [HttpGet("identification")]
        public async Task<IActionResult> GetIdentification()
        {
            GetIdentificationRequestModel model = new GetIdentificationRequestModel
            {
                UserId = User.FindFirstValue(CustomClaimTypes.UserId)
            };
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [Authorize]
        [HttpPost("fcm")]
        public async Task<IActionResult> UpdateFcm(UpdateFcmIdRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [Authorize]
        [HttpPost("profile")]
        public async Task<IActionResult> UpdateProfile(UpdateProfileRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpPost("profile/address")]
        public async Task<IActionResult> UpdateProfileAddress(UpdateProfileAddressRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [Authorize]
        [HttpGet("profile/address")]
        public async Task<IActionResult> GrtProfileAddress()
        {
            GetProfileAddressRequestModel model = new GetProfileAddressRequestModel
            {
                UserId = User.FindFirstValue(CustomClaimTypes.UserId)
            };
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [Authorize]
        [HttpPost("image")]
        public async Task<IActionResult> UpdateImage(UpdateImageRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [Authorize]
        [HttpGet("balance")]
        public async Task<IActionResult> GetBalance()
        {
            var model = new GetBalanceRequestModel {UserId = User.FindFirstValue(CustomClaimTypes.UserId)};
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [Authorize]
        [HttpPost("getByRoleAndPhone")]
        public async Task<IActionResult> GetByRoleAndPhone(GetUserByPhoneAndRoleRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}