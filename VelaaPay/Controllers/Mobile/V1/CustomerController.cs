using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Customers.Commands.DisableCustomer;
using VelaaPay.Application.Customers.Commands.EnableCustomer;
using VelaaPay.Application.Customers.Commands.SendMessageCustomer;
using VelaaPay.Application.Customers.Commands.UpdateCustomer;
using VelaaPay.Application.Customers.Commands.UpdateCustomerPin;
using VelaaPay.Application.Customers.Queries.GetCustomer;
using VelaaPay.Application.Customers.Queries.GetCustomerByPhone;
using VelaaPay.Application.Customers.Queries.GetCustomerDetail;
using VelaaPay.Application.Customers.Queries.GetCustomerInContact;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CustomerController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetCustomers([FromQuery] GetCustomerRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("phone")]
        [Authorize(Roles = RoleNames.Customer + "," + RoleNames.Cashier + "," + RoleNames.Supervisor + "," + RoleNames.Vendor)]
        public async Task<IActionResult> GetCustomerByPhone([FromQuery] GetCustomerByPhoneRequestModel model)
        {
            model.PhoneNumber = model.PhoneNumber?.Replace(" ", "+");
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("searchInContact")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> SearchCustomerInContact(GetCustomerInContactRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPut("enable/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> EnableCustomer([FromRoute] EnableCustomerRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpGet("details/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetCustomerDetail([FromRoute] GetCustomerDetailRequestModel model)
        {
            
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut("update/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdateCustomer([FromRoute] string customerId,[FromBody] UpdateCustomerRequestModel model)
        {
            model.UserId = customerId;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut("update/pin/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdateCustomerPin([FromRoute] string customerId,[FromBody] UpdateCustomerPinRequestModel model)
        {
            model.UserId = customerId;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPut("disable/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> DisableCustomer([FromRoute] DisableCustomerRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPost("message/{CustomerId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> SendMessage([FromRoute]string customerId, [FromBody] SendMessageCustomerRequestModel model)
        {
            model.UserId = customerId;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }


    }
}