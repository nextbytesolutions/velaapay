using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.ShopUsers.Commands.CreateShopUser;
using VelaaPay.Application.ShopUsers.Commands.UpdateShopUser;
using VelaaPay.Application.ShopUsers.Commands.UpdateShopUserPassword;
using VelaaPay.Application.ShopUsers.Queries.GetShopUsers;
using VelaaPay.Application.ShopUsers.Queries.GetShopUsersByShop;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class EmployeeController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor)]
        public async Task<IActionResult> GetEmployees([FromQuery] GetShopUsersRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            if (User.IsInRole(RoleNames.Vendor))
            {
                var response = await Mediator.Send(model);
                return StatusCode(response.Status, response.Response);
            }
            else
            {
                var byShop = new GetShopUsersByShopRequestModel();
                model.Copy(byShop);
                byShop.ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId));
                var response = await Mediator.Send(byShop);
                return StatusCode(response.Status, response.Response);
            }
        }
        
        [HttpPut]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor)]
        public async Task<IActionResult> UpdateEmployee([FromBody] UpdateShopUserRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut("password")]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor)]
        public async Task<IActionResult> UpdateEmployeePassword([FromBody] UpdateShopUserPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPost]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor)]
        public async Task<IActionResult> CreateEmployee([FromBody] CreateShopUserRequestModel model)
        {
            model.UserRole = RoleNames.Supervisor;
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            if (!User.IsInRole(RoleNames.Vendor))
            {
                model.ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId));
            }
            else
            {
                model.UserRole = RoleNames.Vendor;
            }
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        
    }
}