using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CashInRequests.Commands.AcceptCashInRequest;
using VelaaPay.Application.CashInRequests.Commands.CreateCashInRequest;
using VelaaPay.Application.CashInRequests.Commands.RejectCashInRequest;
using VelaaPay.Application.CashInRequests.Queries.GetCashInRequests;
using VelaaPay.Application.CashInRequests.Queries.GetCashInRequestsByUser;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CashInRequestController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Admin)]
        public async Task<IActionResult> GetCashInTrack([FromQuery] GetCashInRequestsRequestModel model)
        {
            ResponseViewModel response;
            if (User.IsInRole(RoleNames.Admin))
            {
                response = await Mediator.Send(model);
            }
            else
            {
                var userModel = new GetCashInRequestByUserRequestModel();
                model.Copy(userModel);
                userModel.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
                response = await Mediator.Send(model);
            }
            return StatusCode(response.Status, response.Response);
        }

        [HttpPost]
        [Authorize(Roles = RoleNames.Vendor)]
        public async Task<IActionResult> CreateCashInRequest(CreateCashInRequestRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("accept/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> AcceptCashInRequest([FromRoute] AcceptCashInRequestRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("reject/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> RejectCashInRequest([FromRoute] RejectCashInRequestRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

    }
}