using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Companies.Commands.CreateCompany;
using VelaaPay.Application.Companies.Commands.DeleteCompany;
using VelaaPay.Application.Companies.Commands.UpdateCompany;
using VelaaPay.Application.Companies.Queries.GetCompanies;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CompanyController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin + "," + RoleNames.Customer)]
        public async Task<IActionResult> GetCompanies([FromQuery] GetCompaniesRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdateCompany([FromBody] UpdateCompanyRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPost]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> CreateCompany([FromBody] CreateCompanyRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpDelete("{CompanyId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> DeleteCompany([FromRoute] DeleteCompanyRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}