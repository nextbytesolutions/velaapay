using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Transactions.Queries.GetTransactions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class TransactionController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetTransactions([FromQuery] GetTransactionsRequestModel model)
        {
            if (!User.IsInRole(RoleNames.Admin))
            {
                model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            }
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);

        }
    }
}