using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Users.Commands.GenerateOtp;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class OtpController : BaseController
    {
        [HttpPost]
        [Authorize(Roles = RoleNames.Admin + "," + RoleNames.Vendor + "," + RoleNames.Supervisor + "," + RoleNames.Cashier)]
        public async Task<IActionResult> GenerateOtp(GenerateOtpRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}