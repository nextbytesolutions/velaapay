using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Payment.Commands.AcceptRequestMoney;
using VelaaPay.Application.Payment.Commands.RejectRequestMoney;
using VelaaPay.Application.Payment.Commands.SendRequestMoney;
using VelaaPay.Application.Payment.Queries.GetRequestMoney;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class RequestController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Customer + "," + RoleNames.Admin)]
        public async Task<IActionResult> GetRequests([FromQuery] GetRequestMoneyRequestModel model)
        {
            if (!User.IsInRole(RoleNames.Admin))
            {
                model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            }
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);

        }
        [HttpPost]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> SendRequest([FromBody] SendRequestMoneyRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        
        [HttpPut("accept")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> AcceptRequest([FromBody] AcceptRequestMoneyRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPut("reject")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> RejectRequest([FromBody] RejectRequestMoneyRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}