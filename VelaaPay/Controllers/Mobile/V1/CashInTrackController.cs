using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CashIns.Queries.GetCashIns;
using VelaaPay.Application.CashIns.Queries.GetCashInsByShop;
using VelaaPay.Application.CashIns.Queries.GetCashInsByUser;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CashInTrackController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor + "," + RoleNames.Cashier)]
        public async Task<IActionResult> GetCashInTrack([FromQuery] GetCashInsRequestModel model)
        {
            ResponseViewModel response;
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            if (User.IsInRole(RoleNames.Vendor))
            {
                response = await Mediator.Send(model);
            }
            else if(User.IsInRole(RoleNames.Supervisor))
            {
                var shopModel = new GetCashInsByShopRequestModel
                {
                    ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId))
                };
                model.Copy(shopModel);
                response = await Mediator.Send(model);
            }
            else
            {
                var userModel = new GetCashInsByUserRequestModel()
                {
                    ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId))
                };
                model.Copy(userModel);
                response = await Mediator.Send(model);
            }
            return StatusCode(response.Status, response.Response);
        }
    }
}