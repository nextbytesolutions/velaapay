using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Model.Entities;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BaseController : Controller
    {
        private IMediator _mediator;
        private UserManager<User> _userManager;

        protected UserManager<User> UserManager => _userManager ?? (_userManager =
                                                       (UserManager<User>) HttpContext.RequestServices.GetService(
                                                           typeof(UserManager<User>)));
        protected IMediator Mediator => _mediator ?? (_mediator = (IMediator)HttpContext.RequestServices.GetService(typeof(IMediator)));
    }
}