using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CashOuts.Queries.GetCashOuts;
using VelaaPay.Application.CashOuts.Queries.GetCashOutsByShop;
using VelaaPay.Application.CashOuts.Queries.GetCashOutsByUser;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CashOutTrackController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor + "," + RoleNames.Cashier)]
        public async Task<IActionResult> GetCashOutTrack([FromQuery] GetCashOutsRequestModel model)
        {
            ResponseViewModel response;
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            if (User.IsInRole(RoleNames.Vendor))
            {
                response = await Mediator.Send(model);
            }
            else if(User.IsInRole(RoleNames.Supervisor))
            {
                var shopModel = new GetCashOutsByShopRequestModel
                {
                    ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId))
                };
                model.Copy(shopModel);
                response = await Mediator.Send(model);
            }
            else
            {
                var userModel = new GetCashOutsByUserRequestModel()
                {
                    ShopId = int.Parse(User.FindFirstValue(CustomClaimTypes.ShopId))
                };
                model.Copy(userModel);
                response = await Mediator.Send(model);
            }
            return StatusCode(response.Status, response.Response);
        }
    }
}