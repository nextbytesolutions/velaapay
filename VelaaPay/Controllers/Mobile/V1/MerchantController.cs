using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Merchant.Commands.CreateMerchant;
using VelaaPay.Application.Merchant.Commands.DisableMerchant;
using VelaaPay.Application.Merchant.Commands.EnableMerchant;
using VelaaPay.Application.Merchant.Commands.SendOtpMerchant;
using VelaaPay.Application.Merchant.Commands.UpdateMerchant;
using VelaaPay.Application.Merchant.Commands.UpdatePasswordMerchant;
using VelaaPay.Application.Merchant.Commands.VerifyMerchant;
using VelaaPay.Application.Merchant.Commands.VerifyOtpMerchant;
using VelaaPay.Application.Merchant.Queries.GetBusinessInfoMerchant;
using VelaaPay.Application.Merchant.Queries.GetDetailMerchant;
using VelaaPay.Application.Merchant.Queries.GetMerchants;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class MerchantController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetMerchants([FromQuery] GetMerchantsRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMerchant([FromBody] CreateMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("detail/{UserId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetMerchantDetail([FromRoute] GetMerchantDetailRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPut("enable/{MerchantId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> EnableMerchant([FromRoute] EnableMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("disable/{MerchantId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> DisableMerchant([FromRoute] DisableMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("sendOtp")]
        public async Task<IActionResult> SendOtp([FromBody] SendOtpMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut("update/password/{MerchantId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdatePasswordMerchant([FromRoute]string merchantId, [FromBody] UpdatePasswordMerchantRequestModel model)
        {
            model.UserId = merchantId;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        [HttpPut("update/business-info")]
        [Authorize]
        public async Task<IActionResult> UpdateMerchantBusinessInfo([FromBody] UpdateMerchantRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpGet("business-info/{UserId}")]
        [Authorize]
        public async Task<IActionResult> GetBusinessInfo([FromRoute] GetBusinessInfoMerchantRequestModel model)
        {
            if (!User.IsInRole(RoleNames.Admin))
            {
                model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            }
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("update/{MerchantId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdateMerchant([FromRoute] string merchantId,
            [FromBody] UpdateMerchantRequestModel model)
        {
            model.UserId = User.IsInRole(RoleNames.Admin) ? merchantId : User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }

        

        [HttpPut("verifyOtp")]
        public async Task<IActionResult> VerifyOtp([FromBody] VerifyOtpMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut("verify/{UserId}")]
        public async Task<IActionResult> VerifyOtp([FromRoute] VerifyMerchantRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }


    }
}