using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Payment.Commands.AddDebitMoney;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class DebitCardController : BaseController
    {
        [HttpPost("addMoney")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> AddMoney(AddDebitMoneyRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}