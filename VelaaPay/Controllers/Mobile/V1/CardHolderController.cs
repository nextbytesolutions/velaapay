using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CardHolder.Commands.CreateCardHolder;
using VelaaPay.Application.CardHolder.Commands.DeleteCardHolder;
using VelaaPay.Application.CardHolder.Commands.UpdateCardHolder;
using VelaaPay.Application.CardHolder.Queries.GetCardHolder;
using VelaaPay.Application.CardHolder.Queries.GetCardHolderByUser;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CardHolderController: BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Customer + "," + RoleNames.Admin)]
        public async Task<IActionResult> GetCardHolders([FromQuery] GetCardHolderRequestModel model)
        {
            ResponseViewModel response;
            if (User.IsInRole(RoleNames.Admin))
            {
                response = await Mediator.Send(model);
            }
            else
            {
                var request = new GetCardHolderByUserRequestModel();
                model.Copy(request);
                request.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
                response = await Mediator.Send(request);
            }
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> CreateCardHolder([FromBody] CreateCardHolderRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPut]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> UpdateCardHolder([FromBody] UpdateCardHolderRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpDelete("{Id}")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> DeleteCardHolder([FromRoute] DeleteCardHolderRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}