using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Bills.Commands.AcceptBill;
using VelaaPay.Application.Bills.Commands.CreateBill;
using VelaaPay.Application.Bills.Commands.RejectBill;
using VelaaPay.Application.Bills.Queries.GetBill;
using VelaaPay.Application.Bills.Queries.GetBillByUser;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class BillController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetBills([FromQuery] GetBillRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("byUser")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> GetBillByUser([FromQuery] GetBillByUserRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> CreateBill([FromBody] CreateBillRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("accept/{BillId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> AcceptBill([FromRoute] AcceptBillRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("reject/{BillId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> RejectBill([FromRoute] RejectBillRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}