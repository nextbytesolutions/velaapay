using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Payment.Commands.PurchaseItem;
using VelaaPay.Application.Payment.Commands.ScanToPay;
using VelaaPay.Application.Payment.Commands.SendMoney;
using VelaaPay.Application.Payment.Commands.TransferMoney;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class PaymentController : BaseController
    {
        [HttpPost("sendMoney")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> SendMoney(SendMoneyRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost("scanToPay")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> SendMoney(ScanToPayRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPost("purchaseItem")]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> PurchaseItem(PurchaseItemRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPost("transfer")]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor+ "," + RoleNames.Cashier)]
        public async Task<IActionResult> TransferMoney(TransferMoneyRequestModel model)
        {
            model.SenderUserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}