using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CashIns.Commands.CreateCashIn;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CashInController : BaseController
    {
        [HttpPost]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor + "," + RoleNames.Cashier)]
        public async Task<IActionResult> CreateCashIn(CreateCashInRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var employeeId = User.FindFirstValue(CustomClaimTypes.EmployeeId);
            if (!string.IsNullOrWhiteSpace(employeeId))
            {
                model.EmployeeId = int.Parse(employeeId);
            }
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}