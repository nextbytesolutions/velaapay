using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.WithdrawRequest.Commands.AcceptWithdraw;
using VelaaPay.Application.WithdrawRequest.Commands.CreateWithdraw;
using VelaaPay.Application.WithdrawRequest.Commands.RejectWithdraw;
using VelaaPay.Application.WithdrawRequest.Queries.GetWithdraw;
using VelaaPay.Application.WithdrawRequest.Queries.GetWithdrawByUser;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class WithdrawController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetWithdraws([FromQuery] GetWithdrawRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpGet("byUser")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetWithdrawByUser([FromQuery] GetWithdrawByUserRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost]
        [Authorize(Roles = RoleNames.Customer)]
        public async Task<IActionResult> CreateWithdraw(CreateWithdrawRequestModel model)
        {
            model.UserId = User.FindFirst(CustomClaimTypes.UserId).Value;
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("accept/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> AcceptWithdraw([FromRoute] AcceptWithdrawRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("reject/{RequestId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> RejectWithdraw([FromRoute] RejectWithdrawRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}