using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.CashOuts.Commands.CreateCashOut;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class CashOutController : BaseController
    {
        [HttpPost]
        [Authorize(Roles = RoleNames.Vendor + "," + RoleNames.Supervisor + "," + RoleNames.Cashier)]
        public async Task<IActionResult> CreateCashIn(CreateCashOutRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            model.EmployeeId = int.Parse(User.FindFirstValue(CustomClaimTypes.EmployeeId));
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}