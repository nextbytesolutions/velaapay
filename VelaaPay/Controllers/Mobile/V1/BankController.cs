using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Banks.Commands.CreateBank;
using VelaaPay.Application.Banks.Commands.DeleteBank;
using VelaaPay.Application.Banks.Commands.UpdateBank;
using VelaaPay.Application.Banks.Queries.GetBanks;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class BankController : BaseController
    {
        [HttpGet]
        [Authorize(Roles = RoleNames.Admin + "," + RoleNames.Customer)]
        public async Task<IActionResult> GetBanks([FromQuery] GetBanksRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> UpdateBank([FromBody] UpdateBankRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        [HttpPost]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> CreateBank([FromBody] CreateBankRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpDelete("{BankId}")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> DeleteBank([FromRoute] DeleteBankRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}