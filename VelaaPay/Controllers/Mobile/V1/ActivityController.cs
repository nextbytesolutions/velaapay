using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Transactions.Queries.GetRecentUsers;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    [Authorize(Roles = RoleNames.Customer)]
    public class ActivityController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetRecentUsers([FromQuery] GetRecentUsersRequestModel model)
        {
            model.UserId = User.FindFirstValue(CustomClaimTypes.UserId);
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}