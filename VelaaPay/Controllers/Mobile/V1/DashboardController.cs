using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Dashboards.Queries.GetAdminDashboard;
using VelaaPay.Common.Constants;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    
    public class DashboardController : BaseController
    {
        [HttpGet("admin")]
        [Authorize(Roles = RoleNames.Admin)]
        public async Task<IActionResult> GetAdminDashboard()
        {
            var model = new GetAdminDashboardRequestModel {UserId = User.FindFirstValue(CustomClaimTypes.UserId)};
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
    }
}