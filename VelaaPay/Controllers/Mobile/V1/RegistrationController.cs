using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VelaaPay.Application.Customers.Commands.RegisterCustomer;
using VelaaPay.Application.Customers.Commands.SetPassword;
using VelaaPay.Application.Customers.Commands.VerifyCustomerPhone;
using VelaaPay.Application.Users.Commands.SetPasswordByToken;

namespace VelaaPay.Presentation.Controllers.Mobile.V1
{
    public class RegistrationController : BaseController
    {
        [HttpPost]
        public async Task<IActionResult> RegisterCustomer([FromBody] RegisterCustomerRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        
        [HttpPost("verify")]
        public async Task<IActionResult> VerifyCustomerPhone([FromBody] VerifyCustomerPhoneRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPost("setPassword")]
        public async Task<IActionResult> SetPassword([FromBody] SetPasswordRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
        
        [HttpPut("setPasswordByToken")]
        public async Task<IActionResult> SetPassword([FromBody] SetPasswordByTokenRequestModel model)
        {
            var response = await Mediator.Send(model);
            return StatusCode(response.Status, response.Response);
        }
    }
}