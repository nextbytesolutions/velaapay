using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashOuts.Commands.CreateCashOut
{

    public class CreateCashOutRequestHandler : IRequestHandler<CreateCashOutRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateCashOutRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(CreateCashOutRequestModel request, CancellationToken cancellationToken)
        {
            var customer = _context.Users.GetBy(p => p.Id == request.CustomerId, p => p.Include(pr => pr.UserBalances));
            if (customer == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.CustomerId);
            }

            if (customer.UserBalances.First().TotalBalance < request.Amount)
            {
                throw new BadRequestException(Messages.InsufficientBalance);
            }
            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(customer,customer.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }
            if (customer.Otp != request.Otp || customer.OtpExpiry < DateTime.UtcNow)
            {
                throw new BadRequestException(Messages.OtpIncorrectOrExpired);
            }
            
            Wallet wallet = new Wallet()
            {
                UserId = request.UserId,
                Amount = request.Amount,
            };
            Wallet wallet2 = new Wallet()
            {
                UserId = request.CustomerId,
                Amount = -request.Amount,
            };
            Transaction transaction = new Transaction()
            {
                SenderUserId = request.CustomerId,
                ReceiverUserId = request.UserId,
                Amount = request.Amount,
                LoadType = (int)LoadType.CashOut,
                Wallets = new List<Wallet>()
                {
                    wallet2,
                    wallet
                }
            };
            var cashOut = new CashOutTrack()
            {
                Amount = request.Amount,
                CustomerId = request.CustomerId,
                EmployeeId = request.EmployeeId,
                Transaction = transaction,
            };
            customer.Otp = null;
            customer.OtpExpiry = null;
            _context.CashOutTracks.Add(cashOut);
            _context.Users.Update(customer);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}