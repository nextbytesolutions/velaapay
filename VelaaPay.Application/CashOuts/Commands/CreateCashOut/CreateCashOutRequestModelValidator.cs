using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashOuts.Commands.CreateCashOut
{
    public class CreateCashOutRequestModelValidator : AbstractValidator<CreateCashOutRequestModel>
    {
        public CreateCashOutRequestModelValidator()
        {
            RuleFor(p => p.CustomerId).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.EmployeeId).Required();
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.Otp).Pin();
        }
    }
}