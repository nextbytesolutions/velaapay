using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOuts
{

    public class GetCashOutsRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }
            if (OrderBy == "shop")
            {
                OrderBy = "Employee.Shop.Name";
            }
            if (OrderBy == "customer")
            {
                OrderBy = "Customer.FullName";
            }
            if (OrderBy == "employee")
            {
                OrderBy = "Employee.User.FullName";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string UserId { get; set; }
        
    }
}