using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOuts
{
    public class GetCashOutsRequestModelValidator : AbstractValidator<GetCashOutsRequestModel>
    {
        public GetCashOutsRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}