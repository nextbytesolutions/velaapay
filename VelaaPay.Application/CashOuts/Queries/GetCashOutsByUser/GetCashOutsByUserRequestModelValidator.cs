using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOutsByUser
{
    public class GetCashOutsByUserRequestModelValidator : AbstractValidator<GetCashOutsByUserRequestModel>
    {
        public GetCashOutsByUserRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);

        }
    }
}