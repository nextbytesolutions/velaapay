using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.CashOuts.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOutsByUser
{

    public class GetCashOutsByUserRequestHandler : IRequestHandler<GetCashOutsByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCashOutsByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCashOutsByUserRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();

            var list = _context.CashOutTracks.GetManyReadOnly(
                    p =>
                        (p.Employee.UserId == request.UserId && p.Employee.ShopId == request.ShopId) &&
                        (p.Employee.User.FullName.Contains(request.Search) ||
                         p.Employee.User.PhoneNumber.Contains(request.Search) ||
                         p.Employee.Shop.Name.Contains(request.Search)), request.OrderBy, request.Page,
                    request.PageSize, request.IsDescending,
                    p => p.Include(pr => pr.Customer)
                        .Include(pr => pr.Employee.Shop)
                        .Include(pr => pr.Employee.User))
                .Select(p => new CashOutInfoResponseModel
                {
                    Shop = p.Employee.Shop.Name,
                    Amount = p.Amount,
                    Customer = p.Customer.FullName,
                    Employee = p.Employee.User.FullName,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    TransactionId = p.TransactionId,
                }).ToList();
            var count = await _context.CashOutTracks.CountAsync(
                p =>
                    (p.Employee.UserId == request.UserId && p.Employee.ShopId == request.ShopId) &&
                    (p.Employee.User.FullName.Contains(request.Search) ||
                     p.Employee.User.PhoneNumber.Contains(request.Search) ||
                     p.Employee.Shop.Name.Contains(request.Search)),cancellationToken);
            return new ResponseViewModel().CreateOk(list,count);

        }
    }

}