using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.CashOuts.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOutsByShop
{

    public class GetCashOutsByShopRequestHandler : IRequestHandler<GetCashOutsByShopRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCashOutsByShopRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCashOutsByShopRequestModel request, CancellationToken cancellationToken)
        {
            var converter = new DateConverter();
            List<CashOutInfoResponseModel> list = _context.CashInTracks.GetManyReadOnly(
                    p =>
                        (p.Employee.Shop.Id == request.ShopId) &&
                        (p.Employee.User.FullName.Contains(request.Search) ||
                         p.Employee.User.PhoneNumber.Contains(request.Search) ||
                         p.Employee.Shop.Name.Contains(request.Search)), request.OrderBy, request.Page,
                    request.PageSize, request.IsDescending,
                    p => p.Include(pr => pr.Customer)
                        .Include(pr => pr.Employee.Shop)
                        .Include(pr => pr.Employee.User))
                .Select(p => new CashOutInfoResponseModel
                {
                    Shop = p.Employee.Shop.Name,
                    Amount = p.Amount,
                    Customer = p.Customer.FullName,
                    Employee = p.Employee.User.FullName,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    TransactionId = p.TransactionId,
                }).ToList();
            var count = await _context.CashInTracks.CountAsync(
                p =>
                    (p.Employee.Shop.Id == request.ShopId) &&
                    (p.Employee.User.FullName.Contains(request.Search) ||
                     p.Employee.User.PhoneNumber.Contains(request.Search) ||
                     p.Employee.Shop.Name.Contains(request.Search)),cancellationToken);
            return new ResponseViewModel().CreateOk(list,count);
        }
    }

}