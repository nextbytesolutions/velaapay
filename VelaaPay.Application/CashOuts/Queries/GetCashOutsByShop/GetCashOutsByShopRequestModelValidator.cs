using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashOuts.Queries.GetCashOutsByShop
{
    public class GetCashOutsByShopRequestModelValidator : AbstractValidator<GetCashOutsByShopRequestModel>
    {
        public GetCashOutsByShopRequestModelValidator()
        {
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}