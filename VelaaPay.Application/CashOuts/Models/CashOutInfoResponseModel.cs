namespace VelaaPay.Application.CashOuts.Models
{
    public class CashOutInfoResponseModel
    {
        public string Employee { get; set; }
        public string Shop { get; set; }
        public string CreatedDate { get; set; }
        public double Amount { get; set; }
        public string Customer { get; set; }
        public string TransactionId { get; set; }
    }
}