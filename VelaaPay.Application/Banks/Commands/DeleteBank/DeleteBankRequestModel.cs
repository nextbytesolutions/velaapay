using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Banks.Commands.DeleteBank
{

    public class DeleteBankRequestModel : IRequest<ResponseViewModel>
    {
        public int BankId { get; set; }
    }
}