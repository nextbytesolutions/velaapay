using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Banks.Commands.DeleteBank
{

    public class DeleteBankRequestHandler : IRequestHandler<DeleteBankRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteBankRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(DeleteBankRequestModel request, CancellationToken cancellationToken)
        {
            var bank = _context.Banks.GetBy(p => p.Id == request.BankId && !p.WithdrawRequests.Any());
            if (bank == null)
            {
                throw new CannotDeleteException(nameof(Bank));
            }

            _context.Banks.Remove(bank);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}