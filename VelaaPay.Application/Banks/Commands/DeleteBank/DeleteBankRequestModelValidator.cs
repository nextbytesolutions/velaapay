using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Banks.Commands.DeleteBank
{
    public class DeleteBankRequestModelValidator : AbstractValidator<DeleteBankRequestModel>
    {
        public DeleteBankRequestModelValidator()
        {
            RuleFor(p => p.BankId).Required();
        }
    }
}