using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Banks.Commands.UpdateBank
{

    public class UpdateBankRequestHandler : IRequestHandler<UpdateBankRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateBankRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateBankRequestModel request, CancellationToken cancellationToken)
        {
            var bank = _context.Banks.GetBy(p => p.Id == request.BankId);
            if (bank == null)
            {
                throw new NotFoundException(nameof(Bank),request.BankId);
            }
            request.Copy(bank);
            _context.Update(bank);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}