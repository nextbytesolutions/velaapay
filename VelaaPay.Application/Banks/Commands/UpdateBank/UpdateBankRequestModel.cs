using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Banks.Commands.UpdateBank
{

    public class UpdateBankRequestModel : IRequest<ResponseViewModel>
    {
        public int BankId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string SwiftCode { get; set; }
    }
}