using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Banks.Commands.UpdateBank
{
    public class UpdateBankRequestModelValidator : AbstractValidator<UpdateBankRequestModel>
    {
        public UpdateBankRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.BankId).Required();
            RuleFor(p => p.PhoneNumber).Required();
            RuleFor(p => p.Address).Required();
            RuleFor(p => p.SwiftCode).Required();
        }
    }
}