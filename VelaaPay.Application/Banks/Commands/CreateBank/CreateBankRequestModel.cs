using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Banks.Commands.CreateBank
{

    public class CreateBankRequestModel : IRequest<ResponseViewModel>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string SwiftCode { get; set; }
    }
}