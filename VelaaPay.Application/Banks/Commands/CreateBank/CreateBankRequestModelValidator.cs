using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Banks.Commands.CreateBank
{
    public class CreateBankRequestModelValidator : AbstractValidator<CreateBankRequestModel>
    {
        public CreateBankRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.PhoneNumber).Required();
            RuleFor(p => p.Address).Required();
            RuleFor(p => p.SwiftCode).Required();
        }
    }
}