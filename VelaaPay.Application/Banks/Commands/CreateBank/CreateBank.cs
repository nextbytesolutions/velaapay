using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Banks.Commands.CreateBank
{

    public class CreateBankRequestHandler : IRequestHandler<CreateBankRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateBankRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(CreateBankRequestModel request, CancellationToken cancellationToken)
        {
            var bank = new Bank();
            request.Copy(bank);
            _context.Banks.Add(bank);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}