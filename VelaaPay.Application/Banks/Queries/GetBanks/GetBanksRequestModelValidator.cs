using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Banks.Queries.GetBanks
{
    public class GetBanksRequestModelValidator : AbstractValidator<GetBanksRequestModel>
    {
        public GetBanksRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}