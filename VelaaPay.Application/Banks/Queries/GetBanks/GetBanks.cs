using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Banks.Queries.GetBanks
{

    public class GetBanksRequestHandler : IRequestHandler<GetBanksRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetBanksRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetBanksRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();
            var list = _context.Banks.GetManyReadOnly(
                p => p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search), request.OrderBy,
                request.Page, request.PageSize, request.IsDescending).Select(p => new BankInfoModel
            {
                Id = p.Id,
                Name = p.Name,
                Address = p.Address,
                SwiftCode = p.SwiftCode,
                CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                PhoneNumber = p.PhoneNumber
            }).ToList();
            var count = _context.Banks.Count(
                p => p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }

        private class BankInfoModel
        {
            public string SwiftCode { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }
            public int Id { get; set; }
            public string PhoneNumber { get; set; }
            public string CreatedDate { get; set; }
        }
    }

}