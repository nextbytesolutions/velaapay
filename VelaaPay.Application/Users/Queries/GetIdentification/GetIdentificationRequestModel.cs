using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Queries.GetIdentification
{

    public class GetIdentificationRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}