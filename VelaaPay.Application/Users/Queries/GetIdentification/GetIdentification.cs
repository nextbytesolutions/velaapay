using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Queries.GetIdentification
{

    public class GetIdentificationRequestHandler : IRequestHandler<GetIdentificationRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetIdentificationRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetIdentificationRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User),request.UserId);
            }
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(new IdentificationInfo()
            {
                Identification = user.Identification,
                IdentificationImageUrl = user.IdentificationImageUrl
            }));
        }

        class IdentificationInfo
        {
            public string Identification { get; set; }
            public string IdentificationImageUrl { get; set; }
        }
    }

}