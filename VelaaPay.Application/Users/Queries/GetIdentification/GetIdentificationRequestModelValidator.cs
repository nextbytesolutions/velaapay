using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Queries.GetIdentification
{
    public class GetIdentificationRequestModelValidator : AbstractValidator<GetIdentificationRequestModel>
    {
        public GetIdentificationRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}