using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Queries.GetProfileAddress
{

    public class GetProfileAddressRequestHandler : IRequestHandler<GetProfileAddressRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetProfileAddressRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetProfileAddressRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User),request.UserId);
            }
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(new AddressInfo()
            {
                Address = user.Address,
            }));
        }
        
        class AddressInfo
        {
            public string Address { get; set; }
        }
    }

}