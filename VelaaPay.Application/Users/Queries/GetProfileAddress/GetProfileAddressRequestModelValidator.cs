using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Queries.GetProfileAddress
{
    public class GetProfileAddressRequestModelValidator : AbstractValidator<GetProfileAddressRequestModel>
    {
        public GetProfileAddressRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}