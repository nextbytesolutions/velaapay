using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Queries.GetProfileAddress
{

    public class GetProfileAddressRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}