using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Queries.GetUserByPhoneAndRole
{
    public class GetUserByPhoneAndRoleRequestModelValidator : AbstractValidator<GetUserByPhoneAndRoleRequestModel>
    {
        public GetUserByPhoneAndRoleRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Role).Required();
        }
    }
}