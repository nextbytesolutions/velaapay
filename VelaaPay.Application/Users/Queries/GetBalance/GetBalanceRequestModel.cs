using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Queries.GetBalance
{

    public class GetBalanceRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}