using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Queries.GetBalance
{

    public class GetBalanceRequestHandler : IRequestHandler<GetBalanceRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetBalanceRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetBalanceRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.UserBalances.GetBy(p => p.UserId == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User),request.UserId);
            }
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(user.TotalBalance));
        }
    }

}