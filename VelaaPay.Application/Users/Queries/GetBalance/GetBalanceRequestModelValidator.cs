using FluentValidation;

namespace VelaaPay.Application.Users.Queries.GetBalance
{
    public class GetBalanceRequestModelValidator : AbstractValidator<GetBalanceRequestModel>
    {
        public GetBalanceRequestModelValidator()
        {
            RuleFor(p => p.UserId);
        }
    }
}