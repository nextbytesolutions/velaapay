using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.ResetPassword
{
    public class ResetPasswordRequestModelValidator : AbstractValidator<ResetPasswordRequestModel>
    {
        public ResetPasswordRequestModelValidator()
        {
            RuleFor(p => p.Otp).Pin();
            RuleFor(p => p.MobileNumber).Phone();
        }
    }
}