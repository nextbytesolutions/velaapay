using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.ResetPassword
{

    public class ResetPasswordRequestHandler : IRequestHandler<ResetPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public ResetPasswordRequestHandler(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<ResponseViewModel> Handle(ResetPasswordRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.PhoneNumber == request.MobileNumber);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.MobileNumber);
            }

            if (!(user.Otp == request.Otp && user.OtpExpiry >= DateTime.UtcNow))
            {
                throw new BadRequestException(Messages.OtpIncorrectOrExpired);
            }
            user.Otp = null;
            user.OtpExpiry = DateTime.UtcNow;
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOkCustom(new List<KeyValuePair<string, object>>() {
                new KeyValuePair<string, object>(nameof(token),token),
                new KeyValuePair<string, object>(nameof(user.Id),user.Id),
            });;
        }
    }
}