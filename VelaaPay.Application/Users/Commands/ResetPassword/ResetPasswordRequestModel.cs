using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.ResetPassword
{

    public class ResetPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string MobileNumber { get; set; }
        public string Otp { get; set; }
    }
}