using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Sms.Commands.SendSms;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.GenerateOtp
{

    public class GenerateOtpRequestHandler : IRequestHandler<GenerateOtpRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public GenerateOtpRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(GenerateOtpRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }

            user.Otp = PinGenerator.CreatePin();
            user.OtpExpiry = DateTime.UtcNow.AddMinutes(5);
            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            await _mediator.Send(new SendSmsRequestModel
            {
                Message = "Your Velaa Pay OTP: " + user.Otp,
                PhoneNumber = user.PhoneNumber
            },cancellationToken);
            return new ResponseViewModel();
        }
    }

}