using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.GenerateOtp
{

    public class GenerateOtpRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}