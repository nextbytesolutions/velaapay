using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.GenerateOtp
{
    public class GenerateOtpRequestModelValidator : AbstractValidator<GenerateOtpRequestModel>
    {
        public GenerateOtpRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}