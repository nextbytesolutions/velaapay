using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.UpdateProfile
{

    public class UpdateProfileRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
    }
}