using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.UpdateProfile
{
    public class UpdateProfileRequestModelValidator : AbstractValidator<UpdateProfileRequestModel>
    {
        public UpdateProfileRequestModelValidator()
        {
            RuleFor(p => p.FullName).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}