using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.UpdateProfile
{

    public class UpdateProfileRequestHandler : IRequestHandler<UpdateProfileRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateProfileRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateProfileRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new  NotFoundException(nameof(User), request.UserId);
            }
            user.FullName = request.FullName;
            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }
}