using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Users.Commands.SetPasswordByToken
{

    public class SetPasswordByTokenRequestHandler : IRequestHandler<SetPasswordByTokenRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public SetPasswordByTokenRequestHandler(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public Task<ResponseViewModel> Handle(SetPasswordByTokenRequestModel request, CancellationToken cancellationToken)
        {
            var user = _userManager.FindByIdAsync(request.UserId).Result;
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }
            var result = _userManager.ResetPasswordAsync(user, request.Token.Replace(" ","+"), request.Pin).Result;
            if (result.Succeeded)
            {
                return Task.FromResult(new ResponseViewModel());
            }
            throw new ValidationException(result.Errors.ToList());
        }
    }

}