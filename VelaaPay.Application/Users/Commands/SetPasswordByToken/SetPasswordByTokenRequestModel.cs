using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.SetPasswordByToken
{

    public class SetPasswordByTokenRequestModel : IRequest<ResponseViewModel>
    {
        public string Pin { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
    }
}