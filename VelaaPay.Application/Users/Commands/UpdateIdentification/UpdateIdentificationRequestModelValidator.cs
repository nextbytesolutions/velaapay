using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.UpdateIdentification
{
    public class UpdateIdentificationRequestModelValidator : AbstractValidator<UpdateIdentificationRequestModel>
    {
        public UpdateIdentificationRequestModelValidator()
        {
            RuleFor(p => p.Identification).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.IdentificationImage).Required();
        }
    }
}