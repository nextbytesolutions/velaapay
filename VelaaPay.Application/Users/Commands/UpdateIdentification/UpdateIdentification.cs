using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.UpdateIdentification
{

    public class UpdateIdentificationRequestHandler : IRequestHandler<UpdateIdentificationRequestModel, ResponseViewModel>
    {
        private readonly IImageService _imageService;
        private readonly ApplicationDbContext _context;

        public UpdateIdentificationRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateIdentificationRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new  NotFoundException(nameof(User), request.UserId);
            }

            if (user.Identification != request.Identification)
            {
                if (_context.Users.Any(p => p.Identification == request.Identification))
                {
                    throw new AlreadyExistsException("Identification");
                }

                user.Identification = request.Identification;
            }

            if (user.IdentificationImageUrl != request.IdentificationImage)
            {
                if (!string.IsNullOrEmpty(user.IdentificationImageUrl))
                {
                    _imageService.DeleteImage(user.IdentificationImageUrl);
                }
                user.IdentificationImageUrl = _imageService.SaveImage(request.IdentificationImage);
            }

            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new UserInfo()
            {
                Identification = user.Identification,
                IdentificationImage = user.IdentificationImageUrl
            });
        }
        class UserInfo
        {
            public string Identification { get; set; }
            public string IdentificationImage { get; set; }
        }
    }

}