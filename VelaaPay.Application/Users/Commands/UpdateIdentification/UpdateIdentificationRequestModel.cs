using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.UpdateIdentification
{

    public class UpdateIdentificationRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Identification { get; set; }
        public string IdentificationImage { get; set; }
    }
}