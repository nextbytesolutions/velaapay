using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.UpdateFcmId
{

    public class UpdateFcmIdRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string FcmId { get; set; }

    }
}