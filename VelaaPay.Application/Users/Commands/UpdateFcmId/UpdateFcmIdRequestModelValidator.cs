using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.UpdateFcmId
{
    public class UpdateFcmIdRequestModelValidator : AbstractValidator<UpdateFcmIdRequestModel>
    {
        public UpdateFcmIdRequestModelValidator()
        {
            RuleFor(p => p.FcmId).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}