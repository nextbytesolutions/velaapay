using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.ChangePassword
{
    public class ChangePasswordRequestModelValidator : AbstractValidator<ChangePasswordRequestModel>
    {
        public ChangePasswordRequestModelValidator()
        {
            RuleFor(p => p.CurrentPassword).Pin();
            RuleFor(p => p.NewPassword).Pin();
            RuleFor(p => p.ConfirmPassword).Pin().Equal(p => p.NewPassword).WithMessage("must match NewPassword");
        }
    }
}