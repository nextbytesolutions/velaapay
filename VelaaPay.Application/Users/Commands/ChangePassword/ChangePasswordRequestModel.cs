using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.ChangePassword
{

    public class ChangePasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string UserId { get; set; }
    }
}