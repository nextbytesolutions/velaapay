using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.UpdateProfileAddress
{
    public class UpdateProfileAddressRequestModelValidator : AbstractValidator<UpdateProfileAddressRequestModel>
    {
        public UpdateProfileAddressRequestModelValidator()
        {
            RuleFor(p => p.Address).Required();
            RuleFor(p => p.UserId).Required();

        }
    }
}