using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.UpdateProfileAddress
{

    public class UpdateProfileAddressRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Address { get; set; }

    }
}