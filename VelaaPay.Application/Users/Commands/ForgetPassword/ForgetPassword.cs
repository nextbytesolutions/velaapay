using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.ForgetPassword
{

    public class ForgetPasswordRequestHandler : IRequestHandler<ForgetPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly ISmsService _smsService;
        public ForgetPasswordRequestHandler(ApplicationDbContext context, ISmsService smsService)
        {
            _context = context;
            _smsService = smsService;
        }

        public async Task<ResponseViewModel> Handle(ForgetPasswordRequestModel request,
            CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.PhoneNumber == request.MobileNumber);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.MobileNumber);
            }

            user.Otp = PinGenerator.CreatePin();
            user.OtpExpiry = DateTime.UtcNow.Add(TimeSpan.FromMinutes(5));
            _context.Update(user);
            await _smsService.SendMessage(request.MobileNumber, "You Velaa Pay Otp : " + user.Otp);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}