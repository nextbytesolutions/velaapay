using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Users.Commands.ForgetPassword
{
    public class ForgetPasswordRequestModelValidator : AbstractValidator<ForgetPasswordRequestModel>
    {
        public ForgetPasswordRequestModelValidator()
        {
            RuleFor(p => p.MobileNumber).Phone();
        }
    }
}