using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.ForgetPassword
{

    public class ForgetPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string MobileNumber { get; set; }
    }
}