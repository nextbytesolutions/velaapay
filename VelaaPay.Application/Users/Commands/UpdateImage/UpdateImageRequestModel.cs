using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Users.Commands.UpdateImage
{

    public class UpdateImageRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Image { get; set; }
    }
}