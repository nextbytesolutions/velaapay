using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Users.Commands.UpdateImage
{

    public class UpdateImageRequestHandler : IRequestHandler<UpdateImageRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public UpdateImageRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateImageRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new  NotFoundException(nameof(User), request.UserId);
            }
            if (user.Image != request.Image)
            {
                if (!string.IsNullOrEmpty(user.Image) && user.Image != Constant.DefaultImageUrl)
                {
                    _imageService.DeleteImage(user.Image);
                }
                user.Image = _imageService.SaveImage(request.Image);
            }
            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new UserInfo()
            {
                Image = user.Image
            });
        }
        class UserInfo
        {
            public string Image { get; set; }
        }
    }

}