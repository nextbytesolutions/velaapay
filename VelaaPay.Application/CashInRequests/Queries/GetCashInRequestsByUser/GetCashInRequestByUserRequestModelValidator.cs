using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashInRequests.Queries.GetCashInRequestsByUser
{
    public class GetCashInRequestByUserRequestModelValidator : AbstractValidator<GetCashInRequestByUserRequestModel>
    {
        public GetCashInRequestByUserRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
            RuleFor(p => p.UserId).Required();
        }
    }
}