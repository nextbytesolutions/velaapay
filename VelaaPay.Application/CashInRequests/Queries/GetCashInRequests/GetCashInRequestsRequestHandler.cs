using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashInRequests.Queries.GetCashInRequests
{

    public class GetCashInRequestsRequestHandler : IRequestHandler<GetCashInRequestsRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCashInRequestsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCashInRequestsRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();
            var list = _context.CashInRequests.GetManyReadOnly(p => p.User.FullName.Contains(request.Search) ||
                                                                    p.TransactionCode.Contains(request.Search),
                request.OrderBy, request.Page, request.PageSize, request.IsDescending
                ,p => p.Include(pr => pr.User)).Select(p =>
                new CashInRequestInfo()
                {
                    RequestId = p.Id,
                    Name = p.User.FullName,
                    Type = p.Type,
                    Image = p.Image,
                    Amount = p.Amount,
                    Status = p.Status,
                    TransactionCode = p.TransactionCode,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                }).ToList();
            var count = await _context.CashInRequests.CountAsync(p => p.User.FullName.Contains(request.Search) ||
                                                           p.TransactionCode.Contains(request.Search), cancellationToken: cancellationToken);
            return new ResponseViewModel().CreateOk(list, count);
        }

        class CashInRequestInfo
        {
            public int RequestId { get; set; }
            public string Name { get; set; }
            public int Type { get; set; }
            public int Status { get; set; }
            public string Image { get; set; }
            public string TransactionCode { get; set; }
            public double Amount { get; set; }
            public string CreatedDate { get; set; }
        }
    }

}