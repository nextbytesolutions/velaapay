using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashInRequests.Queries.GetCashInRequests
{
    public class GetCashInRequestsRequestModelValidator : AbstractValidator<GetCashInRequestsRequestModel>
    {
        public GetCashInRequestsRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}