using FluentValidation;

namespace VelaaPay.Application.CashInRequests.Commands.AcceptCashInRequest
{
    public class AcceptCashInRequestRequestModelValidator : AbstractValidator<AcceptCashInRequestRequestModel>
    {
        public AcceptCashInRequestRequestModelValidator()
        {
            RuleFor(p => p.RequestId);
        }
    }
}