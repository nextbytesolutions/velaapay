using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CashInRequests.Commands.AcceptCashInRequest
{
    public class AcceptCashInRequestRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }

    }
}