using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashInRequests.Commands.AcceptCashInRequest
{

    public class AcceptCashInRequestRequestHandler : IRequestHandler<AcceptCashInRequestRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public AcceptCashInRequestRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(AcceptCashInRequestRequestModel request, CancellationToken cancellationToken)
        {
            var cashInRequest = _context.CashInRequests.GetBy(p => p.Id == request.RequestId && p.Status == (int)RequestStatus.None);
            if (cashInRequest == null)
            {
                throw new NotFoundException(nameof(CashInRequest), request.RequestId);
            }
            cashInRequest.Status = (int) RequestStatus.Accepted;
            cashInRequest.Transaction = new Transaction()
            {
                LoadType = (int) LoadType.Vendor,
                ReceiverUserId = cashInRequest.UserId,
                Amount = cashInRequest.Amount,
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = cashInRequest.UserId,
                        Amount = cashInRequest.Amount,
                    }
                }
            };
            _context.Update(cashInRequest);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}