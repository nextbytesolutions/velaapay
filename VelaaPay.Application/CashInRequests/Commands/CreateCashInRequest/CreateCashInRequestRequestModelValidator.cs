using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.CashInRequests.Commands.CreateCashInRequest
{
    public class CreateCashInRequestRequestModelValidator : AbstractValidator<CreateCashInRequestRequestModel>
    {
        public CreateCashInRequestRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Type).IsInEnum().WithMessage(Messages.IncorrectValue);
            RuleFor(p => p.Image).Required();
            RuleFor(p => p.Amount).Required();
            RuleFor(p => p.TransactionCode).Required();
        }
    }
}