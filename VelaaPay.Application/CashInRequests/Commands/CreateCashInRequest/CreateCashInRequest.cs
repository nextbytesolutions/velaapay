using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.CashInRequests.Commands.CreateCashInRequest
{

    public class CreateCashInRequestRequestHandler : IRequestHandler<CreateCashInRequestRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public CreateCashInRequestRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(CreateCashInRequestRequestModel request, CancellationToken cancellationToken)
        {
            var cashInRequest = new CashInRequest()
            {
                Amount = request.Amount,
                Status = RequestStatus.None.ToInt(),
                Image = _imageService.SaveImage(request.Image),
                Type = request.Type.ToInt(),
                TransactionCode = request.TransactionCode,
                UserId = request.UserId,
            };
            _context.Add(cashInRequest);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}