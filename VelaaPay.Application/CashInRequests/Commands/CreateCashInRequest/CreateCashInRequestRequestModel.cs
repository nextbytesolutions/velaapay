using MediatR;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CashInRequests.Commands.CreateCashInRequest
{

    public class CreateCashInRequestRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public CashInRequestType Type { get; set; }
        public string Image { get; set; }
        public double Amount { get; set; }
        public string TransactionCode { get; set; }
    }
}