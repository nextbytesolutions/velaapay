using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashInRequests.Commands.RejectCashInRequest
{
    public class RejectCashInRequestRequestModelValidator : AbstractValidator<RejectCashInRequestRequestModel>
    {
        public RejectCashInRequestRequestModelValidator()
        {
            RuleFor(p => p.RequestId).Required();
        }
    }
}