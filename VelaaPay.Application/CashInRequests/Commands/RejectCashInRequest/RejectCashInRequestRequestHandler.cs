using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashInRequests.Commands.RejectCashInRequest
{

    public class RejectCashInRequestRequestHandler : IRequestHandler<RejectCashInRequestRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public RejectCashInRequestRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(RejectCashInRequestRequestModel request,
            CancellationToken cancellationToken)
        {
            var cashInRequest =
                _context.CashInRequests.GetBy(p => p.Id == request.RequestId && 
                                                   p.Status == (int) RequestStatus.None);
            if (cashInRequest == null)
            {
                throw new NotFoundException(nameof(CashInRequest), request.RequestId);
            }

            cashInRequest.Status = (int) RequestStatus.Rejected;
            _context.Update(cashInRequest);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}