using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CashInRequests.Commands.RejectCashInRequest
{
    public class RejectCashInRequestRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
    }
}