using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class CardtoaccountAccount {
    /// <summary>
    /// Gets or Sets Fromcustomerid
    /// </summary>
    [DataMember(Name="fromcustomerid", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromcustomerid")]
    public string Fromcustomerid { get; set; }

    /// <summary>
    /// Gets or Sets Recipient
    /// </summary>
    [DataMember(Name="recipient", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "recipient")]
    public string Recipient { get; set; }

    /// <summary>
    /// Gets or Sets Pan
    /// </summary>
    [DataMember(Name="pan", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "pan")]
    public string Pan { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class CardtoaccountAccount {\n");
      sb.Append("  Fromcustomerid: ").Append(Fromcustomerid).Append("\n");
      sb.Append("  Recipient: ").Append(Recipient).Append("\n");
      sb.Append("  Pan: ").Append(Pan).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
