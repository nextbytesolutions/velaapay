using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AccounttocardSource {
    /// <summary>
    /// The name of the operation
    /// </summary>
    /// <value>The name of the operation</value>
    [DataMember(Name="operation", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "operation")]
    public string Operation { get; set; }

    /// <summary>
    /// Gets or Sets Account
    /// </summary>
    [DataMember(Name="account", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "account")]
    public AccountaccounttocardAccount Account { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccounttocardSource {\n");
      sb.Append("  Operation: ").Append(Operation).Append("\n");
      sb.Append("  Account: ").Append(Account).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
