using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Transaction {
    /// <summary>
    /// retrieved reference no
    /// </summary>
    /// <value>retrieved reference no</value>
    [DataMember(Name="retrievalreferencenumber", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "retrievalreferencenumber")]
    public string Retrievalreferencenumber { get; set; }

    /// <summary>
    /// reference no
    /// </summary>
    /// <value>reference no</value>
    [DataMember(Name="reference", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "reference")]
    public string Reference { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Transaction {\n");
      sb.Append("  Retrievalreferencenumber: ").Append(Retrievalreferencenumber).Append("\n");
      sb.Append("  Reference: ").Append(Reference).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
