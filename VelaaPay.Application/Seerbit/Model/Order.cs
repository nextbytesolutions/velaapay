using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class Order {
    /// <summary>
    /// Currency ISO 4217 code
    /// </summary>
    /// <value>Currency ISO 4217 code</value>
    [DataMember(Name="currency", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "currency")]
    public string Currency { get; set; }

    /// <summary>
    /// descriptor for transaction
    /// </summary>
    /// <value>descriptor for transaction</value>
    [DataMember(Name="description", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "description")]
    public string Description { get; set; }

    /// <summary>
    /// Country ISO ALPHA-2 CODE
    /// </summary>
    /// <value>Country ISO ALPHA-2 CODE</value>
    [DataMember(Name="country", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "country")]
    public string Country { get; set; }

    /// <summary>
    /// amount in decimal to be charged
    /// </summary>
    /// <value>amount in decimal to be charged</value>
    [DataMember(Name="amount", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "amount")]
    public string Amount { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class Order {\n");
      sb.Append("  Currency: ").Append(Currency).Append("\n");
      sb.Append("  Description: ").Append(Description).Append("\n");
      sb.Append("  Country: ").Append(Country).Append("\n");
      sb.Append("  Amount: ").Append(Amount).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
