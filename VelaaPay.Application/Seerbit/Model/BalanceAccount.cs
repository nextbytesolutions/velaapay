using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class BalanceAccount {
    /// <summary>
    /// Gets or Sets Fromcustomerid
    /// </summary>
    [DataMember(Name="fromcustomerid", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "fromcustomerid")]
    public string Fromcustomerid { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class BalanceAccount {\n");
      sb.Append("  Fromcustomerid: ").Append(Fromcustomerid).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
