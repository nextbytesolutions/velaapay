using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace VelaaPay.Application.Seerbit.Model {

  /// <summary>
  /// 
  /// </summary>
  [DataContract]
  public class AccountaccounttocardAccount {
    /// <summary>
    /// Gets or Sets Tocustomerid
    /// </summary>
    [DataMember(Name="tocustomerid", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "tocustomerid")]
    public string Tocustomerid { get; set; }

    /// <summary>
    /// Gets or Sets Sender
    /// </summary>
    [DataMember(Name="sender", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "sender")]
    public string Sender { get; set; }

    /// <summary>
    /// Gets or Sets Pan
    /// </summary>
    [DataMember(Name="pan", EmitDefaultValue=false)]
    [JsonProperty(PropertyName = "pan")]
    public string Pan { get; set; }


    /// <summary>
    /// Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()  {
      var sb = new StringBuilder();
      sb.Append("class AccountaccounttocardAccount {\n");
      sb.Append("  Tocustomerid: ").Append(Tocustomerid).Append("\n");
      sb.Append("  Sender: ").Append(Sender).Append("\n");
      sb.Append("  Pan: ").Append(Pan).Append("\n");
      sb.Append("}\n");
      return sb.ToString();
    }

    /// <summary>
    /// Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson() {
      return JsonConvert.SerializeObject(this, Formatting.Indented);
    }

}
}
