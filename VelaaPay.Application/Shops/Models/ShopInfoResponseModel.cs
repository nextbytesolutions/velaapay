namespace VelaaPay.Application.Shops.Models
{
    public class ShopInfoResponseModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int ShopId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string CreatedDate { get; set; }
    }

    public class ShopInfoAdminResponse : ShopInfoResponseModel
    {
        public string Owner { get; set; }
    }
}