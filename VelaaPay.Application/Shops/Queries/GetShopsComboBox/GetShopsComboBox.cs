using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Shops.Queries.GetShopsComboBox
{

    public class GetShopsComboBoxRequestHandler : IRequestHandler<GetShopsComboBoxRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetShopsComboBoxRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetShopsComboBoxRequestModel request, CancellationToken cancellationToken)
        {
            var shops = _context.Shops.Where(p => p.ShopUsers.Any(pr => pr.UserId == request.UserId))
                .Select(p => new ShopComboBox()
                {
                    ShopId = p.Id,
                    Name = p.Name
                }).ToList();
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(shops));
        }

        class ShopComboBox
        {
            public string Name { get; set; }
            public int ShopId { get; set; }
        }
    }

}