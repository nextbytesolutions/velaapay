using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Shops.Queries.GetShopsComboBox
{

    public class GetShopsComboBoxRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}