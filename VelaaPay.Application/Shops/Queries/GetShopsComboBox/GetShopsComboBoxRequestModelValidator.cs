using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Queries.GetShopsComboBox
{
    public class GetShopsComboBoxRequestModelValidator : AbstractValidator<GetShopsComboBoxRequestModel>
    {
        public GetShopsComboBoxRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}