using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Shops.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Shops.Queries.GetShops
{

    public class GetShopsRequestHandler : IRequestHandler<GetShopsRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetShopsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetShopsRequestModel request, CancellationToken cancellationToken)
        {
            var converter = new DateConverter();

            request.SetDefaultValue();
            List<ShopInfoAdminResponse> list = _context.Shops.GetManyReadOnly(
                    p =>
                         (p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search)), request.OrderBy,
                    request.Page, request.PageSize, request.IsDescending, p => p.Include(pr => pr.User))
                .Select(p => new ShopInfoAdminResponse()
                {
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Name = p.Name,
                    Address = p.Address,
                    ShopId = p.Id,
                    EndTime = p.EndTime,
                    StartTime = p.StartTime,
                    PhoneNumber = p.PhoneNumber,
                    Owner = p.User.FullName
                }).ToList();
            var count = _context.Shops.Count(p =>
                                                  (p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}