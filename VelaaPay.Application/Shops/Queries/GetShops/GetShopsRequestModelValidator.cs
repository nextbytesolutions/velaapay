using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Queries.GetShops
{
    public class GetShopsRequestModelValidator : AbstractValidator<GetShopsRequestModel>
    {
        public GetShopsRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}