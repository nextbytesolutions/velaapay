using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Shops.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Shops.Queries.GetShopsByUser
{

    public class GetShopsByUserRequestHandler : IRequestHandler<GetShopsByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetShopsByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetShopsByUserRequestModel request, CancellationToken cancellationToken)
        {
            var converter = new DateConverter();

            request.SetDefaultValue();
            List<ShopInfoResponseModel> list = _context.Shops.GetManyReadOnly(
                    p => p.UserId == request.UserId &&
                         (p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search)), request.OrderBy,
                    request.Page, request.PageSize, request.IsDescending)
                .Select(p => new ShopInfoResponseModel
                {
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Name = p.Name,
                    Address = p.Address,
                    ShopId = p.Id,
                    EndTime = p.EndTime,
                    StartTime = p.StartTime,
                    PhoneNumber = p.PhoneNumber
                }).ToList();
            var count = _context.Shops.Count(p => p.UserId == request.UserId &&
                                                  (p.Name.Contains(request.Search) || p.PhoneNumber.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}