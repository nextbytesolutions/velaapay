using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Queries.GetShopsByUser
{
    public class GetShopsByUserRequestModelValidator : AbstractValidator<GetShopsByUserRequestModel>
    {
        public GetShopsByUserRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}