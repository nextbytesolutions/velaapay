using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Shops.Commands.UpdateShop
{

    public class UpdateShopRequestHandler : IRequestHandler<UpdateShopRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateShopRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateShopRequestModel request, CancellationToken cancellationToken)
        {
            var shop = _context.Shops.GetBy(p => p.Id == request.ShopId && p.UserId == request.UserId);
            if (shop == null)
            {
                throw new NotFoundException(nameof(Shop), request.ShopId);
            }
            request.Copy(shop);
            _context.Shops.Update(shop);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}