using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Commands.UpdateShop
{
    public class UpdateShopRequestModelValidator : AbstractValidator<UpdateShopRequestModel>
    {
        public UpdateShopRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.Address).Required();
            RuleFor(p => p.PhoneNumber).Required();
            RuleFor(p => p.ShopId).Min(1);
            RuleFor(p => p.StartTime).Time();
        }
    }
}