using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Shops.Commands.UpdateShop
{

    public class UpdateShopRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public int ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}