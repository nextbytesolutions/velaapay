using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Shops.Models;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Shops.Commands.CreateShop
{

    public class CreateShopRequestHandler : IRequestHandler<CreateShopRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateShopRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }
 
        public Task<ResponseViewModel> Handle(CreateShopRequestModel request, CancellationToken cancellationToken)
        {
            Shop shop = new Shop()
            {
                UserId = request.UserId,
                Address = request.Address,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                StartTime = request.StartTime,
                EndTime = request.EndTime,
                ShopUsers = new List<ShopUser>()
                {
                    new ShopUser()
                    {
                        UserId = request.UserId,
                        StartTime = request.StartTime,
                        EndTime = request.EndTime,
                        EmployeeLevel = EmployeeLevel.Vendor,
                    }
                }
            };
            _context.Shops.Add(shop);
            _context.SaveChanges();
            return Task.FromResult(new ResponseViewModel().CreateOk(new ShopInfoResponseModel()
            {
                PhoneNumber = shop.PhoneNumber,
                Name = shop.Name,
                ShopId = shop.Id,
                Address = shop.Address,
                EndTime = shop.EndTime,
                StartTime = shop.StartTime,
            }));
        }
    }

}