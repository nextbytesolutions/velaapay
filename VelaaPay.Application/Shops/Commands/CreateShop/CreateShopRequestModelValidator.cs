using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Commands.CreateShop
{
    public class CreateShopRequestModelValidator : AbstractValidator<CreateShopRequestModel>
    {
        public CreateShopRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.Address).Required();
            RuleFor(p => p.PhoneNumber).Required();
            RuleFor(p => p.StartTime).Time();
        }
    }
}