using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Shops.Commands.DeleteShop
{

    public class DeleteShopRequestModel : IRequest<ResponseViewModel>
    {
        public int ShopId { get; set; }
        public string UserId { get; set; }
    }
}