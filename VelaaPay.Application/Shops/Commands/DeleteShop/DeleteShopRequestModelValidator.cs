using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Shops.Commands.DeleteShop
{
    public class DeleteShopRequestModelValidator : AbstractValidator<DeleteShopRequestModel>
    {
        public DeleteShopRequestModelValidator()
        {
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}