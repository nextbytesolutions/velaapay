using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Shops.Commands.DeleteShop
{

    public class DeleteShopRequestHandler : IRequestHandler<DeleteShopRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteShopRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(DeleteShopRequestModel request, CancellationToken cancellationToken)
        {
            var shop = _context.Shops.GetBy(p => p.Id == request.ShopId && !p.ShopUsers.Any(pr => pr.EmployeeLevel > 1 || pr.CashInTracks.Any() || pr.CashOutTracks.Any()), p => p.Include(pr => pr.ShopUsers));
            if (shop == null)
            {
                throw new CannotDeleteException(nameof(Shop));
            }
            _context.RemoveRange(shop.ShopUsers);
            _context.Remove(shop);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}