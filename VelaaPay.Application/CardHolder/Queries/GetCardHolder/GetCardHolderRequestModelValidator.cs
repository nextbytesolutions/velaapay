using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CardHolder.Queries.GetCardHolder
{
    public class GetCardHolderRequestModelValidator : AbstractValidator<GetCardHolderRequestModel>
    {
        public GetCardHolderRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}