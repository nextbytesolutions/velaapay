using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CardHolder.Queries.GetCardHolder
{

    public class GetCardHolderRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }

            if (OrderBy == "customer")
            {
                OrderBy = "User.FullName";
            }
            
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
    }
}