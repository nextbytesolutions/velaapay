using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CardHolder.Queries.GetCardHolderByUser
{

    public class GetCardHolderByUserRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }

        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string UserId { get; set; }
    }
}