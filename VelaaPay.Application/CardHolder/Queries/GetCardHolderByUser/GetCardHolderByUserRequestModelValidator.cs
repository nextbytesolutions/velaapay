using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CardHolder.Queries.GetCardHolderByUser
{
    public class GetCardHolderByUserRequestModelValidator : AbstractValidator<GetCardHolderByUserRequestModel>
    {
        public GetCardHolderByUserRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}