using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CardHolder.Queries.GetCardHolderByUser
{

    public class GetCardHolderByUserRequestHandler : IRequestHandler<GetCardHolderByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCardHolderByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCardHolderByUserRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            Expression<Func<Model.Entities.CardHolder, bool>> searchExpression = p =>
                p.UserId == request.UserId && (
                p.User.FullName.Contains(request.Search) ||
                p.LastDigits.Contains(request.Search));
            var converter = new DateConverter();
            var list = _context.CardHolders.GetManyReadOnly(searchExpression,
                request.OrderBy, request.Page, request.PageSize, request.IsDescending
                ,p => p.Include(pr => pr.User)).Select(p =>
                new CardHolderResponse()
                {
                    Id = p.Id,
                    CardType = p.CardType,
                    ExpiryDate = converter.ConvertToDateTime(p.ExpiryDate),
                    Name = p.Name,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    LastDigits = p.LastDigits,
                }).ToList();
            var count = await _context.CardHolders.CountAsync(searchExpression, cancellationToken);
            return new ResponseViewModel().CreateOk(list, count);
        }
        
        private class CardHolderResponse
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int CardType { get; set; }
            public string CreatedDate { get; set; }
            public string ExpiryDate { get; set; }
            public string LastDigits { get; set; }
        }
    }

}