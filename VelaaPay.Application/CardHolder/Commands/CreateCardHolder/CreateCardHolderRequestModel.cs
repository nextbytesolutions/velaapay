using System;
using MediatR;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CardHolder.Commands.CreateCardHolder
{

    public class CreateCardHolderRequestModel : IRequest<ResponseViewModel>
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public CardType CardType { get; set; }
        public string LastDigits { get; set; }
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string UserId { get; set; }
    }
}