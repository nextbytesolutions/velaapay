using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Stripe;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.CardHolder.Commands.CreateCardHolder
{

    public class CreateCardHolderRequestHandler : IRequestHandler<CreateCardHolderRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateCardHolderRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(CreateCardHolderRequestModel request, CancellationToken cancellationToken)
        {
            var customerOptions = new CustomerCreateOptions {
                Source = request.Token,
                Email = request.Email,
            };
            var customerService = new CustomerService();
            Customer customer = customerService.Create(customerOptions);
            if (customer.StripeResponse.StatusCode == HttpStatusCode.OK)
            {
                var cardHolder = new Model.Entities.CardHolder()
                {
                    Name = request.Name,
                    CardType = request.CardType.ToInt(),
                    ExpiryDate = request.ExpiryDate,
                    StripeToken = customer.Id,
                    UserId = request.UserId,
                    LastDigits = request.LastDigits,
                };

                _context.Add(cardHolder);
                await _context.SaveChangesAsync(cancellationToken);    
                return new ResponseViewModel().CreateOk(new CardHolderReturn()
                {
                    Name = request.Name,
                    CardType = request.CardType.ToInt(),
                    ExpiryDate = request.ExpiryDate,
                    UserId = request.UserId,
                    LastDigits = request.LastDigits,
                });
            }
            throw new ThirdPartyException(customer.StripeResponse.Content);
        }
        private class CardHolderReturn
        {
            public string Name { get; set; }
            public string LastDigits { get; set; }
            public DateTime ExpiryDate { get; set; }
            public string UserId { get; set; }
            public int CardType { get; set; }
        }
    }

}