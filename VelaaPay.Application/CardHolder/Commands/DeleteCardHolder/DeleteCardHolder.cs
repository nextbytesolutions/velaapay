using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Stripe;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CardHolder.Commands.DeleteCardHolder
{

    public class DeleteCardHolderRequestHandler : IRequestHandler<DeleteCardHolderRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public DeleteCardHolderRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(DeleteCardHolderRequestModel request, CancellationToken cancellationToken)
        {
            var cardHolder = _context.CardHolders.GetBy(p => p.UserId == request.UserId && p.Id == request.Id);
            if (cardHolder == null)
            {
                throw new NotFoundException(nameof(Model.Entities.CardHolder),request.Id);
            }
            var customerService = new CustomerService();
            Customer customer = customerService.Delete(cardHolder.StripeToken);
            if (customer.StripeResponse.StatusCode == HttpStatusCode.OK)
            {
                
                _context.Remove(cardHolder);
                await _context.SaveChangesAsync(cancellationToken);
                return new ResponseViewModel();
            }
            throw new ThirdPartyException(customer.StripeResponse.Content);
        }
    }

}