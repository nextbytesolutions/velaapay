using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CardHolder.Commands.DeleteCardHolder
{

    public class DeleteCardHolderRequestModel : IRequest<ResponseViewModel>
    {
        public int Id { get; set; }
        public string UserId { get; set; }
    }
}