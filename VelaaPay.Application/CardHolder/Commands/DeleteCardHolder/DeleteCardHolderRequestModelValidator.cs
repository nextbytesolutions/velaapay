using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CardHolder.Commands.DeleteCardHolder
{
    public class DeleteCardHolderRequestModelValidator : AbstractValidator<DeleteCardHolderRequestModel>
    {
        public DeleteCardHolderRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}