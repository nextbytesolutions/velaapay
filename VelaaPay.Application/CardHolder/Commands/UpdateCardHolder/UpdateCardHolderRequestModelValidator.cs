using System;
using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CardHolder.Commands.UpdateCardHolder
{
    public class UpdateCardHolderRequestModelValidator : AbstractValidator<UpdateCardHolderRequestModel>
    {
        public UpdateCardHolderRequestModelValidator()
        {
            RuleFor(p => p.Name).Max(50);
            RuleFor(p => p.Id).Required();
            RuleFor(p => p.LastDigits).Pin();
            RuleFor(p => p.Token).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Email).Required();
            RuleFor(p => p.ExpiryDate).GreaterThan(DateTime.UtcNow);
            RuleFor(p => p.CardType).IsInEnum();
        }
    }
}