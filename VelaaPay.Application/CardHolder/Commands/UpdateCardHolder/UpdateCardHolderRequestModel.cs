using System;
using MediatR;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CardHolder.Commands.UpdateCardHolder
{

    public class UpdateCardHolderRequestModel : IRequest<ResponseViewModel>
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public CardType CardType { get; set; }
        public string LastDigits { get; set; }
        public string Name { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string UserId { get; set; }
    }
}