using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Stripe;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CardHolder.Commands.UpdateCardHolder
{

    public class UpdateCardHolderRequestHandler : IRequestHandler<UpdateCardHolderRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateCardHolderRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateCardHolderRequestModel request, CancellationToken cancellationToken)
        {
            var cardHolder = _context.CardHolders.GetBy(p => p.UserId == request.UserId && p.Id == request.Id);
            if (cardHolder == null)
            {
                throw new NotFoundException(nameof(Model.Entities.CardHolder),request.Id);
            }
            var customerOptions = new CustomerUpdateOptions  {
                Source = request.Token,
                Email = request.Email,
            };
            var customerService = new CustomerService();
            Customer customer = customerService.Update(cardHolder.StripeToken, customerOptions);
            if (customer.StripeResponse.StatusCode == HttpStatusCode.OK)
            {
                cardHolder.Name = request.Name;
                cardHolder.CardType = request.CardType.ToInt();
                cardHolder.ExpiryDate = request.ExpiryDate;
                cardHolder.StripeToken = customer.Id;
                cardHolder.LastDigits = request.LastDigits;
                _context.Update(cardHolder);
                await _context.SaveChangesAsync(cancellationToken);
                return new ResponseViewModel().CreateOk(new CardHolderReturn()
                {
                    Name = request.Name,
                    CardType = request.CardType.ToInt(),
                    ExpiryDate = request.ExpiryDate,
                    UserId = request.UserId,
                    LastDigits = request.LastDigits,
                });
            }
            throw new ThirdPartyException(customer.StripeResponse.Content);
        }
        private class CardHolderReturn
        {
            public string Name { get; set; }
            public string LastDigits { get; set; }
            public DateTime ExpiryDate { get; set; }
            public string UserId { get; set; }
            public int CardType { get; set; }
        }
    }

}