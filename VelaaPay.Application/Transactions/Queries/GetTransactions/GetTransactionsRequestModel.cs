using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Transactions.Queries.GetTransactions
{

    public class GetTransactionsRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }

            if (OrderBy.Equals("senderName"))
            {
                OrderBy = "SenderUser.FullName";
            }
            if (OrderBy.Equals("receiverName"))
            {
                OrderBy = "ReceiverUser.FullName";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public int Type { get; set; }
        public string UserId { get; set; }
    }
}