using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Transactions.Queries.GetTransactions
{

    public class GetTransactionsRequestHandler : IRequestHandler<GetTransactionsRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetTransactionsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetTransactionsRequestModel request,
            CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var isTypeSearch = request.Type == -1;
            List<TransactionWebInfoViewModel> list;
            int count;
            if (string.IsNullOrEmpty(request.UserId))
            {
                list = _context.Transactions.GetMany(
                        p => (p.LoadType == request.Type || isTypeSearch) &&
                             (p.SenderUser.FullName.Contains(request.Search) ||
                              p.SenderUserId.Equals(request.Search) ||
                              p.Id.Equals(request.Search) ||
                              p.ReceiverUserId.Equals(request.Search) ||
                              p.ReceiverUser.FullName.Contains(request.Search)),
                        request.OrderBy, request.Page, request.PageSize,
                        request.IsDescending, p => p.Include(pr => pr.ReceiverUser)
                            .Include(pr => pr.SenderUser))
                    .Select(p => new TransactionWebInfoViewModel
                    {
                        Amount = p.Amount,
                        TransactionId = p.Id,
                        LoadType = p.LoadType,
                        ReceiverName = p.ReceiverUser == null ? "" : p.ReceiverUser.FullName + "",
                        SenderName = p.SenderUser == null ? "" : p.SenderUser.FullName + "",
                        Date = new DateConverter().ConvertToDateTime(p.CreatedDate)
                    }).ToList();
                count = _context.Transactions.Count(
                    p => (p.LoadType == request.Type || isTypeSearch) &&
                         
                         (p.SenderUser.FullName.Contains(request.Search)
                          || p.SenderUserId.Equals(request.Search) ||
                          p.Id.Equals(request.Search) ||
                          p.ReceiverUserId.Equals(request.Search) ||
                          p.ReceiverUser.FullName.Contains(
                              request.Search)));
            }
            else
            {
                list = _context.Transactions.GetMany(
                        p => (p.LoadType == request.Type || isTypeSearch) &&
                             (p.SenderUserId == request.UserId || p.ReceiverUserId == request.UserId) &&
                             (p.SenderUser.FullName.Contains(request.Search) || p.SenderUserId.Equals(request.Search) ||
                              p.ReceiverUserId.Equals(request.Search) ||
                              p.Id.Equals(request.Search) ||
                              p.ReceiverUser.FullName.Contains(request.Search)),
                        request.OrderBy, request.Page, request.PageSize,
                        request.IsDescending, p => p.Include(pr => pr.ReceiverUser)
                            .Include(pr => pr.SenderUser))
                    .Select(p => new TransactionWebInfoViewModel
                    {
                        Amount = p.Amount,
                        TransactionId = p.Id,
                        LoadType = p.LoadType,
                        ReceiverName = p.ReceiverUser == null ? "" : p.ReceiverUser.FullName + "",
                        SenderName = p.SenderUser == null ? "" : p.SenderUser.FullName + "",
                        Date = new DateConverter().ConvertToDateTime(p.CreatedDate)
                    }).ToList();
                count = _context.Transactions.Count(
                    p => (p.LoadType == request.Type || isTypeSearch) &&
                         (p.SenderUserId == request.UserId || p.ReceiverUserId == request.UserId) &&
                         (p.SenderUser.FullName.Contains(request.Search) || p.SenderUserId.Equals(request.Search) ||
                          p.Id.Equals(request.Search) ||
                          p.ReceiverUserId.Equals(request.Search) || p.ReceiverUser.FullName.Contains(request.Search)));

            }

            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }

        private class TransactionWebInfoViewModel
        {
            public string TransactionId { get; set; }
            public int LoadType { get; set; }
            public string SenderName { get; set; }
            public string ReceiverName { get; set; }
            public double Amount { get; set; }
            public string Date { get; set; }
        }
    }

}