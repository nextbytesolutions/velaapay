using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Transactions.Queries.GetTransactions
{
    public class GetTransactionsRequestModelValidator : AbstractValidator<GetTransactionsRequestModel>
    {
        public GetTransactionsRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);

        }
    }
}