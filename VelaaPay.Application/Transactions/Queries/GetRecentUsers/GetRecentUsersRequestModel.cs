using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Transactions.Queries.GetRecentUsers
{

    public class GetRecentUsersRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}