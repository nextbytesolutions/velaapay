using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Transactions.Queries.GetRecentUsers
{
    public class GetRecentUsersRequestModelValidator : AbstractValidator<GetRecentUsersRequestModel>
    {
        public GetRecentUsersRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Required();
            RuleFor(p => p.PageSize).Required().Max(20);
        }
    }
}