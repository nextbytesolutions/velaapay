using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Transactions.Queries.GetRecentUsers
{

    public class GetRecentUsersRequestHandler : IRequestHandler<GetRecentUsersRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetRecentUsersRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetRecentUsersRequestModel request,
            CancellationToken cancellationToken)
        {
            var list = _context.Transactions
                .Where(p => (p.SenderUserId == request.UserId) &&
                            (p.ReceiverUserId != null))
                .Include(p => p.ReceiverUser)
                .GroupBy(p => p.ReceiverUserId)
                .OrderByDescending(p => p.Max(pr => pr.CreatedDate))
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .Select(p => new UserInfoViewModel()
                {
                    FullName = p.FirstOrDefault().ReceiverUser.FullName,
                    Id = p.FirstOrDefault().ReceiverUser.Id,
                    ImageUrl = p.FirstOrDefault().ReceiverUser.Image,
                    MobileNumber = p.FirstOrDefault().ReceiverUser.PhoneNumber
                }).ToList();

            return Task.FromResult(new ResponseViewModel().CreateOk(list));
        }

        class UserInfoViewModel
        {
            public string Id { get; set; }
            public string MobileNumber { get; set; }
            public string FullName { get; set; }
            public string ImageUrl { get; set; }
        }
    }

}