using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Companies.Commands.UpdateCompany
{
    public class UpdateCompanyRequestModelValidator : AbstractValidator<UpdateCompanyRequestModel>
    {
        public UpdateCompanyRequestModelValidator()
        {
            RuleFor(p => p.CompanyId).Required();
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.ImageUrl).Required();
            RuleFor(p => p.Type).IsInEnum();
        }
    }
}