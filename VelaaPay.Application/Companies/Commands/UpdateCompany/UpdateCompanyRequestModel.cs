using MediatR;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Companies.Commands.UpdateCompany
{

    public class UpdateCompanyRequestModel : IRequest<ResponseViewModel>
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public CompanyType Type { get; set; }

    }
}