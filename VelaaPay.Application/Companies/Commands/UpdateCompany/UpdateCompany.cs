using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Companies.Commands.UpdateCompany
{

    public class UpdateCompanyRequestHandler : IRequestHandler<UpdateCompanyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        
        public UpdateCompanyRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateCompanyRequestModel request, CancellationToken cancellationToken)
        {
            var company = _context.Companies.GetBy(p => p.Id == request.CompanyId);
            if (company == null)
            {
                throw new NotFoundException(nameof(Company), request.CompanyId);
            }

            company.Name = request.Name;
            company.Type = request.Type.ToInt();
            if (company.ImageUrl != request.ImageUrl)
            {
                company.ImageUrl = _imageService.SaveImage(request.ImageUrl);
                request.ImageUrl = company.ImageUrl;
            }

            _context.Update(company);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(request);
        }
    }

}