using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Companies.Commands.DeleteCompany
{

    public class DeleteCompanyRequestHandler : IRequestHandler<DeleteCompanyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public DeleteCompanyRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(DeleteCompanyRequestModel request, CancellationToken cancellationToken)
        {
            var company = _context.Companies.GetBy(p => p.Id == request.CompanyId && !p.AirTopUpRequests.Any() 
                                                                                  && !p.Bills.Any());
            if (company == null)
            {
                throw new CannotDeleteException(nameof(Company));    
            }
            _imageService.DeleteImage(company.ImageUrl);
            _context.Remove(company);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}