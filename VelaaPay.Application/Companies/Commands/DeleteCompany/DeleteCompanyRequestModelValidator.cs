using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Companies.Commands.DeleteCompany
{
    public class DeleteCompanyRequestModelValidator : AbstractValidator<DeleteCompanyRequestModel>
    {
        public DeleteCompanyRequestModelValidator()
        {
            RuleFor(p => p.CompanyId).Required();
        }
    }
}