using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Companies.Commands.DeleteCompany
{

    public class DeleteCompanyRequestModel : IRequest<ResponseViewModel>
    {
        public int CompanyId { get; set; }
    }
}