using MediatR;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Companies.Commands.CreateCompany
{

    public class CreateCompanyRequestModel : IRequest<ResponseViewModel>
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public CompanyType Type { get; set; }

    }
}