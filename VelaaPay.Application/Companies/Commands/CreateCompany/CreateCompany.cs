using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Companies.Commands.CreateCompany
{

    public class CreateCompanyRequestHandler : IRequestHandler<CreateCompanyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public CreateCompanyRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(CreateCompanyRequestModel request, CancellationToken cancellationToken)
        {
            var company = new Company()
            {
                Name = request.Name,
                Type = request.Type.ToInt(),
                ImageUrl = _imageService.SaveImage(request.ImageUrl)
            };
            _context.Add(company);
            await _context.SaveChangesAsync(cancellationToken);
            request.ImageUrl = company.ImageUrl;
            return new ResponseViewModel().CreateOk(request);
        }
    }

}