using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Companies.Commands.CreateCompany
{
    public class CreateCompanyRequestModelValidator : AbstractValidator<CreateCompanyRequestModel>
    {
        public CreateCompanyRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.ImageUrl).Required();
            RuleFor(p => p.Type).IsInEnum();
        }
    }
}