using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Companies.Queries.GetCompanies
{

    public class GetCompaniesRequestHandler : IRequestHandler<GetCompaniesRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCompaniesRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCompaniesRequestModel request,
            CancellationToken cancellationToken)
        {
            var converter = new DateConverter();
            request.SetDefaultValue();
            bool isType = request.Type == 0;

            var list = _context.Companies.GetManyReadOnly(
                p => (p.Type == request.Type || isType) && p.Name.Contains(request.Search), request.OrderBy,
                request.Page, request.PageSize, request.IsDescending).Select(p => new CompanyInfoModel()
            {
                Name = p.Name,
                ImageUrl = p.ImageUrl,
                CompanyId = p.Id,
                Type = p.Type,
                CreatedDate = converter.ConvertToDateTime(p.CreatedDate)
            }).ToList();
            var count = await _context.Companies.CountAsync(
                p => (p.Type == request.Type || isType) && p.Name.Contains(request.Search), cancellationToken);
            return new ResponseViewModel().CreateOk(list, count);
        }
        class CompanyInfoModel
        {
            public string Name { get; set; }
            public int CompanyId { get; set; }
            public string ImageUrl { get; set; }
            public string CreatedDate { get; set; }
            public int Type { get; set; }
        }
    }

}