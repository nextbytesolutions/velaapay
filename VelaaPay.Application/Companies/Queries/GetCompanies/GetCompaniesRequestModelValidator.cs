using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Companies.Queries.GetCompanies
{
    public class GetCompaniesRequestModelValidator : AbstractValidator<GetCompaniesRequestModel>
    {
        public GetCompaniesRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}