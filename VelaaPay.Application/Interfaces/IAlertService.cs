using System.Threading.Tasks;

namespace VelaaPay.Application.Interfaces
{
    public interface IAlertService
    {
        Task<bool> SendAlertToAdmin(string message);
    }
}