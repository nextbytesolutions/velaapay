using System.Threading.Tasks;

namespace VelaaPay.Application.Interfaces
{
    public interface INotificationService
    {
        Task<bool> SendNotification(string fcmId, string title, string body);
    }
}