using VelaaPay.Application.Seerbit.Model;

namespace VelaaPay.Application.Interfaces
{
    public interface ISeerbitService
    {

        void Auth();
        /// <summary>
        /// This is to activate a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Activatecard (Cardactivate body);
        /// <summary>
        /// This is to fetch the balance on a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Balance (Balance body);
        /// <summary>
        /// This is to initiate transfer from a prepaid card to a bank account 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Cardtransfertoaccount (Transfercardtoaccount body);
        /// <summary>
        /// This is to get the details of a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Customerdetails (Customerdetails body);
        /// <summary>
        /// This is to deactivate a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Deactivatecard (Carddeactivate body);
        /// <summary>
        /// This is to fetch the statement on a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Statement (Statement body);
        /// <summary>
        /// This is to initiate transfer from a prepaid card to another prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Transfer (Transfer body);
        /// <summary>
        /// This is to initiate transfer from a bank account to a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param>
        /// <returns></returns>
        string Transferaccounttocard (AccounttocardTransfer body);
    }
}