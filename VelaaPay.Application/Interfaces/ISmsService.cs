using System.Threading.Tasks;

namespace VelaaPay.Application.Interfaces
{
    public interface ISmsService
    {
        Task<bool> SendMessage(string phoneNumber, string message);
    }
}