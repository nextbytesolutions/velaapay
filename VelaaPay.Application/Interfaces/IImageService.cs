namespace VelaaPay.Application.Interfaces
{
    public interface IImageService
    {
        string SaveImage(string base64);
        void DeleteImage(string url);
    }
}