namespace VelaaPay.Application.Interfaces
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ISeerbitOauthTokenService
    {
        /// <summary>
        /// Use this method to get a bearer token to authenticate API calls 
        /// </summary>
        /// <param name="grantType">grant_type</param>
        /// <param name="clientId">assigned client id</param>
        /// <param name="clientSecret">assigned client secret</param>
        /// <returns></returns>
        string Auth (string grantType, string clientId, string clientSecret);
        
    }
}