using FluentValidation;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Sms.Commands.SendSms
{
    public class SendSmsRequestModelValidator : AbstractValidator<SendSmsRequestModel>
    {
        public SendSmsRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).NotEmpty().WithMessage(Messages.EmptyError).MaximumLength(14)
                .WithMessage(Messages.MaxLengthError(14));
            RuleFor(p => p.Message).NotEmpty().WithMessage(Messages.InvalidFormat).MaximumLength(50).WithMessage(Messages.MaxLengthError(50));
        }
    }
}