using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Sms.Commands.SendSms
{

    public class SendSmsRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }
}