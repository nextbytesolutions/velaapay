using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Sms.Commands.SendSms
{

    public class SendSmsRequestHandler : IRequestHandler<SendSmsRequestModel, ResponseViewModel>
    {

        private readonly ISmsService _service;

        public SendSmsRequestHandler(ISmsService service)
        {
            _service = service;
        }

        public async Task<ResponseViewModel> Handle(SendSmsRequestModel request, CancellationToken cancellationToken)
        {
            await _service.SendMessage(request.PhoneNumber, request.Message);
            return new ResponseViewModel();
        }
    }

}