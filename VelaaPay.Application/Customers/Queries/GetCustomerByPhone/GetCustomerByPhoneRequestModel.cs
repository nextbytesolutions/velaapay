using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Queries.GetCustomerByPhone
{

    public class GetCustomerByPhoneRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
    }
}