using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Customers.Models;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerByPhone
{

    public class GetCustomerByPhoneRequestHandler : IRequestHandler<GetCustomerByPhoneRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCustomerByPhoneRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetCustomerByPhoneRequestModel request,
            CancellationToken cancellationToken)
        {
            var customerId = _context.Roles.Where(p => p.Name.Equals(RoleNames.Customer)).Select(p => p.Id).First();
            var merchantId = _context.Roles.Where(p => p.Name.Equals(RoleNames.Vendor)).Select(p => p.Id).First();
            var user = _context.Users.GetByReadOnly(p =>
                p.PhoneNumber.Equals(request.PhoneNumber) && p.Roles.Any(pr => pr.RoleId == customerId || pr.RoleId == merchantId) && p.IsEnabled);
            if (user != null)
            {
                var model = new CustomerInfoPhoneResponseModel
                {
                    Id = user.Id, Name = user.FullName, Image = user.Image, Email = user.Email,
                    Phone = user.PhoneNumber
                };
                return Task.FromResult(new ResponseViewModel().CreateOk(model));
            }
            
            return Task.FromResult(new ResponseViewModel());
        }
    }

}