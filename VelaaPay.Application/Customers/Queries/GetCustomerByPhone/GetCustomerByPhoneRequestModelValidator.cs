using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerByPhone
{
    public class GetCustomerByPhoneRequestModelValidator : AbstractValidator<GetCustomerByPhoneRequestModel>
    {
        public GetCustomerByPhoneRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
        }
    }
}