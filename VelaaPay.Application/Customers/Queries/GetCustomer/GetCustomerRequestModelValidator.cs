using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Customers.Queries.GetCustomer
{
    public class GetCustomerRequestModelValidator : AbstractValidator<GetCustomerRequestModel>
    {
        public GetCustomerRequestModelValidator()
        {
            RuleFor(p => p.Page).NotEmpty().WithMessage(Messages.EmptyError);
            RuleFor(p => p.PageSize).NotEmpty().WithMessage(Messages.EmptyError).Max(20);
        }
    }
}