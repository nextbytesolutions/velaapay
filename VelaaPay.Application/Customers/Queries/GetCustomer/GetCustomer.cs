using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Customers.Models;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomer
{

    public class GetCustomerRequestHandler : IRequestHandler<GetCustomerRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCustomerRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetCustomerRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();
            var customerId = _context.Roles.Where(p => p.Name.Equals(RoleNames.Customer)).Select(p => p.Id).First();
            List<CustomerInfoResponseModel> list = _context.Users
                .GetManyReadOnly(p => p.Roles.Any(pr => pr.RoleId == customerId) &&
                                      (p.Email == request.Search || p.PhoneNumber == request.Search ||
                                       p.Id == request.Search ||
                                       p.FullName.Contains(request.Search)), request.OrderBy, request.Page,
                    request.PageSize,
                    request.IsDescending, p => p.Include(pr => pr.UserBalances))
                .Select(p => new CustomerInfoResponseModel
                {
                    Id = p.Id,
                    FullName = p.FullName,
                    Email = p.Email,
                    TotalBalance = p.UserBalances.First().TotalBalance,
                    PhoneNumber = p.PhoneNumber,
                    IsEnabled = p.IsEnabled,
                    IsVerified = p.IsVerified,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Cnic = p.Identification,
                    CnicImage = p.IdentificationImageUrl,
                }).ToList();
            var count = _context.Users.Count(p =>
                p.Roles.Any(pr => pr.RoleId == customerId) &&
                (p.Email == request.Search || p.PhoneNumber == request.Search || p.Id == request.Search ||
                 p.FullName.Contains(request.Search)));

            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}