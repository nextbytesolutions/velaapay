using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerInContact
{
    public class GetCustomerInContactRequestModelValidator : AbstractValidator<GetCustomerInContactRequestModel>
    {
        public GetCustomerInContactRequestModelValidator()
        {
            RuleFor(p => p.MobileNumbers).Required();
        }
    }
}