using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerInContact
{

    public class GetCustomerInContactRequestHandler : IRequestHandler<GetCustomerInContactRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCustomerInContactRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetCustomerInContactRequestModel request, CancellationToken cancellationToken)
        {
            var customerRoleId = _context.Roles.GetBy(p => p.Name == RoleNames.Customer).Id;
            var users = _context.Users.GetAllReadOnly(p =>
                p.Roles.Any(pr => pr.RoleId == customerRoleId) && request.MobileNumbers.Contains(p.PhoneNumber)).Select(
                p => new MobileName()
                {
                    MobileNumber = p.PhoneNumber,
                    FullName = p.FullName
                }).ToList();
            var list = (from mobileNumber in request.MobileNumbers let user = users.FirstOrDefault(p => p.MobileNumber == mobileNumber) select new MobileStatus() {Name = user?.FullName, IsUser = user != null, MobileNumber = mobileNumber}).ToList();
            return Task.FromResult(new ResponseViewModel().CreateOk(list));
        }

        class MobileName
        {
            public string FullName { get; set; }
            public string MobileNumber { get; set; }
                        
        }
        class MobileStatus
        {
            public string Name { get; set; }
            public string MobileNumber { get; set; }
            public bool IsUser { get; set; }
        }
    }

}