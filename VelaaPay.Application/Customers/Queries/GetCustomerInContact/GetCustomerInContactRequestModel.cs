using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Queries.GetCustomerInContact
{

    public class GetCustomerInContactRequestModel : IRequest<ResponseViewModel>
    {
        public string[] MobileNumbers { get; set; }
    }
}