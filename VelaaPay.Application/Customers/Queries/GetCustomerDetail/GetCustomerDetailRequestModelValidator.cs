using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerDetail
{
    public class GetCustomerDetailRequestModelValidator : AbstractValidator<GetCustomerDetailRequestModel>
    {
        public GetCustomerDetailRequestModelValidator()
        {
            RuleFor(p => p.CustomerId).Required();
        }
    }
}