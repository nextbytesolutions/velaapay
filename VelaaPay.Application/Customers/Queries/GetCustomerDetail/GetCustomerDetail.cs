using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Queries.GetCustomerDetail
{

    public class GetCustomerDetailRequestHandler : IRequestHandler<GetCustomerDetailRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetCustomerDetailRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetCustomerDetailRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.CustomerId, p => p.Include(pr => pr.UserBalances));
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer,"");
            }

            var converter = new DateConverter();
            List<TransactionWebInfoViewModel> recentActivities = _context.Transactions.Where(p => p.SenderUserId == request.CustomerId || p.ReceiverUserId == request.CustomerId).OrderByDescending(p => p.CreatedBy).Take(10).Select(p => new TransactionWebInfoViewModel()
            {
                Amount = p.Amount,
                TransactionId = p.Id,
                LoadType = p.LoadType,
                Date = converter.ConvertToDateTime(p.CreatedDate),
                ReceiverName = p.ReceiverUser != null ? p.ReceiverUser.FullName : "",
                SenderName = p.SenderUser != null ? p.SenderUser.FullName : "",
            }).ToList();
            double wallet = user.UserBalances.First().TotalBalance;
            double totalSent = await _context.Transactions.Where(p => p.SenderUserId == request.CustomerId).SumAsync(p => p.Amount,cancellationToken);
            double totalReceived =await _context.Transactions.Where(p => p.ReceiverUserId == request.CustomerId).SumAsync(p => p.Amount,cancellationToken);
            List<RequestWebInfoViewModel> requests = _context.Requests.Where(p => p.FromUserId == request.CustomerId || p.ToUserId == request.CustomerId)
                .OrderByDescending(p => p.CreatedDate).Take(10)
                .Select(p => new RequestWebInfoViewModel()
                {
                    Amount = p.Amount,
                    ReceiverName = p.ToUser != null ? p.ToUser.FullName : "",
                    SenderName = p.FromUser != null ? p.FromUser.FullName : "",
                    Date = converter.ConvertToDateTime(p.CreatedDate),
                    Status = p.Status,
                }).ToList();

            UserDetailsWebInfoViewModel data = new UserDetailsWebInfoViewModel
            {
                Email = user.Email,
                Name = user.FullName,
                Phone = user.PhoneNumber,
                Transactions = recentActivities,
                Requests = requests,
                Wallet = wallet,
                TotalSent = totalSent,
                TotalReceived = totalReceived,
                Cnic = user.Identification,
                Image = user.Image,
                Date = converter.ConvertToDateTime(user.CreatedDate),
                IsEnabled = user.IsEnabled,
                IsVerified = user.IsVerified
            };
            return new ResponseViewModel().CreateOk(data);
        }
        class UserDetailsWebInfoViewModel
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public List<RequestWebInfoViewModel> Requests { get; set; }
            public List<TransactionWebInfoViewModel> Transactions { get; set; }
            public double Wallet { get; set; }
            public string Cnic { get; set; }
            public string Image { get; set; }
            public double TotalSent { get; set; }
            public double TotalReceived { get; set; }
            public string Date { get; set; }
            public bool IsVerified { get; set; }
            public bool IsEnabled { get; set; }
        }
        class RequestWebInfoViewModel
        {
            public string SenderName { get; set; }
            public string ReceiverName { get; set; }
            public double Amount { get; set; }
            public string Date { get; set; }
            public int Status { get; set; }
        }
        class TransactionWebInfoViewModel
        {
            public string TransactionCode { get; set; }
            public string TransactionId { get; set; }
            public int LoadType { get; set; }
            public string SenderName { get; set; }
            public string ReceiverName { get; set; }
            public double Amount { get; set; }
            public string Date { get; set; }
        }
    }

}