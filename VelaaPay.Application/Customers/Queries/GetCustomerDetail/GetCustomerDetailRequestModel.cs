using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Queries.GetCustomerDetail
{

    public class GetCustomerDetailRequestModel : IRequest<ResponseViewModel>
    {
        public string CustomerId { get; set; }
    }
}