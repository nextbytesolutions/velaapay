namespace VelaaPay.Application.Customers.Models
{
    public class CustomerInfoResponseModel
    {
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public double TotalBalance { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
        public string CreatedDate { get; set; }
        public string Cnic { get; set; }
        public string CnicImage { get; set; }
    }

    public class CustomerInfoPhoneResponseModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string Image { get; set; }
        public string Phone { get; set; }
    }
}