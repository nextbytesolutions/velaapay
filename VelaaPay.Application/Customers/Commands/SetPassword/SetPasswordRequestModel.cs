using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.SetPassword
{

    public class SetPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public string Email { get; set; }
        public string Pin { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
    }
}