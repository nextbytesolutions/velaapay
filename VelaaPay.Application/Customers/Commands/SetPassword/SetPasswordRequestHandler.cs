using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Customers.Commands.SetPassword
{

    public class SetPasswordRequestHandler : IRequestHandler<SetPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _manager;

        public SetPasswordRequestHandler(ApplicationDbContext context, UserManager<User> manager)
        {
            _context = context;
            _manager = manager;
        }

        public async Task<ResponseViewModel> Handle(SetPasswordRequestModel request, CancellationToken cancellationToken)
        {
            var isCustomerExist = _context.Users.Any(p =>
                p.Email.ToLower().Equals(request.Email));
            if (isCustomerExist)
            {
                throw new AlreadyExistsException("Email");
            }
            var user = await _context.Users.FindAsync(request.UserId);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.UserId);
            }
            var passwordResult = await _manager.ResetPasswordAsync(user, request.Token, request.Pin);
            if (passwordResult.Succeeded)
            {
                user.Email = request.Email;
                user.NormalizedEmail = request.Email.ToUpper();
                user.IsEnabled = true;
                _context.Users.Update(user);
                _context.SaveChanges();
                return new ResponseViewModel();
            }
            throw new ValidationException(passwordResult.Errors.ToList());
        }
    }

}