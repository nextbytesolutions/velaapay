using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Customers.Commands.SetPassword
{
    public class SetPasswordRequestModelValidator : AbstractValidator<SetPasswordRequestModel>
    {
        public SetPasswordRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin(4);
            RuleFor(p => p.Email).EmailAddress().WithMessage(Messages.InvalidFormat);
            RuleFor(p => p.Token).NotEmpty().WithMessage(Messages.EmptyError);
            RuleFor(p => p.UserId).NotEmpty().WithMessage(Messages.EmptyError);
        } 
    }
}