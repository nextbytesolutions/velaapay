using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Commands.EnableCustomer
{
    public class EnableCustomerRequestModelValidator : AbstractValidator<EnableCustomerRequestModel>
    {
        public EnableCustomerRequestModelValidator()
        {
            RuleFor(p => p.CustomerId).Required();
        }
    }
}