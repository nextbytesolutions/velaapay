using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Commands.EnableCustomer
{

    public class EnableCustomerRequestHandler : IRequestHandler<EnableCustomerRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public EnableCustomerRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(EnableCustomerRequestModel request, CancellationToken cancellationToken)
        {
            var customerId = _context.Roles.Where(p => p.Name == RoleNames.Customer).Select(p => p.Id).FirstOrDefault();
            var user = _context.Users.GetBy(p => p.Roles.Any(pr => pr.RoleId == customerId) && p.Id == request.CustomerId && !p.IsEnabled);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.CustomerId);                
            }

            user.IsEnabled = true;
            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}