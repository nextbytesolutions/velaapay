using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.EnableCustomer
{

    public class EnableCustomerRequestModel : IRequest<ResponseViewModel>
    {
        public string CustomerId { get; set; }
    }
}