using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Commands.UpdateCustomerPin
{
    public class UpdateCustomerPinRequestModelValidator : AbstractValidator<UpdateCustomerPinRequestModel>
    {
        public UpdateCustomerPinRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin(4);
            RuleFor(p => p.ConfirmPin).Pin(4).Matches(p => p.Pin).WithMessage("must match Pin");
            RuleFor(p => p.UserId).Required();
        }
    }
}