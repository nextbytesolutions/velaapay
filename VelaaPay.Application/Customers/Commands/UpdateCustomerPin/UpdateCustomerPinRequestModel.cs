using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.UpdateCustomerPin
{

    public class UpdateCustomerPinRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Pin { get; set; }
        public string ConfirmPin { get; set; }
    }
}