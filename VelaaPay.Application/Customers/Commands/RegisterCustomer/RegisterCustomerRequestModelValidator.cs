using FluentValidation;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Customers.Commands.RegisterCustomer
{
    public class RegisterCustomerRequestModelValidator : AbstractValidator<RegisterCustomerRequestModel>
    {
        public RegisterCustomerRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).NotEmpty().WithMessage(Messages.EmptyError).MaximumLength(14)
                .WithMessage(Messages.MaxLengthError(14));
            RuleFor(p => p.Name).NotEmpty().WithMessage(Messages.InvalidFormat).MaximumLength(50).WithMessage(Messages.MaxLengthError(50));
        }
    }
}