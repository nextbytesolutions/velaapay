using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Sms.Commands.SendSms;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Commands.RegisterCustomer
{

    public class RegisterCustomerRequestHandler : IRequestHandler<RegisterCustomerRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IMediator _mediator;
        public RegisterCustomerRequestHandler(ApplicationDbContext context, UserManager<User> userManager, IMediator mediator)
        {
            _context = context;
            _userManager = userManager;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(RegisterCustomerRequestModel request, CancellationToken cancellationToken)
        {
            var customerRoleId = _context.Roles.GetBy(p => p.Name == RoleNames.Customer).Id;
            var existingCustomer = _context.Users.GetBy(p =>
                p.Roles.Any(pr => pr.RoleId == customerRoleId) &&
               p.PhoneNumber.ToLower().Equals(request.PhoneNumber));
            if (existingCustomer != null)
            {
                if (!existingCustomer.IsVerified)
                {
                    if (existingCustomer.OtpExpiry <= DateTime.UtcNow)
                    {
                        existingCustomer.Otp = PinGenerator.CreatePin();
                        existingCustomer.OtpExpiry = DateTime.UtcNow.AddMinutes(10);
                        _context.Users.Update(existingCustomer);
                        _context.SaveChanges();
                        await _mediator.Send(new SendSmsRequestModel
                        {
                            Message = "You Velaa Pay Otp is : " + existingCustomer.Otp,
                            PhoneNumber = existingCustomer.PhoneNumber
                        }, cancellationToken);

                    }
                    return new ResponseViewModel();
                }
                throw new AlreadyExistsException("Phone Number");
            }
            else
            {
                var hasher = new PasswordHasher<User>();
                var customerId = _context.Roles.First(p => p.Name.Equals(RoleNames.Customer)).Id;
                var customer = new User()
                {
                    UserName = request.PhoneNumber,
                    NormalizedUserName = request.PhoneNumber,
                    PhoneNumber = request.PhoneNumber,
                    Image = Constant.DefaultImageUrl,
                    PhoneNumberConfirmed = true,
                    IsEnabled = true,
                    IsVerified = false,
                    Otp = PinGenerator.CreatePin(),
                    OtpExpiry = DateTime.UtcNow.AddMinutes(10),
                    FullName = request.Name,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    UserBalances = new List<UserBalance>()
                    {
                        new UserBalance()
                        {
                            TotalBalance = 0
                        }
                    }
                };
                customer.Email = customer.Id + "@yopmail.com";
                customer.Roles = new List<UserRole>()
                {
                    new UserRole()
                    {
                        RoleId = customerId,
                        UserId = customer.Id
                    }
                };
                customer.PasswordHash = hasher.HashPassword(customer, PinGenerator.CreatePin());
                _context.Users.Add(customer);
                _context.SaveChanges();
                await _mediator.Send(new SendSmsRequestModel
                {
                    Message = "You Velaa Pay Otp is : " + customer.Otp,
                    PhoneNumber = customer.PhoneNumber
                }, cancellationToken);
                return new ResponseViewModel();
            }
        }
    }

}