using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.RegisterCustomer
{

    public class RegisterCustomerRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
    }
}