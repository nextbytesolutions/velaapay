using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Commands.UpdateCustomer
{

    public class UpdateCustomerRequestHandler : IRequestHandler<UpdateCustomerRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public UpdateCustomerRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateCustomerRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            user.FullName = request.FullName;
            if (user.Identification != request.Cnic)
            {
                var isExist = _context.Users.Any(p => p.Identification == request.Cnic);
                if (isExist)
                {
                    throw new AlreadyExistsException(nameof(user.Identification));
                }
                user.Identification = request.Cnic;
            }

            if (user.IdentificationImageUrl != request.CnicImage)
            {
                user.IdentificationImageUrl = _imageService.SaveImage(request.CnicImage);
            }

            _context.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new ImageReturnModel()
            {
                Cnic = request.Cnic,
                FullName = request.FullName,
                CnicImage = user.IdentificationImageUrl
            });
        }
        class ImageReturnModel
        {
            public string Cnic { get; set; }
            public string FullName { get; set; }
            public string CnicImage { get; set; }
        }
    }

}