using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.UpdateCustomer
{

    public class UpdateCustomerRequestModel : IRequest<ResponseViewModel>
    {
        public string Cnic { get; set; }
        public string CnicImage { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
    }
}