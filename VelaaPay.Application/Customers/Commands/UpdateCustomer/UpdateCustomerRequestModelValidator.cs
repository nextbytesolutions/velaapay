using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Commands.UpdateCustomer
{
    public class UpdateCustomerRequestModelValidator : AbstractValidator<UpdateCustomerRequestModel>
    {
        public UpdateCustomerRequestModelValidator()
        {
            RuleFor(p => p.Cnic).Identification();
            RuleFor(p => p.CnicImage).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.FullName).Required();
        }
    }
}