using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Customers.Commands.VerifyCustomerPhone
{

    public class VerifyCustomerPhoneRequestHandler : IRequestHandler<VerifyCustomerPhoneRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<User> _userManager;
        public VerifyCustomerPhoneRequestHandler(ApplicationDbContext applicationDbContext, UserManager<User> userManager)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
        }

        public async Task<ResponseViewModel> Handle(VerifyCustomerPhoneRequestModel request, CancellationToken cancellationToken)
        {
            var user = _applicationDbContext.Users.GetBy(p => p.PhoneNumber.Equals(request.PhoneNumber));
            if (user == null)
            {
                throw new NotFoundException(nameof(RoleNames.Customer),request.PhoneNumber);
            }

            if (user.Otp == request.Otp && user.OtpExpiry >= DateTime.UtcNow)
            {
                user.IsVerified = true;
                user.Otp = null;
                user.OtpExpiry = null;
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                _applicationDbContext.Users.Update(user);
                _applicationDbContext.SaveChanges();
                return new ResponseViewModel().CreateOkCustom(new List<KeyValuePair<string, object>>() {
                    new KeyValuePair<string, object>(nameof(token),token),
                    new KeyValuePair<string, object>(nameof(user.Id),user.Id),
                });
            }
            throw new BadRequestException("OTP has been expired. Please register again");
        }
    }

}