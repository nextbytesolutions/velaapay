using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.VerifyCustomerPhone
{

    public class VerifyCustomerPhoneRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Otp { get; set; }
    }
}