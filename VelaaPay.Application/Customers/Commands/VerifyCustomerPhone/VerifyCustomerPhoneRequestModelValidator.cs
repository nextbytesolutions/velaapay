using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Customers.Commands.VerifyCustomerPhone
{
    public class VerifyCustomerPhoneRequestModelValidator : AbstractValidator<VerifyCustomerPhoneRequestModel>
    {
        public VerifyCustomerPhoneRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).NotEmpty().WithMessage(Messages.EmptyError).MaximumLength(14)
                .WithMessage(Messages.MaxLengthError(14));
            RuleFor(p => p.Otp).Pin(4);
        }
    }
}