using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Commands.SendMessageCustomer
{
    public class SendMessageCustomerRequestModelValidator : AbstractValidator<SendMessageCustomerRequestModel>
    {
        public SendMessageCustomerRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Message).Required();
        }
    }
}