using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.SendMessageCustomer
{

    public class SendMessageCustomerRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
    }
}