using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Customers.Commands.SendMessageCustomer
{

    public class SendMessageCustomerRequestHandler : IRequestHandler<SendMessageCustomerRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public SendMessageCustomerRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(SendMessageCustomerRequestModel request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(new SendNotificationRequestModel
            {
                Message = request.Message,
                UserId = request.UserId,
                TransactionId = null,
            },cancellationToken);
        }
    }

}