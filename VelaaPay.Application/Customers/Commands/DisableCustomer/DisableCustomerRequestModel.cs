using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Customers.Commands.DisableCustomer
{

    public class DisableCustomerRequestModel : IRequest<ResponseViewModel>
    {
        public string CustomerId { get; set; }

    }
}