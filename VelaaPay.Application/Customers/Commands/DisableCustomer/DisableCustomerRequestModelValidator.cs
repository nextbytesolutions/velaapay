using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Customers.Commands.DisableCustomer
{
    public class DisableCustomerRequestModelValidator : AbstractValidator<DisableCustomerRequestModel>
    {
        public DisableCustomerRequestModelValidator()
        {
            RuleFor(p => p.CustomerId).Required();
        }
    }
}