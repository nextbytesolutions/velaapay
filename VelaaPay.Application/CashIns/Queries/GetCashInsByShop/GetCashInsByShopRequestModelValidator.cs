using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashIns.Queries.GetCashInsByShop
{
    public class GetCashInsByShopRequestModelValidator : AbstractValidator<GetCashInsByShopRequestModel>
    {
        public GetCashInsByShopRequestModelValidator()
        {
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}