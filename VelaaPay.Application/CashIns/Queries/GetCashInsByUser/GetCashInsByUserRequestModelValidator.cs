using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashIns.Queries.GetCashInsByUser
{
    public class GetCashInsByUserRequestModelValidator : AbstractValidator<GetCashInsByUserRequestModel>
    {
        public GetCashInsByUserRequestModelValidator()
        {
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);

        }
    }
}