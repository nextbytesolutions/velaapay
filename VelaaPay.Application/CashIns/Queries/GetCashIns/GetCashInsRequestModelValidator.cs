using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashIns.Queries.GetCashIns
{
    public class GetCashInsRequestModelValidator : AbstractValidator<GetCashInsRequestModel>
    {
        public GetCashInsRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}