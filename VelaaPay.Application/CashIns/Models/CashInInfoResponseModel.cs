namespace VelaaPay.Application.CashIns.Models
{
    public class CashInInfoResponseModel
    {
        public string Employee { get; set; }
        public string Shop { get; set; }
        public string CreatedDate { get; set; }
        public double Amount { get; set; }
        public string Customer { get; set; }
        public string TransactionId { get; set; }
    }
    
}