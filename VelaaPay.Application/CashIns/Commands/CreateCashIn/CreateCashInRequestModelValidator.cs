using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.CashIns.Commands.CreateCashIn
{
    public class CreateCashInRequestModelValidator : AbstractValidator<CreateCashInRequestModel>
    {
        public CreateCashInRequestModelValidator()
        {
            RuleFor(p => p.CustomerId).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.Otp).Pin();
        }
    }
}