using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.CashIns.Commands.CreateCashIn
{

    public class CreateCashInRequestModel : IRequest<ResponseViewModel>
    {
        public string CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }
        public string Otp { get; set; }
        public double Amount { get; set; }
    }
}