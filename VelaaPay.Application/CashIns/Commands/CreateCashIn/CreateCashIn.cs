using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.CashIns.Commands.CreateCashIn
{

    public class CreateCashInRequestHandler : IRequestHandler<CreateCashInRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateCashInRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(CreateCashInRequestModel request, CancellationToken cancellationToken)
        {
            var customer = _context.Users.GetBy(p => p.Id == request.CustomerId);
            if (customer == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.CustomerId);
            }
            
            var passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(customer,customer.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            if (customer.Otp != request.Otp || customer.OtpExpiry < DateTime.UtcNow)
            {
                throw new BadRequestException(Messages.OtpIncorrectOrExpired);
            }

            if (request.EmployeeId == 0)
            {
                var shopUser = _context.ShopUsers.GetBy(p => p.UserId == request.UserId);
                if (shopUser == null)
                {
                    throw new BadRequestException(Messages.NoShopWasFound);
                }
                request.EmployeeId = shopUser.Id;
            }
            var employee = _context.UserBalances.GetBy(p => p.UserId == request.UserId);
            if (employee == null)
            {
                throw new NotFoundException("User not found");
            }
            
            if (employee.TotalBalance < request.Amount)
            {
                throw new BadRequestException(Messages.InsufficientBalance);
            }
            Wallet wallet = new Wallet()
            {
                UserId = request.UserId,
                Amount = -request.Amount,
            };
            Wallet wallet2 = new Wallet()
            {
                UserId = request.CustomerId,
                Amount = request.Amount,
            };
            Transaction transaction = new Transaction()
            {
                ReceiverUserId = request.CustomerId,
                SenderUserId = request.UserId,
                Amount = request.Amount,
                LoadType = (int)LoadType.CashIn,
                Wallets = new List<Wallet>()
                {
                    wallet2,
                    wallet
                }
            };
            var cashIn = new CashInTrack()
            {
                Amount = request.Amount,
                CustomerId = request.CustomerId,
                EmployeeId = request.EmployeeId,
                Transaction = transaction,
            };
            customer.Otp = null;
            customer.OtpExpiry = null;
            _context.Users.Update(customer);
            _context.CashInTracks.Add(cashIn);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}