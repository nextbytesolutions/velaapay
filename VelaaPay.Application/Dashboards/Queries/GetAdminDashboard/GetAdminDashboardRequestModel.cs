using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Dashboards.Queries.GetAdminDashboard
{

    public class GetAdminDashboardRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}