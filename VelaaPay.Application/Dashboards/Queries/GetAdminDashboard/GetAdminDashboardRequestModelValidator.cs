using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Dashboards.Queries.GetAdminDashboard
{
    public class GetAdminDashboardRequestModelValidator : AbstractValidator<GetAdminDashboardRequestModel>
    {
        public GetAdminDashboardRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}