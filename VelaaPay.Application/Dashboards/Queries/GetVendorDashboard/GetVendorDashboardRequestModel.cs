using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Dashboards.Queries.GetVendorDashboard
{

    public class GetVendorDashboardRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}