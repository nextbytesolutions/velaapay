using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Dashboards.Queries.GetVendorDashboard
{
    public class GetVendorDashboardRequestModelValidator : AbstractValidator<GetVendorDashboardRequestModel>
    {
        public GetVendorDashboardRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}