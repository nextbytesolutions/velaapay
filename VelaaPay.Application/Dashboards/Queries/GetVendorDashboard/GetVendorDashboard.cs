using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Dashboards.Queries.GetVendorDashboard
{

    public class GetVendorDashboardRequestHandler : IRequestHandler<GetVendorDashboardRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetVendorDashboardRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetVendorDashboardRequestModel request, CancellationToken cancellationToken)
        {
            await Task.Delay(0);
            var balance = _context.UserBalances.GetBy(p => p.UserId == request.UserId).TotalBalance;
            return null;
        }
    }

}