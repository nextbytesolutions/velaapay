using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUserPassword
{

    public class UpdateShopUserPasswordRequestHandler : IRequestHandler<UpdateShopUserPasswordRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateShopUserPasswordRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateShopUserPasswordRequestModel request, CancellationToken cancellationToken)
        {
            var shopUser = _context.ShopUsers.GetBy(p => p.Id == request.EmployeeId && p.ShopId == request.ShopId, 
                p => p.Include(pr => pr.User));
            if (shopUser == null)
            {
                throw new NotFoundException("Employee Not found");   
            }
            shopUser.User.PasswordHash = new PasswordHasher<User>().HashPassword(shopUser.User,request.Password);
            _context.Update(shopUser);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}