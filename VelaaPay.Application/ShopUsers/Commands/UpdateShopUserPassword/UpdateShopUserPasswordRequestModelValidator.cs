using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUserPassword
{
    public class UpdateShopUserPasswordRequestModelValidator : AbstractValidator<UpdateShopUserPasswordRequestModel>
    {
        public UpdateShopUserPasswordRequestModelValidator()
        {
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.EmployeeId).Required();
            RuleFor(p => p.Password).Password();
            RuleFor(p => p.ConfirmPassword).Matches(p => p.Password).WithMessage("does not match password");
        }
    }
}