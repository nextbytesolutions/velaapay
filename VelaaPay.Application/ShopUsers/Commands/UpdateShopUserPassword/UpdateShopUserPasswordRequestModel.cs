using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUserPassword
{

    public class UpdateShopUserPasswordRequestModel : IRequest<ResponseViewModel>
    {
        public int ShopId { get; set; }
        public int EmployeeId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }
}