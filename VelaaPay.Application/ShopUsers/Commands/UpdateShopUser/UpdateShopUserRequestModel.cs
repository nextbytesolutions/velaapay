using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUser
{

    public class UpdateShopUserRequestModel : IRequest<ResponseViewModel>
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Image { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ShopId { get; set; }
    }
}