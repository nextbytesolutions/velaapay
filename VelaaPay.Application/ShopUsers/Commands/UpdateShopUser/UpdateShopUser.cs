using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUser
{

    public class UpdateShopUserRequestHandler : IRequestHandler<UpdateShopUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public UpdateShopUserRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateShopUserRequestModel request, CancellationToken cancellationToken)
        {
            var employee = _context.ShopUsers.GetBy(p => p.Id == request.EmployeeId && p.ShopId == request.ShopId,
                p => p.Include(pr => pr.User));
            if (employee == null)
            {
                throw new NotFoundException("Employee", request.EmployeeId);
            }

            if (employee.User.PhoneNumber != request.PhoneNumber)
            {
                if (_context.Users.Any(p => p.PhoneNumber.Equals(request.PhoneNumber)))
                {
                    throw new AlreadyExistsException("Phone Number");
                }
                employee.User.PhoneNumber = request.PhoneNumber;
            }
            request.Copy(employee);
            employee.User.FullName = request.Name;
            if (employee.User.Image != request.Image)
            {
                employee.User.Image = _imageService.SaveImage(request.Image);
                request.Image = employee.User.Image;
            }
            _context.ShopUsers.Update(employee);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(request);
        }
    }

}