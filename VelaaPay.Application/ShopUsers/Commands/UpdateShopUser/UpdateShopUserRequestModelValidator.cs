using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.ShopUsers.Commands.UpdateShopUser
{
    public class UpdateShopUserRequestModelValidator : AbstractValidator<UpdateShopUserRequestModel>
    {
        public UpdateShopUserRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.StartTime).Time();
            RuleFor(p => p.EndTime).Time();
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.ShopId).Min(1);
            RuleFor(p => p.Image).Required();
        }
    }
}