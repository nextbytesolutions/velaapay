using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.ShopUsers.Commands.CreateShopUser
{
    public class CreateShopUserRequestModelValidator : AbstractValidator<CreateShopUserRequestModel>
    {
        public CreateShopUserRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.Password).Pin();
            RuleFor(p => p.StartTime).Time();
            RuleFor(p => p.EndTime).Time();
            RuleFor(p => p.Role).MustBeOneOf(new[] {RoleNames.Supervisor, RoleNames.Cashier});
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.ShopId).Min(1);
            RuleFor(p => p.UserRole).Required();
        }
    }
}