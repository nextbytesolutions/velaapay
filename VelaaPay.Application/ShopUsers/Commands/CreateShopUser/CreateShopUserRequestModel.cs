using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.ShopUsers.Commands.CreateShopUser
{

    public class CreateShopUserRequestModel : IRequest<ResponseViewModel>
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Image { get; set; }
        public string Password { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Role { get; set; }
        public int ShopId { get; set; }
        public string UserId { get; set; }
        public string UserRole { get; set; }
    }
}