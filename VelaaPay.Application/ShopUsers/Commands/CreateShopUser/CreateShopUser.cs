using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.ShopUsers.Commands.CreateShopUser
{

    public class CreateShopUserRequestHandler : IRequestHandler<CreateShopUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;

        public CreateShopUserRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(CreateShopUserRequestModel request, CancellationToken cancellationToken)
        {
            if (request.UserRole == RoleNames.Vendor)
            {
                if (!_context.Shops.Any(p => p.UserId == request.UserId && p.Id == request.ShopId))
                {
                    throw new NotFoundException(nameof(Shop), request.ShopId);
                }
            }
            else if (request.UserRole == RoleNames.Supervisor)
            {
                if (!_context.Shops.Any(p =>
                    p.Id == request.ShopId && p.ShopUsers.Any(pr => pr.UserId == request.UserId)))
                {
                    throw new NotFoundException(nameof(Shop), request.ShopId);
                }
            }
            if (request.Role == RoleNames.Supervisor)
            {
                if (_context.ShopUsers.Any(p =>
                    p.ShopId == request.ShopId && p.EmployeeLevel == GetEmployeeLevel(request.Role)))
                {
                    throw new AlreadyExistsException(RoleNames.Supervisor);
                }
            }

            var roleId = _context.Roles.Where(p => p.Name == request.Role).Select(p => p.Id).FirstOrDefault();
            if (_context.Users.Any(p => p.Roles.Any(pr => pr.RoleId == roleId) && p.PhoneNumber.Equals(request.PhoneNumber)))
            {
                throw new AlreadyExistsException(nameof(request.PhoneNumber));
            }

            var shopUser = new ShopUser()
            {
                ShopId = request.ShopId,
                User = new User()
                {
                    Email = request.PhoneNumber + "@VelaaPay.com",
                    FullName = request.Name,
                    IsEnabled = true,
                    IsVerified = true,
                    PhoneNumber = request.PhoneNumber,
                    UserBalances = new List<UserBalance>()
                    {
                        new UserBalance()
                        {
                            TotalBalance = 0
                        }
                    },
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                },
                EndTime = request.EndTime,
                StartTime = request.StartTime,
                EmployeeLevel = GetEmployeeLevel(request.Role),
            };
            shopUser.User.Roles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = roleId,
                    UserId = shopUser.User.Id
                }
            };
            shopUser.User.Image = string.IsNullOrEmpty(request.Image) ? Constant.DefaultImageUrl : _imageService.SaveImage(request.Image);
            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            shopUser.User.PasswordHash = passwordHasher.HashPassword(shopUser.User, request.Password);
            _context.ShopUsers.Add(shopUser);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }

        private int GetEmployeeLevel(string role)
        {
            if (role == RoleNames.Cashier)
            {
                return EmployeeLevel.Cashier;
            }
            if (role == RoleNames.Supervisor)
            {
                return EmployeeLevel.Supervisor;
            }
            throw new Exception("Incorrect Role");
        }
    }
}
