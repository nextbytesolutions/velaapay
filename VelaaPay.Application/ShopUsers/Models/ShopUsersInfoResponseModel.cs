namespace VelaaPay.Application.ShopUsers.Models
{
    public class ShopUsersInfoResponseModel
    {
        public string ShopName { get; set; }
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeLevel { get; set; }
        public string PhoneNumber { get; set; }
        public string UserId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string CreatedDate { get; set; }
        public string Image { get; set; }
        public int ShopId { get; set; }
        public bool IsEnabled { get; set; }
    }
}