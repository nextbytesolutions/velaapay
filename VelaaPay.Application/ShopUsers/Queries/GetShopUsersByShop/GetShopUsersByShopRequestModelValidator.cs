using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.ShopUsers.Queries.GetShopUsersByShop
{
    public class GetShopUsersByShopRequestModelValidator : AbstractValidator<GetShopUsersByShopRequestModel>
    {
        public GetShopUsersByShopRequestModelValidator()
        {
            RuleFor(p => p.Page).Required();
            RuleFor(p => p.ShopId).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.PageSize).Required().Max(20);
        }
    }
}