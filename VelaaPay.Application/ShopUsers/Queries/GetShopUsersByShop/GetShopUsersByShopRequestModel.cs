using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.ShopUsers.Queries.GetShopUsersByShop
{

    public class GetShopUsersByShopRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }
            if (OrderBy == "shopName")
            {
                OrderBy = "Shop.Name";
            }
            if (OrderBy == "employeeName")
            {
                OrderBy = "User.FullName";
            }
            if (OrderBy == "phoneNumber")
            {
                OrderBy = "User.PhoneNumber";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public string UserId { get; set; }
        public int ShopId { get; set; }
    }
}