using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.ShopUsers.Models;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.ShopUsers.Queries.GetShopUsers
{

    public class GetShopUsersRequestHandler : IRequestHandler<GetShopUsersRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetShopUsersRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetShopUsersRequestModel request, CancellationToken cancellationToken)
        {
            var converter = new DateConverter();
            request.SetDefaultValue();
            List<ShopUsersInfoResponseModel> list = _context.ShopUsers
                .GetManyReadOnly(
                    p => p.Shop.UserId == request.UserId && p.EmployeeLevel > EmployeeLevel.Vendor &&
                         (p.User.FullName.Contains(request.Search) || p.User.PhoneNumber.Contains(request.Search) ||
                          p.Shop.Name.Contains(request.Search)), request.OrderBy, request.Page,
                    request.PageSize,
                    request.IsDescending, p => p
                        .Include(pr => pr.User)
                        .Include(pr => pr.Shop))
                .Select(p => new ShopUsersInfoResponseModel
                {
                    PhoneNumber = p.User.PhoneNumber,
                    UserId = p.UserId,
                    EmployeeId = p.Id,
                    EmployeeLevel = p.EmployeeLevel,
                    ShopName = p.Shop.Name,
                    EmployeeName = p.User.FullName,
                    EndTime = p.EndTime,
                    StartTime = p.StartTime,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Image = p.User.Image,
                    ShopId = p.ShopId,
                    IsEnabled = p.User.IsEnabled
                }).ToList();
            var count = _context.ShopUsers
                .Count(p => p.Shop.UserId == request.UserId &&
                            p.EmployeeLevel > EmployeeLevel.Vendor &&
                            (p.User.FullName.Contains(request.Search) || 
                             p.User.PhoneNumber.Contains(request.Search) ||
                             p.Shop.Name.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}