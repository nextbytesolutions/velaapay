using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.ShopUsers.Queries.GetShopUsers
{
    public class GetShopUsersRequestModelValidator : AbstractValidator<GetShopUsersRequestModel>
    {
        public GetShopUsersRequestModelValidator()
        {
            RuleFor(p => p.Page).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.PageSize).Required().Max(20);
        }
    }
}