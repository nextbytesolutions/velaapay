using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.SetPasswordMerchant
{
    public class SetPasswordMerchantRequestModelValidator : AbstractValidator<SetPasswordMerchantRequestModel>
    {
        public SetPasswordMerchantRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.Token).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}