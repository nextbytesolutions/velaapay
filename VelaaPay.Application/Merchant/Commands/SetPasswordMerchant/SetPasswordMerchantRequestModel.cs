using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.SetPasswordMerchant
{

    public class SetPasswordMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string Pin { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }

    }
}