using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.SetPasswordMerchant
{

    public class SetPasswordMerchantRequestHandler : IRequestHandler<SetPasswordMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;
        public SetPasswordMerchantRequestHandler(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<ResponseViewModel> Handle(SetPasswordMerchantRequestModel request,
            CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.UserId);
            if (user == null)
            {
                          throw new NotFoundException(RoleNames.Vendor,request.UserId);
  }
            var result = await _userManager.ResetPasswordAsync(user, request.Token, request.Pin);
            if (result.Succeeded)
            {
                return new ResponseViewModel();
            }
            throw new ValidationException(result.Errors.ToList());
        }
    }

}