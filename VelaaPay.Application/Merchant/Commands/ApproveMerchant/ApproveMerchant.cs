using FluentValidation;
using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.ApproveMerchant
{

    public class ApproveMerchantRequestModel : IRequest<ResponseViewModel>
    {

    }

    public class ApproveMerchantRequestModelValidator : AbstractValidator<ApproveMerchantRequestModel>
    {

    }
}