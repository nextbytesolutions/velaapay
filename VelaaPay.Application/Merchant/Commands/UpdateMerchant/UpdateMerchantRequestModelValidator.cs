using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.UpdateMerchant
{
    public class UpdateMerchantRequestModelValidator : AbstractValidator<UpdateMerchantRequestModel>
    {
        public UpdateMerchantRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Rccm).Required();
            RuleFor(p => p.Identification).Required();
            RuleFor(p => p.IdentificationImage).Required();
            RuleFor(p => p.RccmImage).Required();
            RuleFor(p => p.NtnNumber).Required();
        }
    }
}