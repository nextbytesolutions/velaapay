using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.UpdateMerchant
{

    public class UpdateMerchantRequestHandler : IRequestHandler<UpdateMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public UpdateMerchantRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<ResponseViewModel> Handle(UpdateMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var merchantRoleId = _context.Roles.GetBy(p => p.Name == RoleNames.Vendor).Id;
            if (string.IsNullOrEmpty(merchantRoleId))
            {
                throw new NotFoundException(nameof(Role),"");
            }
            var merchant = _context.Users.GetBy(p => p.Id == request.UserId);
            if (merchant == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }

            if (merchant.NtnNumber != request.NtnNumber)
            {
                if (_context.Users.Any(p => p.Roles.Any(pr => pr.RoleId == merchantRoleId) && p.NtnNumber == request.NtnNumber))
                {
                    throw new AlreadyExistsException("Ntn Number");
                }
                merchant.NtnNumber = request.NtnNumber;
            }
            if (merchant.Rccm != request.Rccm)
            {
                if (_context.Users.Any(p => p.Rccm == request.Rccm))
                {
                    throw new AlreadyExistsException("Rccm");
                }
                merchant.Rccm = request.Rccm;
            }
            if (merchant.Identification != request.Identification)
            {
                if (_context.Users.Any(p => p.Roles.Any(pr => pr.RoleId == merchantRoleId) && p.Identification == request.Identification))
                {
                    throw new AlreadyExistsException("Identification");
                }
                merchant.Identification = request.Identification;
            }
            if (merchant.IdentificationImageUrl != request.IdentificationImage)
            {
                if (!string.IsNullOrEmpty(merchant.IdentificationImageUrl))
                {
                    _imageService.DeleteImage(merchant.IdentificationImageUrl);
                }
                merchant.IdentificationImageUrl = _imageService.SaveImage(request.IdentificationImage);
            }
            if (merchant.RccmImage != request.RccmImage)
            {
                if (!string.IsNullOrEmpty(merchant.RccmImage))
                {
                    _imageService.DeleteImage(merchant.RccmImage);
                }
                merchant.RccmImage = _imageService.SaveImage(request.RccmImage);
            }
            _context.Update(merchant);
            await _context.SaveChangesAsync(cancellationToken);
            request.IdentificationImage = merchant.IdentificationImageUrl;
            request.RccmImage = merchant.RccmImage;
            return new ResponseViewModel().CreateOk(request);
        }
    }

}