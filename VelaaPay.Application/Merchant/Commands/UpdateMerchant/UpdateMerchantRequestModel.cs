using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.UpdateMerchant
{

    public class UpdateMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Rccm { get; set; }
        public string NtnNumber { get; set; }
        public string RccmImage { get; set; }
        public string Identification { get; set; }
        public string IdentificationImage { get; set; }
    }
}