using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.CreateMerchant
{

    public class CreateMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string BusinessType { get; set; }
        public string Host { get; set; }
        public string Email { get; set; }
    }
}