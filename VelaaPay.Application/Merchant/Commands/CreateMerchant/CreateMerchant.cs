using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.CreateMerchant
{

    public class CreateMerchantRequestHandler : IRequestHandler<CreateMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public CreateMerchantRequestHandler(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<ResponseViewModel> Handle(CreateMerchantRequestModel request, CancellationToken cancellationToken)
        {
            request.Email = $"{request.PhoneNumber}@VelaaPay.com";
            var merchantId = _context.Roles.GetBy(p => p.Name == RoleNames.Vendor).Id;
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new NotFoundException(nameof(Role),"");
            }

            if (_context.Users.Any(p => p.Roles.Any(pr => pr.RoleId == merchantId) && (
                                            p.Email == request.Email || p.PhoneNumber == request.PhoneNumber)))
            {
                throw new AlreadyExistsException(Messages.PhoneNumber);
            }

            string imageUrl = Constant.DefaultImageUrl;
            var vendor = new User
            {
                SecurityStamp = Guid.NewGuid().ToString("D"),
                UserName = request.PhoneNumber + "@" + request.Name.Replace(" ","").ToLower(),
                FullName = request.Name,
                PhoneNumber = request.PhoneNumber,
                Image = imageUrl,
                Email = request.Email,
                IsVerified = false,
                IsEnabled = true,
                BusinessType = request.BusinessType,
                UserBalances = new List<UserBalance>()
                {
                    new UserBalance()
                    {
                        TotalBalance = 0
                    }
                },
            };
            vendor.PasswordHash = new PasswordHasher<User>().HashPassword(vendor,PinGenerator.CreatePin());
            vendor.Roles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = merchantId,
                    UserId = vendor.Id
                }
            };
            _context.Users.Add(vendor);
            await _context.SaveChangesAsync(cancellationToken);
            var token = _userManager.GeneratePasswordResetTokenAsync(vendor).Result;
            return (new ResponseViewModel().CreateOk(new TokenReturnModel()
            {
                Token = token,
                UserId = vendor.Id
            }));
        }

        class TokenReturnModel
        {
            public string Token { get; set; }
            public string UserId { get; set; }
        }
    }

}