using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.CreateMerchant
{
    public class CreateMerchantRequestModelValidator : AbstractValidator<CreateMerchantRequestModel>
    {
        public CreateMerchantRequestModelValidator()
        {
            RuleFor(p => p.Name).Required();
            RuleFor(p => p.PhoneNumber).Phone();
        }
    }
}