using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.VerifyMerchant
{

    public class VerifyMerchantRequestHandler : IRequestHandler<VerifyMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public VerifyMerchantRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(VerifyMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var merchant = _context.Users.GetBy(p => p.Id == request.UserId);
            if (merchant == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }

            merchant.IsVerified = true;
            _context.Update(merchant);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}