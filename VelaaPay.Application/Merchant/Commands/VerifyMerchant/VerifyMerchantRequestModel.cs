using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.VerifyMerchant
{

    public class VerifyMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}