using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.VerifyMerchant
{
    public class VerifyMerchantRequestModelValidator : AbstractValidator<VerifyMerchantRequestModel>
    {
        public VerifyMerchantRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}