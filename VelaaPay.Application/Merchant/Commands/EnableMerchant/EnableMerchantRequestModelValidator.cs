using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.EnableMerchant
{
    public class EnableMerchantRequestModelValidator : AbstractValidator<EnableMerchantRequestModel>
    {
        public EnableMerchantRequestModelValidator()
        {
            RuleFor(p => p.MerchantId).Required();
        }
    }
}