using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.EnableMerchant
{

    public class EnableMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string MerchantId { get; set; }

    }
}