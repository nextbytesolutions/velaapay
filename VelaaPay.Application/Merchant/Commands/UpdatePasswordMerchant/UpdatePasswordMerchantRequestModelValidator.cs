using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.UpdatePasswordMerchant
{
    public class UpdatePasswordMerchantRequestModelValidator : AbstractValidator<UpdatePasswordMerchantRequestModel>
    {
        public UpdatePasswordMerchantRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.ConfirmPin).Pin();
            RuleFor(p => p.UserId).Required();
        }
    }
}