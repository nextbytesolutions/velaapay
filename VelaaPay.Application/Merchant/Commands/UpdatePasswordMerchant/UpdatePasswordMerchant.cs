using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.UpdatePasswordMerchant
{

    public class UpdatePasswordMerchantRequestHandler : IRequestHandler<UpdatePasswordMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdatePasswordMerchantRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdatePasswordMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var merchant = _context.Users.GetBy(p => p.Id == request.UserId);
            if (merchant == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }

            merchant.PasswordHash = new PasswordHasher<User>().HashPassword(merchant, request.Pin);
            _context.Update(merchant);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}