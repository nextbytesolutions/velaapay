using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.UpdatePasswordMerchant
{

    public class UpdatePasswordMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public string Pin { get; set; }
        public string ConfirmPin { get; set; }
    }
}