using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.VerifyOtpMerchant
{

    public class VerifyOtpMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
        public string Role { get; set; }
    }
}