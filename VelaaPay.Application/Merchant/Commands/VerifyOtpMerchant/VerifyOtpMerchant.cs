using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.VerifyOtpMerchant
{

    public class VerifyOtpMerchantRequestHandler : IRequestHandler<VerifyOtpMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public VerifyOtpMerchantRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(VerifyOtpMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var phoneVerify = _context.PhoneVerifies.GetBy(p => p.PhoneNumber == request.PhoneNumber && !p.IsVerified && p.Role == request.Role);
            if (phoneVerify == null)
            {
                throw new NotFoundException(Messages.PhoneNumber, request.PhoneNumber);
            }

            if (!(phoneVerify.Code == request.Code && phoneVerify.ExpiryDate >= DateTime.UtcNow))
            {
                throw new BadRequestException(Messages.OtpIncorrectOrExpired);
            }

            phoneVerify.IsVerified = true;
            _context.Update(phoneVerify);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}