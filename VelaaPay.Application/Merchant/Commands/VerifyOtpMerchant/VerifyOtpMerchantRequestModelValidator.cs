using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.VerifyOtpMerchant
{
    public class VerifyOtpMerchantRequestModelValidator : AbstractValidator<VerifyOtpMerchantRequestModel>
    {
        public VerifyOtpMerchantRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Role).Required();
            RuleFor(p => p.Code).Pin();
        }
    }
}