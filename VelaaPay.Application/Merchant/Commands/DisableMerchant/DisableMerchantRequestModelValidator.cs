using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.DisableMerchant
{
    public class DisableMerchantRequestModelValidator : AbstractValidator<DisableMerchantRequestModel>
    {
        public DisableMerchantRequestModelValidator()
        {
            RuleFor(p => p.MerchantId).Required();
        }
    }
}