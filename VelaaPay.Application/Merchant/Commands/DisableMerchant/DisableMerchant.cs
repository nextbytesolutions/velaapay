using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.DisableMerchant
{

    public class DisableMerchantRequestHandler : IRequestHandler<DisableMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public DisableMerchantRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(DisableMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var merchantId = _context.Roles.Where(p => p.Name == RoleNames.Vendor).Select(p => p.Id).FirstOrDefault();
            var user = _context.Users.GetBy(p => p.Roles.Any(pr => pr.RoleId == merchantId) && p.Id == request.MerchantId && p.IsEnabled);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Vendor, request.MerchantId);                
            }

            user.IsEnabled = false;
            _context.Users.Update(user);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel();
        }
    }

}