using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.DisableMerchant
{

    public class DisableMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string MerchantId { get; set; }
    }
}