using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Commands.SendOtpMerchant
{

    public class SendOtpMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
    }
}