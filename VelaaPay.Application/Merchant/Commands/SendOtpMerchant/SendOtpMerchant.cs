using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Commands.SendOtpMerchant
{

    public class SendOtpMerchantRequestHandler : IRequestHandler<SendOtpMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly ISmsService _smsService;
        public SendOtpMerchantRequestHandler(ApplicationDbContext context, ISmsService smsService)
        {
            _context = context;
            _smsService = smsService;
        }

        public async Task<ResponseViewModel> Handle(SendOtpMerchantRequestModel request,
            CancellationToken cancellationToken)
        {
            var merchantId = _context.Roles.GetBy(p => p.Name == RoleNames.Vendor)?.Id;
            if (string.IsNullOrEmpty(merchantId))
            {
                throw new NotFoundException(nameof(Role),"");
            }
            var isExist = _context.Users.Any(p => p.Roles.Any(pr => pr.RoleId == merchantId) && p.PhoneNumber == request.PhoneNumber);
            if (isExist)
            {
                throw new AlreadyExistsException(Messages.PhoneNumber);
            }

            var phone = _context.PhoneVerifies.GetBy(p => p.PhoneNumber == request.PhoneNumber && p.Role == request.Role);
            if (phone == null)
            {
             
                phone = new PhoneVerify()
                {
                    IsVerified = false,
                    Code = PinGenerator.CreatePin(),
                    ExpiryDate = DateTime.UtcNow.Add(TimeSpan.FromMinutes(5)),
                    PhoneNumber = request.PhoneNumber,
                    Role = RoleNames.Vendor,
                };   
                _context.PhoneVerifies.Add(phone);
            }
            else
            {
                phone.IsVerified = false;
                phone.Code = PinGenerator.CreatePin();
                phone.ExpiryDate = DateTime.UtcNow.Add(TimeSpan.FromMinutes(5));
                _context.PhoneVerifies.Update(phone);
            }

            await _context.SaveChangesAsync(cancellationToken);
            await _smsService.SendMessage(phone.PhoneNumber, Messages.OtMessage(phone.Code));
            return new ResponseViewModel();
        }
    }

}