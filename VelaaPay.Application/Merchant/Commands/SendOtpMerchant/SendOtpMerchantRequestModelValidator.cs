using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Commands.SendOtpMerchant
{
    public class SendOtpMerchantRequestModelValidator : AbstractValidator<SendOtpMerchantRequestModel>
    {
        public SendOtpMerchantRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Role).Required();
        }
    }
}