using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Queries.GetDetailMerchant
{
    public class GetMerchantDetailRequestModelValidator : AbstractValidator<GetMerchantDetailRequestModel>
    {
        public GetMerchantDetailRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}