using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Queries.GetDetailMerchant
{

    public class GetMerchantDetailRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}