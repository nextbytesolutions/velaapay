using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Queries.GetDetailMerchant
{

    public class GetMerchantDetailRequestHandler : IRequestHandler<GetMerchantDetailRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetMerchantDetailRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetMerchantDetailRequestModel request, CancellationToken cancellationToken)
        {
            User userObj = _context.Users.GetBy(p => p.Id == request.UserId, p => p.Include(pr => pr.UserBalances));
            if (userObj == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }
            List<TransactionWebInfoViewModel> recentActivities = _context.Transactions.Where(p => p.SenderUserId == request.UserId || p.ReceiverUserId == request.UserId)
                .OrderByDescending(p => p.CreatedDate).Select(p => new TransactionWebInfoViewModel()
            {
                Amount = p.Amount,
                TransactionId = p.Id,
                LoadType = p.LoadType,
                Date = new DateConverter().ConvertToDateTime(p.CreatedDate),
                ReceiverName = p.ReceiverUser != null ? p.ReceiverUser.FullName : "",
                SenderName = p.SenderUser != null ? p.SenderUser.FullName : "",
            }).ToList();
            double wallet = userObj.UserBalances.First().TotalBalance;
            MerchantDetailsWebInfoViewModel data = new MerchantDetailsWebInfoViewModel
            {
                Email = userObj.Email,
                FullName = userObj.FullName,
                Phone = userObj.PhoneNumber,
                Address = userObj.Address,
                Transactions = recentActivities,
                Wallet = wallet,
                IsEnabled = userObj.IsEnabled,
                UserName = userObj.UserName,
                Image = userObj.Image,
                Date = new DateConverter().ConvertToDateTime(userObj.CreatedDate)
            };
            return Task.FromResult(new ResponseViewModel().CreateOk(data));
        }

        private class TransactionWebInfoViewModel
        {
            public string TransactionId { get; set; }
            public int LoadType { get; set; }
            public string SenderName { get; set; }
            public string ReceiverName { get; set; }
            public double Amount { get; set; }
            public string Date { get; set; }
        }

        private class MerchantDetailsWebInfoViewModel
        {
            public string UserName { get; set; }
            public string FullName { get; set; }
            public string Address { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public bool IsEnabled { get; set; }
            public List<TransactionWebInfoViewModel> Transactions { get; set; }
            public double Wallet { get; set; }
            public string Date { get; set; }
            public string Image { get; set; }
        }
    }

}