using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Merchant.Queries.GetMerchants
{
    public class GetMerchantsRequestModelValidator : AbstractValidator<GetMerchantsRequestModel>
    {
        public GetMerchantsRequestModelValidator()
        {
            RuleFor(p => p.Page).NotEmpty().WithMessage(Messages.EmptyError);
            RuleFor(p => p.PageSize).NotEmpty().WithMessage(Messages.EmptyError).Max(20);
        }
    }
}