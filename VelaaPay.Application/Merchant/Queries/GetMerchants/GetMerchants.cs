using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Merchant.Models;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Queries.GetMerchants
{

    public class GetMerchantsRequestHandler : IRequestHandler<GetMerchantsRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetMerchantsRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetMerchantsRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();
            var customerId = _context.Roles.Where(p => p.Name.Equals(RoleNames.Vendor)).Select(p => p.Id).First();
            List<MerchantInfoResponseModel> list = _context.Users
                .GetManyReadOnly(p => p.Roles.Any(pr => pr.RoleId == customerId) &&
                                      (p.Email == request.Search || p.PhoneNumber == request.Search ||
                                       p.Id == request.Search ||
                                       p.FullName.Contains(request.Search)), request.OrderBy, request.Page,
                    request.PageSize,
                    request.IsDescending, p => p.Include(pr => pr.UserBalances))
                .Select(p => new MerchantInfoResponseModel
                {
                    Id = p.Id,
                    FullName = p.FullName,
                    Email = p.Email,
                    TotalBalance = p.UserBalances.First().TotalBalance,
                    PhoneNumber = p.PhoneNumber,
                    IsEnabled = p.IsEnabled,
                    IsVerified = p.IsVerified,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                }).ToList();
            var count = _context.Users.Count(p =>
                p.Roles.Any(pr => pr.RoleId == customerId) &&
                (p.Email == request.Search || p.PhoneNumber == request.Search || p.Id == request.Search ||
                 p.FullName.Contains(request.Search)));

            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));

        }
    }

}