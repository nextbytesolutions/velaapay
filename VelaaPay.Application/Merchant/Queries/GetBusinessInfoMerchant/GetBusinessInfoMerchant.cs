using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Merchant.Queries.GetBusinessInfoMerchant
{

    public class GetBusinessInfoMerchantRequestHandler : IRequestHandler<GetBusinessInfoMerchantRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetBusinessInfoMerchantRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetBusinessInfoMerchantRequestModel request, CancellationToken cancellationToken)
        {
            var merchant = _context.Users.GetBy(p => p.Id == request.UserId);
            if (merchant == null)
            {
                throw new NotFoundException(RoleNames.Vendor,request.UserId);
            }
            return Task.FromResult<ResponseViewModel>(new ResponseViewModel().CreateOk(new BusinessInfoModel()
            {
                Rccm = merchant.Rccm,
                Identification = merchant.Identification,
                NtnNumber = merchant.NtnNumber,
                RccmImage = merchant.RccmImage,
                IdentificationImage = merchant.IdentificationImageUrl
            }));
        }

        class BusinessInfoModel
        {
            public string Identification { get; set; }
            public string IdentificationImage { get; set; }
            public string Rccm { get; set; }
            public string RccmImage { get; set; }
            public string NtnNumber { get; set; }
            
        }
    }

}