using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Merchant.Queries.GetBusinessInfoMerchant
{

    public class GetBusinessInfoMerchantRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
    }
}