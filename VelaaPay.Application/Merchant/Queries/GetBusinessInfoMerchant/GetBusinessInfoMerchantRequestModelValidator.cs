using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Merchant.Queries.GetBusinessInfoMerchant
{
    public class GetBusinessInfoMerchantRequestModelValidator : AbstractValidator<GetBusinessInfoMerchantRequestModel>
    {
        public GetBusinessInfoMerchantRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }
}