namespace VelaaPay.Application.Merchant.Models
{
    public class MerchantInfoResponseModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Id { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
        public double TotalBalance { get; set; }
        public string CreatedDate { get; set; }
    }
}