using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.AirTimeTopUp.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.AirTimeTopUp.Queries.GetAirTimeTopUpByUser
{

    public class GetAirTimeTopUpByUserRequestHandler : IRequestHandler<GetAirTimeTopUpByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetAirTimeTopUpByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetAirTimeTopUpByUserRequestModel request, CancellationToken cancellationToken)
        {
            var converter = new DateConverter();
            request.SetDefaultValue();
            List<AirTimeTopUpInfoResponseModel> list = _context.AirTopUpRequests.GetMany(
                    p => p.UserId == request.UserId &&
                         (p.Company.Name.Contains(request.Search) || p.MobileNo.Contains(request.Search)),
                    request.OrderBy,
                    request.Page, request.PageSize, request.IsDescending,
                    p => p.Include(pr => pr.User).Include(pr => pr.Company))
                .Select(p => new AirTimeTopUpInfoResponseModel
                {
                    Company = p.Company.Name,
                    Name = p.User.FullName,
                    Status = p.Status,
                    MobileNo = p.MobileNo,
                    RequestId = p.Id,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Amount = p.Amount,
                    Remarks = p.Remarks
                }).ToList();
            var count = _context.AirTopUpRequests.Count(p =>
                p.UserId == request.UserId &&
                (p.Company.Name.Contains(request.Search) || p.MobileNo.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}