using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.AirTimeTopUp.Queries.GetAirTimeTopUpByUser
{
    public class GetAirTimeTopUpByUserRequestModelValidator : AbstractValidator<GetAirTimeTopUpByUserRequestModel>
    {
        public GetAirTimeTopUpByUserRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
            RuleFor(p => p.UserId).Required();

        }
    }
}