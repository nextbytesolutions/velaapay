using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.AirTimeTopUp.Queries.GetAirTimeTopUp
{
    public class GetAirTimeTopUpRequestModelValidator : AbstractValidator<GetAirTimeTopUpRequestModel>
    {
        public GetAirTimeTopUpRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}