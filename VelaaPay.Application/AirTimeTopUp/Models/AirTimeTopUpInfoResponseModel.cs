namespace VelaaPay.Application.AirTimeTopUp.Models
{
    public class AirTimeTopUpInfoResponseModel
    {
        public int Status { get; set; }
        public string CreatedDate { get; set; }
        public string MobileNo { get; set; }
        public string Company { get; set; }
        public string Remarks { get; set; }
        public double Amount { get; set; }
        public string Name { get; set; }
        public int RequestId { get; set; }
    }
}