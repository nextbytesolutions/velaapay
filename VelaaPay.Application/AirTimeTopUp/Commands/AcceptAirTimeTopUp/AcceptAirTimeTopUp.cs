using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.AirTimeTopUp.Commands.AcceptAirTimeTopUp
{

    public class AcceptAirTimeTopUpRequestHandler : IRequestHandler<AcceptAirTimeTopUpRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public AcceptAirTimeTopUpRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(AcceptAirTimeTopUpRequestModel request, CancellationToken cancellationToken)
        {
            var airTime = _context.AirTopUpRequests.Where(p => p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt()).Include(p => p.Transaction).Include(p => p.Company).Include(p => p.User).FirstOrDefault();
            if (airTime == null)
            {
                throw new NotFoundException(nameof(WithdrawRequest),request.RequestId);
            }
            var transaction = airTime.Transaction;
            airTime.Status = RequestStatus.Accepted.ToInt();
            _context.AirTopUpRequests.Update(airTime);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your air time request to Mobile No " + airTime.MobileNo + " is approved",
                UserId = airTime.UserId,
                TransactionId = airTime.TransactionId,
            },cancellationToken);
            return new ResponseViewModel().CreateOk(new SharedRequestResponse()
            {
                Amount = transaction.Amount,
                TransactionId = transaction.Id,
                To = airTime.User.FullName,
                From = airTime.Company.Name,
                Date = transaction.CreatedDate,
                LoadType = transaction.LoadType
            });
        }
    }

}