using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.AirTimeTopUp.Commands.AcceptAirTimeTopUp
{

    public class AcceptAirTimeTopUpRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
        public string UserId { get; set; }
    }
}