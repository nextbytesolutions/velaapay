using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.AirTimeTopUp.Commands.AcceptAirTimeTopUp
{
    public class AcceptAirTimeTopUpRequestModelValidator : AbstractValidator<AcceptAirTimeTopUpRequestModel>
    {
        public AcceptAirTimeTopUpRequestModelValidator()
        {
            RuleFor(p => p.RequestId).Required();
            RuleFor(p => p.UserId).Required();
        }
    }
}