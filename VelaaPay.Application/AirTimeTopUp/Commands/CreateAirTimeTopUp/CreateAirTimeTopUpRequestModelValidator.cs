using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.AirTimeTopUp.Commands.CreateAirTimeTopUp
{
    public class CreateAirTimeTopUpRequestModelValidator : AbstractValidator<CreateAirTimeTopUpRequestModel>
    {
        public CreateAirTimeTopUpRequestModelValidator()
        {
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.MobileNo).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.CompanyId).Min(1);
            RuleFor(p => p.Pin).Pin(4);
        }
    }
}