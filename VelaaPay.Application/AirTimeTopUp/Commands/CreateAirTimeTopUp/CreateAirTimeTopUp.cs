using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.AirTimeTopUp.Commands.CreateAirTimeTopUp
{

    public class CreateAirTimeTopUpRequestHandler : IRequestHandler<CreateAirTimeTopUpRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateAirTimeTopUpRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(CreateAirTimeTopUpRequestModel request,
            CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.IsEnabled && p.Id == request.UserId,
                p => p.Include(pr => pr.UserBalances));
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(user, user.PasswordHash, request.Pin) ==
                PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            if (user.UserBalances.First().TotalBalance < request.Amount)
            {
                throw new BadRequestException(Messages.InsufficientBalance);
            }

            var airTime = new AirTopUpRequest()
            {
                Amount = request.Amount,
                UserId = request.UserId,
                MobileNo = request.MobileNo,
                Status = RequestStatus.None.ToInt(),
                CompanyId = request.CompanyId,
            };
            var transaction = new Transaction()
            {
                Amount = request.Amount,
                SenderUserId = request.UserId,
                LoadType = LoadType.AirTimeTopUpRequest.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        Amount = -request.Amount,
                        UserId = request.UserId,
                    }
                }
            };
            airTime.Transaction = transaction;
            _context.AirTopUpRequests.Add(airTime);
            _context.SaveChanges();
            return Task.FromResult(new ResponseViewModel());
        }
    }

}