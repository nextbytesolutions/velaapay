using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.AirTimeTopUp.Commands.CreateAirTimeTopUp
{

    public class CreateAirTimeTopUpRequestModel : IRequest<ResponseViewModel>
    {
        public string MobileNo { get; set; }
        public int CompanyId { get; set; }
        public double Amount { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }
    }
}