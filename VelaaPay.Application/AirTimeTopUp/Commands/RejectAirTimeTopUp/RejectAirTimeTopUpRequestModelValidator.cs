using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.AirTimeTopUp.Commands.RejectAirTimeTopUp
{
    public class RejectAirTimeTopUpRequestModelValidator : AbstractValidator<RejectAirTimeTopUpRequestModel>
    {
        public RejectAirTimeTopUpRequestModelValidator()
        {
            RuleFor(p => p.RequestId).Min(1);
        }
    }
}