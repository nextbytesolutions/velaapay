using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.AirTimeTopUp.Commands.RejectAirTimeTopUp
{

    public class RejectAirTimeTopUpRequestHandler : IRequestHandler<RejectAirTimeTopUpRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public RejectAirTimeTopUpRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(RejectAirTimeTopUpRequestModel request, CancellationToken cancellationToken)
        {
            var airTime = _context.AirTopUpRequests.FirstOrDefault(p => p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt());
            if (airTime == null)
            {
                throw new NotFoundException(nameof(WithdrawRequest),request.RequestId);
            }
            airTime.Status = RequestStatus.Rejected.ToInt();
            var transaction = new Transaction()
            {
                Amount = airTime.Amount,
                ReceiverUserId = airTime.UserId,
                LoadType = LoadType.AirTimeTopUpRequestRejected.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = airTime.UserId,
                        Amount = airTime.Amount,
                    },
                }
            };
            _context.AirTopUpRequests.Update(airTime);
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your air time request to Phone No " + airTime.MobileNo + " has been rejected",
                UserId = airTime.UserId,
                TransactionId = airTime.TransactionId,
            },cancellationToken);
            return new ResponseViewModel();
        }
    }

}