using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.AirTimeTopUp.Commands.RejectAirTimeTopUp
{

    public class RejectAirTimeTopUpRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
    }
}