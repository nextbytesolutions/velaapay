using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Payment.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Queries.GetRequestMoney
{

    public class GetRequestMoneyRequestHandler : IRequestHandler<GetRequestMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetRequestMoneyRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetRequestMoneyRequestModel request,
            CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var converter = new DateConverter();
            if (string.IsNullOrEmpty(request.UserId))
            {
                var list = _context.Requests.GetManyReadOnly(
                        p => p.TransactionId.Equals(request.Search) ||
                             p.FromUser.FullName.Contains(request.Search) ||
                             p.ToUser.FullName.Contains(request.Search) ||
                             p.ToUserId.Equals(request.Search) ||
                             p.FromUserId.Equals(request.Search), request.OrderBy,
                        request.Page,
                        request.PageSize,
                        request.IsDescending,
                        p => p.Include(pr => pr.ToUser)
                            .Include(pr => pr.FromUser))
                    .Select(p => new RequestWebInfoResponseModel
                    {
                        SenderName = p.FromUser.FullName,
                        ReceiverName = p.ToUser.FullName,
                        Amount = p.Amount,
                        Status = p.Status,
                        RequestId = p.Id,
                        IsSender = p.FromUserId == request.UserId,
                        Date = converter.ConvertToDateTime(p.CreatedDate),
                    }).ToList();
                var count = _context.Requests.Count(p => p.TransactionId.Equals(request.Search) ||
                                                         p.FromUser.FullName.Contains(request.Search) ||
                                                         p.ToUser.FullName.Contains(request.Search) ||
                                                         p.ToUserId.Equals(request.Search) ||
                                                         p.FromUserId.Equals(request.Search));
                return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
            }
            else
            {
                var list = _context.Requests.GetManyReadOnly(
                        p => p.FromUserId.Equals(request.UserId) 
                             || p.ToUserId.Equals(request.UserId), 
                        request.OrderBy,
                        request.Page,
                        request.PageSize,
                        request.IsDescending,
                        p => p.Include(pr => pr.ToUser)
                            .Include(pr => pr.FromUser))
                    .Select(p => new RequestAndroidInfoModel()
                    {
                        Sender = p.FromUser.FullName,
                        Receiver = p.ToUser.FullName,
                        Amount = p.Amount,
                        Status = p.Status,
                        RequestId = p.Id,
                        CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                        IsSender = p.FromUserId == request.UserId
                    }).ToList();
                var count = _context.Requests.Count(p => p.FromUserId == request.UserId || p.ToUserId == request.UserId);
                return Task.FromResult(new ResponseViewModel().CreateOk(list,count));

            }
        }

        class RequestWebInfoModel
        {
            public string Sender { get; set; }
            public string Receiver { get; set; }
            public int RequestId { get; set; }
            public string CreatedDate { get; set; }
            public double Amount { get; set; }
            public int Status { get; set; }
        }
        class RequestAndroidInfoModel : RequestWebInfoModel
        {
            public bool IsSender { get; set; }
        }
    }

}