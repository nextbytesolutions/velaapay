using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Queries.GetRequestMoney
{
    public class GetRequestMoneyRequestModelValidator : AbstractValidator<GetRequestMoneyRequestModel>
    {
        public GetRequestMoneyRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}