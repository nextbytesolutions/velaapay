using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Queries.GetRequestMoney
{

    public class GetRequestMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }
            if (OrderBy == "date")
            {
                OrderBy = "CreatedDate";
            }
            if (OrderBy == "sender" || OrderBy == "senderName")
            {
                OrderBy = "FromUser.FullName";
            }
            if (OrderBy == "receiver" || OrderBy == "receiverName")
            {
                OrderBy = "ToUser.FullName";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
    }
}