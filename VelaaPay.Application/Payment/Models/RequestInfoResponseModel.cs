namespace VelaaPay.Application.Payment.Models
{
    public class RequestWebInfoResponseModel
    {
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public int RequestId { get; set; }
        public string Date { get; set; }
        public double Amount { get; set; }
        public bool IsSender { get; set; }
        public int Status { get; set; }
    }
}