using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.SendRequestMoney
{
    public class SendRequestMoneyRequestModelValidator : AbstractValidator<SendRequestMoneyRequestModel>
    {
        public SendRequestMoneyRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Amount).Min(1);
        }
    }
}