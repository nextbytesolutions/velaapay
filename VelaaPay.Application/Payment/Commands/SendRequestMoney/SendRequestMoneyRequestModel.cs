using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.SendRequestMoney
{

    public class SendRequestMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public string Pin { get; set; }
        public string UserId { get; set; }
        public double Amount { get; set; }
    }
}