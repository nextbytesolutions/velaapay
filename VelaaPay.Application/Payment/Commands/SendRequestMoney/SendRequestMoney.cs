using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.SendRequestMoney
{

    public class SendRequestMoneyRequestHandler : IRequestHandler<SendRequestMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public SendRequestMoneyRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(SendRequestMoneyRequestModel request,
            CancellationToken cancellationToken)
        {
            var senderUser =
                _context.Users.GetByReadOnly(p => p.Id == request.UserId, p => p.Include(pr => pr.UserBalances));
            if (senderUser == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            var receiverUser = _context.Users.GetBy(p => p.PhoneNumber.Equals(request.PhoneNumber) && p.IsEnabled);
            if (receiverUser == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.PhoneNumber);
            }
            var passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(senderUser,senderUser.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            _context.Requests.Add(new Request()
            {
                Amount = request.Amount,
                Status = RequestStatus.None.ToInt(),
                FromUserId = senderUser.Id,
                ToUserId = receiverUser.Id,
            });
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "You have received a request of MVR " + request.Amount,
                UserId = receiverUser.Id,
            }, cancellationToken);
            return new ResponseViewModel();
        }
    }

}