using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.TransferMoney
{

    public class TransferMoneyRequestHandler : IRequestHandler<TransferMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public TransferMoneyRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(TransferMoneyRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.Id == request.SenderUserId, 
                p => p.Include(pr => pr.UserBalances));
            if (user == null)
            {
                throw new NotFoundException("Merchant Not found");
            }

            if (user.UserBalances.First().TotalBalance < request.Amount)
            {
                throw new BadRequestException(Messages.InsufficientBalance);
            }

            var receiverUser = _context.Users.GetBy(p => p.Id == request.ReceiverUserId && p.IsEnabled);
            if (receiverUser == null)
            {
                throw new NotFoundException("Employee Not found");
            }
            var transaction = new Transaction()
            {
                Amount = request.Amount,
                SenderUserId = request.SenderUserId,
                ReceiverUserId = request.ReceiverUserId,
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = request.SenderUserId,
                        Amount = -request.Amount
                    },
                    new Wallet()
                    {
                        UserId = request.ReceiverUserId,
                        Amount = request.Amount
                    },
                },
                LoadType = LoadType.TransferMoney.ToInt(),
            };

            _context.Add(transaction);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new SharedRequestResponse()
            {
                Amount = transaction.Amount,
                TransactionId = transaction.Id,
                From = user.FullName,
                To = receiverUser.FullName,
                Date = transaction.CreatedDate,
                LoadType = transaction.LoadType
            });
        }
    }

}