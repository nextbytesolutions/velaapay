using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.TransferMoney
{
    public class TransferMoneyRequestModelValidator : AbstractValidator<TransferMoneyRequestModel>
    {
        public TransferMoneyRequestModelValidator()
        {
            RuleFor(p => p.ReceiverUserId).Required();
            RuleFor(p => p.SenderUserId).Required();
            RuleFor(p => p.Amount).Required();
        }
    }
}