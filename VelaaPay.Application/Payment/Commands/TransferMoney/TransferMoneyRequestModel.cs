using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.TransferMoney
{

    public class TransferMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public string ReceiverUserId { get; set; }
        public string SenderUserId { get; set; }
        public double Amount { get; set; }
    }
}