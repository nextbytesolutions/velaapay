using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.ScanToPay
{
    public class ScanToPayRequestModelValidator : AbstractValidator<ScanToPayRequestModel>
    {
        public ScanToPayRequestModelValidator()
        {
            RuleFor(p => p.ReceiverId).IsGuid();
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.Amount).Required();
        }
    }
}