using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.AddDebitMoney
{

    public class AddDebitMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public double Amount { get; set; }
        public int CardId { get; set; }
        public string Pin { get; set; }
    }
}