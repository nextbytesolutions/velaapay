using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.AddDebitMoney
{
    public class AddDebitMoneyRequestModelValidator : AbstractValidator<AddDebitMoneyRequestModel>
    {
        public AddDebitMoneyRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Amount).Required();
        }
    }
}