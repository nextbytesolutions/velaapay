using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Interfaces;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;
using Transaction = VelaaPay.Model.Entities.Transaction;

namespace VelaaPay.Application.Payment.Commands.AddDebitMoney
{

    public class AddDebitMoneyRequestHandler : IRequestHandler<AddDebitMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        private readonly ISeerbitService _seerbitService;
        public AddDebitMoneyRequestHandler(ApplicationDbContext context, IMediator mediator, ISeerbitService seerbitService, ISeerbitOauthTokenService seerbitOauthTokenService)
        {
            _context = context;
            _mediator = mediator;
            _seerbitService = seerbitService;
        }

        public async Task<ResponseViewModel> Handle(AddDebitMoneyRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.Users.GetBy(p => p.IsEnabled && p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(user, user.PasswordHash, request.Pin) ==
                PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            
            var transaction = new Transaction()
            {
                Amount = request.Amount,
                ReceiverUserId = request.UserId,
                LoadType = LoadType.DebitCard.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = request.UserId,
                        Amount = request.Amount
                    }
                }
            };
            _context.Add(transaction);
            await _context.SaveChangesAsync(cancellationToken);
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "You have credited " + request.Amount + " from your debit card",
                UserId = request.UserId,
                TransactionId = transaction.Id,
            },cancellationToken);
            return new ResponseViewModel().CreateOk(new SharedRequestResponse()
            {
                Amount = transaction.Amount,
                TransactionId = transaction.Id,
                From = "Stripe",
                To = user.FullName,
                Date = transaction.CreatedDate,
                LoadType = transaction.LoadType
            });
        }
    }

}