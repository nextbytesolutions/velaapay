using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.AcceptRequestMoney
{
    public class AcceptRequestMoneyRequestModelValidator : AbstractValidator<AcceptRequestMoneyRequestModel>
    {
        public AcceptRequestMoneyRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin(4);
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.RequestId).Min(1);
        }
    }
}