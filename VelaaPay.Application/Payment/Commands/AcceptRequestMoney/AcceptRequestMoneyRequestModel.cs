using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.AcceptRequestMoney
{

    public class AcceptRequestMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }
    }
}