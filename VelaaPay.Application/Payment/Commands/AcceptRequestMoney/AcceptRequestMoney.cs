using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.AcceptRequestMoney
{

    public class AcceptRequestMoneyRequestHandler : IRequestHandler<AcceptRequestMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        
        public AcceptRequestMoneyRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(AcceptRequestMoneyRequestModel request, CancellationToken cancellationToken)
        {
            var requestMoney = _context.Requests.Where(p =>
                    p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt()).Include(p => p.FromUser)
                .FirstOrDefault();
            if (requestMoney == null)
            {
                throw new NotFoundException(nameof(Request),request.RequestId);
            }
            var user =
                _context.Users.GetByReadOnly(p => p.Id == request.UserId, p => p.Include(pr => pr.UserBalances));
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.UserId);
            }
            var passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(user,user.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }
            var todayLimit = _context.Transactions.Sum(p => p.SenderUserId == request.UserId && p.CreatedDate.Date == DateTime.Today, p => p.Amount);
            var globalLimitObject = _context.Settings.GetBy(p => p.Key.ToUpper() == SettingConstant.TodayLimit.ToUpper());
            double limit = (double)SettingValueConverter.GetValue(EnumUtil.ParseEnum<SettingDataType>(globalLimitObject.DataType),globalLimitObject.Value);
            if (todayLimit + requestMoney.Amount >= limit)
            {
                throw new TransactionExceededException(user.PhoneNumber);
            }
            if (user.UserBalances.First().TotalBalance >= requestMoney.Amount)
            {
                requestMoney.Status = RequestStatus.Accepted.ToInt();
                var transaction = new Transaction()
                {
                    Amount = requestMoney.Amount,
                    SenderUserId = user.Id,
                    ReceiverUserId = requestMoney.FromUserId,
                    LoadType = LoadType.Sender.ToInt(),
                    Wallets = new List<Wallet>()
                    {
                        new Wallet()
                        {
                            UserId = user.Id,
                            Amount = -requestMoney.Amount,
                        },
                        new Wallet()
                        {
                            UserId = requestMoney.FromUserId,
                            Amount = requestMoney.Amount,
                        }
                    }
                };
                requestMoney.Transaction = transaction;
                _context.Requests.Update(requestMoney);
                _context.SaveChanges();
                await _mediator.Send(new SendNotificationRequestModel
                {
                    Message = "Your request of MVR " + requestMoney.Amount + " was accepted",
                    UserId = requestMoney.FromUserId,
                    TransactionId = transaction.Id,
                },cancellationToken);
                await _mediator.Send(new SendNotificationRequestModel
                {
                    Message = "You accepted the request of MVR " + requestMoney.Amount + "",
                    UserId = request.UserId,
                    TransactionId = transaction.Id,
                },cancellationToken);
                return new ResponseViewModel().CreateOk(new SharedRequestResponse()
                {
                    Amount = transaction.Amount,
                    TransactionId = transaction.Id,
                    To = requestMoney.FromUser.FullName,
                    From = user.FullName,
                    Date = transaction.CreatedDate,
                    LoadType = transaction.LoadType
                });
            }
            throw new BadRequestException(Messages.InsufficientBalance);
        }

        
    }

}