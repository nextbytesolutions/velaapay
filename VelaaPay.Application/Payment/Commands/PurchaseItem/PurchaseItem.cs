using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.PurchaseItem
{

    public class PurchaseItemRequestHandler : IRequestHandler<PurchaseItemRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;


        public PurchaseItemRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(PurchaseItemRequestModel request,
            CancellationToken cancellationToken)
        {
            var senderUser = _context.Users.GetBy(p => p.IsEnabled && p.Id == request.UserId,
                p => p.Include(pr => pr.UserBalances));
            if (senderUser == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            var balance = senderUser.UserBalances.First().TotalBalance;

            var merchant = _context.Roles.GetBy(p => p.NormalizedName == RoleNames.Vendor.ToUpper());
            if (merchant == null)
            {
                throw new NotFoundException(nameof(RoleNames.Vendor),"");
            }
            var receiverUser = _context.Users.GetBy(p => p.IsEnabled && p.Id == request.ReceiverId && p.Roles.Any(pr => pr.RoleId == merchant.Id));
            if (receiverUser == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.ReceiverId);
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(senderUser, senderUser.PasswordHash, request.Pin) ==
                PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            if (balance < request.Amount)
            {
                throw new BadRequestException(Messages.InsufficientBalance);
            }

            var transaction = new Transaction()
            {
                Amount = request.Amount,
                SenderUserId = senderUser.Id,
                ReceiverUserId = receiverUser.Id,
                LoadType = LoadType.PurchaseItem.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = senderUser.Id,
                        Amount = -request.Amount,
                    },
                    new Wallet()
                    {
                        UserId = receiverUser.Id,
                        Amount = request.Amount,
                    }
                }
            };
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "You sold an Item of MVR " + request.Amount + "",
                UserId = receiverUser.Id,
                TransactionId = transaction.Id,
            }, cancellationToken);
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "You purchases an Item of MVR " + request.Amount + "",
                UserId = request.UserId,
                TransactionId = transaction.Id,
            }, cancellationToken);

            return new ResponseViewModel().CreateOk(new SharedRequestResponse()
            {
                Amount = transaction.Amount,
                TransactionId = transaction.Id,
                From = senderUser.FullName,
                To = receiverUser.FullName,
                Date = transaction.CreatedDate,
                LoadType = transaction.LoadType
            });
        }
    }

}