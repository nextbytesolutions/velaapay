using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.PurchaseItem
{

    public class PurchaseItemRequestModel : IRequest<ResponseViewModel>
    {
        public string ReceiverId { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }
        public double Amount { get; set; }
    }
}