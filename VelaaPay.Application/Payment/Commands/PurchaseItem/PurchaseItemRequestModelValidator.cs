using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.PurchaseItem
{
    public class PurchaseItemRequestModelValidator : AbstractValidator<PurchaseItemRequestModel>
    {
        public PurchaseItemRequestModelValidator()
        {
            RuleFor(p => p.ReceiverId).IsGuid();
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.Amount).Required();
        }
    }
}