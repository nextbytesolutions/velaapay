using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Payment.Commands.RejectRequestMoney
{
    public class RejectRequestMoneyRequestModelValidator : AbstractValidator<RejectRequestMoneyRequestModel>
    {
        public RejectRequestMoneyRequestModelValidator()
        {
            RuleFor(p => p.Pin).Pin(4);
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.RequestId).Min(1);
        }
    }
}