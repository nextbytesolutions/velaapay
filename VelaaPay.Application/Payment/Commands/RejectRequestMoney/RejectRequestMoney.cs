using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.RejectRequestMoney
{

    public class RejectRequestMoneyRequestHandler : IRequestHandler<RejectRequestMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;

        public RejectRequestMoneyRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(RejectRequestMoneyRequestModel request, CancellationToken cancellationToken)
        {
            var requestMoney = _context.Requests.FirstOrDefault(p =>
                p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt());
            if (requestMoney == null)
            {
                throw new NotFoundException(nameof(Request),request.RequestId);
            }
            var user =
                _context.Users.GetByReadOnly(p => p.Id == request.UserId);
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.UserId);
            }
            var passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(user,user.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }
            requestMoney.Status = RequestStatus.Rejected.ToInt();
            _context.Requests.Update(requestMoney);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your request of MVR " + requestMoney.Amount + "was rejected",
                UserId = requestMoney.FromUserId,
            },cancellationToken);
            return new ResponseViewModel();
        }
    }

}