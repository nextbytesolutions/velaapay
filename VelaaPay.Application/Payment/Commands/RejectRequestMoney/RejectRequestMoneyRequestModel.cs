using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.RejectRequestMoney
{

    public class RejectRequestMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }

    }
}