using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Payment.Commands.SendMoney
{

    public class SendMoneyRequestHandler : IRequestHandler<SendMoneyRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public SendMoneyRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(SendMoneyRequestModel request, CancellationToken cancellationToken)
        {
            var senderUser =
                _context.Users.GetByReadOnly(p => p.Id == request.UserId, p => p.Include(pr => pr.UserBalances));
            if (senderUser == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.UserId);
            }

            var receiverUser = _context.Users.GetBy(p => p.PhoneNumber.Equals(request.PhoneNumber) && p.IsEnabled);
            if (receiverUser == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.PhoneNumber);
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(senderUser,senderUser.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }
            var todayLimit = _context.Transactions.Sum(p => p.SenderUserId == request.UserId && p.CreatedDate.Date == DateTime.Today, p => p.Amount);
            var globalLimitObject = _context.Settings.GetBy(p => p.Key.ToUpper() == SettingConstant.TodayLimit.ToUpper());
            double limit = (double)SettingValueConverter.GetValue(EnumUtil.ParseEnum<SettingDataType>(globalLimitObject.DataType),globalLimitObject.Value);
            if (todayLimit + request.Amount >= limit)
            {
                throw new TransactionExceededException(senderUser.PhoneNumber);
            }
            if (senderUser.UserBalances.First().TotalBalance >= request.Amount)
            {
                var transaction = new Transaction()
                {
                    Amount = request.Amount,
                    SenderUserId = senderUser.Id,
                    ReceiverUserId = receiverUser.Id,
                    LoadType = LoadType.SendMoney.ToInt(),
                    Wallets = new List<Wallet>()
                    {
                        new Wallet()
                        {
                            UserId = senderUser.Id,
                            Amount = -request.Amount,
                        },
                        new Wallet()
                        {
                            UserId = receiverUser.Id,
                            Amount = request.Amount,
                        }
                    }
                };
                _context.Transactions.Add(transaction);
                _context.SaveChanges();
                await _mediator.Send(new SendNotificationRequestModel
                {
                    Message = "You got MVR " + request.Amount + "",
                    UserId = receiverUser.Id,
                    TransactionId = transaction.Id,
                },cancellationToken);
                await _mediator.Send(new SendNotificationRequestModel
                {
                    Message = "You sent MVR " + request.Amount + "",
                    UserId = request.UserId,
                    TransactionId = transaction.Id,
                },cancellationToken);

                return new ResponseViewModel().CreateOk(new SharedRequestResponse()
                {
                    Amount = transaction.Amount,
                    TransactionId = transaction.Id,
                    From = senderUser.FullName,
                    To = receiverUser.FullName,
                    Date = transaction.CreatedDate,
                    LoadType = transaction.LoadType
                });
            }
            throw new BadRequestException(Messages.InsufficientBalance);
        }
    }

}