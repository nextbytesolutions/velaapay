using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Payment.Commands.SendMoney
{

    public class SendMoneyRequestModel : IRequest<ResponseViewModel>
    {
        public string PhoneNumber { get; set; }
        public double Amount { get; set; }
        public string Pin { get; set; }
        public string UserId { get; set; }
    }
}