using FluentValidation;
using VelaaPay.Application.Extensions;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Payment.Commands.SendMoney
{
    public class SendMoneyRequestModelValidator : AbstractValidator<SendMoneyRequestModel>
    {
        public SendMoneyRequestModelValidator()
        {
            RuleFor(p => p.PhoneNumber).Phone();
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.UserId).NotEmpty().WithMessage(Messages.EmptyError);
            RuleFor(p => p.Pin).Pin(4);
        }
    }
}