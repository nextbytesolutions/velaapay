using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.WithdrawRequest.Commands.CreateWithdraw
{

    public class CreateWithdrawRequestHandler : IRequestHandler<CreateWithdrawRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateWithdrawRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(CreateWithdrawRequestModel request, CancellationToken cancellationToken)
        {
            var userBalance =
                _context.UserBalances.GetBy(p => p.UserId == request.UserId, p => p.Include(pr => pr.User));
            if (userBalance == null)
            {
                throw new NotFoundException(RoleNames.Customer, request.UserId);
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(userBalance.User, 
                    userBalance.User.PasswordHash,
                    request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }


            if (!(userBalance.TotalBalance >= request.Amount))
                throw new BadRequestException(Messages.InsufficientBalance);
            _context.WithdrawRequests.Add(new Model.Entities.WithdrawRequest()
            {
                Status = RequestStatus.None.ToInt(),
                UserId = request.UserId,
                Amount = request.Amount,
                BankId = request.BankId,
                AccountNo = request.AccountNo,
                Transaction = new Transaction()
                {
                    Amount = request.Amount,
                    SenderUserId = request.UserId,
                    LoadType = LoadType.Withdraw.ToInt(),
                    Wallets = new List<Wallet>()
                    {
                        new Wallet()
                        {
                            UserId = request.UserId,
                            Amount = -request.Amount,
                        },
                    }
                }
            });
            _context.SaveChanges();
            return Task.FromResult(new ResponseViewModel());

        }
    }

}