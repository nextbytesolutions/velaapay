using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.WithdrawRequest.Commands.CreateWithdraw
{

    public class CreateWithdrawRequestModel : IRequest<ResponseViewModel>
    {
        public double Amount { get; set; }
        public string AccountNo { get; set; }
        public string Pin { get; set; }
        public int BankId { get; set; }
        public string UserId { get; set; }
    }
}