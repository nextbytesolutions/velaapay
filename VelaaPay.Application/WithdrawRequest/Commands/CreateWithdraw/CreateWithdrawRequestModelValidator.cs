using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.WithdrawRequest.Commands.CreateWithdraw
{
    public class CreateWithdrawRequestModelValidator : AbstractValidator<CreateWithdrawRequestModel>
    {
        public CreateWithdrawRequestModelValidator()
        {
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.AccountNo).Required();
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.BankId).Min(1);
            RuleFor(p => p.Pin).Pin(4);
        }
    }
}