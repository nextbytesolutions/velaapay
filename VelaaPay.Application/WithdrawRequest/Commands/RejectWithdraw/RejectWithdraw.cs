using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.WithdrawRequest.Commands.RejectWithdraw
{

    public class RejectWithdrawRequestHandler : IRequestHandler<RejectWithdrawRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public RejectWithdrawRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(RejectWithdrawRequestModel request, CancellationToken cancellationToken)
        {
            var withdraw = _context.WithdrawRequests.FirstOrDefault(p => p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt());
            if (withdraw == null)
            {
                throw new NotFoundException(nameof(WithdrawRequest),request.RequestId);
            }
            withdraw.Status = RequestStatus.Rejected.ToInt();
            var transaction = new Transaction()
            {
                Amount = withdraw.Amount,
                ReceiverUserId = withdraw.UserId,
                LoadType = LoadType.WithdrawRequestRejected.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = withdraw.UserId,
                        Amount = withdraw.Amount,
                    },
                }
            };
            _context.WithdrawRequests.Update(withdraw);
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your withdraw request to Acct No " + withdraw.AccountNo + " has been rejected",
                UserId = withdraw.UserId,
                TransactionId = withdraw.TransactionId,
            },cancellationToken);
            return new ResponseViewModel();
        }
    }

}