using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.WithdrawRequest.Commands.RejectWithdraw
{

    public class RejectWithdrawRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
        public string Remarks { get; set; }

    }
}