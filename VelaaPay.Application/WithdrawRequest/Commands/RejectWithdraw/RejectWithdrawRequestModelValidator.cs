using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.WithdrawRequest.Commands.RejectWithdraw
{
    public class RejectWithdrawRequestModelValidator : AbstractValidator<RejectWithdrawRequestModel>
    {
        public RejectWithdrawRequestModelValidator()
        {
            RuleFor(p => p.RequestId).Min(1);
        }
    }
}