using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.WithdrawRequest.Commands.AcceptWithdraw
{
    public class AcceptWithdrawRequestModelValidator : AbstractValidator<AcceptWithdrawRequestModel>
    {
        public AcceptWithdrawRequestModelValidator()
        {
            RuleFor(p => p.RequestId).Min(1);
        }
    }
}