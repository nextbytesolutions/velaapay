using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.WithdrawRequest.Commands.AcceptWithdraw
{

    public class AcceptWithdrawRequestModel : IRequest<ResponseViewModel>
    {
        public int RequestId { get; set; }
    }
}