using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.WithdrawRequest.Commands.AcceptWithdraw
{

    public class AcceptWithdrawRequestHandler : IRequestHandler<AcceptWithdrawRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public AcceptWithdrawRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(AcceptWithdrawRequestModel request, CancellationToken cancellationToken)
        {
            var withdraw = _context.WithdrawRequests.FirstOrDefault(p => p.Id == request.RequestId && p.Status == RequestStatus.None.ToInt());
            if (withdraw == null)
            {
                throw new NotFoundException(nameof(WithdrawRequest),request.RequestId);
            }

            withdraw.Status = RequestStatus.Accepted.ToInt();
            _context.WithdrawRequests.Update(withdraw);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your withdraw request to Acct No " + withdraw.AccountNo + " is approved",
                UserId = withdraw.UserId,
                TransactionId = withdraw.TransactionId
            },cancellationToken);
            return new ResponseViewModel();
        }
    }

}