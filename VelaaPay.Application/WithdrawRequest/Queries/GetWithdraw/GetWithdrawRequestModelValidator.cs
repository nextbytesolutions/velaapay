using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.WithdrawRequest.Queries.GetWithdraw
{
    public class GetWithdrawRequestModelValidator : AbstractValidator<GetWithdrawRequestModel>
    {
        public GetWithdrawRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);

        }
    }
}