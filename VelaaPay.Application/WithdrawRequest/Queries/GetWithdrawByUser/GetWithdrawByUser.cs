using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.WithdrawRequest.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.WithdrawRequest.Queries.GetWithdrawByUser
{

    public class GetWithdrawByUserRequestHandler : IRequestHandler<GetWithdrawByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetWithdrawByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetWithdrawByUserRequestModel request,
            CancellationToken cancellationToken)
        {
            var converter = new DateConverter();
            request.SetDefaultValue();
            List<WithdrawInfoResponseModel> list = _context.WithdrawRequests.GetMany(
                    p => p.UserId == request.UserId &&
                         (p.Bank.Name.Contains(request.Search) || p.AccountNo.Contains(request.Search)),
                    request.OrderBy,
                    request.Page, request.PageSize, request.IsDescending,
                    p => p.Include(pr => pr.User).Include(pr => pr.Bank))
                .Select(p => new WithdrawInfoResponseModel
                {
                    Bank = p.Bank.Name,
                    Name = p.User.FullName,
                    Status = p.Status,
                    AccountNo = p.AccountNo,
                    RequestId = p.Id,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    Amount = p.Amount
                }).ToList();
            var count = _context.WithdrawRequests.Count(p =>
                p.UserId == request.UserId &&
                (p.Bank.Name.Contains(request.Search) || p.AccountNo.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}