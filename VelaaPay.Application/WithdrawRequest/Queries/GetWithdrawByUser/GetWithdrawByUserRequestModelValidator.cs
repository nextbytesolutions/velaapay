using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.WithdrawRequest.Queries.GetWithdrawByUser
{
    public class GetWithdrawByUserRequestModelValidator : AbstractValidator<GetWithdrawByUserRequestModel>
    {
        public GetWithdrawByUserRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
            RuleFor(p => p.UserId).Required();
        }
    }
}