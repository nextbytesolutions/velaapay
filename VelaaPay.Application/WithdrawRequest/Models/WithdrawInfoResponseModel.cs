namespace VelaaPay.Application.WithdrawRequest.Models
{
    public class WithdrawInfoResponseModel
    {
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public string CreatedDate { get; set; }
        public int Status { get; set; }
        public int RequestId { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
    }
}