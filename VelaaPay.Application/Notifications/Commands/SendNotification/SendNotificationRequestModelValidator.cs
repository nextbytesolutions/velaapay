using FluentValidation;
using VelaaPay.Common.Constants;

namespace VelaaPay.Application.Notifications.Commands.SendNotification
{
    public class SendNotificationRequestModelValidator : AbstractValidator<SendNotificationRequestModel>
    {
        public SendNotificationRequestModelValidator()
        {
            RuleFor(p => p.Message).NotEmpty().WithMessage(Messages.EmptyError);
            RuleFor(p => p.UserId).NotEmpty().WithMessage(Messages.EmptyError);
        }
    }
}