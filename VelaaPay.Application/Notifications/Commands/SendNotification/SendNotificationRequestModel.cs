using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Notifications.Commands.SendNotification
{

    public class SendNotificationRequestModel : IRequest<ResponseViewModel>
    {
        public string TransactionId { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
    }
}