using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Interfaces;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Notifications.Commands.SendNotification
{

    public class SendNotificationRequestHandler : IRequestHandler<SendNotificationRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly INotificationService _notificationService;
        public SendNotificationRequestHandler(ApplicationDbContext context, INotificationService notificationService)
        {
            _context = context;
            _notificationService = notificationService;
        }

        public async Task<ResponseViewModel> Handle(SendNotificationRequestModel request, CancellationToken cancellationToken)
        {
            var fcmId = _context.Users.Where(p => p.Id == request.UserId).Select(p => p.FcmId).FirstOrDefault();
            if (fcmId != null)
            {
                string title;
                if (string.IsNullOrWhiteSpace(request.TransactionId))
                {
                    title = "Info";
                }
                else
                {
                    var transaction = _context.Transactions.Where(p => p.Id == request.TransactionId)
                        .Select(p => p.LoadType).FirstOrDefault();
                    title = Messages.GetLoadTypeTitle((LoadType) transaction);
                }
                var notification = new Notification()
                {
                    UserId = request.UserId,
                    Message = request.Message,
                    TransactionId = request.TransactionId
                };
                _context.Notifications.Add(notification);
                if (await _notificationService.SendNotification(fcmId, title, request.Message))
                {
                    _context.SaveChanges();
                }
            }
            return new ResponseViewModel();
        }
    }

}