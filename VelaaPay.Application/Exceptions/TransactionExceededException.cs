using System;

namespace VelaaPay.Application.Exceptions
{
    public class TransactionExceededException : Exception
    {
        public TransactionExceededException(string phone) : base($"Transaction Exceeded for {DateTime.UtcNow.Date} Phone: {phone}")
        {
            
        }
    }
}