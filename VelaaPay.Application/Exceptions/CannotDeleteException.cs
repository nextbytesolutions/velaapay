using System;

namespace VelaaPay.Application.Exceptions
{
    public class CannotDeleteException : Exception
    {
        public CannotDeleteException(string name) : base($"\"{name}\" cannot be deleted as its already in use or doesn't exist")
        {

        }
    }
}