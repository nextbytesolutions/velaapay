using System;

namespace VelaaPay.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
            
        }
        public NotFoundException(string name, object key)
            : base($"\"{name}\" was not found ({key})")
        {
        }
    }
}