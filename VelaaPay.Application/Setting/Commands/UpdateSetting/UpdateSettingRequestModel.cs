using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Setting.Commands.UpdateSetting
{

    public class UpdateSettingRequestModel : IRequest<ResponseViewModel>
    {
        public string Key { get; set; }
        public string DataType { get; set; }
        public string Value { get; set; }
    }
}