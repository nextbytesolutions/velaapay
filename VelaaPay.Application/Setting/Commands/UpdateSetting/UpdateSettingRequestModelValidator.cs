using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Setting.Commands.UpdateSetting
{
    public class UpdateSettingRequestModelValidator : AbstractValidator<UpdateSettingRequestModel>
    {
        public UpdateSettingRequestModelValidator()
        {
            RuleFor(p => p.Key).Required();
            RuleFor(p => p.DataType).Required();
            RuleFor(p => p.Value).Required();
        }
    }
}