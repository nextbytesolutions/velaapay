using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Setting.Model;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Setting.Commands.UpdateSetting
{

    public class UpdateSettingRequestHandler : IRequestHandler<UpdateSettingRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public UpdateSettingRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(UpdateSettingRequestModel request, CancellationToken cancellationToken)
        {
            var setting = _context.Settings.GetBy(p => p.Key.ToUpper() == request.Key.ToUpper());
            if (setting == null)
            {
                setting = new VelaaPay.Model.Entities.Setting()
                {
                    Key = request.Key,
                    Value = request.Value,
                    DataType = request.DataType
                };
            }
            else
            {
                request.Copy(setting);
            }

            _context.Settings.Update(setting);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new SettingResponseViewModel
            {
                Key = request.Key,
                Value = request.Value,
                DataType = request.DataType
            });
        }
    }

}