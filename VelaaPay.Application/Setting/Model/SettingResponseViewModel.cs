namespace VelaaPay.Application.Setting.Model
{
    public class SettingResponseViewModel
    {
        public string DataType { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
    }
}