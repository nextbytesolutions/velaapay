using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Setting.Queries
{

    public class GetSettingRequestModel : IRequest<ResponseViewModel>
    {
        public string Key { get; set; }
    }
}