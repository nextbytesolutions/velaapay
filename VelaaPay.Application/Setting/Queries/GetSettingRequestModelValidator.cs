using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Setting.Queries
{
    public class GetSettingRequestModelValidator : AbstractValidator<GetSettingRequestModel>
    {
        public GetSettingRequestModelValidator()
        {
            RuleFor(p => p.Key).Required();
        }
    }
}