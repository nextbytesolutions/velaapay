using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Setting.Model;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Setting.Queries
{

    public class GetSettingRequestHandler : IRequestHandler<GetSettingRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetSettingRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(GetSettingRequestModel request, CancellationToken cancellationToken)
        {
            var setting = _context.Settings.GetBy(p => p.Key.ToUpper() == request.Key.ToUpper());
            if (setting == null)
            {
                throw new NotFoundException(nameof(VelaaPay.Model.Entities.Setting),request.Key);
            }
            _context.Settings.Update(setting);
            await _context.SaveChangesAsync(cancellationToken);
            return new ResponseViewModel().CreateOk(new SettingResponseViewModel
            {
                Key = setting.Key,
                Value = setting.Value,
                DataType = setting.DataType
            });
        }
    }

}