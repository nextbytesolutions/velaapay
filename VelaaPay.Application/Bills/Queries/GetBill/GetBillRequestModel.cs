using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Bills.Queries.GetBill
{

    public class GetBillRequestModel : IRequest<ResponseViewModel>
    {
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }

            if (OrderBy == "customer")
            {
                OrderBy = "User.FullName";
            }
            if (OrderBy == "company")
            {
                OrderBy = "Company.Name";
            }
            if (OrderBy == "amount")
            {
                OrderBy = "Transaction.Amount";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
    }
}