using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Bills.Queries.GetBill
{
    public class GetBillRequestModelValidator : AbstractValidator<GetBillRequestModel>
    {
        public GetBillRequestModelValidator()
        {
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}