using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Bills.Queries.GetBillByUser
{

    public class GetBillByUserRequestModel : IRequest<ResponseViewModel>
    {
        public string UserId { get; set; }
        public void SetDefaultValue()
        {
            if (string.IsNullOrWhiteSpace(Search))
            {
                Search = "";
            }

            if (string.IsNullOrWhiteSpace(OrderBy))
            {
                OrderBy = "CreatedDate";
            }
        }
        public string Search { get; set; }
        public bool IsDescending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }

    }
}