using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Bills.Models;
using VelaaPay.Common.Response;
using VelaaPay.Common.Util;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Bills.Queries.GetBillByUser
{

    public class GetBillByUserRequestHandler : IRequestHandler<GetBillByUserRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public GetBillByUserRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task<ResponseViewModel> Handle(GetBillByUserRequestModel request,
            CancellationToken cancellationToken)
        {
            var converter = new DateConverter();

            request.SetDefaultValue();
            var list = _context.Bills.GetManyReadOnly(
                    p => p.UserId == request.UserId &&
                         (p.BillNo.Contains(request.Search) || p.ReferenceNo.Contains(request.Search)), request.OrderBy,
                    request.Page, request.PageSize, request.IsDescending,
                    p => p.Include(pr => pr.Transaction).Include(pr => pr.Company))
                .Select(p => new BillInfoResponseModel
                {
                    Amount = p.Transaction.Amount,
                    Status = p.Status,
                    Remarks = p.Remarks,
                    BillId = p.Id,
                    BillNo = p.BillNo,
                    CreatedDate = converter.ConvertToDateTime(p.CreatedDate),
                    ReferenceNo = p.ReferenceNo,
                    Company = p.Company.Name
                }).ToList();
            var count = _context.Bills.Count(p => p.UserId == request.UserId &&
                                                  (p.BillNo.Contains(request.Search) ||
                                                   p.ReferenceNo.Contains(request.Search)));
            return Task.FromResult(new ResponseViewModel().CreateOk(list, count));
        }
    }

}