using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Bills.Queries.GetBillByUser
{
    public class GetBillByUserRequestModelValidator : AbstractValidator<GetBillByUserRequestModel>
    {
        public GetBillByUserRequestModelValidator()
        {
            RuleFor(p => p.UserId).Required();
            RuleFor(p => p.Page).Min(1);
            RuleFor(p => p.PageSize).Min(10).Max(20);
        }
    }
}