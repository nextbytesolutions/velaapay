using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Application.Bills.Commands.AcceptBill
{

    public class AcceptBillRequestHandler : IRequestHandler<AcceptBillRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public AcceptBillRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(AcceptBillRequestModel request, CancellationToken cancellationToken)
        {
            var bill = _context.Bills.FirstOrDefault(p => p.Id == request.BillId && p.Status == RequestStatus.None.ToInt());
            if (bill == null)
            {
                throw new NotFoundException(nameof(Bill),request.BillId);
            }

            bill.Status = RequestStatus.Accepted.ToInt();
            _context.Bills.Update(bill);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your bill with ref. " + bill.ReferenceNo + " has been approved",
                UserId = bill.UserId,
                TransactionId = bill.TransactionId,
            });
            return new ResponseViewModel();
        }
    }

}