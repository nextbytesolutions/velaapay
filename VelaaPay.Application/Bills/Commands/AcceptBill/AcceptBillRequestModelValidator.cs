using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Bills.Commands.AcceptBill
{
    public class AcceptBillRequestModelValidator : AbstractValidator<AcceptBillRequestModel>
    {
        public AcceptBillRequestModelValidator()
        {
            RuleFor(p => p.BillId).Min(1);
        }
    }
}