using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Bills.Commands.AcceptBill
{

    public class AcceptBillRequestModel : IRequest<ResponseViewModel>
    {
        public int BillId { get; set; }
    }
}