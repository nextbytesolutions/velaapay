using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Bills.Commands.CreateBill
{
    public class CreateBillRequestModelValidator : AbstractValidator<CreateBillRequestModel>
    {
        public CreateBillRequestModelValidator()
        {
            RuleFor(p => p.CompanyId).Min(1);
            RuleFor(p => p.Amount).Min(1);
            RuleFor(p => p.BillNo).Required();
            RuleFor(p => p.ReferenceNo).Required();
            RuleFor(p => p.Pin).Pin();
            RuleFor(p => p.UserId).Required();
        }
    }
}