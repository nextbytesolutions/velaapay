using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Common.Constants;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Bills.Commands.CreateBill
{

    public class CreateBillRequestHandler : IRequestHandler<CreateBillRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;

        public CreateBillRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ResponseViewModel> Handle(CreateBillRequestModel request, CancellationToken cancellationToken)
        {
            var user = _context.UserBalances.GetBy(p => p.UserId == request.UserId, p => p.Include(pr => pr.User));
            if (user == null)
            {
                throw new NotFoundException(RoleNames.Customer,request.UserId);
            }

            var passwordHasher = new PasswordHasher<User>();
            if (passwordHasher.VerifyHashedPassword(user.User,user.User.PasswordHash,request.Pin) == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(Messages.IncorrectPin);
            }

            var company = _context.Companies.GetBy(p => p.Id == request.CompanyId);
            if (company == null)
            {
                throw new NotFoundException(nameof(Company),request.CompanyId);
            }
            if (user.TotalBalance >= request.Amount)
            {
                var bill = new Bill()
                {
                    Status = RequestStatus.None.ToInt(),
                    CompanyId = request.CompanyId,
                    UserId = request.UserId,
                    BillNo = request.BillNo,
                    ReferenceNo = request.ReferenceNo,
                };
               bill.Transaction = new Transaction()
                {
                    Amount = request.Amount,
                    SenderUserId = request.UserId,
                    LoadType = LoadType.Bill.ToInt(),
                    Wallets = new List<Wallet>()
                    {
                        new Wallet()
                        {
                            UserId = request.UserId,
                            Amount = -request.Amount,
                        },
                    }
                };
               _context.Bills.Add(bill);
               await _context.SaveChangesAsync(cancellationToken);
               var transaction = bill.Transaction;
               return new ResponseViewModel().CreateOk(new SharedRequestResponse()
               {
                   Amount = transaction.Amount,
                   TransactionId = transaction.Id,
                   To = user.User.FullName,
                   From = bill.Company.Name,
                   Date = transaction.CreatedDate,
                   LoadType = transaction.LoadType
               });
            }
            throw new BadRequestException(Messages.InsufficientBalance);
        }
    }

}