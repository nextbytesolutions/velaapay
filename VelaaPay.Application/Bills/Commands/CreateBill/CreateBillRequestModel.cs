using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Bills.Commands.CreateBill
{

    public class CreateBillRequestModel : IRequest<ResponseViewModel>
    {
        public string BillNo { get; set; }
        public int CompanyId { get; set; }
        public string ReferenceNo { get; set; }
        public string UserId { get; set; }
        public string Pin { get; set; }
        public double Amount { get; set; }
    }
}