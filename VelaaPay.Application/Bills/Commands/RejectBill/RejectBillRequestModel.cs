using MediatR;
using VelaaPay.Common.Response;

namespace VelaaPay.Application.Bills.Commands.RejectBill
{

    public class RejectBillRequestModel : IRequest<ResponseViewModel>
    {
        public int BillId { get; set; }
        public string Remarks { get; set; }
    }
}