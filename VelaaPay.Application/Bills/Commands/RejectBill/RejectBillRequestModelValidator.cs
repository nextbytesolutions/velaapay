using FluentValidation;
using VelaaPay.Application.Extensions;

namespace VelaaPay.Application.Bills.Commands.RejectBill
{
    public class RejectBillRequestModelValidator : AbstractValidator<RejectBillRequestModel>
    {
        public RejectBillRequestModelValidator()
        {
            RuleFor(p => p.BillId).Min(1);
        }        
    }
}