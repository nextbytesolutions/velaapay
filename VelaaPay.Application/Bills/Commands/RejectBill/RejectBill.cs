using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Application.Exceptions;
using VelaaPay.Application.Notifications.Commands.SendNotification;
using VelaaPay.Common.Enums;
using VelaaPay.Common.Extensions;
using VelaaPay.Common.Response;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;
using VelaaPay.Persistence.DbSetExtensions;

namespace VelaaPay.Application.Bills.Commands.RejectBill
{

    public class RejectBillRequestHandler : IRequestHandler<RejectBillRequestModel, ResponseViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        public RejectBillRequestHandler(ApplicationDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<ResponseViewModel> Handle(RejectBillRequestModel request, CancellationToken cancellationToken)
        {
            var bill = _context.Bills.GetBy(p => p.Id == request.BillId && p.Status == RequestStatus.None.ToInt(), p => p.Include(pr => pr.Transaction));
            if (bill == null)
            {
                throw new NotFoundException(nameof(Bill),request.BillId);
            }

            bill.Status = RequestStatus.Rejected.ToInt();
            bill.Remarks = request.Remarks;
            var transaction = new Transaction()
            {
                Amount = bill.Transaction.Amount,
                ReceiverUserId = bill.UserId,
                LoadType = LoadType.BillRejected.ToInt(),
                Wallets = new List<Wallet>()
                {
                    new Wallet()
                    {
                        UserId = bill.UserId,
                        Amount = bill.Transaction.Amount,
                    },
                }
            };
            _context.Bills.Update(bill);
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
            await _mediator.Send(new SendNotificationRequestModel
            {
                Message = "Your bill with ref. " + bill.ReferenceNo + " has been rejected",
                UserId = bill.UserId,
                TransactionId = bill.TransactionId,
            }, cancellationToken);
            return new ResponseViewModel();
        }
    }

}