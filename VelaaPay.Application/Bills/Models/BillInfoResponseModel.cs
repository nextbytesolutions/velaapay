namespace VelaaPay.Application.Bills.Models
{
    public class BillInfoResponseModel
    {
        public string Customer { get; set; }
        public string BillNo { get; set; }
        public string ReferenceNo { get; set; }
        public int BillId { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
        public string Company { get; set; }
        public string CreatedDate { get; set; }
    }
}