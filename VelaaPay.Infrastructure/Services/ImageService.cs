using System;
using System.IO;
using VelaaPay.Application.Interfaces;

namespace VelaaPay.Infrastructure.Services
{
    public class ImageService : IImageService
    {
        public string SaveImage(string base64)
        {
            byte[] image = LoadImage(base64);
            string extension = ".jpg";
            string fileName = Guid.NewGuid() + extension;
            string path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot/assets/img",
                fileName);

            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                stream.Write(image, 0, image.Length);
                return "/assets/img/" + fileName;
            }
        }

        public void DeleteImage(string url)
        {
            if (File.Exists("wwwroot" + url))
            {
                File.Delete("wwwroot" + url);
            }
        }

        byte[] LoadImage(string baseString)
        {
            baseString = baseString.Remove(0, baseString.IndexOf(',') + 1);
            return Convert.FromBase64String(baseString);
        }
    }
}