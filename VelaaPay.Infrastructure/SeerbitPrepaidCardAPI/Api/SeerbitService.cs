using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using RestSharp;
using VelaaPay.Application.Interfaces;
using VelaaPay.Application.Seerbit.Model;
using VelaaPay.Infrastructure.Options;
using VelaaPay.Infrastructure.SeerbitPrepaidCardAPI.Client;

namespace VelaaPay.Infrastructure.SeerbitPrepaidCardAPI.Api
{
    
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class SeerbitService : ISeerbitService
    {
        private readonly ISeerbitOauthTokenService _seerbitOauthTokenService;
        private readonly IOptions<SeerbitOption> _options;

        /// <summary>
        /// Initializes a new instance of the <see cref="SeerbitService"/> class.
        /// </summary>
        /// <param name="seerbitOauthTokenService"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public SeerbitService(ISeerbitOauthTokenService seerbitOauthTokenService, IOptions<SeerbitOption> options)
        {
            _seerbitOauthTokenService = seerbitOauthTokenService;
            _options = options;
            ApiClient = Configuration.DefaultApiClient; 
        }
        
        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            ApiClient.BasePath = basePath;
        }
    
        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return ApiClient.BasePath;
        }
    
        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient {get; set;}

        public void Auth()
        {
            if (!(_options.Value.AccessToken != null && _options.Value.Expiry > DateTime.UtcNow))
            {
                dynamic json = _seerbitOauthTokenService.Auth("client_credentials", _options.Value.ClientId,
                    _options.Value.ClientSecret);
                _options.Value.AccessToken = json.access_token;
                int expiry = json.expires_in;
                _options.Value.Expiry = DateTime.UtcNow.AddSeconds(expiry);
            }
        }

        /// <summary>
        /// This is to activate a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Activatecard (Cardactivate body)
        {
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Activatecard");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Activatecard");
            
    
            var path = "/prepaid/card/activate";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Activatecard: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Activatecard: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }

        /// <summary>
        /// This is to fetch the balance on a prepaid card 
        /// </summary>
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Balance(Balance body)
        {

            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null)
                throw new ApiException(400, "Missing required parameter 'authorization' when calling Balance");

            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Balance");


            var path = "/prepaid/card/balance";
            path = path.Replace("{format}", "json");

            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;

            headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
            postBody = ApiClient.Serialize(body); // http body (model) parameter

            // authentication setting, if any
            String[] authSettings = { };

            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody,
                headerParams, formParams, fileParams, authSettings);

            if (((int) response.StatusCode) >= 400)
                throw new ApiException((int) response.StatusCode, "Error calling Balance: " + response.Content,
                    response.Content);
            if (((int) response.StatusCode) == 0)
                throw new ApiException((int) response.StatusCode, "Error calling Balance: " + response.ErrorMessage,
                    response.ErrorMessage);
            return response.Content;
        }

        /// <summary>
        /// This is to initiate transfer from a prepaid card to a bank account 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Cardtransfertoaccount (Transfercardtoaccount body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Cardtransfertoaccount");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Cardtransfertoaccount");
            
    
            var path = "/prepaid/card/transfer/account";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Cardtransfertoaccount: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Cardtransfertoaccount: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
        /// <summary>
        /// This is to get the details of a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Customerdetails (Customerdetails body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Customerdetails");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Customerdetails");
            
    
            var path = "/prepaid/card/customer/details";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Customerdetails: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Customerdetails: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
        /// <summary>
        /// This is to deactivate a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Deactivatecard (Carddeactivate body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Deactivatecard");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Deactivatecard");
            
    
            var path = "/prepaid/card/deactivate";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Deactivatecard: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Deactivatecard: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
        /// <summary>
        /// This is to fetch the statement on a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Statement (Statement body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Statement");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Statement");
            
    
            var path = "/prepaid/card/customer/statement";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Statement: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Statement: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
        /// <summary>
        /// This is to initiate transfer from a prepaid card to another prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Transfer (Transfer body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Transfer");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Transfer");
            
    
            var path = "/prepaid/card/transfer/card";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Transfer: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Transfer: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
        /// <summary>
        /// This is to initiate transfer from a bank account to a prepaid card 
        /// </summary>
        /// <param name="authorization">Authorization parameter provided in header to authenticate the API call</param> 
        /// <param name="body">Transaction parameters that are needed to perform charge request</param> 
        /// <returns></returns>            
        public string Transferaccounttocard (AccounttocardTransfer body)
        {
            
            Auth();
            string authorization = _options.Value.AccessToken;
            // verify the required parameter 'authorization' is set
            if (authorization == null) throw new ApiException(400, "Missing required parameter 'authorization' when calling Transferaccounttocard");
            
            // verify the required parameter 'body' is set
            if (body == null) throw new ApiException(400, "Missing required parameter 'body' when calling Transferaccounttocard");
            
    
            var path = "/prepaid/account/transfer/card";
            path = path.Replace("{format}", "json");
                
            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;
    
                         if (authorization != null) headerParams.Add("Authorization", ApiClient.ParameterToString(authorization)); // header parameter
                        postBody = ApiClient.Serialize(body); // http body (model) parameter
    
            // authentication setting, if any
            String[] authSettings = {  };
    
            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody, headerParams, formParams, fileParams, authSettings);
    
            if (((int)response.StatusCode) >= 400)
                throw new ApiException ((int)response.StatusCode, "Error calling Transferaccounttocard: " + response.Content, response.Content);
            if (((int)response.StatusCode) == 0)
                throw new ApiException ((int)response.StatusCode, "Error calling Transferaccounttocard: " + response.ErrorMessage, response.ErrorMessage);
            return response.Content;
        }
    
    }
}
