using System;
using System.Collections.Generic;
using RestSharp;
using VelaaPay.Application.Interfaces;
using VelaaPay.Infrastructure.SeerbitPrepaidCardAPI.Client;

namespace VelaaPay.Infrastructure.SeerbitPrepaidCardAPI.Api
{
    

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public class SeerbitOauthTokenService : ISeerbitOauthTokenService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SeerbitOauthTokenService"/> class.
        /// </summary>
        /// <returns></returns>
        public SeerbitOauthTokenService()
        {
            ApiClient = Configuration.DefaultApiClient;
        }


        /// <summary>
        /// Sets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public void SetBasePath(String basePath)
        {
            ApiClient.BasePath = basePath;
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <param name="basePath">The base path</param>
        /// <value>The base path</value>
        public String GetBasePath(String basePath)
        {
            return ApiClient.BasePath;
        }

        /// <summary>
        /// Gets or sets the API client.
        /// </summary>
        /// <value>An instance of the ApiClient</value>
        public ApiClient ApiClient { get; set; }

        
        /// <summary>
        /// Use this method to get a bearer token to authenticate API calls 
        /// </summary>
        /// <param name="grantType">grant_type</param> 
        /// <param name="clientId">assigned client id</param> 
        /// <param name="clientSecret">assigned client secret</param> 
        /// <returns></returns>            
        public string Auth(string grantType, string clientId, string clientSecret)
        {

            // verify the required parameter 'grantType' is set
            if (grantType == null)
                throw new ApiException(400, "Missing required parameter 'grantType' when calling Auth");

            // verify the required parameter 'clientId' is set
            if (clientId == null)
                throw new ApiException(400, "Missing required parameter 'clientId' when calling Auth");

            // verify the required parameter 'clientSecret' is set
            if (clientSecret == null)
                throw new ApiException(400, "Missing required parameter 'clientSecret' when calling Auth");


            var path = "/auth";
            path = path.Replace("{format}", "json");

            var queryParams = new Dictionary<String, String>();
            var headerParams = new Dictionary<String, String>();
            var formParams = new Dictionary<String, String>();
            var fileParams = new Dictionary<String, FileParameter>();
            String postBody = null;

            if (grantType != null)
                formParams.Add("grant_type", ApiClient.ParameterToString(grantType)); // form parameter
            if (clientId != null) formParams.Add("client_id", ApiClient.ParameterToString(clientId)); // form parameter
            if (clientSecret != null)
                formParams.Add("client_secret", ApiClient.ParameterToString(clientSecret)); // form parameter

            // authentication setting, if any
            String[] authSettings = { };

            // make the HTTP request
            IRestResponse response = (IRestResponse) ApiClient.CallApi(path, Method.POST, queryParams, postBody,
                headerParams, formParams, fileParams, authSettings);

            if (((int) response.StatusCode) >= 400)
                throw new ApiException((int) response.StatusCode, "Error calling Auth: " + response.Content,
                    response.Content);
            if (((int) response.StatusCode) == 0)
                throw new ApiException((int) response.StatusCode, "Error calling Auth: " + response.ErrorMessage,
                    response.ErrorMessage);

            return response.Content;
        }
    }
}
