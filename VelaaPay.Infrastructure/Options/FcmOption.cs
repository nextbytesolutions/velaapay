namespace VelaaPay.Infrastructure.Options
{
    public class FcmOption
    {
        public string ServerKey { get; set; }
        public string SenderKey { get; set; }
    }
}