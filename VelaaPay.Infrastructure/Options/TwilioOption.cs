namespace VelaaPay.Infrastructure.Options
{
    public class TwilioOption
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string Phone { get; set; }
    }
}