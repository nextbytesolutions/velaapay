using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Model.Interfaces;

namespace VelaaPay.Model.Entities
{
    public class User : IdentityUser, IBase
    {

        public string FullName { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FcmId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<UserRole> Roles { get; set; }

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<UserClaim> Claims { get; set; }
        /// <summary>
        /// Users Total Balance
        /// </summary>
        public ICollection<UserBalance> UserBalances { get; set; }
        /// <summary>
        /// Identification of the Vendor/Customer
        /// </summary>
        public string Identification { get; set; }
        /// <summary>
        /// Image of Identification
        /// </summary>
        public string IdentificationImageUrl { get; set; }

        public bool IsVerified { get; set; }
        public bool IsEnabled { get; set; }
        public IEnumerable<AirTopUpRequest> AirTopUpRequests { get; set; }
        public IEnumerable<Bill> Bills { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public IEnumerable<Request> SenderRequests { get; set; }
        public IEnumerable<Request> ReceiverRequests { get; set; }
        public IEnumerable<Transaction> SenderTransactions { get; set; }
        public IEnumerable<Transaction> ReceiverTransactions { get; set; }
        public IEnumerable<Wallet> Wallets { get; set; }
        public IEnumerable<WithdrawRequest> WithdrawRequests { get; set; }
        public string Otp { get; set; }
        public DateTime? OtpExpiry { get; set; }
        public string NtnNumber { get; set; }
        public string Rccm { get; set; }
        public string RccmImage { get; set; }
        public string Pin { get; set; }
        public IEnumerable<CashOutTrack> CashOutTracks { get; set; }
        public IEnumerable<CashOutTrack> CustomerCashOutTracks { get; set; }
        public IEnumerable<Shop> Shops { get; set; }
        public IEnumerable<ShopUser> ShopUsers { get; set; }
        public IEnumerable<CashInTrack> CashInTracks { get; set; }
        public string BusinessType { get; set; }
        public IEnumerable<CashInRequest> CashInRequests { get; set; }
        public IEnumerable<ExceptionLog> ExceptionLogs { get; set; }
        public IEnumerable<SystemAccessLog> SystemAccessLogs { get; set; }
        public IEnumerable<StripeLog> StripeLogs { get; set; }
        public IEnumerable<CardHolder> CardHolders { get; set; }


    }
}