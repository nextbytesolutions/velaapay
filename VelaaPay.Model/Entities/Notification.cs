namespace VelaaPay.Model.Entities
{
    public class Notification : Base
    {
        public string Message { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
    }
}