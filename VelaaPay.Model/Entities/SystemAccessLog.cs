namespace VelaaPay.Model.Entities
{
    public class SystemAccessLog
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string QueryData { get; set; }
        public string FormData { get; set; }
        public string BodyData { get; set; }
        public string Method { get; set; }
        public string IPAddress { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}