namespace VelaaPay.Model.Entities
{
    public class CashOutTrack : Base
    {
        /// <summary>
        /// Can be Employee or Vendor or Supervisor
        /// </summary>
        public int EmployeeId { get; set; }
        public ShopUser Employee { get; set; }
        public string CustomerId { get; set; }
        public User Customer { get; set; }
        public double Amount { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
    }
}