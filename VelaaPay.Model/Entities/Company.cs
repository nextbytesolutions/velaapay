using System.Collections.Generic;

namespace VelaaPay.Model.Entities
{
    public class Company : Base
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public int Type { get; set; }
        public IEnumerable<Bill> Bills { get; set; }
        public IEnumerable<AirTopUpRequest> AirTopUpRequests { get; set; }
    }
}