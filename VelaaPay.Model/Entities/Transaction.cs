using System;
using System.Collections.Generic;
using VelaaPay.Common.Constants;
using VelaaPay.Model.Interfaces;

namespace VelaaPay.Model.Entities
{
    public class Transaction : IBase
    {
        public Transaction() : base()
        {
            Id = Guid.NewGuid().ToString();
            Tax = 0;
            Currency = Constant.Currency;
        }
        public string Id { get; set; }
        public double Amount { get; set; }
        public string SenderUserId { get; set; }
        public User SenderUser { get; set; }
        public string ReceiverUserId { get; set; }
        public User ReceiverUser { get; set; }
        public string Currency { get; set; }
        public int LoadType { get; set; }
        public double Tax { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public IEnumerable<AirTopUpRequest> AirTopUpRequests { get; set; }
        public IEnumerable<Bill> Bills { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public IEnumerable<Wallet> Wallets { get; set; }
        public IEnumerable<WithdrawRequest> WithdrawRequests { get; set; }
        public IEnumerable<CashOutTrack> CashOutTracks { get; set; }
        public IEnumerable<CashInTrack> CashInTracks { get; set; }
        public IEnumerable<CashInRequest> CashInRequests { get; set; }
    }
}