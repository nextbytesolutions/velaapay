namespace VelaaPay.Model.Entities
{
    public class Setting : Base
    {
        public string Key { get; set; }
        public string DataType { get; set; }
        public string Value { get; set; }
    }
}