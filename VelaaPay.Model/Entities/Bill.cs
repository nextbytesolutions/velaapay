namespace VelaaPay.Model.Entities
{
    public class Bill : Base
    {
        public string BillNo { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int Status { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public string ReferenceNo { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public string Remarks { get; set; }
    }
}