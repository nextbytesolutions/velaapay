namespace VelaaPay.Model.Entities
{
    public class UserBalance
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public double TotalBalance { get; set; }
    }
}