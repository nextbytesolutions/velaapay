namespace VelaaPay.Model.Entities
{
    public class StripeLog : Base
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public string TransactionId { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
    }
}