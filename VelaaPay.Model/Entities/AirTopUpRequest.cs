namespace VelaaPay.Model.Entities
{
    public class AirTopUpRequest : Base
    {
        public string MobileNo { get; set; }
        public int CompanyId { get; set; }
        public Company Company { get; set; }
        public int Status { get; set; }
        public double Amount { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Remarks { get; set; }
    }
}