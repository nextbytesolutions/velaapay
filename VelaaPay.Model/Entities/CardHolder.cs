using System;

namespace VelaaPay.Model.Entities
{
    public class CardHolder : Base
    {
        public string Name { get; set; }
        public string LastDigits { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string StripeToken { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int CardType { get; set; }
    }
}