using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Model.Interfaces;

namespace VelaaPay.Model.Entities
{
    public class Role : IdentityRole, IBase
    {
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Navigation property for the users in this role.
        /// </summary>
        public virtual ICollection<IdentityUserRole<string>> Users { get; set; }

        /// <summary>
        /// Navigation property for claims in this role.
        /// </summary>
        public virtual ICollection<IdentityRoleClaim<string>> Claims { get; set; }   
        
    }
}