using System.Collections.Generic;

namespace VelaaPay.Model.Entities
{
    public class ShopUser : Base
    {
        public string UserId { get; set; }
        public int ShopId { get; set; }
        public User User { get; set; }
        public Shop Shop { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int EmployeeLevel { get; set; }
        public IEnumerable<CashInTrack> CashInTracks { get; set; }
        public IEnumerable<CashOutTrack> CashOutTracks { get; set; }
    }
}