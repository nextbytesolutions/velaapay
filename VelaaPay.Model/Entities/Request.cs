namespace VelaaPay.Model.Entities
{
    public class Request : Base
    {
        public string FromUserId { get; set; }
        public string ToUserId { get; set; }
        public User FromUser { get; set; }
        public User ToUser { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public double Amount { get; set; }
        public int Status { get; set; }
    }
}