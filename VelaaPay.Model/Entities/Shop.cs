using System.Collections.Generic;

namespace VelaaPay.Model.Entities
{
    public class Shop : Base
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public ICollection<ShopUser> ShopUsers { get; set; }
    }
}