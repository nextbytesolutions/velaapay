namespace VelaaPay.Model.Entities
{
    public class CashInRequest : Base
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public int Type { get; set; }
        public double Amount { get; set; }
        public string Image { get; set; }
        public string TransactionCode { get; set; }
        public int Status { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }   
    }
}