namespace VelaaPay.Model.Entities
{
    public class WithdrawRequest : Base
    {
        public int Status { get; set; }
        public double Amount { get; set; }
        public string AccountNo { get; set; }
        public int BankId { get; set; }
        public Bank Bank { get; set; }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Remarks { get; set; }
    }
}