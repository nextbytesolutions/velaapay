using System;

namespace VelaaPay.Model.Entities
{
    public class Wallet : Base
    {
        public Wallet()
        {
            CreatedDate = DateTime.Now;
        }
        public string TransactionId { get; set; }
        public Transaction Transaction { get; set; }
        public double Amount { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}