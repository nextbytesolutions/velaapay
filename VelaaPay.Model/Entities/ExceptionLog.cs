using System;

namespace VelaaPay.Model.Entities
{
    public class ExceptionLog {
        public ExceptionLog()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string Id { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string InnerException { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
    }
}