using System.Collections.Generic;

namespace VelaaPay.Model.Entities
{
    public class Bank : Base
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string SwiftCode { get; set; }
        public ICollection<WithdrawRequest> WithdrawRequests { get; set; }
    }
}