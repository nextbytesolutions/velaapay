using System;

namespace VelaaPay.Model.Entities
{
    public class PhoneVerify : Base
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool IsVerified { get; set; }
        public string Role { get; set; }
    }
}