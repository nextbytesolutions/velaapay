using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VelaaPay.Model.Entities;
using VelaaPay.Model.Interfaces;

namespace VelaaPay.Persistence.Context
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string>
    {
        public string CurrentUserId { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IHttpContextAccessor httpAccessor) : base(options)
        {
            if (httpAccessor.HttpContext != null)
            {
                CurrentUserId = httpAccessor.HttpContext.User.FindFirst(p => p.Type == new IdentityOptions().ClaimsIdentity.UserIdClaimType)?.Value?.Trim();
            }
        }

        public DbSet<AirTopUpRequest> AirTopUpRequests { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CashInTrack> CashInTracks { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<UserBalance> UserBalances { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<PhoneVerify> PhoneVerifies { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<ShopUser> ShopUsers { get; set; }
        public DbSet<CashOutTrack> CashOutTracks { get; set; }
        public DbSet<CashInRequest> CashInRequests { get; set; }
        public DbSet<WithdrawRequest> WithdrawRequests { get; set; }
        public DbSet<StripeLog> StripeLogs { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<CardHolder> CardHolders { get; set; }
        public DbSet<SystemAccessLog> SystemAccessLogs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            var entities = ChangeTracker.Entries();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IBase && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IBase) entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                    entity.CreatedBy = CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = CurrentUserId;
            }
        }
    }
}