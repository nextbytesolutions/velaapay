using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class ShopUserConfiguration : IEntityTypeConfiguration<ShopUser>
    {

        public void Configure(EntityTypeBuilder<ShopUser> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.StartTime).IsRequired();
            builder.Property(p => p.EndTime).IsRequired();
            builder.HasOne(p => p.User)
                .WithMany(b => b.ShopUsers)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Shop)
                .WithMany(b => b.ShopUsers)
                .HasForeignKey(p => p.ShopId);
        }

    }

}