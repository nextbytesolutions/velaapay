using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class PhoneVerifyConfiguration : IEntityTypeConfiguration<PhoneVerify>
    {

        public void Configure(EntityTypeBuilder<PhoneVerify> builder)
        {
            builder.Property(p => p.PhoneNumber).IsRequired().HasMaxLength(13);
            builder.Property(p => p.Code).HasMaxLength(4);
        }

    }

}