using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{
    public class CardHolderConfiguration : IEntityTypeConfiguration<CardHolder>
    {

        public void Configure(EntityTypeBuilder<CardHolder> builder)
        {
            builder.Property(p => p.LastDigits).IsRequired().HasMaxLength(4);
            builder.Property(p => p.StripeToken).IsRequired();
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.HasOne(p => p.User)
                .WithMany(b => b.CardHolders)
                .HasForeignKey(p => p.UserId);
        }

    }
}