using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class CashInTrackConfiguration : IEntityTypeConfiguration<CashInTrack>
    {
        public void Configure(EntityTypeBuilder<CashInTrack> builder)
        {
            builder.Property(p => p.CustomerId).IsRequired();
            builder.Property(p => p.EmployeeId).IsRequired();
            builder.HasOne(p => p.Customer)
                .WithMany(b => b.CashInTracks)
                .HasForeignKey(p => p.CustomerId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.CashInTracks)
                .HasForeignKey(p => p.TransactionId);
            builder.HasOne(p => p.Employee)
                .WithMany(b => b.CashInTracks)
                .HasForeignKey(p => p.EmployeeId);
        }
    }

}