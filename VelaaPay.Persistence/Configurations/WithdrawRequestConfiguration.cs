using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class WithdrawRequestConfiguration : IEntityTypeConfiguration<WithdrawRequest>
    {

        public void Configure(EntityTypeBuilder<WithdrawRequest> builder)
        {
            builder.Property(p => p.AccountNo).IsRequired();
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.WithdrawRequests)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Bank)
                .WithMany(b => b.WithdrawRequests)
                .HasForeignKey(p => p.BankId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.WithdrawRequests)
                .HasForeignKey(p => p.TransactionId);

        }

    }

}