using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class WalletConfiguration : IEntityTypeConfiguration<Wallet>
    {

        public void Configure(EntityTypeBuilder<Wallet> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.Wallets)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.Wallets)
                .HasForeignKey(p => p.TransactionId);
        }

    }

}