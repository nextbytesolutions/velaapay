using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{
    public class SystemAccessLogConfiguration : IEntityTypeConfiguration<SystemAccessLog>
    {
        public void Configure(EntityTypeBuilder<SystemAccessLog> builder)
        {
            builder.HasOne(p => p.User)
                .WithMany(b => b.SystemAccessLogs)
                .HasForeignKey(p => p.UserId);
        }
    }
}