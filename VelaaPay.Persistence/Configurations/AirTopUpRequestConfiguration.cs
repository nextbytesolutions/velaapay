using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{
    public class AirTopUpRequestConfiguration : IEntityTypeConfiguration<AirTopUpRequest>
    {
        public void Configure(EntityTypeBuilder<AirTopUpRequest> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.AirTopUpRequests)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.AirTopUpRequests)
                .HasForeignKey(p => p.TransactionId);
            builder.HasOne(p => p.Company)
                .WithMany(b => b.AirTopUpRequests)
                .HasForeignKey(p => p.CompanyId);
        }
    }
    
    
    public class SettingConfiguration : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.HasIndex(p => p.Key).IsUnique();
            builder.Property(p => p.Key).IsRequired();
            builder.Property(p => p.Value).IsRequired();
            builder.Property(p => p.DataType).IsRequired();
        }
    }
}