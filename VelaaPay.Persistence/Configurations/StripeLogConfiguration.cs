using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{
    public class StripeLogConfiguration : IEntityTypeConfiguration<StripeLog>
    {
        public void Configure(EntityTypeBuilder<StripeLog> builder)
        {
            builder.HasOne(p => p.User)
                .WithMany(b => b.StripeLogs)
                .HasForeignKey(p => p.UserId);
        }
    }
}