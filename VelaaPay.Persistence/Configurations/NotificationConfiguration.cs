using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {

        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.Property(p => p.Message).IsRequired();
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.Notifications)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.Notifications)
                .HasForeignKey(p => p.TransactionId);
        }
    }

}