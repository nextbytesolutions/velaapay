using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class BillConfiguration : IEntityTypeConfiguration<Bill>
    {

        public void Configure(EntityTypeBuilder<Bill> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.Bills)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.Bills)
                .HasForeignKey(p => p.TransactionId);
            builder.HasOne(p => p.Company)
                .WithMany(b => b.Bills)
                .HasForeignKey(p => p.CompanyId);
            
        }

    }

}