using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class CashOutTrackConfiguration : IEntityTypeConfiguration<CashOutTrack>
    {

        public void Configure(EntityTypeBuilder<CashOutTrack> builder)
        {
            builder.Property(p => p.CustomerId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.Employee)
                .WithMany(b => b.CashOutTracks)
                .HasForeignKey(p => p.EmployeeId);
            
            builder.HasOne(p => p.Customer)
                .WithMany(b => b.CustomerCashOutTracks)
                .HasForeignKey(p => p.CustomerId);

            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.CashOutTracks)
                .HasForeignKey(p => p.TransactionId);
            
        }

    }

}