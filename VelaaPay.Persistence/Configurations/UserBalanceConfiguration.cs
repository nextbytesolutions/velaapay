using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class UserBalanceConfiguration : IEntityTypeConfiguration<UserBalance>
    {

        public void Configure(EntityTypeBuilder<UserBalance> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.HasKey(p => p.UserId);
            builder.HasOne(p => p.User)
                .WithMany(b => b.UserBalances)
                .HasForeignKey(p => p.UserId);
        }

    }

}