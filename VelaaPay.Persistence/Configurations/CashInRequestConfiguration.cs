using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class CashInRequestConfiguration : IEntityTypeConfiguration<CashInRequest>
    {

        public void Configure(EntityTypeBuilder<CashInRequest> builder)
        {
            builder.Property(p => p.UserId).IsRequired();
            builder.Property(p => p.TransactionId).IsRequired(false);
            builder.HasOne(p => p.User)
                .WithMany(b => b.CashInRequests)
                .HasForeignKey(p => p.UserId);
            builder.HasOne(p => p.Transaction)
                .WithMany(b => b.CashInRequests)
                .HasForeignKey(p => p.TransactionId);
        }

    }

}