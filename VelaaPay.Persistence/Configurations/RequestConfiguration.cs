using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class RequestConfiguration : IEntityTypeConfiguration<Request>
    {

        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder
                .Property(p => p.ToUserId).IsRequired();
            builder
                .Property(p => p.FromUserId).IsRequired();
            builder
                .Property(p => p.TransactionId).IsRequired(false);
            builder
                .HasOne(p => p.FromUser)
                .WithMany(b => b.SenderRequests)
                .HasForeignKey(p => p.FromUserId);
            builder
                .HasOne(p => p.ToUser)
                .WithMany(b => b.ReceiverRequests)
                .HasForeignKey(p => p.ToUserId);
        }

    }

}