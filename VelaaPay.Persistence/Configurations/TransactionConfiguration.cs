using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {

        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasOne(p => p.SenderUser)
                .WithMany(b => b.SenderTransactions)
                .HasForeignKey(p => p.SenderUserId);
            builder.HasOne(p => p.ReceiverUser)
                .WithMany(b => b.ReceiverTransactions)
                .HasForeignKey(p => p.ReceiverUserId);

        }

    }

}