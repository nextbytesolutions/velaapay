using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VelaaPay.Model.Entities;

namespace VelaaPay.Persistence.Configurations
{

    public class BankConfiguration : IEntityTypeConfiguration<Bank>
    {

        public void Configure(EntityTypeBuilder<Bank> builder)
        {
            builder.Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Property(p => p.SwiftCode).IsRequired().HasMaxLength(10);
        }

    }

}