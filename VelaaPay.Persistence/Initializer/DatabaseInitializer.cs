using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using VelaaPay.Common.Constants;
using VelaaPay.Model.Entities;
using VelaaPay.Persistence.Context;

namespace VelaaPay.Persistence.Initializer
{
    public class DatabaseInitializer
    {

        public static void Initialize(ApplicationDbContext context)
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            initializer.SeedEverything(context);
        }

        private void SeedEverything(ApplicationDbContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
            SeedSettings(context);
        }

        private void SeedSettings(ApplicationDbContext context)
        {
            if (context.Settings.Any())
            {
                return;
            }

            context.Settings.Add(new Setting()
            {
                Key = SettingConstant.TodayLimit,
                Value = "500",
                DataType = SettingDataType.Double.ToString(),
            });
            context.SaveChanges();
        }
        private void SeedRoles(ApplicationDbContext context)
        {
            if (context.Roles.Any())
            {
                return;
            }

            context.Roles.Add(new Role()
            {
                Name = RoleNames.Customer,
                NormalizedName = RoleNames.Customer.ToUpper(),
                Description = RoleNames.Customer
            });
            context.Roles.Add(new Role()
            {
                Name = RoleNames.Vendor,
                NormalizedName = RoleNames.Vendor.ToUpper(),
                Description = RoleNames.Vendor
            });
            context.Roles.Add(new Role()
            {
                Name = RoleNames.Admin,
                NormalizedName = RoleNames.Admin.ToUpper(),
                Description = RoleNames.Admin
            });
            context.Roles.Add(new Role()
            {
                Name = RoleNames.Supervisor,
                NormalizedName = RoleNames.Supervisor.ToUpper(),
                Description = RoleNames.Supervisor
            });
            context.Roles.Add(new Role()
            {
                Name = RoleNames.Cashier,
                NormalizedName = RoleNames.Cashier.ToUpper(),
                Description = RoleNames.Cashier
            });
            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            if (context.Users.Any())
            {
                return;
            }

            var passwordHasher = new PasswordHasher<User>();
            var adminRoleId = context.Roles.First(p => p.Name == RoleNames.Admin).Id;
            var customerId = context.Roles.First(p => p.Name == RoleNames.Customer).Id;
            var admin = new User()
            {
                PhoneNumber = Constant.AdminPhone,
                Email = Constant.AdminEmail,
                IsVerified = true,
                IsEnabled = true,
                NormalizedEmail = Constant.AdminEmail.ToUpper(),
                Image = Constant.DefaultImageUrl,
                FullName = "Admin",
                UserBalances = new List<UserBalance>()
                {
                    new UserBalance()
                    {
                        TotalBalance = 0
                    }
                },
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };
            admin.Roles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = adminRoleId,
                    UserId = admin.Id
                }
            };
            admin.PasswordHash = passwordHasher.HashPassword(admin, Constant.AdminPassword);
            var customer = new User()
            {
                Image = Constant.DefaultImageUrl,
                PhoneNumber = Constant.CustomerPhone,
                Email = Constant.CustomerEmail,
                IsVerified = true,
                IsEnabled = true,
                NormalizedEmail = Constant.CustomerEmail.ToUpper(),
                FullName = "Customer",
                UserBalances = new List<UserBalance>()
                {
                    new UserBalance()
                    {
                        TotalBalance = 0
                    }
                },
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };
            customer.Roles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = customerId,
                    UserId = customer.Id
                }
            };
            customer.PasswordHash = passwordHasher.HashPassword(customer, Constant.CustomerPassword);
            context.Users.Add(admin);
            context.Users.Add(customer);
            context.SaveChanges();
        }
    }
}